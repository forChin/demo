package com.example.demo.service;

import com.example.demo.domain.*;
import com.example.demo.repos.PrognozVrpRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PrognozVrpService {

    @Autowired
    PrognozVrpRepo prognozVrpRepo;

    public List<PrognozVrp> getPrognozVrpByYear(String year){
        return prognozVrpRepo.getPrognozVrpByYear(year);
    }

    public List<PrognozVrpFact> getPrognozVrpFactByYear(String year){
        return prognozVrpRepo.getPrognozVrpFactByYear(year);
    }

    public List<PrognozVrpEkzFact> getPrognozVrpEkzFactByYear(String year){
        return prognozVrpRepo.getPrognozVrpEkzFactByYear(year);
    }

    public List<PrognozVrpOblast> getPrognozVrpOblastByScenario(String scenario){
        return prognozVrpRepo.getPrognozVrpOblastByScenario(scenario);
    }

    public List<PrognozVrpOblastYear> getPrognozVrpOblastYearByYear(String year){
        return prognozVrpRepo.getPrognozVrpOblastYearByYear(year);
    }

    public List<PrognozVrpRaion> getPrognozVrpRaionByYear(String year){
        return prognozVrpRepo.getPrognozVrpRaionByYear(year);
    }

    public List<PrognozVrpRaionScenario> getPrognozVrpRaionScenarioByScenario(String scenario){
        return prognozVrpRepo.getPrognozVrpRaionScenarioByYear(scenario);
    }

    public List<PrognozVrpTable> getPrognozVrpTableByYear(String year, String scenario, String ind){
        List<PrognozVrpTable> prognozVrpTables = prognozVrpRepo.getPrognozVrpTableByYear(year, scenario, ind);

        setIndustrys(prognozVrpTables);

        return prognozVrpTables;
    }

    private void setIndustrys(List<PrognozVrpTable> prognozVrpTables){
        List<String> industrys = prognozVrpRepo.getPrognozVrpTableIndustrys();

        for (PrognozVrpTable pTable : prognozVrpTables) {
            String region = pTable.getRegion();
            String year = pTable.getYear();
            String scenario = pTable.getScenario();
            String ind = pTable.getIndicator();
            for (String i : industrys) {
                Double value =
                        prognozVrpRepo.getPrognozVrpTableByYearAndRegionAndIndustry(year, region, i, scenario, ind);
                pTable.getIndustry().put(i, value);
            }
        }
    }




}
