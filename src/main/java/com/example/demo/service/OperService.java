package com.example.demo.service;

import com.example.demo.domain.*;
import com.example.demo.repos.OperMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OperService {

    @Autowired
    OperMapper operMapper;

    public List<OperMPMonitoring> getOperMPMonitoring(String region, String year, String month){
        return operMapper.getOperMPMonitoring(region, year, month);
    }

    public List<OperMPMonitoring> getOperMPMonitoringByAll(){
        return operMapper.getOperMPMonitoringByAll();
    }


    public List<String> getOperMPMonitoringRegions() {
        return operMapper.getOperMPMonitoringRegions();
    }

    public List<String> getOperMPMonitoringYears() {
        return operMapper.getOperMPMonitoringYears();
    }

    public List<String> getOperMPMonitoringMonths() {
        return operMapper.getOperMPMonitoringMonths();
    }

    public List<OperMOpMonitoring> getOperMOpMonitoring(String year, String month){
        return operMapper.getOperMOpMonitoring(year, month);
    }

    public List<OperMOpMonitoring> getOperMOpMonitoringByAll(){
        return operMapper.getOperMOpMonitoringByAll();
    }


    public List<String> getOperMOpMonitoringYears() {
        return operMapper.getOperMOpMonitoringYears();
    }

    public List<String> getOperMOpMonitoringMonths() {
        return operMapper.getOperMOpMonitoringMonths();
    }


    public List<OperMPrMonitoring> getOperMPrMonitoring(String region, String year, String month){
        return operMapper.getOperMPrMonitoring(region, year, month);
    }

    public List<OperMPrMonitoring> getOperMPrMonitoringByAll(){
        return operMapper.getOperMPrMonitoringByAll();
    }

    public List<String> getOperMPrMonitoringRegions() {
        return operMapper.getOperMPrMonitoringRegions();
    }

    public List<String> getOperMPrMonitoringYears() {
        return operMapper.getOperMPrMonitoringYears();
    }

    public List<String> getOperMPrMonitoringMonths() {
        return operMapper.getOperMPrMonitoringMonths();
    }

    public List<OperMPtMonitoring> getOperMPtMonitoring(String year, String month){
        return operMapper.getOperMPtMonitoring(year, month);
    }

    public List<OperMPtMonitoring> getOperMPtMonitoringByAll(){
        return operMapper.getOperMPtMonitoringByAll();
    }


    public List<String> getOperMPtMonitoringYears() {
        return operMapper.getOperMPtMonitoringYears();
    }

    public List<String> getOperMPtMonitoringMonths() {
        return operMapper.getOperMPtMonitoringMonths();
    }

    public List<OperMSDMonitoring> getOperMSDMonitoring(String region, String ruralDistrict, String year, String month){
        return operMapper.getOperMSDMonitoring(region, ruralDistrict, year, month);
    }

    public List<String> getOperMSDMonitoringRegions() {
        return operMapper.getOperMSDMonitoringRegions();
    }

    public List<String> getOperMSDMonitoringDistricts() {
        return operMapper.getOperMSDMonitoringDistricts();
    }

    public List<String> getOperMSDMonitoringYears() {
        return operMapper.getOperMSDMonitoringYears();
    }

    public List<String> getOperMSDMonitoringMonths() {
        return operMapper.getOperMSDMonitoringMonths();
    }

    public List<OperMSpMonitoring> getOperMSpMonitoring(String region, String year, String month){
        return operMapper.getOperMSpMonitoring(region, year, month);
    }

    public List<String> getOperMSpMonitoringRegions() {
        return operMapper.getOperMSpMonitoringRegions();
    }

    public List<String> getOperMSpMonitoringYears() {
        return operMapper.getOperMSpMonitoringYears();
    }

    public List<String> getOperMSpMonitoringMonths() {
        return operMapper.getOperMSpMonitoringMonths();
    }

    public List<OperMProMonitoring> getOperMProMonitoringByRegion(String region, String month){
        return operMapper.getOperMProMonitoringByRegion(region, month);
    }

    public List<String> getOperMProMonitoringRegions() {
        return operMapper.getOperMProMonitoringRegions();
    }

    public List<String> getOperMProMonitoringMonths() {
        return operMapper.getOperMProMonitoringMonths();
    }

    public List<String> getOperMProMonitoringDates() {
        return operMapper.getOperMProMonitoringDates();
    }

    public List<OperMProMonitoring> getOperMProMonitoring(){
        return operMapper.getOperMProMonitoring();
    }

    public List<OperMSpMonitoring> getOperMSpMonitoringByAll(){
        return operMapper.getOperMSpMonitoringByAll();
    }

}
