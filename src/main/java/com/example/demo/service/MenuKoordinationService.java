package com.example.demo.service;

import com.example.demo.domain.*;
import com.example.demo.repos.MenuKoordinationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuKoordinationService {

    @Autowired
    MenuKoordinationMapper menuKoordinationMapper;

    public List<MenuKoordinationMonitoring> getMenuKoordinationMonitoringByYear(String year){
        return menuKoordinationMapper.getMenuKoordinationMonitoringByYear(year);
    }

    public List<MenuKoordinationRaion> getMenuKoordinationRaionByYear(String year){
        return menuKoordinationMapper.getMenuKoordinationRaionByYear(year);
    }

    public List<MenuKoordinationMonitoringT1> getMenuKoordinationMonitoringT1ByYear(String year){
        return menuKoordinationMapper.getMenuKoordinationMonitoringT1ByYear(year);
    }

    public List<MenuKoordinationMonitoringT2> getMenuKoordinationMonitoringT2ByYear(String year){
        return menuKoordinationMapper.getMenuKoordinationMonitoringT2ByYear(year);
    }

    public List<MenuKoordinationRaionT1> getMenuKoordinationRaionT1ByYear(String year){
        return menuKoordinationMapper.getMenuKoordinationRaionT1ByYear(year);
    }

    public List<MenuKoordinationRaionT2> getMenuKoordinationRaionT2ByYear(String year){
        return menuKoordinationMapper.getMenuKoordinationRaionT2ByYear(year);
    }

    public List<MenuKoordinationRegionM> getMenuKoordinationRegionMByYear(String year){
        return menuKoordinationMapper.getMenuKoordinationRegionMByYear(year);
    }

    public List<String> getMenuKoordinationMonitoringYears(){
        return menuKoordinationMapper.getMenuKoordinationMonitoringYears();
    }

    public List<String> getMenuKoordinationRaionYears(){
        return menuKoordinationMapper.getMenuKoordinationRaionYears();
    }

    public List<String> getMenuKoordinationMonitoringT1Years(){
        return menuKoordinationMapper.getMenuKoordinationMonitoringT1Years();
    }

    public List<String> getMenuKoordinationMonitoringT2Years(){
        return menuKoordinationMapper.getMenuKoordinationMonitoringT2Years();
    }

    public List<String> getMenuKoordinationRaionT1Years(){
        return menuKoordinationMapper.getMenuKoordinationRaionT1Years();
    }

    public List<String> getMenuKoordinationRaionT2Years(){
        return menuKoordinationMapper.getMenuKoordinationRaionT2Years();
    }

    public List<String> getMenuKoordinationRegionMYears(){
        return menuKoordinationMapper.getMenuKoordinationRegionMYears();
    }

//table

    public List<MenuTKRT1Monitoring> getMenuTKRT1MonitoringByYear(String year, String region){
        List<MenuTKRT1Monitoring> menuTKRT1Monitorings = menuKoordinationMapper.getMenuTKRT1MonitoringByYear(year, region);

        setNames(menuTKRT1Monitorings);

        return menuTKRT1Monitorings;
    }

    private void setNames(List<MenuTKRT1Monitoring> menuTKRT1Monitorings){
        List<String> names = menuKoordinationMapper.getMenuTKRT1MonitoringNames();

        for (MenuTKRT1Monitoring pTable : menuTKRT1Monitorings) {
            String year = pTable.getYear();
            String region = pTable.getRegion();
              for (String i : names) {
                Double inte =
                        menuKoordinationMapper.getMenuTKRT1MonitoringByYearAndRegionAndName(year, region, i);
                pTable.getName().put(i, inte);
            }
        }
    }


    public List<MenuKoord2Monitoring> getMenuKoord2MonitoringByYear(String year){
        return menuKoordinationMapper.getMenuKoord2MonitoringByYear(year);
    }

    public List<MenuKoord3Monitoring> getMenuKoord3MonitoringByYear(String year){
        return menuKoordinationMapper.getMenuKoord3MonitoringByYear(year);
    }

    public List<MenuKoord4Monitoring> getMenuKoord4MonitoringByYear(String year){
        return menuKoordinationMapper.getMenuKoord4MonitoringByYear(year);
    }


    public List<MenuTKRT1allMonitoring> getMenuTKRT1allMonitoringByAll(){
        return menuKoordinationMapper.getMenuTKRT1allMonitoringByAll();
    }

    public List<MenuKoordinationRegionM2> getMenuKoordinationRegionMByAll(){
        return menuKoordinationMapper.getMenuKoordinationRegionMByAll();
    }


}
