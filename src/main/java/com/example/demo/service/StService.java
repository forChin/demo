package com.example.demo.service;

import com.example.demo.domain.St3Monitoring;
import com.example.demo.domain.St4Monitoring;
import com.example.demo.domain.StMonitoring;
import com.example.demo.repos.StMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StService {

    @Autowired
    StMapper stMapper;

    public List<StMonitoring> getStMonitoring(){
        return stMapper.getStMonitoring();
    }

    public List<String> getStMonitoringRaions() {
        return stMapper.getStMonitoringRaions();
    }

    public List<String> getStMonitoringYears() {
        return stMapper.getStMonitoringYears();
    }

    public List<String> getStMonitoringPeriods() {
        return stMapper.getStMonitoringPeriods();
    }

    public List<StMonitoring> getStMonitoringByRaion(String raion, String year, String period){
        return stMapper.getStMonitoringByRaion(raion, year, period);
    }

    public List<St3Monitoring> getSt3Monitoring(){
        return stMapper.getSt3Monitoring();
    }

    public List<St4Monitoring> getSt4Monitoring(){
        return stMapper.getSt4Monitoring();
    }





}
