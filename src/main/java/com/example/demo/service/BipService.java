package com.example.demo.service;

import com.example.demo.domain.*;
import com.example.demo.repos.BipMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BipService {

    @Autowired
    BipMapper bipMapper;

    public List<MenuBipObrazMonitoring> getMenuBipObrazMonitoringByRegion(String region){
        return bipMapper.getMenuBipObrazMonitoringByRegion(region);
    }

    public List<String> getMenuBipObrazMonitoringRegions(){
        return bipMapper.getMenuBipObrazMonitoringRegions();
    }

    public List<String> getMenuBipObrazMonitoringRurals(){
        return bipMapper.getMenuBipObrazMonitoringRurals();
    }

    public List<String> getMenuBipObrazMonitoringRSs(){
        return bipMapper.getMenuBipObrazMonitoringRSs();
    }

    public List<MenuBipObrazMonitoring> getMenuBipObrazMonitoringByRegion2(String region, String ruralDistrict, String ruralSettlement) {
        List<MenuBipObrazMonitoring> menuBipObrazMonitorings = bipMapper.getMenuBipObrazMonitoringByRegion2(region, ruralDistrict, ruralSettlement);

        return menuBipObrazMonitorings;
    }

    public List<String> getMenuBipZdravMonitoringRegions(){
        return bipMapper.getMenuBipZdravMonitoringRegions();
    }

    public List<String> getMenuBipZdravMonitoringRurals(){
        return bipMapper.getMenuBipZdravMonitoringRurals();
    }

    public List<String> getMenuBipZdravMonitoringRSs(){
        return bipMapper.getMenuBipZdravMonitoringRSs();
    }

    public List<MenuBipZdravMonitoring> getMenuBipZdravMonitoringByRegion2(String region, String ruralDistrict, String ruralSettlement) {
        List<MenuBipZdravMonitoring> menuBipZdravMonitorings = bipMapper.getMenuBipZdravMonitoringByRegion2(region, ruralDistrict, ruralSettlement);

        return menuBipZdravMonitorings;
    }

    public List<MenuBipZdravMonitoring> getMenuBipZdravMonitoringByRegion(String region){
        return bipMapper.getMenuBipZdravMonitoringByRegion(region);
    }

    public List<String> getMenuBipDorogiMonitoringRegions(){
        return bipMapper.getMenuBipDorogiMonitoringRegions();
    }

    public List<String> getMenuBipDorogiMonitoringRSnps(){
        return bipMapper.getMenuBipDorogiMonitoringRSnps();
    }

    public List<String> getMenuBipDorogiMonitoringRTs(){
        return bipMapper.getMenuBipDorogiMonitoringRTs();
    }

    public List<MenuBipDorogiMonitoring> getMenuBipDorogiMonitoringByRegion2(String region, String roadSnp, String roadType){
        List<MenuBipDorogiMonitoring> menuBipDorogiMonitorings = bipMapper.getMenuBipDorogiMonitoringByRegion2(region, roadSnp, roadType);

        return menuBipDorogiMonitorings;
    }

    public List<MenuBipDorogiMonitoring> getMenuBipDorogiMonitoringByRegion(String region){
        return bipMapper.getMenuBipDorogiMonitoringByRegion(region);
    }

    public List<String> getMenuBipCulturaMonitoringRegions(){
        return bipMapper.getMenuBipCulturaMonitoringRegions();
    }

    public List<String> getMenuBipCulturaMonitoringRDistricts(){
        return bipMapper.getMenuBipCulturaMonitoringRDistricts();
    }

    public List<String> getMenuBipCulturaMonitoringRSettlements(){
        return bipMapper.getMenuBipCulturaMonitoringRSettlements();
    }

    public List<MenuBipCulturaMonitoring> getMenuBipCulturaMonitoringByRegion(String region){
        return bipMapper.getMenuBipCulturaMonitoringByRegion(region);
    }

    public List<MenuBipCulturaMonitoring> getMenuBipCulturaMonitoringByRegion2(String region, String ruralDistrict, String ruralSettlement){
        List<MenuBipCulturaMonitoring> menuBipCulturaMonitorings = bipMapper.getMenuBipCulturaMonitoringByRegion2(region, ruralDistrict, ruralSettlement);

        return menuBipCulturaMonitorings;
    }

    public List<BCatalogZdravMonitoring> getBCatalogZdravMonitoring(){
        return bipMapper.getBCatalogZdravMonitoring();
    }

    public List<BCategoryWeightMonitoring> getBCategoryWeightMonitoring(){
        return bipMapper.getBCategoryWeightMonitoring();
    }

    public List<BCulturePointsMonitoring> getBCulturePointsMonitoring(){
        return bipMapper.getBCulturePointsMonitoring();
    }

    public List<BDorogiPointsMonitoring> getBDorogiPointsMonitoring(){
        return bipMapper.getBDorogiPointsMonitoring();
    }

    public List<BEduPointsMonitoring> getBEduPointsMonitoring(){
        return bipMapper.getBEduPointsMonitoring();
    }

    public List<BInflationMonitoring> getBInflationMonitoring(){
        return bipMapper.getBInflationMonitoring();
    }

    public List<BNumbersMonitoring> getBNumbersMonitoring(){
        return bipMapper.getBNumbersMonitoring();
    }

    public List<BSpisokDdoMonitoring> getBSpisokDdoMonitoring(){
        return bipMapper.getBSpisokDdoMonitoring();
    }

    public List<BSpisokDorogiMonitoring> getBSpisokDorogiMonitoring(){
        return bipMapper.getBSpisokDorogiMonitoring();
    }

    public List<BSpisokShkolyMonitoring> getBSpisokShkolyMonitoring(){
        return bipMapper.getBSpisokShkolyMonitoring();
    }

    public List<BSpisokZdravMonitoring> getBSpisokZdravMonitoring(){
        return bipMapper.getBSpisokZdravMonitoring();
    }

    public List<BZdravPointsMonitoring> getBZdravPointsMonitoring(){
        return bipMapper.getBZdravPointsMonitoring();
    }

//vse

    public List<MenuBipObrazMonitoring> getMenuBipObrazMonitoring(){
        return bipMapper.getMenuBipObrazMonitoring();
    }


    public List<MenuBipZdravMonitoring> getMenuBipZdravMonitoring(){
        return bipMapper.getMenuBipZdravMonitoring();
    }

    public List<MenuBipDorogiMonitoring> getMenuBipDorogiMonitoring(){
        return bipMapper.getMenuBipDorogiMonitoring();
    }

    public List<MenuBipCulturaMonitoring> getMenuBipCulturaMonitoring(){
        return bipMapper.getMenuBipCulturaMonitoring();
    }

}
