package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.domain.Coordination;
import com.example.demo.repos.CoordinationMapper;

@Service
public class CoordinationService {

	@Autowired
	CoordinationMapper coordinationMapper;
	
	
	public List<Coordination> getLabourMarketByYear(String year) {
		return coordinationMapper.getLabourMarketByYear(year);
	}

}
