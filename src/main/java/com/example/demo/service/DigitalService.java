package com.example.demo.service;

import com.example.demo.domain.*;
import com.example.demo.repos.DigitalMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DigitalService {

    @Autowired
    DigitalMapper digitalMapper;

    public List<DigitalSocialObjectsMonitoring> getDigitalSocialObjectsMonitoringByYear(String yearEnd, String industry, String source) {
        List<DigitalSocialObjectsMonitoring> digitalSocialObjectsMonitorings = digitalMapper.getDigitalSocialObjectsMonitoringByYear(yearEnd, industry, source);
    return digitalSocialObjectsMonitorings;
    }

    public List<String> getDigitalSocialObjectsMonitoringYearEnd() {
        return digitalMapper.getDigitalSocialObjectsMonitoringYearEnd();
    }

    public List<String> getDigitalSocialObjectsMonitoringIndustry() {
        return digitalMapper.getDigitalSocialObjectsMonitoringIndustry();
    }

    public List<String> getDigitalSocialObjectsMonitoringSource() {
        return digitalMapper.getDigitalSocialObjectsMonitoringSource();
    }

    public List<DigitalSocialObjectsMonitoring> getDigitalSocialObjectsMonitoring(){
        return digitalMapper.getDigitalSocialObjectsMonitoring();
    }

    public List<DiplomMonitoring> getDiplomMonitoring(){
        return digitalMapper.getDiplomMonitoring();
    }

    public List<DiplomTableMonitoring> getDiplomTableMonitoring(){
        return digitalMapper.getDiplomTableMonitoring();
    }

    public List<DiplomSnpMonitoring> getDiplomSnpMonitoring(){
        return digitalMapper.getDiplomSnpMonitoring();
    }

    public List<DiplomMonitoring> getDiplomMonitoringByRegion(String regionIn, String fieldActivity,  String typeEducation, String statusEmployee,  String pol) {
        List<DiplomMonitoring> diplomMonitorings = digitalMapper.getDiplomMonitoringByRegion(regionIn, fieldActivity, typeEducation, statusEmployee, pol);
        return diplomMonitorings;
    }

    public List<String> getDiplomMonitoringRegionIn() {
        return digitalMapper.getDiplomMonitoringRegionIn();
    }

    public List<String> getDiplomMonitoringFieldActivity() {
        return digitalMapper.getDiplomMonitoringFieldActivity();
    }

    public List<String> getDiplomMonitoringTypeEducation() {
        return digitalMapper.getDiplomMonitoringTypeEducation();
    }

    public List<String> getDiplomMonitoringStatusEmployee() {
        return digitalMapper.getDiplomMonitoringStatusEmployee();
    }

    public List<String> getDiplomMonitoringPol() {
        return digitalMapper.getDiplomMonitoringPol();
    }

    public List<BlokEcologicalMonitoring> getBlokEcologicalMonitoring(){
        return digitalMapper.getBlokEcologicalMonitoring();
    }

    public List<BlokEconomicMonitoring> getBlokEconomicMonitoring(){
        return digitalMapper.getBlokEconomicMonitoring();
    }

    public List<BlokEngineeringMonitoring> getBlokEngineeringMonitoring(){
        return digitalMapper.getBlokEngineeringMonitoring();
    }

    public List<BlokSocialMonitoring> getBlokSocialMonitoring(){
        return digitalMapper.getBlokSocialMonitoring();
    }

    public List<IdaMonitoring> getIdaMonitoring(){
        return digitalMapper.getIdaMonitoring();
    }

    public List<IdinMonitoring> getIdinMonitoring(){
        return digitalMapper.getIdinMonitoring();
    }

    public List<IdisMonitoring> getIdisMonitoring(){
        return digitalMapper.getIdisMonitoring();
    }

    public List<IdkuMonitoring> getIdkuMonitoring(){
        return digitalMapper.getIdkuMonitoring();
    }

    public List<IdkyMonitoring> getIdkyMonitoring(){
        return digitalMapper.getIdkyMonitoring();
    }

    public List<IdmahMonitoring> getIdmahMonitoring(){
        return digitalMapper.getIdmahMonitoring();
    }

    public List<IdmakMonitoring> getIdmakMonitoring(){
        return digitalMapper.getIdmakMonitoring();
    }

    public List<IdzhMonitoring> getIdzhMonitoring(){
        return digitalMapper.getIdzhMonitoring();
    }

    public List<MenuSnpMonitoring> getMenuSnpMonitoring(){
        return digitalMapper.getMenuSnpMonitoring();
    }


}
