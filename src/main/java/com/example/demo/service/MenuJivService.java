package com.example.demo.service;

import com.example.demo.domain.*;
import com.example.demo.repos.MenuJivMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuJivService {

    @Autowired
    MenuJivMapper menuJivMapper;

    public List<MenuJivMonitoring> getMenuJivMonitoringByYear(String region){
        return menuJivMapper.getMenuJivMonitoringByYear(region);
    }

    public List<String> getMenuJivMonitoringYears(){
        return menuJivMapper.getMenuJivMonitoringYears();
    }

    public List<MenuRasMonitoring> getMenuRasMonitoringByYear(String year){
        return menuJivMapper.getMenuRasMonitoringByYear(year);
    }

    public List<MenuRasMonitoring> getMenuRasMonitoringByRegion(String region){
        return menuJivMapper.getMenuRasMonitoringByRegion(region);
    }

    public List<String> getMenuRasMonitoringYears() {
        return menuJivMapper.getMenuRasMonitoringYears();
    }

    public List<String> getMenuRasMonitoringRegions() {
        return menuJivMapper.getMenuRasMonitoringRegions();
    }

    public List<MenuRybAqva2Monitoring> getMenuRybAqva2MonitoringByYear(String year){
        return menuJivMapper.getMenuRybAqva2MonitoringByYear(year);
    }

    public List<String> getMenuRybAqva2MonitoringYears(){
        return menuJivMapper.getMenuRybAqva2MonitoringYears();
    }

    public List<MenuRybAqvaTable> getMenuRybAqvaTableByView(String view){
        return menuJivMapper.getMenuRybAqvaTableByView(view);
    }
//------------------
    public List<MenuRybAqvaTable> getMenu2RybAqvaTableByView(){
        return menuJivMapper.getMenu2RybAqvaTableByView();
    }

    public List<String> getMenuRybAqvaTableViews(){
        return menuJivMapper.getMenuRybAqvaTableViews();
    }

    public List<MenuRybAqva2TableMonitoring> getMenuRybAqva2TableMonitoringByYear(String year){
        return menuJivMapper.getMenuRybAqva2TableMonitoringByYear(year);
    }

    public List<String> getMenuRybAqva2TableYears(){
        return menuJivMapper.getMenuRybAqva2TableYears();
    }

    public List<MenuVetTableMonitoring> getMenuVetTableMonitoringByYear(String year){
        return menuJivMapper.getMenuVetTableMonitoringByYear(year);
    }

    public List<MenuVetTableMonitoring> getMenuVetTableMonitoringByRegion(String region){
        return menuJivMapper.getMenuVetTableMonitoringByRegion(region);
    }

    public List<MenuVetTableMonitoring> getMenuVetTableMonitoringByType(String type){
        return menuJivMapper.getMenuVetTableMonitoringByType(type);
    }

    public List<String> getMenuVetTableMonitoringYears(){
        return menuJivMapper.getMenuVetTableMonitoringYears();
    }

    public List<String> getMenuVetTableMonitoringTypes(){
        return menuJivMapper.getMenuVetTableMonitoringTypes();
    }

    public List<MenuJiv2Monitoring> getMenuJiv2MonitoringByYear(){
        return menuJivMapper.getMenuJiv2MonitoringByYear();
    }

    public List<MenuJiv2kshMonitoring> getMenuJiv2kshMonitoringByYear(){
        return menuJivMapper.getMenuJiv2kshMonitoringByYear();
    }

    public List<MenuJiv2kshCowsMonitoring> getMenuJiv2kshCowsMonitoringByYear(){
        return menuJivMapper.getMenuJiv2kshCowsMonitoringByYear();
    }

    public List<MenuJiv2kshSGMonitoring> getMenuJiv2kshSGMonitoringByYear(){
        return menuJivMapper.getMenuJiv2kshSGMonitoringByYear();
    }

    public List<MenuJiv2kshPMonitoring> getMenuJiv2kshPMonitoringByYear(){
        return menuJivMapper.getMenuJiv2kshPMonitoringByYear();
    }

    public List<MenuJiv2kshHMonitoring> getMenuJiv2kshHMonitoringByYear(){
        return menuJivMapper.getMenuJiv2kshHMonitoringByYear();
    }

    public List<MenuJiv2kshCMonitoring> getMenuJiv2kshCMonitoringByYear(){
        return menuJivMapper.getMenuJiv2kshCMonitoringByYear();
    }

    public List<MenuJiv2kshBMonitoring> getMenuJiv2kshBMonitoringByYear(){
        return menuJivMapper.getMenuJiv2kshBMonitoringByYear();
    }

    public List<MenuJiv2kshYMonitoring> getMenuJiv2kshYMonitoringByYear(){
        return menuJivMapper.getMenuJiv2kshYMonitoringByYear();
    }

    public List<MenuJiv2kshLMonitoring> getMenuJiv2kshLMonitoringByYear(){
        return menuJivMapper.getMenuJiv2kshLMonitoringByYear();
    }

    public List<MenuJiv2kshKMonitoring> getMenuJiv2kshKMonitoringByYear(){
        return menuJivMapper.getMenuJiv2kshKMonitoringByYear();
    }

    public List<MenuJiv2kshFMonitoring> getMenuJiv2kshFMonitoringByYear(){
        return menuJivMapper.getMenuJiv2kshFMonitoringByYear();
    }

    public List<MenuJiv2kshCSMonitoring> getMenuJiv2kshCSMonitoringByYear(){
        return menuJivMapper.getMenuJiv2kshCSMonitoringByYear();
    }

    public List<MenuJiv2sKRSMonitoring> getMenuJiv2sKRSMonitoringByYear(){
        return menuJivMapper.getMenuJiv2sKRSMonitoringByYear();
    }

    public List<MenuJiv2sCowsMonitoring> getMenuJiv2sCowsMonitoringByYear(){
        return menuJivMapper.getMenuJiv2sCowsMonitoringByYear();
    }

    public List<MenuJiv2sSMonitoring> getMenuJiv2sSMonitoringByYear(){
        return menuJivMapper.getMenuJiv2sSMonitoringByYear();
    }

    public List<MenuJiv2sPMonitoring> getMenuJiv2sPMonitoringByYear(){
        return menuJivMapper.getMenuJiv2sPMonitoringByYear();
    }

    public List<MenuJiv2sHMonitoring> getMenuJiv2sHMonitoringByYear(){
        return menuJivMapper.getMenuJiv2sHMonitoringByYear();
    }

    public List<MenuJiv2sCMonitoring> getMenuJiv2sCMonitoringByYear(){
        return menuJivMapper.getMenuJiv2sCMonitoringByYear();
    }

    public List<MenuJiv2sBMonitoring> getMenuJiv2sBMonitoringByYear(){
        return menuJivMapper.getMenuJiv2sBMonitoringByYear();
    }

    public List<MenuJiv2sYMonitoring> getMenuJiv2sYMonitoringByYear(){
        return menuJivMapper.getMenuJiv2sYMonitoringByYear();
    }

    public List<MenuJiv2sLMonitoring> getMenuJiv2sLMonitoringByYear(){
        return menuJivMapper.getMenuJiv2sLMonitoringByYear();
    }

    public List<MenuJiv2sKMonitoring> getMenuJiv2sKMonitoringByYear(){
        return menuJivMapper.getMenuJiv2sKMonitoringByYear();
    }

    public List<MenuJiv2sFMonitoring> getMenuJiv2sFMonitoringByYear(){
        return menuJivMapper.getMenuJiv2sFMonitoringByYear();
    }

    public List<MenuJiv2sCSMonitoring> getMenuJiv2sCSMonitoringByYear(){
        return menuJivMapper.getMenuJiv2sCSMonitoringByYear();
    }

    public List<MenuJiv2hnKRSMonitoring> getMenuJiv2hnKRSMonitoringByYear(){
        return menuJivMapper.getMenuJiv2hnKRSMonitoringByYear();
    }

    public List<MenuJiv2hnCowsMonitoring> getMenuJiv2hnCowsMonitoringByYear(){
        return menuJivMapper.getMenuJiv2hnCowsMonitoringByYear();
    }

    public List<MenuJiv2hnSMonitoring> getMenuJiv2hnSMonitoringByYear(){
        return menuJivMapper.getMenuJiv2hnSMonitoringByYear();
    }

    public List<MenuJiv2hnPMonitoring> getMenuJiv2hnPMonitoringByYear(){
        return menuJivMapper.getMenuJiv2hnPMonitoringByYear();
    }

    public List<MenuJiv2hnHMonitoring> getMenuJiv2hnHMonitoringByYear(){
        return menuJivMapper.getMenuJiv2hnHMonitoringByYear();
    }

    public List<MenuJiv2hnCAMonitoring> getMenuJiv2hnCAMonitoringByYear(){
        return menuJivMapper.getMenuJiv2hnCAMonitoringByYear();
    }

    public List<MenuJiv2hnBMonitoring> getMenuJiv2hnBMonitoringByYear(){
        return menuJivMapper.getMenuJiv2hnBMonitoringByYear();
    }

    public List<MenuJiv2hnYMonitoring> getMenuJiv2hnYMonitoringByYear(){
        return menuJivMapper.getMenuJiv2hnYMonitoringByYear();
    }

    public List<MenuJiv2hnLMonitoring> getMenuJiv2hnLMonitoringByYear(){
        return menuJivMapper.getMenuJiv2hnLMonitoringByYear();
    }

    public List<MenuJiv2hnKMonitoring> getMenuJiv2hnKMonitoringByYear(){
        return menuJivMapper.getMenuJiv2hnKMonitoringByYear();
    }

    public List<MenuJiv2hnFMonitoring> getMenuJiv2hnFMonitoringByYear(){
        return menuJivMapper.getMenuJiv2hnFMonitoringByYear();
    }

    public List<MenuJiv2hnCSMonitoring> getMenuJiv2hnCSMonitoringByYear(){
        return menuJivMapper.getMenuJiv2hnCSMonitoringByYear();
    }

    public List<ShValVypMonitoring> getShValVypMonitoring(){
        return menuJivMapper.getShValVypMonitoring();
    }

    public List<ShValSborMonitoring> getShValSborMonitoring(){
        return menuJivMapper.getShValSborMonitoring();
    }

    public List<ShVvpMonitoring> getShVvpMonitoring(){
        return menuJivMapper.getShVvpMonitoring();
    }

    public List<ShJivotMonitoring> getShJivotMonitoring(){
        return menuJivMapper.getShJivotMonitoring();
    }

    public List<ShORMonitoring> getShORMonitoring(){
        return menuJivMapper.getShORMonitoring();
    }

    public List<ShPSMonitoring> getShPSMonitoring(){
        return menuJivMapper.getShPSMonitoring();
    }

    public List<ShPMonitoring> getShPMonitoring(){
        return menuJivMapper.getShPMonitoring();
    }

    public List<ShPPMonitoring> getShPPMonitoring(){
        return menuJivMapper.getShPPMonitoring();
    }

    public List<ShPpoPMonitoring> getShPpoPMonitoring(){
        return menuJivMapper.getShPpoPMonitoring();
    }

    public List<ShPriplodMonitoring> getShPriplodMonitoring(){
        return menuJivMapper.getShPriplodMonitoring();
    }

    public List<ShProizvProdMonitoring> getShProizvProdMonitoring(){
        return menuJivMapper.getShProizvProdMonitoring();
    }

    public List<ShRKMonitoring> getShRKMonitoring(){
        return menuJivMapper.getShRKMonitoring();
    }

    public List<ShRastenMonitoring> getShRastenMonitoring(){
        return menuJivMapper.getShRastenMonitoring();
    }

    public List<ShRybaMonitoring> getShRybaMonitoring(){
        return menuJivMapper.getShRybaMonitoring();
    }

    public List<ShRybolovMonitoring> getShRybolovMonitoring(){
        return menuJivMapper.getShRybolovMonitoring();
    }

    public List<ShUPMonitoring> getShUPMonitoring(){
        return menuJivMapper.getShUPMonitoring();
    }

    public List<ShUrojMonitoring> getShUrojMonitoring(){
        return menuJivMapper.getShUrojMonitoring();
    }



}
