package com.example.demo.service;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.example.demo.domain.*;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.repos.MonitoringMapper;

@Service
public class MonitoringService {

	@Autowired
	MonitoringMapper monitoringMapper;
	
	public HealthcareMonitoring getHealthcareMonitoringByYear(String year) {
		HealthcareMonitoring healthcareMonitoring = 
				monitoringMapper.getHealthcareMonitoringByYear(year);

		setMonthes(healthcareMonitoring);
		
		return healthcareMonitoring;
	}
	
	public Map<String, String[]> getHealthcareYears() {
		Map<String, String[]> years = new HashMap<>();
		
		years.put("Года мониторинга Здравохранения по месяцам", monitoringMapper.getMonthHealthcareYears());
		years.put("Года мониторинга Здравохранения по кварталам", monitoringMapper.getQuarterHealthcareYears());
		
		return years;
	}
	
	public HealthcareMonitoring2 getHealthcareMonitoringByQuartersOfYear(String year) {
		HealthcareMonitoring2 healthcareMonitoring2 = 
				monitoringMapper.getHealthcareMonitoring2ByYear(year);

		setQuarters(healthcareMonitoring2);
		
		return healthcareMonitoring2;
	}
	
	public List<HealthcareMonitoring3> getHealthcareMonitoring3() {
		return monitoringMapper.getHealthcareMonitoring3();
	}
	
	public List<BudgetMonitoring> getBudgetMonitoring() {
		return monitoringMapper.getBudgetMonitoring();
	}

	public List<LandRelations> getLandRelations(String year) {
		return monitoringMapper.getLandRelations(year);
	}

	public List<String> getLandYears() {
		return monitoringMapper.getLandYears();
	}


	public List<ArchitectureMonitoring> getArchitectureByYear(String year) {
		return monitoringMapper.getArchitectureByYear(year);
	}
	
	public List<String> getArchitectureYears() {
		return monitoringMapper.getArchitectureYears();
	}
	
	public List<GovermentPurchases> getGovermentPurchasesByYear(String year) {
		return monitoringMapper.getGovermentPurchasesByYear(year);
	}
	
	public List<String> getGovermentPurchasesYears() {
		return monitoringMapper.getGovermentPurchasesYears();
	}
	
	public List<ConstructionMonitoring> getConstructionsByYear(String year) {
		return monitoringMapper.getConstructionsByYear(year);
	}
	
	public HealthcareMonitoringUnits getHealthcareMonitoringUnits() {
		return monitoringMapper.getHealthcareMonitoringUnits();
	}

	public List<String> getConstructionYears() {
		return monitoringMapper.getConstructionYears();
	}

//	StateLaborInspectorate aao

	public List<StateLaborInspectorate> getStateLaborInspectorateByYear(String year) {
		return monitoringMapper.getStateLaborInspectorateByYear(year);
	}
	
	public List<String> getStateLaborInspectorateYears() {
		return monitoringMapper.getStateLaborInspectorateYears();
	}

// StateLaborInspectorate okbot

	public List<StateLaborInspectorateOkbotMonitoring> getStateLaborInspectorateOkbotMonitoringByYear(String year) {
		return monitoringMapper.getStateLaborInspectorateOkbotMonitoringByYear(year);
	}

	public List<String> getStateLaborInspectorateOkbotMonitoringYears() {
		return monitoringMapper.getStateLaborInspectorateOkbotMonitoringYears();
	}

// StateLaborInspectorate okto

	public List<StateLaborInspectorateOktoMonitoring> getStateLaborInspectorateOktoMonitoringByYear(String year) {
		return monitoringMapper.getStateLaborInspectorateOktoMonitoringByYear(year);
	}

	public List<String> getStateLaborInspectorateOktoMonitoringYears() {
		return monitoringMapper.getStateLaborInspectorateOktoMonitoringYears();
	}


    public List<FizMonitoring> getFizMonitoring() {
        return monitoringMapper.getFizMonitoringByYear();
    }

//    public List<String> getFizByYears(String year) {return monitoringMapper.getFizByYears(year); }


	public List<EducationMonitoring> getEducationStatistics() {
		return monitoringMapper.getEducationStatistics();
	}
	
	public List<ReligionMonitoring> getReligionByYear(String year) {
		return monitoringMapper.getReligionByYear(year);
	}
	
	public List<String> getReligionYears() {
		return monitoringMapper.getReligionYears();
	}
	
	public List<ControlOverLandUseMonitoring> getControlOverLandUseMonitoringByYear(String year) {
		return monitoringMapper.getControlOverLandUseMonitoringByYear(year);
	}
	
	public List<String> getControlOverLandUseMonitoringYears() {
		return monitoringMapper.getControlOverLandUseMonitoringYears();
	}
	
	public List<EconomicMonitoring> getEconomicMonthesByYear(String year) {
		return monitoringMapper.getEconomicMonthesByYear(year);
	}
	
	public List<EconomicMonitoring2> getEconomicQuartersByYear(String year) {
		return monitoringMapper.getEconomicQuartersByYear(year);
	}
	
	public Map<String, List<String>> geteconomicYears() {
		Map<String, List<String>>  years = new HashMap<>();
		
		years.put("Года ежемесячной экономики", monitoringMapper.getYearsOfEconomicMonthes());
		years.put("Года ежеквартальной экономики", monitoringMapper.getYearsOfEconomicQuarters());
		
		return years;
	}
	
	public List<TransportMonitoring> getTransportMonthesByYear(String year) {
		return monitoringMapper.getTransportMonthesByYear(year);
	}
	
	public List<String> getTransportYears() {
		return monitoringMapper.getTransportYears();
	}
	
	public List<TransportMonitoring2> getTransportsByYears() {
		return monitoringMapper.getTransportsByYears();
	}
	
	public List<AgriculturalIndustryMonitoring> getAgriculturalIndustryByYear(String year) {
		return monitoringMapper.getAgriculturalIndustryByYear(year);
	}
	
	public List<String> getAgriculturalIndustryYears() {
		return monitoringMapper.getAgriculturalIndustryYears();
	}
	
	public List<VeterenaryMonitoring> getVeterenaryByYear(String year) {
		return monitoringMapper.getVeterenaryByYear(year);
	}
	
	public List<String> getVeterenaryYears() {
		return monitoringMapper.getVeterenaryYears();
	}

	public List<EnergetikaIJxk>getEnergetikaIJkxMonthesByYears(String year){
		return monitoringMapper.getEnergerikaIJkzMonthesByYear(year);
	}

	public List<String>getEnergetikaIJkxYears(){
		return monitoringMapper.getEnergetikaIJkxYears();
	}

	public List<EnergetikaIJkx2> getEnergetikaIJkxsByYears() {
		return monitoringMapper.getEnergetikaIJkxsByYears();
	}

	public List<KoordyearMonitoring>getKoordyearMonitoring(){
		return monitoringMapper.getKoordyearMonitoring();
	}




	private void setMonthes(HealthcareMonitoring healthcareMonitoring) {
		String[] monthes = {"Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", 
					"Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"};
				
		List<Method> getters = new ArrayList<>();
		
		try {
			for(PropertyDescriptor propertyDescriptor : 
			    Introspector.getBeanInfo(healthcareMonitoring.getClass(), Object.class).getPropertyDescriptors()) {
				Method getter = propertyDescriptor.getReadMethod();
				String fullGetterName = getter.toString();
				String getterName = getter.toString().substring(fullGetterName.lastIndexOf(".") + 1);
				
				if (!getterName.equals("getYear()")) {
					getters.add(getter);
				}
			}
		} catch (IntrospectionException e1) {
			e1.printStackTrace();
		}
		
		for (String m : monthes) {
			String strValues = monitoringMapper.getHealthcareValuesByYearAndMonth(healthcareMonitoring.getYear(), m);
			
			List<Integer> values = convertStringToArray(strValues, Integer.class);
			
			for (int i = 0; i < getters.size(); i++) {
				Method getter = getters.get(i);
				
				try {
					Map<String, Integer> field = (Map<String, Integer>) getter.invoke(healthcareMonitoring);
					field.put(m, values.get(i));
				} catch (SecurityException | IllegalAccessException | 
						IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private void setQuarters(HealthcareMonitoring2 healthcareMonitoring2) {
		String[] quarters = {"I квартал", "II квартал", "III квартал", "IV квартал"};
		
		List<Method> getters = new ArrayList<>();
		
		try {
			for(PropertyDescriptor propertyDescriptor : 
			    Introspector.getBeanInfo(healthcareMonitoring2.getClass(), Object.class).getPropertyDescriptors()) {
				Method getter = propertyDescriptor.getReadMethod();
				String fullGetterName = getter.toString();
				String getterName = getter.toString().substring(fullGetterName.lastIndexOf(".") + 1);
				
				if (!getterName.equals("getYear()")) {
					getters.add(getter);
				}
			}
		} catch (IntrospectionException e1) {
			e1.printStackTrace();
		}
		
		for (String q : quarters) {
			String strValues = monitoringMapper.getHealthcareValuesByYearAndQuarter(healthcareMonitoring2.getYear(), q);

			// Если нету определнного квартала, то он пропускает
			if (strValues == null) {
				continue;
			}

			List<Double> values = convertStringToArray(strValues, Double.class);

			for (int i = 0; i < getters.size(); i++) {
				Method getter = getters.get(i);
				
				try {
					Map<String, Double> field = (Map<String, Double>) getter.invoke(healthcareMonitoring2);
					field.put(q, values.get(i));
				} catch (SecurityException | IllegalAccessException | 
						IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private <T> List<T> convertStringToArray(String str, Class<T> cls) {
		String[] strValues = str.substring(1, str.length()-1)
				.replaceAll("\"", "")
				.split(",");
		
		List<T> newValues = Arrays.asList(strValues)
				.stream()
				.map(n -> {
					try {
						Method method = cls.getDeclaredMethod("valueOf", String.class);
						return n.equals("NULL") ? null : (T) method.invoke(null, n);
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
							| NoSuchMethodException | SecurityException e) {
						e.printStackTrace();
					}
					
					return (T) n;
				})
				.collect(Collectors.toList());
		
		return newValues;
	}

	public List<Architectura2> getArchitectura2ByYear(String year){
		return monitoringMapper.getArchitectura2ByYear(year);
	}

	public List<String> getArchitectura2Years() {
		return monitoringMapper.getArchitectura2Years();
	}


//KoordQuart

	public KoordquartMonitoring getKoordquartMonitoringByQuatersOfYear(String year){
		KoordquartMonitoring koordquartMonitoring = monitoringMapper.getKoordquartMonitoringByYear(year);

		setQuarters(koordquartMonitoring);

		return koordquartMonitoring;
	}

	public KoordquartMonitoringUnits getKoordquartMonitoringUnits(){
		return monitoringMapper.getKoordinquartMonitoringUnits();
	}


	private void setQuarters(KoordquartMonitoring koordquartMonitoring) {
		String[] quarters = {"I квартал", "II квартал", "III квартал", "IV квартал"};

		List<Method> getters = new ArrayList<>();

		try {
			for(PropertyDescriptor propertyDescriptor :
					Introspector.getBeanInfo(koordquartMonitoring.getClass(), Object.class).getPropertyDescriptors()) {
				Method getter = propertyDescriptor.getReadMethod();
				String fullGetterName = getter.toString();
				String getterName = getter.toString().substring(fullGetterName.lastIndexOf(".") + 1);

				if (!getterName.equals("getYear()")) {
					getters.add(getter);
				}
			}
		} catch (IntrospectionException e1) {
			e1.printStackTrace();
		}

		for (String q : quarters) {
			String strValues = monitoringMapper.getKoordinValuesByYearAndQuarter(koordquartMonitoring.getYear(), q);

			// Если нету определнного квартала, то он пропускает
			if (strValues == null) {
				continue;
			}

			List<Double> values = convertStringToArray(strValues, Double.class);

			for (int i = 0; i < getters.size(); i++) {
				Method getter = getters.get(i);

				try {
					Map<String, Double> field = (Map<String, Double>) getter.invoke(koordquartMonitoring);
					field.put(q, values.get(i));
				} catch (SecurityException | IllegalAccessException |
						IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
	}

//KoordmonthMonitoring

	public KoordmonthMonitoring getKoordmonthMonitoringByYear(String year){
		KoordmonthMonitoring koordmonthMonitoring =monitoringMapper.getKoordmonthMonitoringByYear(year);

		setMonthes(koordmonthMonitoring);

		return koordmonthMonitoring;
	}

	public Map<String, String[]> getKoordinYears(){
		Map<String, String[]> years = new HashMap<>();

		years.put("Года мониторинга Координации по месяцам", monitoringMapper.getMonthKoordinYears());
		years.put("Года мониторинга Координации по кварталам", monitoringMapper.getQuarterKoordinYears());

		return years;
	}


	private void setMonthes(KoordmonthMonitoring koordmonthMonitoring) {
		String[] monthes = {"Январь", "Февраль", "Март", "Апрель", "Май", "Июнь",
				"Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"};

		List<Method> getters = new ArrayList<>();

		try {
			for(PropertyDescriptor propertyDescriptor :
					Introspector.getBeanInfo(koordmonthMonitoring.getClass(), Object.class).getPropertyDescriptors()) {
				Method getter = propertyDescriptor.getReadMethod();
				String fullGetterName = getter.toString();
				String getterName = getter.toString().substring(fullGetterName.lastIndexOf(".") + 1);

				if (!getterName.equals("getYear()")) {
					getters.add(getter);
				}
			}
		} catch (IntrospectionException e1) {
			e1.printStackTrace();
		}

		for (String m : monthes) {
			String strValues = monitoringMapper.getKoordinValuesByYearAndMonth(koordmonthMonitoring.getYear(), m);

			List<Double> values = convertStringToArray(strValues, Double.class);

			for (int i = 0; i < getters.size(); i++) {
				Method getter = getters.get(i);

				try {
					Map<String, Double> field = (Map<String, Double>) getter.invoke(koordmonthMonitoring);
					field.put(m, values.get(i));
				} catch (SecurityException | IllegalAccessException |
						IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
	}


//Promyshlennost

	public List<PromyshlennostYearMonitoring> getPromByYears(){
		return monitoringMapper.getPromByYears();
	}

//PromYear2
	public List<PromyshlennostYear2Monitoring> getProm2ByYears(){
		return monitoringMapper.getProm2ByYears();
	}

//PromQuart

	public PromyshlennostQuaterMonitoring getPromyshlennostquartMonitoringByQuatersOfYear(String year){
		PromyshlennostQuaterMonitoring promyshlennostQuaterMonitoring = monitoringMapper.getPromyshlennostQuaterMonitoringByYear(year);

		setQuarters(promyshlennostQuaterMonitoring);

		return promyshlennostQuaterMonitoring;
	}

//	public KoordquartMonitoringUnits getKoordquartMonitoringUnits(){
//		return monitoringMapper.getKoordinquartMonitoringUnits();
//	}


	private void setQuarters(PromyshlennostQuaterMonitoring promyshlennostQuaterMonitoring) {
		String[] quarters = {"I квартал", "II квартал", "III квартал", "IV квартал"};

		List<Method> getters = new ArrayList<>();

		try {
			for(PropertyDescriptor propertyDescriptor :
					Introspector.getBeanInfo(promyshlennostQuaterMonitoring.getClass(), Object.class).getPropertyDescriptors()) {
				Method getter = propertyDescriptor.getReadMethod();
				String fullGetterName = getter.toString();
				String getterName = getter.toString().substring(fullGetterName.lastIndexOf(".") + 1);

				if (!getterName.equals("getYear()")) {
					getters.add(getter);
				}
			}
		} catch (IntrospectionException e1) {
			e1.printStackTrace();
		}

		for (String q : quarters) {
			String strValues = monitoringMapper.getPromyshlennostValuesByYearAndQuarter(promyshlennostQuaterMonitoring.getYear(), q);

			// Если нету определнного квартала, то он пропускает
			if (strValues == null) {
				continue;
			}

			List<Double> values = convertStringToArray(strValues, Double.class);

			for (int i = 0; i < getters.size(); i++) {
				Method getter = getters.get(i);

				try {
					Map<String, Double> field = (Map<String, Double>) getter.invoke(promyshlennostQuaterMonitoring);
					field.put(q, values.get(i));
				} catch (SecurityException | IllegalAccessException |
						IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
	}
//PromQuart_2

	public PromQuaterOropMonitoring getPromquartMonitoringByQuatersOfYear(String year){
		PromQuaterOropMonitoring promQuaterOropMonitoring = monitoringMapper.getPromQuaterOropMonitoringByYear(year);

		setQuarters(promQuaterOropMonitoring);

		return promQuaterOropMonitoring;
	}

	private void setQuarters(PromQuaterOropMonitoring promQuaterOropMonitoring) {
		String[] quarters = {"I квартал", "II квартал", "III квартал", "IV квартал"};

		List<Method> getters = new ArrayList<>();

		try {
			for(PropertyDescriptor propertyDescriptor :
					Introspector.getBeanInfo(promQuaterOropMonitoring.getClass(), Object.class).getPropertyDescriptors()) {
				Method getter = propertyDescriptor.getReadMethod();
				String fullGetterName = getter.toString();
				String getterName = getter.toString().substring(fullGetterName.lastIndexOf(".") + 1);

				if (!getterName.equals("getYear()")) {
					getters.add(getter);
				}
			}
		} catch (IntrospectionException e1) {
			e1.printStackTrace();
		}

		for (String q : quarters) {
			String strValues = monitoringMapper.getPromValuesByYearAndQuarter(promQuaterOropMonitoring.getYear(), q);

			// Если нету определнного квартала, то он пропускает
			if (strValues == null) {
				continue;
			}

			List<Double> values = convertStringToArray(strValues, Double.class);

			for (int i = 0; i < getters.size(); i++) {
				Method getter = getters.get(i);

				try {
					Map<String, Double> field = (Map<String, Double>) getter.invoke(promQuaterOropMonitoring);
					field.put(q, values.get(i));
				} catch (SecurityException | IllegalAccessException |
						IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
	}

//PromQuart_ort

	public PromQuaterOrtMonitoring getPromquartOrtMonitoringByQuatersOfYear(String year){
		PromQuaterOrtMonitoring promQuaterOrtMonitoring = monitoringMapper.getPromQuaterOrtMonitoringByYear(year);

		setQuarters(promQuaterOrtMonitoring);

		return promQuaterOrtMonitoring;
	}

	private void setQuarters(PromQuaterOrtMonitoring promQuaterOrtMonitoring) {
		String[] quarters = {"I квартал", "II квартал", "III квартал", "IV квартал"};

		List<Method> getters = new ArrayList<>();

		try {
			for(PropertyDescriptor propertyDescriptor :
					Introspector.getBeanInfo(promQuaterOrtMonitoring.getClass(), Object.class).getPropertyDescriptors()) {
				Method getter = propertyDescriptor.getReadMethod();
				String fullGetterName = getter.toString();
				String getterName = getter.toString().substring(fullGetterName.lastIndexOf(".") + 1);

				if (!getterName.equals("getYear()")) {
					getters.add(getter);
				}
			}
		} catch (IntrospectionException e1) {
			e1.printStackTrace();
		}

		for (String q : quarters) {
			String strValues = monitoringMapper.getPromOrtValuesByYearAndQuarter(promQuaterOrtMonitoring.getYear(), q);

			// Если нету определнного квартала, то он пропускает
			if (strValues == null) {
				continue;
			}

			List<Double> values = convertStringToArray(strValues, Double.class);

			for (int i = 0; i < getters.size(); i++) {
				Method getter = getters.get(i);

				try {
					Map<String, Double> field = (Map<String, Double>) getter.invoke(promQuaterOrtMonitoring);
					field.put(q, values.get(i));
				} catch (SecurityException | IllegalAccessException |
						IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
	}
//PromQuart_ovs

	public PromQuaterOvsMonitoring getPromquartOvsMonitoringByQuatersOfYear(String year){
		PromQuaterOvsMonitoring promQuaterOvsMonitoring = monitoringMapper.getPromQuaterOvsMonitoringByYear(year);

		setQuarters(promQuaterOvsMonitoring);

		return promQuaterOvsMonitoring;
	}

	private void setQuarters(PromQuaterOvsMonitoring promQuaterOvsMonitoring) {
		String[] quarters = {"I квартал", "II квартал", "III квартал", "IV квартал"};

		List<Method> getters = new ArrayList<>();

		try {
			for(PropertyDescriptor propertyDescriptor :
					Introspector.getBeanInfo(promQuaterOvsMonitoring.getClass(), Object.class).getPropertyDescriptors()) {
				Method getter = propertyDescriptor.getReadMethod();
				String fullGetterName = getter.toString();
				String getterName = getter.toString().substring(fullGetterName.lastIndexOf(".") + 1);

				if (!getterName.equals("getYear()")) {
					getters.add(getter);
				}
			}
		} catch (IntrospectionException e1) {
			e1.printStackTrace();
		}

		for (String q : quarters) {
			String strValues = monitoringMapper.getPromOvsValuesByYearAndQuarter(promQuaterOvsMonitoring.getYear(), q);

			// Если нету определнного квартала, то он пропускает
			if (strValues == null) {
				continue;
			}

			List<Double> values = convertStringToArray(strValues, Double.class);

			for (int i = 0; i < getters.size(); i++) {
				Method getter = getters.get(i);

				try {
					Map<String, Double> field = (Map<String, Double>) getter.invoke(promQuaterOvsMonitoring);
					field.put(q, values.get(i));
				} catch (SecurityException | IllegalAccessException |
						IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
	}


//Prom_M_Mon

	public PromMMonitoring getPromSmonthMonitoringByYear(String year){
		PromMMonitoring promMMonitoring = monitoringMapper.getPrommonthMonitoringByYear(year);

		setMonthes2(promMMonitoring);

		return promMMonitoring;
	}

	public Map<String, String[]> getProminYears(){
		Map<String, String[]> years = new HashMap<>();

		years.put("Года мониторинга Промышленность по месяцам /Все остальные", monitoringMapper.getMonthPromYears());
		years.put("Года мониторинга Промышленность по месяцам /Количество зарегистрированных субъектов МСП, /Действующие субъекты МСП", monitoringMapper.getMonthPromOpprYears());
		years.put("Года мониторинга Промышленность по кварталам для /Объем продукции МСП, / Численность занятых, /Доля МСП от экономическиого активного населения", monitoringMapper.getQuarterPromYears());
		years.put("Года мониторинга Промышленность по кварталам для /Рост объема не сырьевого экспорта ", monitoringMapper.getQuarterPromOropYears());
		years.put("Года мониторинга Промышленность по кварталам для /Объем услуг оказанных в сфере туризма, /Количество посетителей въездного туризма, /Количество посетителей внутреннего туризма ", monitoringMapper.getQuarterPromOrtYears());
		years.put("Года мониторинга Промышленность по кварталам для /Розничный товарооборот, /Внешнеторговый оборот, /Экспорт, /Импорт, /Товарооборот со странами ЕАЭС, /Экспорт, в страны ЕАЭС, /Испорт в страны ЕАЭС ", monitoringMapper.getQuarterPromOvsYears());

		return years;
	}


	private void setMonthes2(PromMMonitoring promMMonitoring) {
		String[] monthes = {"Январь", "Февраль", "Март", "Апрель", "Май", "Июнь",
				"Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"};

		List<Method> getters = new ArrayList<>();

		try {
			for(PropertyDescriptor propertyDescriptor :
					Introspector.getBeanInfo(promMMonitoring.getClass(), Object.class).getPropertyDescriptors()) {
				Method getter = propertyDescriptor.getReadMethod();
				String fullGetterName = getter.toString();
				String getterName = getter.toString().substring(fullGetterName.lastIndexOf(".") + 1);

				if (!getterName.equals("getYear()")) {
					getters.add(getter);
				}
			}
		} catch (IntrospectionException e1) {
			e1.printStackTrace();
		}

		for (String m : monthes) {
			String strValues = monitoringMapper.getPromValuesByYearAndMonth(promMMonitoring.getYear(), m);


            // Если нету определнного месяца
            if (strValues == null) {
                continue;
            }


            List<Double> values = convertStringToArray(strValues, Double.class);

			for (int i = 0; i < getters.size(); i++) {
				Method getter = getters.get(i);

				try {
					Map<String, Double> field = (Map<String, Double>) getter.invoke(promMMonitoring);
					field.put(m, values.get(i));
				} catch (SecurityException | IllegalAccessException |
						IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
	}

//Prom_M_oppr_Mon

    public PromMOpprMonitoring getPromSmonthOpprMonitoringByYear(String year){
        PromMOpprMonitoring promMOpprMonitoring = monitoringMapper.getPrommonthOpprMonitoringByYear(year);

        setMonthes2(promMOpprMonitoring);

        return promMOpprMonitoring;
    }



    private void setMonthes2(PromMOpprMonitoring promMOpprMonitoring) {
        String[] monthes = {"Январь", "Февраль", "Март", "Апрель", "Май", "Июнь",
                "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"};

        List<Method> getters = new ArrayList<>();

        try {
            for(PropertyDescriptor propertyDescriptor :
                    Introspector.getBeanInfo(promMOpprMonitoring.getClass(), Object.class).getPropertyDescriptors()) {
                Method getter = propertyDescriptor.getReadMethod();
                String fullGetterName = getter.toString();
                String getterName = getter.toString().substring(fullGetterName.lastIndexOf(".") + 1);

                if (!getterName.equals("getYear()")) {
                    getters.add(getter);
                }
            }
        } catch (IntrospectionException e1) {
            e1.printStackTrace();
        }

        for (String m : monthes) {
            String strValues = monitoringMapper.getPromOpprValuesByYearAndMonth(promMOpprMonitoring.getYear(), m);


            // Если нету определнного месяца
            if (strValues == null) {
                continue;
            }


            List<Double> values = convertStringToArray(strValues, Double.class);

            for (int i = 0; i < getters.size(); i++) {
                Method getter = getters.get(i);

                try {
                    Map<String, Double> field = (Map<String, Double>) getter.invoke(promMOpprMonitoring);
                    field.put(m, values.get(i));
                } catch (SecurityException | IllegalAccessException |
                        IllegalArgumentException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
    }







//NaturalYear

	public List<NaturalResourcesYearMonitoring> getNaturalByYears(){
		return monitoringMapper.getNaturalByYears();
	}

//NaturalYear2

	public List<NaturalResourcesYear2Monitoring> getNatural2ByYears(){
		return monitoringMapper.getNatural2ByYears();
	}

//NaturalQuart_opp

	public NaturalResQuarterOppMonitoring getNaturalResQuarterOppMonitoringByQuatersOfYear(String year){
		NaturalResQuarterOppMonitoring naturalResQuarterOppMonitoring = monitoringMapper.getNaturalResQuarterOppByYear(year);

		setQuarters(naturalResQuarterOppMonitoring);

		return naturalResQuarterOppMonitoring;
	}

	private void setQuarters(NaturalResQuarterOppMonitoring naturalResQuarterOppMonitoring) {
		String[] quarters = {"I квартал", "II квартал", "III квартал", "IV квартал"};

		List<Method> getters = new ArrayList<>();

		try {
			for(PropertyDescriptor propertyDescriptor :
					Introspector.getBeanInfo(naturalResQuarterOppMonitoring.getClass(), Object.class).getPropertyDescriptors()) {
				Method getter = propertyDescriptor.getReadMethod();
				String fullGetterName = getter.toString();
				String getterName = getter.toString().substring(fullGetterName.lastIndexOf(".") + 1);

				if (!getterName.equals("getYear()")) {
					getters.add(getter);
				}
			}
		} catch (IntrospectionException e1) {
			e1.printStackTrace();
		}

		for (String q : quarters) {
			String strValues = monitoringMapper.getNaturalResQuarterOppValuesByYearAndQuarter(naturalResQuarterOppMonitoring.getYear(), q);

			// Если нету определнного квартала, то он пропускает
			if (strValues == null) {
				continue;
			}

			List<Double> values = convertStringToArray(strValues, Double.class);

			for (int i = 0; i < getters.size(); i++) {
				Method getter = getters.get(i);

				try {
					Map<String, Double> field = (Map<String, Double>) getter.invoke(naturalResQuarterOppMonitoring);
					field.put(q, values.get(i));
				} catch (SecurityException | IllegalAccessException |
						IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
	}

//NaturalQuart_opp

	public NaturalResQuarterOlohMonitoring getNaturalResQuarterOlohMonitoringByQuatersOfYear(String year){
		NaturalResQuarterOlohMonitoring naturalResQuarterOlohMonitoring = monitoringMapper.getNaturalResQuarterOlohByYear(year);

		setQuarters(naturalResQuarterOlohMonitoring);

		return naturalResQuarterOlohMonitoring;
	}

	private void setQuarters(NaturalResQuarterOlohMonitoring naturalResQuarterOlohMonitoring) {
		String[] quarters = {"I квартал", "II квартал", "III квартал", "IV квартал"};

		List<Method> getters = new ArrayList<>();

		try {
			for(PropertyDescriptor propertyDescriptor :
					Introspector.getBeanInfo(naturalResQuarterOlohMonitoring.getClass(), Object.class).getPropertyDescriptors()) {
				Method getter = propertyDescriptor.getReadMethod();
				String fullGetterName = getter.toString();
				String getterName = getter.toString().substring(fullGetterName.lastIndexOf(".") + 1);

				if (!getterName.equals("getYear()")) {
					getters.add(getter);
				}
			}
		} catch (IntrospectionException e1) {
			e1.printStackTrace();
		}

		for (String q : quarters) {
			String strValues = monitoringMapper.getNaturalResQuarterOlohValuesByYearAndQuarter(naturalResQuarterOlohMonitoring.getYear(), q);

			// Если нету определнного квартала, то он пропускает
			if (strValues == null) {
				continue;
			}

			List<Double> values = convertStringToArray(strValues, Double.class);

			for (int i = 0; i < getters.size(); i++) {
				Method getter = getters.get(i);

				try {
					Map<String, Double> field = (Map<String, Double>) getter.invoke(naturalResQuarterOlohMonitoring);
					field.put(q, values.get(i));
				} catch (SecurityException | IllegalAccessException |
						IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
	}


//NaturalQuart_ogeevr

	public NaturalResQuarterOgeevrMonitoring getNaturalResQuarterOgeevrMonitoringByQuatersOfYear(String year){
		NaturalResQuarterOgeevrMonitoring naturalResQuarterOgeevrMonitoring = monitoringMapper.getNaturalResQuarterOgeevrByYear(year);

		setQuarters(naturalResQuarterOgeevrMonitoring);

		return naturalResQuarterOgeevrMonitoring;
	}

	private void setQuarters(NaturalResQuarterOgeevrMonitoring naturalResQuarterOgeevrMonitoring) {
		String[] quarters = {"I квартал", "II квартал", "III квартал", "IV квартал"};

		List<Method> getters = new ArrayList<>();

		try {
			for(PropertyDescriptor propertyDescriptor :
					Introspector.getBeanInfo(naturalResQuarterOgeevrMonitoring.getClass(), Object.class).getPropertyDescriptors()) {
				Method getter = propertyDescriptor.getReadMethod();
				String fullGetterName = getter.toString();
				String getterName = getter.toString().substring(fullGetterName.lastIndexOf(".") + 1);

				if (!getterName.equals("getYear()")) {
					getters.add(getter);
				}
			}
		} catch (IntrospectionException e1) {
			e1.printStackTrace();
		}

		for (String q : quarters) {
			String strValues = monitoringMapper.getNaturalResQuarterOgeevrValuesByYearAndQuarter(naturalResQuarterOgeevrMonitoring.getYear(), q);

			// Если нету определнного квартала, то он пропускает
			if (strValues == null) {
				continue;
			}

			List<Double> values = convertStringToArray(strValues, Double.class);

			for (int i = 0; i < getters.size(); i++) {
				Method getter = getters.get(i);

				try {
					Map<String, Double> field = (Map<String, Double>) getter.invoke(naturalResQuarterOgeevrMonitoring);
					field.put(q, values.get(i));
				} catch (SecurityException | IllegalAccessException |
						IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
	}

//Nat_half_oapkr

	public NatRhyOapkrMonitoring getNatRhyOapkrMonitoringByHalfOfYear(String year){
		NatRhyOapkrMonitoring natRhyOapkrMonitoring = monitoringMapper.getNatRhyOapkrByYear(year);

		setHalfs(natRhyOapkrMonitoring);

		return natRhyOapkrMonitoring;
	}

	private void setHalfs(NatRhyOapkrMonitoring natRhyOapkrMonitoring) {
		String[] halfs = {"I полугодие", "II полугодие"};

		List<Method> getters = new ArrayList<>();

		try {
			for(PropertyDescriptor propertyDescriptor :
					Introspector.getBeanInfo(natRhyOapkrMonitoring.getClass(), Object.class).getPropertyDescriptors()) {
				Method getter = propertyDescriptor.getReadMethod();
				String fullGetterName = getter.toString();
				String getterName = getter.toString().substring(fullGetterName.lastIndexOf(".") + 1);

				if (!getterName.equals("getYear()")) {
					getters.add(getter);
				}
			}
		} catch (IntrospectionException e1) {
			e1.printStackTrace();
		}

		for (String q : halfs) {
			String strValues = monitoringMapper.getNatRhyOapkrValuesByYearAndHalf(natRhyOapkrMonitoring.getYear(), q);

			// Если нету определнного полугодия
			if (strValues == null) {
				continue;
			}

			List<Double> values = convertStringToArray(strValues, Double.class);

			for (int i = 0; i < getters.size(); i++) {
				Method getter = getters.get(i);

				try {
					Map<String, Double> field = (Map<String, Double>) getter.invoke(natRhyOapkrMonitoring);
					field.put(q, values.get(i));
				} catch (SecurityException | IllegalAccessException |
						IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
	}

//Nat_half_ogeevr

	public NatRhyOgeevrMonitoring getNatRhyOgeevrMonitoringByHalfOfYear(String year){
		NatRhyOgeevrMonitoring natRhyOgeevrMonitoring = monitoringMapper.getNatRhyOgeevrByYear(year);

		setHalfs(natRhyOgeevrMonitoring);

		return natRhyOgeevrMonitoring;
	}

	private void setHalfs(NatRhyOgeevrMonitoring natRhyOgeevrMonitoring) {
		String[] halfs = {"I полугодие", "II полугодие"};

		List<Method> getters = new ArrayList<>();

		try {
			for(PropertyDescriptor propertyDescriptor :
					Introspector.getBeanInfo(natRhyOgeevrMonitoring.getClass(), Object.class).getPropertyDescriptors()) {
				Method getter = propertyDescriptor.getReadMethod();
				String fullGetterName = getter.toString();
				String getterName = getter.toString().substring(fullGetterName.lastIndexOf(".") + 1);

				if (!getterName.equals("getYear()")) {
					getters.add(getter);
				}
			}
		} catch (IntrospectionException e1) {
			e1.printStackTrace();
		}

		for (String q : halfs) {
			String strValues = monitoringMapper.getNatRhyOgeevrValuesByYearAndHalf(natRhyOgeevrMonitoring.getYear(), q);

			// Если нету определнного полугодия
			if (strValues == null) {
				continue;
			}

			List<Double> values = convertStringToArray(strValues, Double.class);

			for (int i = 0; i < getters.size(); i++) {
				Method getter = getters.get(i);

				try {
					Map<String, Double> field = (Map<String, Double>) getter.invoke(natRhyOgeevrMonitoring);
					field.put(q, values.get(i));
				} catch (SecurityException | IllegalAccessException |
						IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
	}

//Nat_half_oloh

	public NatRhyOlohMonitoring getNatRhyOlohMonitoringByHalfOfYear(String year){
		NatRhyOlohMonitoring natRhyOlohMonitoring = monitoringMapper.getNatRhyOlohByYear(year);

		setHalfs(natRhyOlohMonitoring);

		return natRhyOlohMonitoring;
	}

	private void setHalfs(NatRhyOlohMonitoring natRhyOlohMonitoring) {
		String[] halfs = {"I полугодие", "II полугодие"};

		List<Method> getters = new ArrayList<>();

		try {
			for(PropertyDescriptor propertyDescriptor :
					Introspector.getBeanInfo(natRhyOlohMonitoring.getClass(), Object.class).getPropertyDescriptors()) {
				Method getter = propertyDescriptor.getReadMethod();
				String fullGetterName = getter.toString();
				String getterName = getter.toString().substring(fullGetterName.lastIndexOf(".") + 1);

				if (!getterName.equals("getYear()")) {
					getters.add(getter);
				}
			}
		} catch (IntrospectionException e1) {
			e1.printStackTrace();
		}

		for (String q : halfs) {
			String strValues = monitoringMapper.getNatRhyOlohValuesByYearAndHalf(natRhyOlohMonitoring.getYear(), q);

			// Если нету определнного полугодия
			if (strValues == null) {
				continue;
			}

			List<Double> values = convertStringToArray(strValues, Double.class);

			for (int i = 0; i < getters.size(); i++) {
				Method getter = getters.get(i);

				try {
					Map<String, Double> field = (Map<String, Double>) getter.invoke(natRhyOlohMonitoring);
					field.put(q, values.get(i));
				} catch (SecurityException | IllegalAccessException |
						IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
	}

//Nat_half_onvh

	public NatRhyOnvhMonitoring getNatRhyOnvhMonitoringByHalfOfYear(String year){
		NatRhyOnvhMonitoring natRhyOnvhMonitoring = monitoringMapper.getNatRhyOnvhByYear(year);

		setHalfs(natRhyOnvhMonitoring);

		return natRhyOnvhMonitoring;
	}

	private void setHalfs(NatRhyOnvhMonitoring natRhyOnvhMonitoring) {
		String[] halfs = {"I полугодие", "II полугодие"};

		List<Method> getters = new ArrayList<>();

		try {
			for(PropertyDescriptor propertyDescriptor :
					Introspector.getBeanInfo(natRhyOnvhMonitoring.getClass(), Object.class).getPropertyDescriptors()) {
				Method getter = propertyDescriptor.getReadMethod();
				String fullGetterName = getter.toString();
				String getterName = getter.toString().substring(fullGetterName.lastIndexOf(".") + 1);

				if (!getterName.equals("getYear()")) {
					getters.add(getter);
				}
			}
		} catch (IntrospectionException e1) {
			e1.printStackTrace();
		}

		for (String q : halfs) {
			String strValues = monitoringMapper.getNatRhyOnvhValuesByYearAndHalf(natRhyOnvhMonitoring.getYear(), q);

			// Если нету определнного полугодия
			if (strValues == null) {
				continue;
			}

			List<Double> values = convertStringToArray(strValues, Double.class);

			for (int i = 0; i < getters.size(); i++) {
				Method getter = getters.get(i);

				try {
					Map<String, Double> field = (Map<String, Double>) getter.invoke(natRhyOnvhMonitoring);
					field.put(q, values.get(i));
				} catch (SecurityException | IllegalAccessException |
						IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
	}

//Nat_half_opp

	public NatRhyOppMonitoring getNatRhyOppMonitoringByHalfOfYear(String year){
		NatRhyOppMonitoring natRhyOppMonitoring = monitoringMapper.getNatRhyOppByYear(year);

		setHalfs(natRhyOppMonitoring);

		return natRhyOppMonitoring;
	}

	private void setHalfs(NatRhyOppMonitoring natRhyOppMonitoring) {
		String[] halfs = {"I полугодие", "II полугодие"};

		List<Method> getters = new ArrayList<>();

		try {
			for(PropertyDescriptor propertyDescriptor :
					Introspector.getBeanInfo(natRhyOppMonitoring.getClass(), Object.class).getPropertyDescriptors()) {
				Method getter = propertyDescriptor.getReadMethod();
				String fullGetterName = getter.toString();
				String getterName = getter.toString().substring(fullGetterName.lastIndexOf(".") + 1);

				if (!getterName.equals("getYear()")) {
					getters.add(getter);
				}
			}
		} catch (IntrospectionException e1) {
			e1.printStackTrace();
		}

		for (String q : halfs) {
			String strValues = monitoringMapper.getNatRhyOppValuesByYearAndHalf(natRhyOppMonitoring.getYear(), q);

			// Если нету определнного полугодия
			if (strValues == null) {
				continue;
			}

			List<Double> values = convertStringToArray(strValues, Double.class);

			for (int i = 0; i < getters.size(); i++) {
				Method getter = getters.get(i);

				try {
					Map<String, Double> field = (Map<String, Double>) getter.invoke(natRhyOppMonitoring);
					field.put(q, values.get(i));
				} catch (SecurityException | IllegalAccessException |
						IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
	}

//only years

	public Map<String, String[]> getNatinYears(){
		Map<String, String[]> years = new HashMap<>();

		years.put("Года Природ ресурсы и регулирование природопользования по кварталам /Проведение общественных слушаний ", monitoringMapper.getQuarterNatOgeevrYears());
		years.put("Года Природ ресурсы и регулирование природопользования по кварталам /Выполнение плана природоохранных мероприятий, /Истребление волков в природе ", monitoringMapper.getQuarterNatOppYears());
		years.put("Года Природ ресурсы и регулирование природопользования по кварталам /Выдача лесорубочного и лесного билета ", monitoringMapper.getQuarterNatOlohYears());
		years.put("Года Природ ресурсы и регулирование природопользования по полугодию /Обращение физических и юридических лиц ", monitoringMapper.getHalfNatOapkrYears());
		years.put("Года Природ ресурсы и регулирование природопользования по полугодию /Выдача заключений гос экол экспертизы для объектов 2,3 и 4 категорий, /Выдача разрешений на эмиссии в окружающую среду для объектов 2,3 и 4 категорий, /Проведение общественных слушаний ", monitoringMapper.getHalfNatOgeevrYears());
		years.put("Года Природ ресурсы и регулирование природопользования по полугодию /Государственная регистрация договора долгосрочного леспоисполь на участках государственного лесного фонда ", monitoringMapper.getHalfNatOlohYears());
		years.put("Года Природ ресурсы и регулирование природопользования по полугодию /Заключение регистрация и хранение контрактов на разведку добычу общераспрастраненных полезных ископаемых, /Выдача письменного разрешения для получения права недропользования по разведке и добыче ОПИ ", monitoringMapper.getHalfNatOnvhYears());
		years.put("Года Природ ресурсы и регулирование природопользования по полугодию /Объем накопленных твердо-бытовых отходов, /Выполнение плана природоохранных мероприятий, /Истребление волков в природе ", monitoringMapper.getHalfNatOppYears());

		return years;
	}




//Molodejnaya

	public List<MolodejnayaPolitikaYearMonitoring> getMolodejnayaByYears(){
		return monitoringMapper.getMolodejnayaByYears();
	}

//MolodejP_Quart

	public MolodejPQMonitoring getMolodejPQMonitoringByQuatersOfYear(String year){
		MolodejPQMonitoring molodejPQMonitoring = monitoringMapper.getMolodejPQByYear(year);

		setQuarters(molodejPQMonitoring);

		return molodejPQMonitoring;
	}

	private void setQuarters(MolodejPQMonitoring molodejPQMonitoring) {
		String[] quarters = {"I квартал", "II квартал", "III квартал", "IV квартал"};

		List<Method> getters = new ArrayList<>();

		try {
			for(PropertyDescriptor propertyDescriptor :
					Introspector.getBeanInfo(molodejPQMonitoring.getClass(), Object.class).getPropertyDescriptors()) {
				Method getter = propertyDescriptor.getReadMethod();
				String fullGetterName = getter.toString();
				String getterName = getter.toString().substring(fullGetterName.lastIndexOf(".") + 1);

				if (!getterName.equals("getYear()")) {
					getters.add(getter);
				}
			}
		} catch (IntrospectionException e1) {
			e1.printStackTrace();
		}

		for (String q : quarters) {
			String strValues = monitoringMapper.getMolodejPQValuesByYearAndQuarter(molodejPQMonitoring.getYear(), q);

			// Если нету определнного квартала, то он пропускает
			if (strValues == null) {
				continue;
			}

			List<Double> values = convertStringToArray(strValues, Double.class);

			for (int i = 0; i < getters.size(); i++) {
				Method getter = getters.get(i);

				try {
					Map<String, Double> field = (Map<String, Double>) getter.invoke(molodejPQMonitoring);
					field.put(q, values.get(i));
				} catch (SecurityException | IllegalAccessException |
						IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
	}

//MolP_half

	public MolPhyMonitoring getMolPhyMonitoringByHalfOfYear(String year){
		MolPhyMonitoring molPhyMonitoring = monitoringMapper.getMolPhyByYear(year);

		setHalfs(molPhyMonitoring);

		return molPhyMonitoring;
	}

	private void setHalfs(MolPhyMonitoring molPhyMonitoring) {
		String[] halfs = {"I полугодие", "II полугодие"};

		List<Method> getters = new ArrayList<>();

		try {
			for(PropertyDescriptor propertyDescriptor :
					Introspector.getBeanInfo(molPhyMonitoring.getClass(), Object.class).getPropertyDescriptors()) {
				Method getter = propertyDescriptor.getReadMethod();
				String fullGetterName = getter.toString();
				String getterName = getter.toString().substring(fullGetterName.lastIndexOf(".") + 1);

				if (!getterName.equals("getYear()")) {
					getters.add(getter);
				}
			}
		} catch (IntrospectionException e1) {
			e1.printStackTrace();
		}

		for (String q : halfs) {
			String strValues = monitoringMapper.getMolPhyValuesByYearAndHalf(molPhyMonitoring.getYear(), q);

			// Если нету определнного полугодия
			if (strValues == null) {
				continue;
			}

			List<Double> values = convertStringToArray(strValues, Double.class);

			for (int i = 0; i < getters.size(); i++) {
				Method getter = getters.get(i);

				try {
					Map<String, Double> field = (Map<String, Double>) getter.invoke(molPhyMonitoring);
					field.put(q, values.get(i));
				} catch (SecurityException | IllegalAccessException |
						IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
	}









//Internal_P_Quart

	public InternalPQMonitoring getInternalPQMonitoringByQuatersOfYear(String year){
		InternalPQMonitoring internalPQMonitoring = monitoringMapper.getInternalPQByYear(year);

		setQuarters(internalPQMonitoring);

		return internalPQMonitoring;
	}

	private void setQuarters(InternalPQMonitoring internalPQMonitoring) {
		String[] quarters = {"I квартал", "II квартал", "III квартал", "IV квартал"};

		List<Method> getters = new ArrayList<>();

		try {
			for(PropertyDescriptor propertyDescriptor :
					Introspector.getBeanInfo(internalPQMonitoring.getClass(), Object.class).getPropertyDescriptors()) {
				Method getter = propertyDescriptor.getReadMethod();
				String fullGetterName = getter.toString();
				String getterName = getter.toString().substring(fullGetterName.lastIndexOf(".") + 1);

				if (!getterName.equals("getYear()")) {
					getters.add(getter);
				}
			}
		} catch (IntrospectionException e1) {
			e1.printStackTrace();
		}

		for (String q : quarters) {
			String strValues = monitoringMapper.getInternalPQValuesByYearAndQuarter(internalPQMonitoring.getYear(), q);

			// Если нету определнного квартала, то он пропускает
			if (strValues == null) {
				continue;
			}

			List<Double> values = convertStringToArray(strValues, Double.class);

			for (int i = 0; i < getters.size(); i++) {
				Method getter = getters.get(i);

				try {
					Map<String, Double> field = (Map<String, Double>) getter.invoke(internalPQMonitoring);
					field.put(q, values.get(i));
				} catch (SecurityException | IllegalAccessException |
						IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
	}

//Internal_P_Quart_hy

	public InternalPhyMonitoring getInternalPhyMonitoringByQuatersOfYear(String year){
		InternalPhyMonitoring internalPhyMonitoring = monitoringMapper.getInternalPhyByYear(year);

		setQuarters(internalPhyMonitoring);

		return internalPhyMonitoring;
	}

	private void setQuarters(InternalPhyMonitoring internalPhyMonitoring) {
		String[] quarters = {"I квартал", "II квартал", "III квартал", "IV квартал"};

		List<Method> getters = new ArrayList<>();

		try {
			for(PropertyDescriptor propertyDescriptor :
					Introspector.getBeanInfo(internalPhyMonitoring.getClass(), Object.class).getPropertyDescriptors()) {
				Method getter = propertyDescriptor.getReadMethod();
				String fullGetterName = getter.toString();
				String getterName = getter.toString().substring(fullGetterName.lastIndexOf(".") + 1);

				if (!getterName.equals("getYear()")) {
					getters.add(getter);
				}
			}
		} catch (IntrospectionException e1) {
			e1.printStackTrace();
		}

		for (String q : quarters) {
			String strValues = monitoringMapper.getInternalPhyValuesByYearAndQuarter(internalPhyMonitoring.getYear(), q);

			// Если нету определнного квартала, то он пропускает
			if (strValues == null) {
				continue;
			}

			List<Double> values = convertStringToArray(strValues, Double.class);

			for (int i = 0; i < getters.size(); i++) {
				Method getter = getters.get(i);

				try {
					Map<String, Double> field = (Map<String, Double>) getter.invoke(internalPhyMonitoring);
					field.put(q, values.get(i));
				} catch (SecurityException | IllegalAccessException |
						IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
	}

//only years

	public Map<String, String[]> getInterinYears(){
		Map<String, String[]> years = new HashMap<>();

		years.put("Года Внутрення политика по кварталам /послания Президента (полож тональность), /(отриц тональность), /аким области (полож тон), /(отриц тон), /Кол полож газет материалов, опубликов в СМИ ", monitoringMapper.getQuarterInternalPhyYears());
		years.put("Года Внутрення политика по кварталам /Перебои в обесп жителей питьевой водой теплом газом электроэнергией, /Уровень доверия населения институтам власти(акиму области), /Уровень поддержки населением курса гос.политики(Президенту Правительству), /Уровень осведом населения об основных направ послания Президента и ходе их реализ, /Количество поданных заявок на проведение митингов, /Карта очагов социаль напряжен по А области ", monitoringMapper.getQuarterInternalPQYears());

		return years;
	}





//LanguageDevYear

	public List<LanguagesDevelopmentYearMonitoring> getLangDevYearByYears(){
		return monitoringMapper.getLangDevYearByYears();
	}

//LangDev_Quart

	public LanguagesDevQMonitoring getLanguagesDevQMonitoringByQuatersOfYear(String year){
		LanguagesDevQMonitoring languagesDevQMonitoring = monitoringMapper.getLanguagesDevQByYear(year);

		setQuarters(languagesDevQMonitoring);

		return languagesDevQMonitoring;
	}

	private void setQuarters(LanguagesDevQMonitoring languagesDevQMonitoring) {
		String[] quarters = {"I квартал", "II квартал", "III квартал", "IV квартал"};

		List<Method> getters = new ArrayList<>();

		try {
			for(PropertyDescriptor propertyDescriptor :
					Introspector.getBeanInfo(languagesDevQMonitoring.getClass(), Object.class).getPropertyDescriptors()) {
				Method getter = propertyDescriptor.getReadMethod();
				String fullGetterName = getter.toString();
				String getterName = getter.toString().substring(fullGetterName.lastIndexOf(".") + 1);

				if (!getterName.equals("getYear()")) {
					getters.add(getter);
				}
			}
		} catch (IntrospectionException e1) {
			e1.printStackTrace();
		}

		for (String q : quarters) {
			String strValues = monitoringMapper.getLanguagesDevQValuesByYearAndQuarter(languagesDevQMonitoring.getYear(), q);

			// Если нету определнного квартала, то он пропускает
			if (strValues == null) {
				continue;
			}

			List<Double> values = convertStringToArray(strValues, Double.class);

			for (int i = 0; i < getters.size(); i++) {
				Method getter = getters.get(i);

				try {
					Map<String, Double> field = (Map<String, Double>) getter.invoke(languagesDevQMonitoring);
					field.put(q, values.get(i));
				} catch (SecurityException | IllegalAccessException |
						IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
	}

//only years

	public Map<String, String[]> getLanginYears(){
		Map<String, String[]> years = new HashMap<>();

		years.put("Года Развитие языков по кварталам ", monitoringMapper.getQuarterLanguagesDevQYears());

		return years;
	}





//Archive_hy

	public ArchiveHyMonitoring getArchivehyMonitoringByHalfOfYear(String year){
		ArchiveHyMonitoring archiveHyMonitoring = monitoringMapper.getArchivehyByYear(year);

		setHalfs(archiveHyMonitoring);

		return archiveHyMonitoring;
	}

	private void setHalfs(ArchiveHyMonitoring archiveHyMonitoring) {
		String[] halfs = {"I полугодие", "II полугодие"};

		List<Method> getters = new ArrayList<>();

		try {
			for(PropertyDescriptor propertyDescriptor :
					Introspector.getBeanInfo(archiveHyMonitoring.getClass(), Object.class).getPropertyDescriptors()) {
				Method getter = propertyDescriptor.getReadMethod();
				String fullGetterName = getter.toString();
				String getterName = getter.toString().substring(fullGetterName.lastIndexOf(".") + 1);

				if (!getterName.equals("getYear()")) {
					getters.add(getter);
				}
			}
		} catch (IntrospectionException e1) {
			e1.printStackTrace();
		}

		for (String q : halfs) {
			String strValues = monitoringMapper.getArchivehyValuesByYearAndHalf(archiveHyMonitoring.getYear(), q);

			// Если нету определнного полугодия
			if (strValues == null) {
				continue;
			}

			List<Double> values = convertStringToArray(strValues, Double.class);

			for (int i = 0; i < getters.size(); i++) {
				Method getter = getters.get(i);

				try {
					Map<String, Double> field = (Map<String, Double>) getter.invoke(archiveHyMonitoring);
					field.put(q, values.get(i));
				} catch (SecurityException | IllegalAccessException |
						IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
	}




//Kult_hy

	public KulturahyMonitoring getKulturahyMonitoringByHalfOfYear(String year){
		KulturahyMonitoring kulturahyMonitoring = monitoringMapper.getKulturahyByYear(year);

		setHalfs(kulturahyMonitoring);

		return kulturahyMonitoring;
	}

	private void setHalfs(KulturahyMonitoring kulturahyMonitoring) {
		String[] halfs = {"I полугодие", "II полугодие"};

		List<Method> getters = new ArrayList<>();

		try {
			for(PropertyDescriptor propertyDescriptor :
					Introspector.getBeanInfo(kulturahyMonitoring.getClass(), Object.class).getPropertyDescriptors()) {
				Method getter = propertyDescriptor.getReadMethod();
				String fullGetterName = getter.toString();
				String getterName = getter.toString().substring(fullGetterName.lastIndexOf(".") + 1);

				if (!getterName.equals("getYear()")) {
					getters.add(getter);
				}
			}
		} catch (IntrospectionException e1) {
			e1.printStackTrace();
		}

		for (String q : halfs) {
			String strValues = monitoringMapper.getKulturahyValuesByYearAndHalf(kulturahyMonitoring.getYear(), q);

			// Если нету определнного полугодия
			if (strValues == null) {
				continue;
			}

			List<Double> values = convertStringToArray(strValues, Double.class);

			for (int i = 0; i < getters.size(); i++) {
				Method getter = getters.get(i);

				try {
					Map<String, Double> field = (Map<String, Double>) getter.invoke(kulturahyMonitoring);
					field.put(q, values.get(i));
				} catch (SecurityException | IllegalAccessException |
						IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
	}


	public List<NatMonthOapikrMonitoring> getNatMonthOapikrMonthByYear(String year) {
		return monitoringMapper.getNatMonthOapikrMonthByYear(year);
	}

	public List<NatMonthOgeeivrMonitoring> getNatMonthOgeeivrMonthByYear(String year) {
		return monitoringMapper.getNatMonthOgeeivrMonthByYear(year);
	}

	public List<NatMonthOnivhMonitoring> getNatMonthOnivhMonthByYear(String year) {
		return monitoringMapper.getNatMonthOnivhMonthByYear(year);
	}



}
