package com.example.demo.service;

import com.example.demo.domain.ZdravKCatalogMonitoring;
import com.example.demo.domain.ZdravKPIMonitoring;
import com.example.demo.domain.ZdravKPITableMonitoring;
import com.example.demo.domain.ZdravKSpisokMonitoring;
import com.example.demo.repos.ZdravKPIMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ZdravKPIService {

    @Autowired
    ZdravKPIMapper zdravKPIMapper;

    public List<ZdravKPITableMonitoring> getZdravKPITableMonitoringByKind(String kindOfSport){
        return zdravKPIMapper.getZdravKPITableMonitoringByKind(kindOfSport);
    }

    public List<ZdravKCatalogMonitoring> getZdravKCatalogMonitoringByIndex(String index){
        return zdravKPIMapper.getZdravKCatalogMonitoringByIndex(index);
    }

    public List<ZdravKCatalogMonitoring> getZdrav2KCatalogMonitoringByIndex(){
        return zdravKPIMapper.getZdrav2KCatalogMonitoringByIndex();
    }

    public List<String> getMenuKCatalogMonitoringIndex(){
        return zdravKPIMapper.getMenuKCatalogMonitoringIndex();
    }

    public List<ZdravKSpisokMonitoring> getZdravKSpisokMonitoringByRaion(String raion){
        return zdravKPIMapper.getZdravKSpisokMonitoringByRaion(raion);
    }

    public List<ZdravKSpisokMonitoring> getZdrav2KSpisokMonitoringByRaion(){
        return zdravKPIMapper.getZdrav2KSpisokMonitoringByRaion();
    }

    public List<String> getMenuKSpisokMonitoringRaion(){
        return zdravKPIMapper.getMenuKSpisokMonitoringRaion();
    }

    public List<String> getMenuKSpisokMonitoringRD(){
        return zdravKPIMapper.getMenuKSpisokMonitoringRD();
    }

    public List<String> getMenuKSpisokMonitoringSnp(){
        return zdravKPIMapper.getMenuKSpisokMonitoringSnp();
    }

    public List<ZdravKPIMonitoring> getZdravKPI2MonitoringByRegion(String region){
        return zdravKPIMapper.getZdravKPI2MonitoringByRegion(region);
    }

    public List<ZdravKPIMonitoring> getZdravKPI3MonitoringByYear(String year){
        return zdravKPIMapper.getZdravKPI3MonitoringByYear(year);
    }

    public List<String> getMenuKPI2MonitoringRegions(){
        return zdravKPIMapper.getMenuKPI2MonitoringRegions();
    }

    public List<String> getMenuKPI2MonitoringYears(){
        return zdravKPIMapper.getMenuKPI2MonitoringYears();
    }

    public List<ZdravKSpisokMonitoring> getZdrav3KSpisokMonitoringByRaion(String raion, String ruralDistruct, String snp, String category){
        List<ZdravKSpisokMonitoring> zdravKSpisokMonitorings = zdravKPIMapper.getZdrav3KSpisokMonitoringByRaion(raion, ruralDistruct, snp, category);
        return zdravKSpisokMonitorings;
    }
}
