package com.example.demo.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.demo.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.repos.HealthcareMapper;

@Service
public class HealthcareService {
	
	@Autowired
	HealthcareMapper healthcareMapper;
	public List<Demography> getDemographicsByRegion(String region) {
		return healthcareMapper.getDemographicsByRegion(region);
	}
	
	public List<Demography> getDemographicsByYear(String year) {
		return healthcareMapper.getDemographicsByYear(year);
	}
	
	public List<Mortality> getMortalitiesByRegion(String region) {
		return healthcareMapper.getMortalitiesByRegion(region);
	}
	
	public List<Mortality> getMortalitiesByYear(String year) {
		return healthcareMapper.getMortalitiesByYear(year);
	}
	
	public List<Morbidity> getMorbiditiesByRegion(String region) {
		return healthcareMapper.getMorbiditiesByRegion(region);
	}
	
	public List<Morbidity> getMorbiditiesByYear(String year) {
		return healthcareMapper.getMorbiditiesByYear(year);
	}
	
	public List<OrganizationsInfo> getOrganizationsInfoByYear(String year) {
		return healthcareMapper.getOrganizationsInfoByYear(year);
	}
	
	public List<Personnel> getPersonnelByRegion(String region) {
		return healthcareMapper.getPersonnelByRegion(region);
	}
	
	public List<Personnel> getPersonnelByYear(String year) {
		return healthcareMapper.getPersonnelByYear(year);
	}

	public List<ZkpiMonitoring> getZkpiMonitoringByYearAndRegion(String year, String region) {
		return healthcareMapper.getZkpiMonitoringByYearAndRegion(year, region);
	}

	public List<Demography> getDemographyByYearAndRegion() {
		return healthcareMapper.getDemographyByYearAndRegion();
	}

	public List<Mortality> getMortalityByYearAndRegion(String year, String region) {
		return healthcareMapper.getMortalityByYearAndRegion(year, region);
	}

	public List<Morbidity> getMorbidityByYearAndRegion(String year, String region) {
		return healthcareMapper.getMorbidityByYearAndRegion(year, region);
	}

	public List<OrganizationsInfo> getOrganizationsInfoByYearAndRegion(String year, String region) {
		return healthcareMapper.getOrganizationsInfoByYearAndRegion(year, region);
	}

	public List<Personnel> getPersonnelByYearAndRegion(String year, String region) {
		return healthcareMapper.getPersonnelByYearAndRegion(year, region);
	}


}
