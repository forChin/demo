package com.example.demo.service;

import com.example.demo.domain.*;
import com.example.demo.repos.EduMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EduService {

    @Autowired
    EduMapper eduMapper;

    public List<EduddoMonitoring> getEduddoMonitoring(){
        return eduMapper.getEduddoMonitoring();
    }

    public List<EduCatalogDdoMonitoring> getEduCatalogDdoMonitoring(){
        return eduMapper.getEduCatalogDdoMonitoring();
    }

    public List<EduCatalogSchoolMonitoring> getEduCatalogSchoolMonitoring(){
        return eduMapper.getEduCatalogSchoolMonitoring();
    }

    public List<EduCollegeMonitoring> getEduCollegeMonitoring(){
        return eduMapper.getEduCollegeMonitoring();
    }

    public List<Edu2DdoMonitoring> getEdu2DdoMonitoring(){
        return eduMapper.getEdu2DdoMonitoring();
    }

    public List<EduDdoNeedMonitoring> getEduDdoNeedMonitoring(){
        return eduMapper.getEduDdoNeedMonitoring();
    }

    public List<EduNumbersMonitoring> getEduNumbersMonitoring(){
        return eduMapper.getEduNumbersMonitoring();
    }

    public List<EduQueueDdoMonitoring> getEduQueueDdoMonitoring(){
        return eduMapper.getEduQueueDdoMonitoring();
    }

    public List<EduSchoolMonitoring> getEduSchoolMonitoring(){
        return eduMapper.getEduSchoolMonitoring();
    }

    public List<EduSchoolNeedMonitoring> getEduSchoolNeedMonitoring(){
        return eduMapper.getEduSchoolNeedMonitoring();
    }




}
