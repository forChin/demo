package com.example.demo.domain;

import java.util.HashMap;
import java.util.Map;

public class KulturahyMonitoring {

    private String year;

    private Map<String, Double> attendanceCulturalObjectsPopulation;
    private Map<String, Double> eventsHeldTheatre;
    private Map<String, Double> clubEvents;
    private Map<String, Double> numberCulturalObjects;
    private Map<String, Double> objectsClub;
    private Map<String, Double> numberLibraryObjects;
    private Map<String, Double> numberMuseumObjects;
    private Map<String, Double> numberParkObjects;
    private Map<String, Double> numberEmergencyObjectsCulture;

    public KulturahyMonitoring(){
        this.attendanceCulturalObjectsPopulation = new HashMap<>();
        this.clubEvents = new HashMap<>();
        this.eventsHeldTheatre = new HashMap<>();
        this.numberCulturalObjects = new HashMap<>();
        this.numberEmergencyObjectsCulture = new HashMap<>();
        this.numberLibraryObjects = new HashMap<>();
        this.numberMuseumObjects = new HashMap<>();
        this.numberParkObjects = new HashMap<>();
        this.objectsClub = new HashMap<>();
    }

    public String getYear() {
        return year;
    }

    public Map<String, Double> getAttendanceCulturalObjectsPopulation() {
        return attendanceCulturalObjectsPopulation;
    }

    public Map<String, Double> getEventsHeldTheatre() {
        return eventsHeldTheatre;
    }

    public Map<String, Double> getClubEvents() {
        return clubEvents;
    }

    public Map<String, Double> getNumberCulturalObjects() {
        return numberCulturalObjects;
    }

    public Map<String, Double> getObjectsClub() {
        return objectsClub;
    }

    public Map<String, Double> getNumberLibraryObjects() {
        return numberLibraryObjects;
    }

    public Map<String, Double> getNumberMuseumObjects() {
        return numberMuseumObjects;
    }

    public Map<String, Double> getNumberParkObjects() {
        return numberParkObjects;
    }

    public Map<String, Double> getNumberEmergencyObjectsCulture() {
        return numberEmergencyObjectsCulture;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setAttendanceCulturalObjectsPopulation(Map<String, Double> attendanceCulturalObjectsPopulation) {
        this.attendanceCulturalObjectsPopulation = attendanceCulturalObjectsPopulation;
    }

    public void setEventsHeldTheatre(Map<String, Double> eventsHeldTheatre) {
        this.eventsHeldTheatre = eventsHeldTheatre;
    }

    public void setClubEvents(Map<String, Double> clubEvents) {
        this.clubEvents = clubEvents;
    }

    public void setNumberCulturalObjects(Map<String, Double> numberCulturalObjects) {
        this.numberCulturalObjects = numberCulturalObjects;
    }

    public void setObjectsClub(Map<String, Double> objectsClub) {
        this.objectsClub = objectsClub;
    }

    public void setNumberLibraryObjects(Map<String, Double> numberLibraryObjects) {
        this.numberLibraryObjects = numberLibraryObjects;
    }

    public void setNumberMuseumObjects(Map<String, Double> numberMuseumObjects) {
        this.numberMuseumObjects = numberMuseumObjects;
    }

    public void setNumberParkObjects(Map<String, Double> numberParkObjects) {
        this.numberParkObjects = numberParkObjects;
    }

    public void setNumberEmergencyObjectsCulture(Map<String, Double> numberEmergencyObjectsCulture) {
        this.numberEmergencyObjectsCulture = numberEmergencyObjectsCulture;
    }
}
