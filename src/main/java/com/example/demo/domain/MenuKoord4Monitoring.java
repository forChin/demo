package com.example.demo.domain;

public class MenuKoord4Monitoring {

    //sed_y_koordin_region

    private Double totalBenefitsOne;
    private Double totalBenefitsTwo;
    private Double totalBenefitsWithOneDisabled;
    private Double totalBenefitsWithSixDisabled;
    private Double invalidsFirstAmountTotal;
    private Double invalidsSecondAmountTotal;
    private Double invalidsThirdAmountAmountTotal;
    private Double totalInvalidsChildren;
    private Double totalamountMaternityBenefits;
    private Double totalamountMaternityBenefitsA;
    private Double totalInvalidsFirstGroup;
    private Double totalInvalidsSecondGroup;
    private Double totalInvalidsThirdGroup;
    private Double totalLargeMothersAllowance;
    private Double totalAllowanceRaisingDisabledchild;
    private Double totalDisabledCareAllowance;
    private Double totalLargeFamilyAllowance4Children;
    private Double totalLargeFamilyAllowance5Children;
    private Double totalLargeFamilyAllowance6Children;
    private Double totalLargeFamilyAllowance7Children;
    private Double totalBurialGrants;
    private Double totalBurialAllowancesParticipantsVov;
    private Double totalInvalidsParticipantsVov;
    private Double totalPersonsEquatedDisabledVov;
    private Double totalPersonsEquatedParticipants;
    private Double totalVdovamPogibshikh;
    private Double totalAbc;
    private Double totalBenefitsFrontWorkers;
    private Double totalAbcd;
    private Double totalIndividualsAwardedTitles;
    private Double totalHeroesSocialistLabor;
    private Double totalVictimsPoliticalRepression;
    private Double totalPersonalPensioners;

    public Double getTotalBenefitsOne() {
        return totalBenefitsOne;
    }

    public Double getTotalBenefitsTwo() {
        return totalBenefitsTwo;
    }

    public Double getTotalBenefitsWithOneDisabled() {
        return totalBenefitsWithOneDisabled;
    }

    public Double getTotalBenefitsWithSixDisabled() {
        return totalBenefitsWithSixDisabled;
    }

    public Double getInvalidsFirstAmountTotal() {
        return invalidsFirstAmountTotal;
    }

    public Double getInvalidsSecondAmountTotal() {
        return invalidsSecondAmountTotal;
    }

    public Double getInvalidsThirdAmountAmountTotal() {
        return invalidsThirdAmountAmountTotal;
    }

    public Double getTotalInvalidsChildren() {
        return totalInvalidsChildren;
    }

    public Double getTotalamountMaternityBenefits() {
        return totalamountMaternityBenefits;
    }

    public Double getTotalamountMaternityBenefitsA() {
        return totalamountMaternityBenefitsA;
    }

    public Double getTotalInvalidsFirstGroup() {
        return totalInvalidsFirstGroup;
    }

    public Double getTotalInvalidsSecondGroup() {
        return totalInvalidsSecondGroup;
    }

    public Double getTotalInvalidsThirdGroup() {
        return totalInvalidsThirdGroup;
    }

    public Double getTotalLargeMothersAllowance() {
        return totalLargeMothersAllowance;
    }

    public Double getTotalAllowanceRaisingDisabledchild() {
        return totalAllowanceRaisingDisabledchild;
    }

    public Double getTotalDisabledCareAllowance() {
        return totalDisabledCareAllowance;
    }

    public Double getTotalLargeFamilyAllowance4Children() {
        return totalLargeFamilyAllowance4Children;
    }

    public Double getTotalLargeFamilyAllowance5Children() {
        return totalLargeFamilyAllowance5Children;
    }

    public Double getTotalLargeFamilyAllowance6Children() {
        return totalLargeFamilyAllowance6Children;
    }

    public Double getTotalLargeFamilyAllowance7Children() {
        return totalLargeFamilyAllowance7Children;
    }

    public Double getTotalBurialGrants() {
        return totalBurialGrants;
    }

    public Double getTotalBurialAllowancesParticipantsVov() {
        return totalBurialAllowancesParticipantsVov;
    }

    public Double getTotalInvalidsParticipantsVov() {
        return totalInvalidsParticipantsVov;
    }

    public Double getTotalPersonsEquatedDisabledVov() {
        return totalPersonsEquatedDisabledVov;
    }

    public Double getTotalPersonsEquatedParticipants() {
        return totalPersonsEquatedParticipants;
    }

    public Double getTotalVdovamPogibshikh() {
        return totalVdovamPogibshikh;
    }

    public Double getTotalAbc() {
        return totalAbc;
    }

    public Double getTotalBenefitsFrontWorkers() {
        return totalBenefitsFrontWorkers;
    }

    public Double getTotalAbcd() {
        return totalAbcd;
    }

    public Double getTotalIndividualsAwardedTitles() {
        return totalIndividualsAwardedTitles;
    }

    public Double getTotalHeroesSocialistLabor() {
        return totalHeroesSocialistLabor;
    }

    public Double getTotalVictimsPoliticalRepression() {
        return totalVictimsPoliticalRepression;
    }

    public Double getTotalPersonalPensioners() {
        return totalPersonalPensioners;
    }

    public void setTotalBenefitsOne(Double totalBenefitsOne) {
        this.totalBenefitsOne = totalBenefitsOne;
    }

    public void setTotalBenefitsTwo(Double totalBenefitsTwo) {
        this.totalBenefitsTwo = totalBenefitsTwo;
    }

    public void setTotalBenefitsWithOneDisabled(Double totalBenefitsWithOneDisabled) {
        this.totalBenefitsWithOneDisabled = totalBenefitsWithOneDisabled;
    }

    public void setTotalBenefitsWithSixDisabled(Double totalBenefitsWithSixDisabled) {
        this.totalBenefitsWithSixDisabled = totalBenefitsWithSixDisabled;
    }

    public void setInvalidsFirstAmountTotal(Double invalidsFirstAmountTotal) {
        this.invalidsFirstAmountTotal = invalidsFirstAmountTotal;
    }

    public void setInvalidsSecondAmountTotal(Double invalidsSecondAmountTotal) {
        this.invalidsSecondAmountTotal = invalidsSecondAmountTotal;
    }

    public void setInvalidsThirdAmountAmountTotal(Double invalidsThirdAmountAmountTotal) {
        this.invalidsThirdAmountAmountTotal = invalidsThirdAmountAmountTotal;
    }

    public void setTotalInvalidsChildren(Double totalInvalidsChildren) {
        this.totalInvalidsChildren = totalInvalidsChildren;
    }

    public void setTotalamountMaternityBenefits(Double totalamountMaternityBenefits) {
        this.totalamountMaternityBenefits = totalamountMaternityBenefits;
    }

    public void setTotalamountMaternityBenefitsA(Double totalamountMaternityBenefitsA) {
        this.totalamountMaternityBenefitsA = totalamountMaternityBenefitsA;
    }

    public void setTotalInvalidsFirstGroup(Double totalInvalidsFirstGroup) {
        this.totalInvalidsFirstGroup = totalInvalidsFirstGroup;
    }

    public void setTotalInvalidsSecondGroup(Double totalInvalidsSecondGroup) {
        this.totalInvalidsSecondGroup = totalInvalidsSecondGroup;
    }

    public void setTotalInvalidsThirdGroup(Double totalInvalidsThirdGroup) {
        this.totalInvalidsThirdGroup = totalInvalidsThirdGroup;
    }

    public void setTotalLargeMothersAllowance(Double totalLargeMothersAllowance) {
        this.totalLargeMothersAllowance = totalLargeMothersAllowance;
    }

    public void setTotalAllowanceRaisingDisabledchild(Double totalAllowanceRaisingDisabledchild) {
        this.totalAllowanceRaisingDisabledchild = totalAllowanceRaisingDisabledchild;
    }

    public void setTotalDisabledCareAllowance(Double totalDisabledCareAllowance) {
        this.totalDisabledCareAllowance = totalDisabledCareAllowance;
    }

    public void setTotalLargeFamilyAllowance4Children(Double totalLargeFamilyAllowance4Children) {
        this.totalLargeFamilyAllowance4Children = totalLargeFamilyAllowance4Children;
    }

    public void setTotalLargeFamilyAllowance5Children(Double totalLargeFamilyAllowance5Children) {
        this.totalLargeFamilyAllowance5Children = totalLargeFamilyAllowance5Children;
    }

    public void setTotalLargeFamilyAllowance6Children(Double totalLargeFamilyAllowance6Children) {
        this.totalLargeFamilyAllowance6Children = totalLargeFamilyAllowance6Children;
    }

    public void setTotalLargeFamilyAllowance7Children(Double totalLargeFamilyAllowance7Children) {
        this.totalLargeFamilyAllowance7Children = totalLargeFamilyAllowance7Children;
    }

    public void setTotalBurialGrants(Double totalBurialGrants) {
        this.totalBurialGrants = totalBurialGrants;
    }

    public void setTotalBurialAllowancesParticipantsVov(Double totalBurialAllowancesParticipantsVov) {
        this.totalBurialAllowancesParticipantsVov = totalBurialAllowancesParticipantsVov;
    }

    public void setTotalInvalidsParticipantsVov(Double totalInvalidsParticipantsVov) {
        this.totalInvalidsParticipantsVov = totalInvalidsParticipantsVov;
    }

    public void setTotalPersonsEquatedDisabledVov(Double totalPersonsEquatedDisabledVov) {
        this.totalPersonsEquatedDisabledVov = totalPersonsEquatedDisabledVov;
    }

    public void setTotalPersonsEquatedParticipants(Double totalPersonsEquatedParticipants) {
        this.totalPersonsEquatedParticipants = totalPersonsEquatedParticipants;
    }

    public void setTotalVdovamPogibshikh(Double totalVdovamPogibshikh) {
        this.totalVdovamPogibshikh = totalVdovamPogibshikh;
    }

    public void setTotalAbc(Double totalAbc) {
        this.totalAbc = totalAbc;
    }

    public void setTotalBenefitsFrontWorkers(Double totalBenefitsFrontWorkers) {
        this.totalBenefitsFrontWorkers = totalBenefitsFrontWorkers;
    }

    public void setTotalAbcd(Double totalAbcd) {
        this.totalAbcd = totalAbcd;
    }

    public void setTotalIndividualsAwardedTitles(Double totalIndividualsAwardedTitles) {
        this.totalIndividualsAwardedTitles = totalIndividualsAwardedTitles;
    }

    public void setTotalHeroesSocialistLabor(Double totalHeroesSocialistLabor) {
        this.totalHeroesSocialistLabor = totalHeroesSocialistLabor;
    }

    public void setTotalVictimsPoliticalRepression(Double totalVictimsPoliticalRepression) {
        this.totalVictimsPoliticalRepression = totalVictimsPoliticalRepression;
    }

    public void setTotalPersonalPensioners(Double totalPersonalPensioners) {
        this.totalPersonalPensioners = totalPersonalPensioners;
    }
}
