package com.example.demo.domain;

public class EduddoMonitoring {

// new edu catalog college points

    private String index;
    private Double shareInfluence;
    private String point;

    public String getIndex() {
        return index;
    }

    public Double getShareInfluence() {
        return shareInfluence;
    }

    public String getPoint() {
        return point;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public void setShareInfluence(Double shareInfluence) {
        this.shareInfluence = shareInfluence;
    }

    public void setPoint(String point) {
        this.point = point;
    }
}
