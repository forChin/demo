package com.example.demo.domain;

public class BlokSocialMonitoring {
    // blok social

    private String raion;
    private String ruralDistrict;
    private String ruralSettlement;
    private Integer populationConcentration;
    private String educationSchools;
    private String preschoolInstitution;
    private String health;
    private Integer employment;
    private Integer poverty;
    private Double itog;
    private String year;

    public String getRaion() {
        return raion;
    }

    public String getRuralDistrict() {
        return ruralDistrict;
    }

    public String getRuralSettlement() {
        return ruralSettlement;
    }

    public Integer getPopulationConcentration() {
        return populationConcentration;
    }

    public String getEducationSchools() {
        return educationSchools;
    }

    public String getPreschoolInstitution() {
        return preschoolInstitution;
    }

    public String getHealth() {
        return health;
    }

    public Integer getEmployment() {
        return employment;
    }

    public Integer getPoverty() {
        return poverty;
    }

    public Double getItog() {
        return itog;
    }

    public String getYear() {
        return year;
    }

    public void setRaion(String raion) {
        this.raion = raion;
    }

    public void setRuralDistrict(String ruralDistrict) {
        this.ruralDistrict = ruralDistrict;
    }

    public void setRuralSettlement(String ruralSettlement) {
        this.ruralSettlement = ruralSettlement;
    }

    public void setPopulationConcentration(Integer populationConcentration) {
        this.populationConcentration = populationConcentration;
    }

    public void setEducationSchools(String educationSchools) {
        this.educationSchools = educationSchools;
    }

    public void setPreschoolInstitution(String preschoolInstitution) {
        this.preschoolInstitution = preschoolInstitution;
    }

    public void setHealth(String health) {
        this.health = health;
    }

    public void setEmployment(Integer employment) {
        this.employment = employment;
    }

    public void setPoverty(Integer poverty) {
        this.poverty = poverty;
    }

    public void setItog(Double itog) {
        this.itog = itog;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
