package com.example.demo.domain;

public class MenuJiv2kshKMonitoring {

    private String year;            //ush_stat_jivotnovodstvo
    private String area;
    private String region;
    private Double outletKidsFarms;

    public String getYear() {
        return year;
    }

    public String getArea() {
        return area;
    }

    public String getRegion() {
        return region;
    }

    public Double getOutletKidsFarms() {
        return outletKidsFarms;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setOutletKidsFarms(Double outletKidsFarms) {
        this.outletKidsFarms = outletKidsFarms;
    }
}
