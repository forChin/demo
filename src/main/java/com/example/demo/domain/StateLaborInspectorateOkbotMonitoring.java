package com.example.demo.domain;

public class StateLaborInspectorateOkbotMonitoring {

    private String year;
    private String month;

    private Double numberOfAccidents; // o_number_accidents | Количество несчастных случаев
	private Double numberOfTeamAccidents; // o_number_accidents_team | Количество несчастных случаев (Из них групповых)
	private Double numberOfVictims; // o_number_victims | Количество пострадавших
	private Double numberOfVictimsDead; // o_number_victims_dead | Количество пострадавших (Из них погибших)
    private Double administrativeFinesImposed; // o_administrative_fines_imposed | Наложено администратиынфх штрафов

    public StateLaborInspectorateOkbotMonitoring(){

    }

    public String getYear() {
        return year;
    }

    public String getMonth() {
        return month;
    }

    public Double getNumberOfAccidents() {
        return numberOfAccidents;
    }

    public Double getNumberOfTeamAccidents() {
        return numberOfTeamAccidents;
    }

    public Double getNumberOfVictims() {
        return numberOfVictims;
    }

    public Double getNumberOfVictimsDead() {
        return numberOfVictimsDead;
    }

    public Double getAdministrativeFinesImposed() {
        return administrativeFinesImposed;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public void setNumberOfAccidents(Double numberOfAccidents) {
        this.numberOfAccidents = numberOfAccidents;
    }

    public void setNumberOfTeamAccidents(Double numberOfTeamAccidents) {
        this.numberOfTeamAccidents = numberOfTeamAccidents;
    }

    public void setNumberOfVictims(Double numberOfVictims) {
        this.numberOfVictims = numberOfVictims;
    }

    public void setNumberOfVictimsDead(Double numberOfVictimsDead) {
        this.numberOfVictimsDead = numberOfVictimsDead;
    }

    public void setAdministrativeFinesImposed(Double administrativeFinesImposed) {
        this.administrativeFinesImposed = administrativeFinesImposed;
    }

}
