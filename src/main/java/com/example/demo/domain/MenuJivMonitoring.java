package com.example.demo.domain;

public class MenuJivMonitoring {

    private String year;            //ush_stat_jivotnovodstvo
    private String area;
    private String region;
    private Double numberLivestockPoultry;
    private Double grossOutputProductsAff;
    private Double volumeGrossoutputAnimalhusbandry;
    private Double ifoVolumeAnimalhusbandry;
    private Double issuePeasantFamfarms;
    private Double ifoIssuePeasantFamfarms;
    private Double releaseAgriculturalEnterprises;
    private Double ifoReleaseAgriculturalEnterprises;
    private Double grossOutputprivateHouseholds;
    private Double ifoGrossOutputprivateHouseholds;
    private Double cattleFellDied;
    private Double horsesFellDied;
    private Double sheepGoatsFellDied;
    private Double pigsFellDied;
    private Double milkProduction;
    private Double slaughterWeightB;
    private Double woolProduction;
    private Double eggProduction;
    private Double numberCattleFarms;
    private Double numberCowsFarms;
    private Double numberSheepgoatsFarms;
    private Double numberPigsFarms;
    private Double numberHorsesFarms;
    private Double numberCamelsFarms;
    private Double numberBirdsFarms;
    private Double calvesYieldFarms;
    private Double outputLambsFarms;
    private Double outletKidsFarms;
    private Double outputFoalsFarms;
    private Double outputCamelcoltsFarms;
    private Double numberCattleAgricultural;
    private Double numberCowsAgricultural;
    private Double numberSheepgoatsAgricultural;
    private Double numberPigsAgricultural;
    private Double numberHorsesAgricultural;
    private Double numberCamelsAgricultural;
    private Double numberBirdsAgricultural;
    private Double calvesYieldAgricultural;
    private Double outputLambsAgricultural;
    private Double outletKidsAgricultural;
    private Double outputFoalsAgricultural;
    private Double outputCamelscoltsAgricultural;
    private Double numberCattleHouseholds;
    private Double numberCowsHouseholds;
    private Double numberSheepgoatsHouseholds;
    private Double numberPigsHouseholds;
    private Double numberHorsesHouseholds;
    private Double numberCamelsHouseholds;
    private Double numberBirdHouseholdssB;
    private Double calvesYieldHouseholds;
    private Double outputLambsHouseholds;
    private Double outletKidsHouseholds;
    private Double outputFoalsHouseholds;
    private Double outputCamelscoltsHouseholds;


    public String getYear() {
        return year;
    }

    public String getArea() {
        return area;
    }

    public String getRegion() {
        return region;
    }

    public Double getNumberLivestockPoultry() {
        return numberLivestockPoultry;
    }

    public Double getGrossOutputProductsAff() {
        return grossOutputProductsAff;
    }

    public Double getVolumeGrossoutputAnimalhusbandry() {
        return volumeGrossoutputAnimalhusbandry;
    }

    public Double getIfoVolumeAnimalhusbandry() {
        return ifoVolumeAnimalhusbandry;
    }

    public Double getIssuePeasantFamfarms() {
        return issuePeasantFamfarms;
    }

    public Double getIfoIssuePeasantFamfarms() {
        return ifoIssuePeasantFamfarms;
    }

    public Double getReleaseAgriculturalEnterprises() {
        return releaseAgriculturalEnterprises;
    }

    public Double getIfoReleaseAgriculturalEnterprises() {
        return ifoReleaseAgriculturalEnterprises;
    }

    public Double getGrossOutputprivateHouseholds() {
        return grossOutputprivateHouseholds;
    }

    public Double getIfoGrossOutputprivateHouseholds() {
        return ifoGrossOutputprivateHouseholds;
    }

    public Double getCattleFellDied() {
        return cattleFellDied;
    }

    public Double getHorsesFellDied() {
        return horsesFellDied;
    }

    public Double getSheepGoatsFellDied() {
        return sheepGoatsFellDied;
    }

    public Double getPigsFellDied() {
        return pigsFellDied;
    }

    public Double getMilkProduction() {
        return milkProduction;
    }

    public Double getSlaughterWeightB() {
        return slaughterWeightB;
    }

    public Double getWoolProduction() {
        return woolProduction;
    }

    public Double getEggProduction() {
        return eggProduction;
    }

    public Double getNumberCattleFarms() {
        return numberCattleFarms;
    }

    public Double getNumberCowsFarms() {
        return numberCowsFarms;
    }

    public Double getNumberSheepgoatsFarms() {
        return numberSheepgoatsFarms;
    }

    public Double getNumberPigsFarms() {
        return numberPigsFarms;
    }

    public Double getNumberHorsesFarms() {
        return numberHorsesFarms;
    }

    public Double getNumberCamelsFarms() {
        return numberCamelsFarms;
    }

    public Double getNumberBirdsFarms() {
        return numberBirdsFarms;
    }

    public Double getCalvesYieldFarms() {
        return calvesYieldFarms;
    }

    public Double getOutputLambsFarms() {
        return outputLambsFarms;
    }

    public Double getOutletKidsFarms() {
        return outletKidsFarms;
    }

    public Double getOutputFoalsFarms() {
        return outputFoalsFarms;
    }

    public Double getOutputCamelcoltsFarms() {
        return outputCamelcoltsFarms;
    }

    public Double getNumberCattleAgricultural() {
        return numberCattleAgricultural;
    }

    public Double getNumberCowsAgricultural() {
        return numberCowsAgricultural;
    }

    public Double getNumberSheepgoatsAgricultural() {
        return numberSheepgoatsAgricultural;
    }

    public Double getNumberPigsAgricultural() {
        return numberPigsAgricultural;
    }

    public Double getNumberHorsesAgricultural() {
        return numberHorsesAgricultural;
    }

    public Double getNumberCamelsAgricultural() {
        return numberCamelsAgricultural;
    }

    public Double getNumberBirdsAgricultural() {
        return numberBirdsAgricultural;
    }

    public Double getCalvesYieldAgricultural() {
        return calvesYieldAgricultural;
    }

    public Double getOutputLambsAgricultural() {
        return outputLambsAgricultural;
    }

    public Double getOutletKidsAgricultural() {
        return outletKidsAgricultural;
    }

    public Double getOutputFoalsAgricultural() {
        return outputFoalsAgricultural;
    }

    public Double getOutputCamelscoltsAgricultural() {
        return outputCamelscoltsAgricultural;
    }

    public Double getNumberCattleHouseholds() {
        return numberCattleHouseholds;
    }

    public Double getNumberCowsHouseholds() {
        return numberCowsHouseholds;
    }

    public Double getNumberSheepgoatsHouseholds() {
        return numberSheepgoatsHouseholds;
    }

    public Double getNumberPigsHouseholds() {
        return numberPigsHouseholds;
    }

    public Double getNumberHorsesHouseholds() {
        return numberHorsesHouseholds;
    }

    public Double getNumberCamelsHouseholds() {
        return numberCamelsHouseholds;
    }

    public Double getNumberBirdHouseholdssB() {
        return numberBirdHouseholdssB;
    }

    public Double getCalvesYieldHouseholds() {
        return calvesYieldHouseholds;
    }

    public Double getOutputLambsHouseholds() {
        return outputLambsHouseholds;
    }

    public Double getOutletKidsHouseholds() {
        return outletKidsHouseholds;
    }

    public Double getOutputFoalsHouseholds() {
        return outputFoalsHouseholds;
    }

    public Double getOutputCamelscoltsHouseholds() {
        return outputCamelscoltsHouseholds;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setNumberLivestockPoultry(Double numberLivestockPoultry) {
        this.numberLivestockPoultry = numberLivestockPoultry;
    }

    public void setGrossOutputProductsAff(Double grossOutputProductsAff) {
        this.grossOutputProductsAff = grossOutputProductsAff;
    }

    public void setVolumeGrossoutputAnimalhusbandry(Double volumeGrossoutputAnimalhusbandry) {
        this.volumeGrossoutputAnimalhusbandry = volumeGrossoutputAnimalhusbandry;
    }

    public void setIfoVolumeAnimalhusbandry(Double ifoVolumeAnimalhusbandry) {
        this.ifoVolumeAnimalhusbandry = ifoVolumeAnimalhusbandry;
    }

    public void setIssuePeasantFamfarms(Double issuePeasantFamfarms) {
        this.issuePeasantFamfarms = issuePeasantFamfarms;
    }

    public void setIfoIssuePeasantFamfarms(Double ifoIssuePeasantFamfarms) {
        this.ifoIssuePeasantFamfarms = ifoIssuePeasantFamfarms;
    }

    public void setReleaseAgriculturalEnterprises(Double releaseAgriculturalEnterprises) {
        this.releaseAgriculturalEnterprises = releaseAgriculturalEnterprises;
    }

    public void setIfoReleaseAgriculturalEnterprises(Double ifoReleaseAgriculturalEnterprises) {
        this.ifoReleaseAgriculturalEnterprises = ifoReleaseAgriculturalEnterprises;
    }

    public void setGrossOutputprivateHouseholds(Double grossOutputprivateHouseholds) {
        this.grossOutputprivateHouseholds = grossOutputprivateHouseholds;
    }

    public void setIfoGrossOutputprivateHouseholds(Double ifoGrossOutputprivateHouseholds) {
        this.ifoGrossOutputprivateHouseholds = ifoGrossOutputprivateHouseholds;
    }

    public void setCattleFellDied(Double cattleFellDied) {
        this.cattleFellDied = cattleFellDied;
    }

    public void setHorsesFellDied(Double horsesFellDied) {
        this.horsesFellDied = horsesFellDied;
    }

    public void setSheepGoatsFellDied(Double sheepGoatsFellDied) {
        this.sheepGoatsFellDied = sheepGoatsFellDied;
    }

    public void setPigsFellDied(Double pigsFellDied) {
        this.pigsFellDied = pigsFellDied;
    }

    public void setMilkProduction(Double milkProduction) {
        this.milkProduction = milkProduction;
    }

    public void setSlaughterWeightB(Double slaughterWeightB) {
        this.slaughterWeightB = slaughterWeightB;
    }

    public void setWoolProduction(Double woolProduction) {
        this.woolProduction = woolProduction;
    }

    public void setEggProduction(Double eggProduction) {
        this.eggProduction = eggProduction;
    }

    public void setNumberCattleFarms(Double numberCattleFarms) {
        this.numberCattleFarms = numberCattleFarms;
    }

    public void setNumberCowsFarms(Double numberCowsFarms) {
        this.numberCowsFarms = numberCowsFarms;
    }

    public void setNumberSheepgoatsFarms(Double numberSheepgoatsFarms) {
        this.numberSheepgoatsFarms = numberSheepgoatsFarms;
    }

    public void setNumberPigsFarms(Double numberPigsFarms) {
        this.numberPigsFarms = numberPigsFarms;
    }

    public void setNumberHorsesFarms(Double numberHorsesFarms) {
        this.numberHorsesFarms = numberHorsesFarms;
    }

    public void setNumberCamelsFarms(Double numberCamelsFarms) {
        this.numberCamelsFarms = numberCamelsFarms;
    }

    public void setNumberBirdsFarms(Double numberBirdsFarms) {
        this.numberBirdsFarms = numberBirdsFarms;
    }

    public void setCalvesYieldFarms(Double calvesYieldFarms) {
        this.calvesYieldFarms = calvesYieldFarms;
    }

    public void setOutputLambsFarms(Double outputLambsFarms) {
        this.outputLambsFarms = outputLambsFarms;
    }

    public void setOutletKidsFarms(Double outletKidsFarms) {
        this.outletKidsFarms = outletKidsFarms;
    }

    public void setOutputFoalsFarms(Double outputFoalsFarms) {
        this.outputFoalsFarms = outputFoalsFarms;
    }

    public void setOutputCamelcoltsFarms(Double outputCamelcoltsFarms) {
        this.outputCamelcoltsFarms = outputCamelcoltsFarms;
    }

    public void setNumberCattleAgricultural(Double numberCattleAgricultural) {
        this.numberCattleAgricultural = numberCattleAgricultural;
    }

    public void setNumberCowsAgricultural(Double numberCowsAgricultural) {
        this.numberCowsAgricultural = numberCowsAgricultural;
    }

    public void setNumberSheepgoatsAgricultural(Double numberSheepgoatsAgricultural) {
        this.numberSheepgoatsAgricultural = numberSheepgoatsAgricultural;
    }

    public void setNumberPigsAgricultural(Double numberPigsAgricultural) {
        this.numberPigsAgricultural = numberPigsAgricultural;
    }

    public void setNumberHorsesAgricultural(Double numberHorsesAgricultural) {
        this.numberHorsesAgricultural = numberHorsesAgricultural;
    }

    public void setNumberCamelsAgricultural(Double numberCamelsAgricultural) {
        this.numberCamelsAgricultural = numberCamelsAgricultural;
    }

    public void setNumberBirdsAgricultural(Double numberBirdsAgricultural) {
        this.numberBirdsAgricultural = numberBirdsAgricultural;
    }

    public void setCalvesYieldAgricultural(Double calvesYieldAgricultural) {
        this.calvesYieldAgricultural = calvesYieldAgricultural;
    }

    public void setOutputLambsAgricultural(Double outputLambsAgricultural) {
        this.outputLambsAgricultural = outputLambsAgricultural;
    }

    public void setOutletKidsAgricultural(Double outletKidsAgricultural) {
        this.outletKidsAgricultural = outletKidsAgricultural;
    }

    public void setOutputFoalsAgricultural(Double outputFoalsAgricultural) {
        this.outputFoalsAgricultural = outputFoalsAgricultural;
    }

    public void setOutputCamelscoltsAgricultural(Double outputCamelscoltsAgricultural) {
        this.outputCamelscoltsAgricultural = outputCamelscoltsAgricultural;
    }

    public void setNumberCattleHouseholds(Double numberCattleHouseholds) {
        this.numberCattleHouseholds = numberCattleHouseholds;
    }

    public void setNumberCowsHouseholds(Double numberCowsHouseholds) {
        this.numberCowsHouseholds = numberCowsHouseholds;
    }

    public void setNumberSheepgoatsHouseholds(Double numberSheepgoatsHouseholds) {
        this.numberSheepgoatsHouseholds = numberSheepgoatsHouseholds;
    }

    public void setNumberPigsHouseholds(Double numberPigsHouseholds) {
        this.numberPigsHouseholds = numberPigsHouseholds;
    }

    public void setNumberHorsesHouseholds(Double numberHorsesHouseholds) {
        this.numberHorsesHouseholds = numberHorsesHouseholds;
    }

    public void setNumberCamelsHouseholds(Double numberCamelsHouseholds) {
        this.numberCamelsHouseholds = numberCamelsHouseholds;
    }

    public void setNumberBirdHouseholdssB(Double numberBirdHouseholdssB) {
        this.numberBirdHouseholdssB = numberBirdHouseholdssB;
    }

    public void setCalvesYieldHouseholds(Double calvesYieldHouseholds) {
        this.calvesYieldHouseholds = calvesYieldHouseholds;
    }

    public void setOutputLambsHouseholds(Double outputLambsHouseholds) {
        this.outputLambsHouseholds = outputLambsHouseholds;
    }

    public void setOutletKidsHouseholds(Double outletKidsHouseholds) {
        this.outletKidsHouseholds = outletKidsHouseholds;
    }

    public void setOutputFoalsHouseholds(Double outputFoalsHouseholds) {
        this.outputFoalsHouseholds = outputFoalsHouseholds;
    }

    public void setOutputCamelscoltsHouseholds(Double outputCamelscoltsHouseholds) {
        this.outputCamelscoltsHouseholds = outputCamelscoltsHouseholds;
    }
}
