package com.example.demo.domain;

public class OperMPrMonitoring {
    //oper_mon_pravonarusheniya

    private String year;
    private String month;
    private String period;
    private String dateEvent;
    private String region;
    private String unitMisconducts;
    private String totalMisdemeanors;
    private Integer administrativeLegalviolations;
    private Integer civilLawviolations;
    private Integer disciplinaryOffences;
    private String unitCrime;
    private Integer totalOffences;
    private Integer heavy;
    private Integer especiallyGrave;
    private Integer murder;
    private Integer heavyEspeciallyheavy;
    private Integer dtp;
    private Integer committedMinors;

    public String getYear() {
        return year;
    }

    public String getMonth() {
        return month;
    }

    public String getPeriod() {
        return period;
    }

    public String getDateEvent() {
        return dateEvent;
    }

    public String getRegion() {
        return region;
    }

    public String getUnitMisconducts() {
        return unitMisconducts;
    }

    public String getTotalMisdemeanors() {
        return totalMisdemeanors;
    }

    public Integer getAdministrativeLegalviolations() {
        return administrativeLegalviolations;
    }

    public Integer getCivilLawviolations() {
        return civilLawviolations;
    }

    public Integer getDisciplinaryOffences() {
        return disciplinaryOffences;
    }

    public String getUnitCrime() {
        return unitCrime;
    }

    public Integer getTotalOffences() {
        return totalOffences;
    }

    public Integer getHeavy() {
        return heavy;
    }

    public Integer getEspeciallyGrave() {
        return especiallyGrave;
    }

    public Integer getMurder() {
        return murder;
    }

    public Integer getHeavyEspeciallyheavy() {
        return heavyEspeciallyheavy;
    }

    public Integer getDtp() {
        return dtp;
    }

    public Integer getCommittedMinors() {
        return committedMinors;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public void setDateEvent(String dateEvent) {
        this.dateEvent = dateEvent;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setUnitMisconducts(String unitMisconducts) {
        this.unitMisconducts = unitMisconducts;
    }

    public void setTotalMisdemeanors(String totalMisdemeanors) {
        this.totalMisdemeanors = totalMisdemeanors;
    }

    public void setAdministrativeLegalviolations(Integer administrativeLegalviolations) {
        this.administrativeLegalviolations = administrativeLegalviolations;
    }

    public void setCivilLawviolations(Integer civilLawviolations) {
        this.civilLawviolations = civilLawviolations;
    }

    public void setDisciplinaryOffences(Integer disciplinaryOffences) {
        this.disciplinaryOffences = disciplinaryOffences;
    }

    public void setUnitCrime(String unitCrime) {
        this.unitCrime = unitCrime;
    }

    public void setTotalOffences(Integer totalOffences) {
        this.totalOffences = totalOffences;
    }

    public void setHeavy(Integer heavy) {
        this.heavy = heavy;
    }

    public void setEspeciallyGrave(Integer especiallyGrave) {
        this.especiallyGrave = especiallyGrave;
    }

    public void setMurder(Integer murder) {
        this.murder = murder;
    }

    public void setHeavyEspeciallyheavy(Integer heavyEspeciallyheavy) {
        this.heavyEspeciallyheavy = heavyEspeciallyheavy;
    }

    public void setDtp(Integer dtp) {
        this.dtp = dtp;
    }

    public void setCommittedMinors(Integer committedMinors) {
        this.committedMinors = committedMinors;
    }
}
