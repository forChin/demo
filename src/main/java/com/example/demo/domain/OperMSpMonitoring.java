package com.example.demo.domain;

public class OperMSpMonitoring {
    //oper_mon_skor_pom

    private String year;
    private String month;
    private String dateEvent;
    private String periodcity;
    private String region;
    private Integer numberCallsAmbulanceStation;
    private Integer ofThemAdults;
    private Integer ofThemChildren;
    private Integer ofthemTakenHospital;
    private Integer ofthemPlacedHospital;
    private Integer ofthemDtp;
    private Integer oftheseIndustrialInjuries;
    private Integer oftheseDomesticInjuries;
    private Integer ofthemBurns;
    private Integer numberAmbulances;
    private Integer numberAmbulanceCrewsSsmp;

    public String getYear() {
        return year;
    }

    public String getMonth() {
        return month;
    }

    public String getDateEvent() {
        return dateEvent;
    }

    public String getPeriodcity() {
        return periodcity;
    }

    public String getRegion() {
        return region;
    }

    public Integer getNumberCallsAmbulanceStation() {
        return numberCallsAmbulanceStation;
    }

    public Integer getOfThemAdults() {
        return ofThemAdults;
    }

    public Integer getOfThemChildren() {
        return ofThemChildren;
    }

    public Integer getOfthemTakenHospital() {
        return ofthemTakenHospital;
    }

    public Integer getOfthemPlacedHospital() {
        return ofthemPlacedHospital;
    }

    public Integer getOfthemDtp() {
        return ofthemDtp;
    }

    public Integer getOftheseIndustrialInjuries() {
        return oftheseIndustrialInjuries;
    }

    public Integer getOftheseDomesticInjuries() {
        return oftheseDomesticInjuries;
    }

    public Integer getOfthemBurns() {
        return ofthemBurns;
    }

    public Integer getNumberAmbulances() {
        return numberAmbulances;
    }

    public Integer getNumberAmbulanceCrewsSsmp() {
        return numberAmbulanceCrewsSsmp;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public void setDateEvent(String dateEvent) {
        this.dateEvent = dateEvent;
    }

    public void setPeriodcity(String periodcity) {
        this.periodcity = periodcity;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setNumberCallsAmbulanceStation(Integer numberCallsAmbulanceStation) {
        this.numberCallsAmbulanceStation = numberCallsAmbulanceStation;
    }

    public void setOfThemAdults(Integer ofThemAdults) {
        this.ofThemAdults = ofThemAdults;
    }

    public void setOfThemChildren(Integer ofThemChildren) {
        this.ofThemChildren = ofThemChildren;
    }

    public void setOfthemTakenHospital(Integer ofthemTakenHospital) {
        this.ofthemTakenHospital = ofthemTakenHospital;
    }

    public void setOfthemPlacedHospital(Integer ofthemPlacedHospital) {
        this.ofthemPlacedHospital = ofthemPlacedHospital;
    }

    public void setOfthemDtp(Integer ofthemDtp) {
        this.ofthemDtp = ofthemDtp;
    }

    public void setOftheseIndustrialInjuries(Integer oftheseIndustrialInjuries) {
        this.oftheseIndustrialInjuries = oftheseIndustrialInjuries;
    }

    public void setOftheseDomesticInjuries(Integer oftheseDomesticInjuries) {
        this.oftheseDomesticInjuries = oftheseDomesticInjuries;
    }

    public void setOfthemBurns(Integer ofthemBurns) {
        this.ofthemBurns = ofthemBurns;
    }

    public void setNumberAmbulances(Integer numberAmbulances) {
        this.numberAmbulances = numberAmbulances;
    }

    public void setNumberAmbulanceCrewsSsmp(Integer numberAmbulanceCrewsSsmp) {
        this.numberAmbulanceCrewsSsmp = numberAmbulanceCrewsSsmp;
    }
}
