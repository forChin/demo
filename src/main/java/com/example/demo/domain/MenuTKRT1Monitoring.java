package com.example.demo.domain;

import java.util.HashMap;
import java.util.Map;

public class MenuTKRT1Monitoring {

    private String year;
    private String region;

    private Map<String, Double> name = new HashMap<>();

    public MenuTKRT1Monitoring(){

    }

    public String getYear() {
        return year;
    }

    public String getRegion() {
        return region;
    }

    public Map<String, Double> getName() {
        return name;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setName(Map<String, Double> name) {
        this.name = name;
    }
}
