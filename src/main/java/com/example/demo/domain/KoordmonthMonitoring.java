package com.example.demo.domain;

import java.util.HashMap;
import java.util.Map;

public class KoordmonthMonitoring {

    private String year;
    private Map<String, Double> numberPopulation = new HashMap<>();
    private Map<String, Double> numberJobsCreated = new HashMap<>();

    public KoordmonthMonitoring(){

    }

    public String getYear() {
        return year;
    }

    public Map<String, Double> getNumberPopulation() {
        return numberPopulation;
    }

    public Map<String, Double> getNumberJobsCreated() {
        return numberJobsCreated;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setNumberPopulation(Map<String, Double> numberPopulation) {
        this.numberPopulation = numberPopulation;
    }

    public void setNumberJobsCreated(Map<String, Double> numberJobsCreated) {
        this.numberJobsCreated = numberJobsCreated;
    }

}
