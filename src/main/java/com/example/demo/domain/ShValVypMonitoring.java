package com.example.demo.domain;

public class ShValVypMonitoring {
    //sh_valovyi_vypusk

    private String year;
    private String raion;
    private String name;
    private String pokazatel;
    private String index;

    public String getYear() {
        return year;
    }

    public String getRaion() {
        return raion;
    }

    public String getName() {
        return name;
    }

    public String getPokazatel() {
        return pokazatel;
    }

    public String getIndex() {
        return index;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setRaion(String raion) {
        this.raion = raion;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPokazatel(String pokazatel) {
        this.pokazatel = pokazatel;
    }

    public void setIndex(String index) {
        this.index = index;
    }


}
