package com.example.demo.domain;

public class BCulturePointsMonitoring {
    // new culture points

    private String index;
    private Double maxPoint;

    public String getIndex() {
        return index;
    }

    public Double getMaxPoint() {
        return maxPoint;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public void setMaxPoint(Double maxPoint) {
        this.maxPoint = maxPoint;
    }
}
