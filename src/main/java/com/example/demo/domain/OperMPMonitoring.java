package com.example.demo.domain;

public class OperMPMonitoring {
    //mon_prest

    private String year;
    private String period;
    private String month;
    private String numberErdr;
    private String dateEvent;
    private String region;
    private String registrationAuthority;
    private String descriptionOffense;
    private String qualification;
    private String dateStart;
    private String time;

    public String getYear() {
        return year;
    }

    public String getPeriod() {
        return period;
    }

    public String getMonth() {
        return month;
    }

    public String getNumberErdr() {
        return numberErdr;
    }

    public String getDateEvent() {
        return dateEvent;
    }

    public String getRegion() {
        return region;
    }

    public String getRegistrationAuthority() {
        return registrationAuthority;
    }

    public String getDescriptionOffense() {
        return descriptionOffense;
    }

    public String getQualification() {
        return qualification;
    }

    public String getDateStart() {
        return dateStart;
    }

    public String getTime() {
        return time;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public void setNumberErdr(String numberErdr) {
        this.numberErdr = numberErdr;
    }

    public void setDateEvent(String dateEvent) {
        this.dateEvent = dateEvent;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setRegistrationAuthority(String registrationAuthority) {
        this.registrationAuthority = registrationAuthority;
    }

    public void setDescriptionOffense(String descriptionOffense) {
        this.descriptionOffense = descriptionOffense;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
