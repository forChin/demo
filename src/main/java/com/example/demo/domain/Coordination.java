package com.example.demo.domain;

public class Coordination {

	private String year;
	private String area;
	
	private String region; // o_region
	private Integer populationNumber; // o_number_population | Численность населения, чел
	private Integer countLabour; // Рабочая сила, чел
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public Integer getPopulationNumber() {
		return populationNumber;
	}
	public void setPopulationNumber(Integer populationNumber) {
		this.populationNumber = populationNumber;
	}
	public Integer getCountLabour() {
		return countLabour;
	}
	public void setCountLabour(Integer countLabour) {
		this.countLabour = countLabour;
	}
	
	
}

