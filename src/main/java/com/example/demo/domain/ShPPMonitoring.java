package com.example.demo.domain;

public class ShPPMonitoring {
    //sh_posevnaya_pl

    private String year;
    private String raion;
    private String name;
    private String kategory;
    private String pokazatel;

    public String getYear() {
        return year;
    }

    public String getRaion() {
        return raion;
    }

    public String getName() {
        return name;
    }

    public String getKategory() {
        return kategory;
    }

    public String getPokazatel() {
        return pokazatel;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setRaion(String raion) {
        this.raion = raion;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setKategory(String kategory) {
        this.kategory = kategory;
    }

    public void setPokazatel(String pokazatel) {
        this.pokazatel = pokazatel;
    }
}
