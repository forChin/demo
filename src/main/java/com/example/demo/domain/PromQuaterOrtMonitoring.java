package com.example.demo.domain;

import java.util.HashMap;
import java.util.Map;

public class PromQuaterOrtMonitoring {

    private String year;

    private Map<String, Double> scopeTourismServices;
    private Map<String, Double> visitorsInboundTourism;
    private Map<String, Double> domesticTourismVisitors;

    public PromQuaterOrtMonitoring(){
        this.scopeTourismServices = new HashMap<>();
        this.visitorsInboundTourism = new HashMap<>();
        this.domesticTourismVisitors = new HashMap<>();
    }

    public String getYear() {
        return year;
    }

    public Map<String, Double> getScopeTourismServices() {
        return scopeTourismServices;
    }

    public Map<String, Double> getVisitorsInboundTourism() {
        return visitorsInboundTourism;
    }

    public Map<String, Double> getDomesticTourismVisitors() {
        return domesticTourismVisitors;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setScopeTourismServices(Map<String, Double> scopeTourismServices) {
        this.scopeTourismServices = scopeTourismServices;
    }

    public void setVisitorsInboundTourism(Map<String, Double> visitorsInboundTourism) {
        this.visitorsInboundTourism = visitorsInboundTourism;
    }

    public void setDomesticTourismVisitors(Map<String, Double> domesticTourismVisitors) {
        this.domesticTourismVisitors = domesticTourismVisitors;
    }


}
