package com.example.demo.domain;

public class ZdravKPIMonitoring {

//zdrav_kpi

    private String year;
    //private String area;
    private String region;
    private Double fertility;
    private Double overallMortalityRate;
    private Double naturalPopulationGrowth;
    private Double lifeExpectancy;
    private Double maternalMortality;
    private Double infantMortality;
    private Double mortalityFromTuberculosis;
    private Double mortalityFromOfThe;
    private Double deathFromCancer;
    private Double mortalityFromInjuries;
    private Double mortalityHeartDisease;
    private Double totalMorbidityPopulation;
    private Double lncidenceOfTuberculosis;
    private Double incidenceOfCancer;
    private Double morbidityMbd;
    private Double morbidityMbdsu;
    private Double morbidityFromInjuries;
    private Double morbidityBloodCirculation;
    private Double morbidityArterialHypertension;
    private Double morbidityCoronaryHeartDisease;
    private Double morbidityAcuteMyocardialInfarction;
    private Double prevalenceOfHiv;
    private Double numberProvidingInpatientCare;
    private Double numberOfHospitalBeds;
    private Double provisionHospitalBeds;
    private Double numberResidentsAho;
    private Double numberDhbMonSystem;
    private Double totalNumberBdsTna;
    private Double numberOrganizationsPatientPolyclinicHelp;
    private Double numberVisitDoctors;
    private Double numberOoHbdh;
    private Double numberDoctors;
    private Double provisionPopulationDoctors;
    private Double numberMedicalStaffPositions;
    private Double numberMedicalPositionsHeld;
    private Double numberNurses;
    private Double provisionMedicalPersonnel;
    private Double staffingMedicalPersonnel;

    public String getYear() {
        return year;
    }

//    public String getArea() {
//        return area;
//    }

    public String getRegion() {
        return region;
    }

    public Double getFertility() {
        return fertility;
    }

    public Double getOverallMortalityRate() {
        return overallMortalityRate;
    }

    public Double getNaturalPopulationGrowth() {
        return naturalPopulationGrowth;
    }

    public Double getLifeExpectancy() {
        return lifeExpectancy;
    }

    public Double getMaternalMortality() {
        return maternalMortality;
    }

    public Double getInfantMortality() {
        return infantMortality;
    }

    public Double getMortalityFromTuberculosis() {
        return mortalityFromTuberculosis;
    }

    public Double getMortalityFromOfThe() {
        return mortalityFromOfThe;
    }

    public Double getDeathFromCancer() {
        return deathFromCancer;
    }

    public Double getMortalityFromInjuries() {
        return mortalityFromInjuries;
    }

    public Double getMortalityHeartDisease() {
        return mortalityHeartDisease;
    }

    public Double getTotalMorbidityPopulation() {
        return totalMorbidityPopulation;
    }

    public Double getLncidenceOfTuberculosis() {
        return lncidenceOfTuberculosis;
    }

    public Double getIncidenceOfCancer() {
        return incidenceOfCancer;
    }

    public Double getMorbidityMbd() {
        return morbidityMbd;
    }

    public Double getMorbidityMbdsu() {
        return morbidityMbdsu;
    }

    public Double getMorbidityFromInjuries() {
        return morbidityFromInjuries;
    }

    public Double getMorbidityBloodCirculation() {
        return morbidityBloodCirculation;
    }

    public Double getMorbidityArterialHypertension() {
        return morbidityArterialHypertension;
    }

    public Double getMorbidityCoronaryHeartDisease() {
        return morbidityCoronaryHeartDisease;
    }

    public Double getMorbidityAcuteMyocardialInfarction() {
        return morbidityAcuteMyocardialInfarction;
    }

    public Double getPrevalenceOfHiv() {
        return prevalenceOfHiv;
    }

    public Double getNumberProvidingInpatientCare() {
        return numberProvidingInpatientCare;
    }

    public Double getNumberOfHospitalBeds() {
        return numberOfHospitalBeds;
    }

    public Double getProvisionHospitalBeds() {
        return provisionHospitalBeds;
    }

    public Double getNumberResidentsAho() {
        return numberResidentsAho;
    }

    public Double getNumberDhbMonSystem() {
        return numberDhbMonSystem;
    }

    public Double getTotalNumberBdsTna() {
        return totalNumberBdsTna;
    }

    public Double getNumberOrganizationsPatientPolyclinicHelp() {
        return numberOrganizationsPatientPolyclinicHelp;
    }

    public Double getNumberVisitDoctors() {
        return numberVisitDoctors;
    }

    public Double getNumberOoHbdh() {
        return numberOoHbdh;
    }

    public Double getNumberDoctors() {
        return numberDoctors;
    }

    public Double getProvisionPopulationDoctors() {
        return provisionPopulationDoctors;
    }

    public Double getNumberMedicalStaffPositions() {
        return numberMedicalStaffPositions;
    }

    public Double getNumberMedicalPositionsHeld() {
        return numberMedicalPositionsHeld;
    }

    public Double getNumberNurses() {
        return numberNurses;
    }

    public Double getProvisionMedicalPersonnel() {
        return provisionMedicalPersonnel;
    }

    public Double getStaffingMedicalPersonnel() {
        return staffingMedicalPersonnel;
    }

    public void setYear(String year) {
        this.year = year;
    }

//    public void setArea(String area) {
//        this.area = area;
//    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setFertility(Double fertility) {
        this.fertility = fertility;
    }

    public void setOverallMortalityRate(Double overallMortalityRate) {
        this.overallMortalityRate = overallMortalityRate;
    }

    public void setNaturalPopulationGrowth(Double naturalPopulationGrowth) {
        this.naturalPopulationGrowth = naturalPopulationGrowth;
    }

    public void setLifeExpectancy(Double lifeExpectancy) {
        this.lifeExpectancy = lifeExpectancy;
    }

    public void setMaternalMortality(Double maternalMortality) {
        this.maternalMortality = maternalMortality;
    }

    public void setInfantMortality(Double infantMortality) {
        this.infantMortality = infantMortality;
    }

    public void setMortalityFromTuberculosis(Double mortalityFromTuberculosis) {
        this.mortalityFromTuberculosis = mortalityFromTuberculosis;
    }

    public void setMortalityFromOfThe(Double mortalityFromOfThe) {
        this.mortalityFromOfThe = mortalityFromOfThe;
    }

    public void setDeathFromCancer(Double deathFromCancer) {
        this.deathFromCancer = deathFromCancer;
    }

    public void setMortalityFromInjuries(Double mortalityFromInjuries) {
        this.mortalityFromInjuries = mortalityFromInjuries;
    }

    public void setMortalityHeartDisease(Double mortalityHeartDisease) {
        this.mortalityHeartDisease = mortalityHeartDisease;
    }

    public void setTotalMorbidityPopulation(Double totalMorbidityPopulation) {
        this.totalMorbidityPopulation = totalMorbidityPopulation;
    }

    public void setLncidenceOfTuberculosis(Double lncidenceOfTuberculosis) {
        this.lncidenceOfTuberculosis = lncidenceOfTuberculosis;
    }

    public void setIncidenceOfCancer(Double incidenceOfCancer) {
        this.incidenceOfCancer = incidenceOfCancer;
    }

    public void setMorbidityMbd(Double morbidityMbd) {
        this.morbidityMbd = morbidityMbd;
    }

    public void setMorbidityMbdsu(Double morbidityMbdsu) {
        this.morbidityMbdsu = morbidityMbdsu;
    }

    public void setMorbidityFromInjuries(Double morbidityFromInjuries) {
        this.morbidityFromInjuries = morbidityFromInjuries;
    }

    public void setMorbidityBloodCirculation(Double morbidityBloodCirculation) {
        this.morbidityBloodCirculation = morbidityBloodCirculation;
    }

    public void setMorbidityArterialHypertension(Double morbidityArterialHypertension) {
        this.morbidityArterialHypertension = morbidityArterialHypertension;
    }

    public void setMorbidityCoronaryHeartDisease(Double morbidityCoronaryHeartDisease) {
        this.morbidityCoronaryHeartDisease = morbidityCoronaryHeartDisease;
    }

    public void setMorbidityAcuteMyocardialInfarction(Double morbidityAcuteMyocardialInfarction) {
        this.morbidityAcuteMyocardialInfarction = morbidityAcuteMyocardialInfarction;
    }

    public void setPrevalenceOfHiv(Double prevalenceOfHiv) {
        this.prevalenceOfHiv = prevalenceOfHiv;
    }

    public void setNumberProvidingInpatientCare(Double numberProvidingInpatientCare) {
        this.numberProvidingInpatientCare = numberProvidingInpatientCare;
    }

    public void setNumberOfHospitalBeds(Double numberOfHospitalBeds) {
        this.numberOfHospitalBeds = numberOfHospitalBeds;
    }

    public void setProvisionHospitalBeds(Double provisionHospitalBeds) {
        this.provisionHospitalBeds = provisionHospitalBeds;
    }

    public void setNumberResidentsAho(Double numberResidentsAho) {
        this.numberResidentsAho = numberResidentsAho;
    }

    public void setNumberDhbMonSystem(Double numberDhbMonSystem) {
        this.numberDhbMonSystem = numberDhbMonSystem;
    }

    public void setTotalNumberBdsTna(Double totalNumberBdsTna) {
        this.totalNumberBdsTna = totalNumberBdsTna;
    }

    public void setNumberOrganizationsPatientPolyclinicHelp(Double numberOrganizationsPatientPolyclinicHelp) {
        this.numberOrganizationsPatientPolyclinicHelp = numberOrganizationsPatientPolyclinicHelp;
    }

    public void setNumberVisitDoctors(Double numberVisitDoctors) {
        this.numberVisitDoctors = numberVisitDoctors;
    }

    public void setNumberOoHbdh(Double numberOoHbdh) {
        this.numberOoHbdh = numberOoHbdh;
    }

    public void setNumberDoctors(Double numberDoctors) {
        this.numberDoctors = numberDoctors;
    }

    public void setProvisionPopulationDoctors(Double provisionPopulationDoctors) {
        this.provisionPopulationDoctors = provisionPopulationDoctors;
    }

    public void setNumberMedicalStaffPositions(Double numberMedicalStaffPositions) {
        this.numberMedicalStaffPositions = numberMedicalStaffPositions;
    }

    public void setNumberMedicalPositionsHeld(Double numberMedicalPositionsHeld) {
        this.numberMedicalPositionsHeld = numberMedicalPositionsHeld;
    }

    public void setNumberNurses(Double numberNurses) {
        this.numberNurses = numberNurses;
    }

    public void setProvisionMedicalPersonnel(Double provisionMedicalPersonnel) {
        this.provisionMedicalPersonnel = provisionMedicalPersonnel;
    }

    public void setStaffingMedicalPersonnel(Double staffingMedicalPersonnel) {
        this.staffingMedicalPersonnel = staffingMedicalPersonnel;
    }
}
