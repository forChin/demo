package com.example.demo.domain;

public class MenuEduCollege {
    private String area;        //edu_college
    private String region;
    private String year;
    private String nameTipo;
    private Integer countTeachers;
    private Integer countAcceptedStudents;
    private Integer countGraduates;
    private Integer countReleasedStudents;
    private Integer countDesignCapacity;
    private Integer countCategoryC;
    private Integer countCategoryA;
    private Integer countCategoryB;
    private Integer countMagistrate;
    private Integer countDoctorEngineering;
    private Integer countCategoryD;
    private Integer countEducationHigher;
    private Integer countEducationColleges;
    private Integer countGeneralSecondaryEducation;

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getNameTipo() {
        return nameTipo;
    }

    public void setNameTipo(String nameTipo) {
        this.nameTipo = nameTipo;
    }

    public Integer getCountTeachers() {
        return countTeachers;
    }

    public void setCountTeachers(Integer countTeachers) {
        this.countTeachers = countTeachers;
    }

    public Integer getCountAcceptedStudents() {
        return countAcceptedStudents;
    }

    public void setCountAcceptedStudents(Integer countAcceptedStudents) {
        this.countAcceptedStudents = countAcceptedStudents;
    }

    public Integer getCountGraduates() {
        return countGraduates;
    }

    public void setCountGraduates(Integer countGraduates) {
        this.countGraduates = countGraduates;
    }

    public Integer getCountReleasedStudents() {
        return countReleasedStudents;
    }

    public void setCountReleasedStudents(Integer countReleasedStudents) {
        this.countReleasedStudents = countReleasedStudents;
    }

    public Integer getCountDesignCapacity() {
        return countDesignCapacity;
    }

    public void setCountDesignCapacity(Integer countDesignCapacity) {
        this.countDesignCapacity = countDesignCapacity;
    }

    public Integer getCountCategoryC() {
        return countCategoryC;
    }

    public void setCountCategoryC(Integer countCategoryC) {
        this.countCategoryC = countCategoryC;
    }

    public Integer getCountCategoryA() {
        return countCategoryA;
    }

    public void setCountCategoryA(Integer countCategoryA) {
        this.countCategoryA = countCategoryA;
    }

    public Integer getCountCategoryB() {
        return countCategoryB;
    }

    public void setCountCategoryB(Integer countCategoryB) {
        this.countCategoryB = countCategoryB;
    }

    public Integer getCountMagistrate() {
        return countMagistrate;
    }

    public void setCountMagistrate(Integer countMagistrate) {
        this.countMagistrate = countMagistrate;
    }

    public Integer getCountDoctorEngineering() {
        return countDoctorEngineering;
    }

    public void setCountDoctorEngineering(Integer countDoctorEngineering) {
        this.countDoctorEngineering = countDoctorEngineering;
    }

    public Integer getCountCategoryD() {
        return countCategoryD;
    }

    public void setCountCategoryD(Integer countCategoryD) {
        this.countCategoryD = countCategoryD;
    }

    public Integer getCountEducationHigher() {
        return countEducationHigher;
    }

    public void setCountEducationHigher(Integer countEducationHigher) {
        this.countEducationHigher = countEducationHigher;
    }

    public Integer getCountEducationColleges() {
        return countEducationColleges;
    }

    public void setCountEducationColleges(Integer countEducationColleges) {
        this.countEducationColleges = countEducationColleges;
    }

    public Integer getCountGeneralSecondaryEducation() {
        return countGeneralSecondaryEducation;
    }

    public void setCountGeneralSecondaryEducation(Integer countGeneralSecondaryEducation) {
        this.countGeneralSecondaryEducation = countGeneralSecondaryEducation;
    }
}
