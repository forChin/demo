package com.example.demo.domain;

public class ShProizvProdMonitoring {
    // sh_proizv_prod

    private String year;
    private String raion;
    private String name;
    private String pokazatel;

    public String getYear() {
        return year;
    }

    public String getRaion() {
        return raion;
    }

    public String getName() {
        return name;
    }

    public String getPokazatel() {
        return pokazatel;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setRaion(String raion) {
        this.raion = raion;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPokazatel(String pokazatel) {
        this.pokazatel = pokazatel;
    }
}
