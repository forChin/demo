package com.example.demo.domain;

public class IdmakMonitoring {
    //id_makat

    private Integer number;
    private String snp;

    public Integer getNumber() {
        return number;
    }

    public String getSnp() {
        return snp;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public void setSnp(String snp) {
        this.snp = snp;
    }
}
