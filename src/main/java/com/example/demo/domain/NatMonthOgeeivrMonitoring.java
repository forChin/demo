package com.example.demo.domain;

public class NatMonthOgeeivrMonitoring {

// sed_m_uprirp_ogeevr

    private String year;
    private String month;
    private Integer examination;
    private Integer issuancePermitsEmissions;


    public String getYear() {
        return year;
    }

    public String getMonth() {
        return month;
    }

    public Integer getExamination() {
        return examination;
    }

    public Integer getIssuancePermitsEmissions() {
        return issuancePermitsEmissions;
    }


    public void setYear(String year) {
        this.year = year;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public void setExamination(Integer examination) {
        this.examination = examination;
    }

    public void setIssuancePermitsEmissions(Integer issuancePermitsEmissions) {
        this.issuancePermitsEmissions = issuancePermitsEmissions;
    }
}
