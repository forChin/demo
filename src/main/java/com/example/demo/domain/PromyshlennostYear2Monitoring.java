package com.example.demo.domain;

public class PromyshlennostYear2Monitoring {

    private String year;
    private Double volumeInnovativeProducts;
    private Double levelActivityEnterprises;

    public PromyshlennostYear2Monitoring(){

    }

    public String getYear() {
        return year;
    }

    public Double getVolumeInnovativeProducts() {
        return volumeInnovativeProducts;
    }

    public Double getLevelActivityEnterprises() {
        return levelActivityEnterprises;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setVolumeInnovativeProducts(Double volumeInnovativeProducts) {
        this.volumeInnovativeProducts = volumeInnovativeProducts;
    }

    public void setLevelActivityEnterprises(Double levelActivityEnterprises) {
        this.levelActivityEnterprises = levelActivityEnterprises;

    }
}
