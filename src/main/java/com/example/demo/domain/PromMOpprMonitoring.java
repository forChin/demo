package com.example.demo.domain;

import java.util.HashMap;
import java.util.Map;

public class PromMOpprMonitoring {

    private String year;

    private Map<String, Double> registeredEntities = new HashMap<>();
    private Map<String, Double> actor = new HashMap<>();

    public PromMOpprMonitoring(){

    }

    public String getYear() {
        return year;
    }

    public Map<String, Double> getRegisteredEntities() {
        return registeredEntities;
    }

    public Map<String, Double> getActor() {
        return actor;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setRegisteredEntities(Map<String, Double> registeredEntities) {
        this.registeredEntities = registeredEntities;
    }

    public void setActor(Map<String, Double> actor) {
        this.actor = actor;
    }
}
