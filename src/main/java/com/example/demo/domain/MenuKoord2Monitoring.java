package com.example.demo.domain;

public class MenuKoord2Monitoring {

    //sed_y_koordin_region

    private Double numberOne;
    private Double numberTwo;
    private Double numberWithOneDisabled;
    private Double numberWithSixDisabled;
    private Double invalidsTheFirst;
    private Double invalidsSecond;
    private Double invalidsThird;
    private Double invalidsChildren;
    private Double childbirthAllowance;
    private Double childbirthAllowanceA;
    private Double invalidsFirstGroup;
    private Double invalidsSecondGroup;
    private Double invalidsThirdGroup;
    private Double largeMothersAllowance;
    private Double allowanceRaisingDisabledchild;
    private Double disabledCareAllowance;
    private Double largeFamilyAllowance4Children;
    private Double largeFamilyAllowance5Children;
    private Double largeFamilyAllowance6Children;
    private Double largeFamilyAllowance7Children;
    private Double burialGrants;
    private Double burialAllowancesParticipantsVov;
    private Double invalidsParticipantsVov;
    private Double personsEquatedDisabledVov;
    private Double personsEquatedParticipants;
    private Double vdovamPogibshikh;
    private Double abc;
    private Double benefitsFrontWorkers;
    private Double abcd;
    private Double individualsAwardedTitles;
    private Double heroesSocialistLabor;
    private Double victimsPoliticalRepression;
    private Double personalPensioners;

    public Double getNumberOne() {
        return numberOne;
    }

    public Double getNumberTwo() {
        return numberTwo;
    }

    public Double getNumberWithOneDisabled() {
        return numberWithOneDisabled;
    }

    public Double getNumberWithSixDisabled() {
        return numberWithSixDisabled;
    }

    public Double getInvalidsTheFirst() {
        return invalidsTheFirst;
    }

    public Double getInvalidsSecond() {
        return invalidsSecond;
    }

    public Double getInvalidsThird() {
        return invalidsThird;
    }

    public Double getInvalidsChildren() {
        return invalidsChildren;
    }

    public Double getChildbirthAllowance() {
        return childbirthAllowance;
    }

    public Double getChildbirthAllowanceA() {
        return childbirthAllowanceA;
    }

    public Double getInvalidsFirstGroup() {
        return invalidsFirstGroup;
    }

    public Double getInvalidsSecondGroup() {
        return invalidsSecondGroup;
    }

    public Double getInvalidsThirdGroup() {
        return invalidsThirdGroup;
    }

    public Double getLargeMothersAllowance() {
        return largeMothersAllowance;
    }

    public Double getAllowanceRaisingDisabledchild() {
        return allowanceRaisingDisabledchild;
    }

    public Double getDisabledCareAllowance() {
        return disabledCareAllowance;
    }

    public Double getLargeFamilyAllowance4Children() {
        return largeFamilyAllowance4Children;
    }

    public Double getLargeFamilyAllowance5Children() {
        return largeFamilyAllowance5Children;
    }

    public Double getLargeFamilyAllowance6Children() {
        return largeFamilyAllowance6Children;
    }

    public Double getLargeFamilyAllowance7Children() {
        return largeFamilyAllowance7Children;
    }

    public Double getBurialGrants() {
        return burialGrants;
    }

    public Double getBurialAllowancesParticipantsVov() {
        return burialAllowancesParticipantsVov;
    }

    public Double getInvalidsParticipantsVov() {
        return invalidsParticipantsVov;
    }

    public Double getPersonsEquatedDisabledVov() {
        return personsEquatedDisabledVov;
    }

    public Double getPersonsEquatedParticipants() {
        return personsEquatedParticipants;
    }

    public Double getVdovamPogibshikh() {
        return vdovamPogibshikh;
    }

    public Double getAbc() {
        return abc;
    }

    public Double getBenefitsFrontWorkers() {
        return benefitsFrontWorkers;
    }

    public Double getAbcd() {
        return abcd;
    }

    public Double getIndividualsAwardedTitles() {
        return individualsAwardedTitles;
    }

    public Double getHeroesSocialistLabor() {
        return heroesSocialistLabor;
    }

    public Double getVictimsPoliticalRepression() {
        return victimsPoliticalRepression;
    }

    public Double getPersonalPensioners() {
        return personalPensioners;
    }

    public void setNumberOne(Double numberOne) {
        this.numberOne = numberOne;
    }

    public void setNumberTwo(Double numberTwo) {
        this.numberTwo = numberTwo;
    }

    public void setNumberWithOneDisabled(Double numberWithOneDisabled) {
        this.numberWithOneDisabled = numberWithOneDisabled;
    }

    public void setNumberWithSixDisabled(Double numberWithSixDisabled) {
        this.numberWithSixDisabled = numberWithSixDisabled;
    }

    public void setInvalidsTheFirst(Double invalidsTheFirst) {
        this.invalidsTheFirst = invalidsTheFirst;
    }

    public void setInvalidsSecond(Double invalidsSecond) {
        this.invalidsSecond = invalidsSecond;
    }

    public void setInvalidsThird(Double invalidsThird) {
        this.invalidsThird = invalidsThird;
    }

    public void setInvalidsChildren(Double invalidsChildren) {
        this.invalidsChildren = invalidsChildren;
    }

    public void setChildbirthAllowance(Double childbirthAllowance) {
        this.childbirthAllowance = childbirthAllowance;
    }

    public void setChildbirthAllowanceA(Double childbirthAllowanceA) {
        this.childbirthAllowanceA = childbirthAllowanceA;
    }

    public void setInvalidsFirstGroup(Double invalidsFirstGroup) {
        this.invalidsFirstGroup = invalidsFirstGroup;
    }

    public void setInvalidsSecondGroup(Double invalidsSecondGroup) {
        this.invalidsSecondGroup = invalidsSecondGroup;
    }

    public void setInvalidsThirdGroup(Double invalidsThirdGroup) {
        this.invalidsThirdGroup = invalidsThirdGroup;
    }

    public void setLargeMothersAllowance(Double largeMothersAllowance) {
        this.largeMothersAllowance = largeMothersAllowance;
    }

    public void setAllowanceRaisingDisabledchild(Double allowanceRaisingDisabledchild) {
        this.allowanceRaisingDisabledchild = allowanceRaisingDisabledchild;
    }

    public void setDisabledCareAllowance(Double disabledCareAllowance) {
        this.disabledCareAllowance = disabledCareAllowance;
    }

    public void setLargeFamilyAllowance4Children(Double largeFamilyAllowance4Children) {
        this.largeFamilyAllowance4Children = largeFamilyAllowance4Children;
    }

    public void setLargeFamilyAllowance5Children(Double largeFamilyAllowance5Children) {
        this.largeFamilyAllowance5Children = largeFamilyAllowance5Children;
    }

    public void setLargeFamilyAllowance6Children(Double largeFamilyAllowance6Children) {
        this.largeFamilyAllowance6Children = largeFamilyAllowance6Children;
    }

    public void setLargeFamilyAllowance7Children(Double largeFamilyAllowance7Children) {
        this.largeFamilyAllowance7Children = largeFamilyAllowance7Children;
    }

    public void setBurialGrants(Double burialGrants) {
        this.burialGrants = burialGrants;
    }

    public void setBurialAllowancesParticipantsVov(Double burialAllowancesParticipantsVov) {
        this.burialAllowancesParticipantsVov = burialAllowancesParticipantsVov;
    }

    public void setInvalidsParticipantsVov(Double invalidsParticipantsVov) {
        this.invalidsParticipantsVov = invalidsParticipantsVov;
    }

    public void setPersonsEquatedDisabledVov(Double personsEquatedDisabledVov) {
        this.personsEquatedDisabledVov = personsEquatedDisabledVov;
    }

    public void setPersonsEquatedParticipants(Double personsEquatedParticipants) {
        this.personsEquatedParticipants = personsEquatedParticipants;
    }

    public void setVdovamPogibshikh(Double vdovamPogibshikh) {
        this.vdovamPogibshikh = vdovamPogibshikh;
    }

    public void setAbc(Double abc) {
        this.abc = abc;
    }

    public void setBenefitsFrontWorkers(Double benefitsFrontWorkers) {
        this.benefitsFrontWorkers = benefitsFrontWorkers;
    }

    public void setAbcd(Double abcd) {
        this.abcd = abcd;
    }

    public void setIndividualsAwardedTitles(Double individualsAwardedTitles) {
        this.individualsAwardedTitles = individualsAwardedTitles;
    }

    public void setHeroesSocialistLabor(Double heroesSocialistLabor) {
        this.heroesSocialistLabor = heroesSocialistLabor;
    }

    public void setVictimsPoliticalRepression(Double victimsPoliticalRepression) {
        this.victimsPoliticalRepression = victimsPoliticalRepression;
    }

    public void setPersonalPensioners(Double personalPensioners) {
        this.personalPensioners = personalPensioners;
    }
}
