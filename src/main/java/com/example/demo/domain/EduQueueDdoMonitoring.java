package com.example.demo.domain;

public class EduQueueDdoMonitoring {

    // new edu queue ddo

    private String year;
    private String raion;
    private Integer queue;

    public String getYear() {
        return year;
    }

    public String getRaion() {
        return raion;
    }

    public Integer getQueue() {
        return queue;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setRaion(String raion) {
        this.raion = raion;
    }

    public void setQueue(Integer queue) {
        this.queue = queue;
    }
}
