package com.example.demo.domain;

public class MenuKoordinationMonitoringT1 {

    private String name;            //sed_y_koordin_region_table1
    private Double int1;
    private String year;
    private String area;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getInt1() {
        return int1;
    }

    public void setInt1(Double int1) {
        this.int1 = int1;
    }

    public String getYear() {
        return year;
    }

    public String getArea() {
        return area;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setArea(String area) {
        this.area = area;
    }
}
