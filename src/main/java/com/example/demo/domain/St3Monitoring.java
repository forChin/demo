package com.example.demo.domain;

public class St3Monitoring {
    //strateg_upr_3

    private String raion;
    private String napravlenie;
    private String otrasl;
    private String pokazatel;
    private String edIzm;
    private String year;
    private String zna4enie;

    public String getRaion() {
        return raion;
    }

    public String getNapravlenie() {
        return napravlenie;
    }

    public String getOtrasl() {
        return otrasl;
    }

    public String getPokazatel() {
        return pokazatel;
    }

    public String getEdIzm() {
        return edIzm;
    }

    public String getYear() {
        return year;
    }

    public String getZna4enie() {
        return zna4enie;
    }

    public void setRaion(String raion) {
        this.raion = raion;
    }

    public void setNapravlenie(String napravlenie) {
        this.napravlenie = napravlenie;
    }

    public void setOtrasl(String otrasl) {
        this.otrasl = otrasl;
    }

    public void setPokazatel(String pokazatel) {
        this.pokazatel = pokazatel;
    }

    public void setEdIzm(String edIzm) {
        this.edIzm = edIzm;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setZna4enie(String zna4enie) {
        this.zna4enie = zna4enie;
    }
}
