package com.example.demo.domain;

public class ShRybolovMonitoring {
    //sh_rybolovstvo

    private String raion;
    private String ulov;
    private String view;
    private String year;

    public String getRaion() {
        return raion;
    }

    public String getUlov() {
        return ulov;
    }

    public String getView() {
        return view;
    }

    public String getYear() {
        return year;
    }

    public void setRaion(String raion) {
        this.raion = raion;
    }

    public void setUlov(String ulov) {
        this.ulov = ulov;
    }

    public void setView(String view) {
        this.view = view;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
