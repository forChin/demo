package com.example.demo.domain;

public class BSpisokZdravMonitoring {
    // new spisok zdrav

    private String raion;
    private String ruralDistruct;
    private String snp;
    private Double remoteness;
    private String category;
    private String yearConstruction;
    private String yearCapRenovation;
    private String type;
    private Double accidentRate;

    public String getRaion() {
        return raion;
    }

    public String getRuralDistruct() {
        return ruralDistruct;
    }

    public String getSnp() {
        return snp;
    }

    public Double getRemoteness() {
        return remoteness;
    }

    public String getCategory() {
        return category;
    }

    public String getYearConstruction() {
        return yearConstruction;
    }

    public String getYearCapRenovation() {
        return yearCapRenovation;
    }

    public String getType() {
        return type;
    }

    public Double getAccidentRate() {
        return accidentRate;
    }

    public void setRaion(String raion) {
        this.raion = raion;
    }

    public void setRuralDistruct(String ruralDistruct) {
        this.ruralDistruct = ruralDistruct;
    }

    public void setSnp(String snp) {
        this.snp = snp;
    }

    public void setRemoteness(Double remoteness) {
        this.remoteness = remoteness;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setYearConstruction(String yearConstruction) {
        this.yearConstruction = yearConstruction;
    }

    public void setYearCapRenovation(String yearCapRenovation) {
        this.yearCapRenovation = yearCapRenovation;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setAccidentRate(Double accidentRate) {
        this.accidentRate = accidentRate;
    }
}
