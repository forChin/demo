package com.example.demo.domain;

public class MenuKoordinationMonitoring {

    private String year;    //sed_y_koordin_region
    private String area;
    private Double minimumWage;
    private Double minimumPension;
    private Double livingWage;
    private Double sizeBasicPensionPayment;
    private Double monthlyCalculationIndex;
    private Double numberOne;
    private Double amountPaymentsOne;
    private Double totalBenefitsOne;
    private Double numberTwo;
    private Double amountPaymentsTwo;
    private Double totalBenefitsTwo;
    private Double numberWithOneDisabled;
    private Double amountBenefitsWithOneDisabled;
    private Double totalBenefitsWithOneDisabled;
    private Double numberWithSixDisabled;
    private Double amountBenefitsWithSixDisabled ;
    private Double totalBenefitsWithSixDisabled;
    private Double invalidsTheFirstAmount;
    private Double invalidsFirstAmountTotal;
    private Double invalidsSecond;
    private Double invalidsSecondAmount;
    private Double invalidsSecondAmountTotal;
    private Double invalidsThird;
    private Double invalidsThirdAmount;
    private Double invalidsThirdAmountAmountTotal;
    private Double invalidsChildren;
    private Double amountInvalidsChildren;
    private Double totalInvalidsChildren;
    private Double childbirthAllowance;
    private Double amountMaternityBenefits;
    private Double totalAmountMaternityBenefits;
    private Double childbirthAllowanceA;
    private Double amountMaternityBenefitsA;
    private Double totalAmountMaternityBenefitsA;
    private Double invalidsFirstGroup;
    private Double amountInvalidsFirstGroup;
    private Double totalInvalidsFirstGroup;
    private Double invalidsSecondGroup;
    private Double amountInvalidsSecondGroup;
    private Double totalInvalidsSecondGroup;
    private Double invalidsThirdGroup;
    private Double amountInvalidsThirdGroup;
    private Double totalInvalidsThirdGroup;
    private Double largeMothersAllowance;
    private Double amountLargeMothersAllowance;
    private Double totalLargeMothersAllowance;
    private Double allowanceRaisingDisabledChild;
    private Double amountAllowanceRaiSingDisabledChild;
    private Double totalAllowanceRaisingDisabledChild;
    private Double disabledCareAllowance;
    private Double amountDisabledCareAllowance;
    private Double totalDisabledCareAllowance;
    private Double largeFamilyAllowance4Children;
    private Double amountLargeFamilyAllowance4Children;
    private Double totalLargeFamilyAllowance4Children;
    private Double largeFamilyAllowance5Children;
    private Double amountLargeFamilyAllowance5Children;
    private Double totalLargeFamilyAllowance5Children;
    private Double largeFamilyAllowance6Children;
    private Double amountLargeFamilyAllowance6Children;
    private Double totalLargeFamilyAllowance6Children;
    private Double largeFamilyAllowance7Children;
    private Double amountLargeFamilyAllowance7Children;
    private Double totalLargeFamilyAllowance7Children;
    private Double burialGrants;
    private Double amountBurialGrants;
    private Double totalBurialGrants;
    private Double burialAllowancesParticipantsVov;
    private Double amountBurialAllowancesParticipantsVov;
    private Double totalBurialAllowancesParticipantsVov;
    private Double invalidsParticipantsVov;
    private Double amountInvalidsParticipantsVov;
    private Double totalInvalidsParticipantsVov;
    private Double personsEquatedDisabledVov;
    private Double amountPersonsEquatedDisabledVov;
    private Double totalPersonsEquatedDisabledVov;
    private Double personsEquatedParticipants;
    private Double amountPersonsEquatedParticipants;
    private Double totalPersonsEquatedParticipants;
    private Double vdovamPogibshikh;
    private Double amountVdovamPogibshikh;
    private Double totalVdovamPogibshikh;
    private Double abc;
    private Double amountAbc;
    private Double totalAbc;
    private Double benefitsFrontWorkers;
    private Double amountBenefitsFrontWorkers;
    private Double totalBenefitsFrontWorkers;
    private Double abcd;
    private Double amountAbcd;
    private Double totalAbcd;
    private Double individualsAwardedTitles;
    private Double amountIndividualsAwardedTitles;
    private Double totalIndividualsAwardedTitles;
    private Double heroesSocialistLabor;
    private Double amountHeroesSocialistLabor;
    private Double totalHeroesSocialistLabor;
    private Double victimsPoliticalRepression;
    private Double amountVictimsPoliticalRepression;
    private Double totalVictimsPoliticalRepression;
    private Double personalPensioners;
    private Double amountPersonalPensioners;
    private Double totalPersonalPensioners;
    private Double workPermitIssued;
    private Double allocatedQuotas;
    private Double unemploymentRate;
    private Double youthUnemploymentRate;
    private Double vacantNewJobs;
    private Double countSocialJobs;
    private Double countYouthPractice;
    private Double countPublicWorks;
    private Double countProgramParticipant;
    private Double countVocationalTraining;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Double getMinimumWage() {
        return minimumWage;
    }

    public void setMinimumWage(Double minimumWage) {
        this.minimumWage = minimumWage;
    }

    public Double getMinimumPension() {
        return minimumPension;
    }

    public void setMinimumPension(Double minimumPension) {
        this.minimumPension = minimumPension;
    }

    public Double getLivingWage() {
        return livingWage;
    }

    public void setLivingWage(Double livingWage) {
        this.livingWage = livingWage;
    }

    public Double getSizeBasicPensionPayment() {
        return sizeBasicPensionPayment;
    }

    public void setSizeBasicPensionPayment(Double sizeBasicPensionPayment) {
        this.sizeBasicPensionPayment = sizeBasicPensionPayment;
    }

    public Double getMonthlyCalculationIndex() {
        return monthlyCalculationIndex;
    }

    public void setMonthlyCalculationIndex(Double monthlyCalculationIndex) {
        this.monthlyCalculationIndex = monthlyCalculationIndex;
    }

    public Double getNumberOne() {
        return numberOne;
    }

    public void setNumberOne(Double numberOne) {
        this.numberOne = numberOne;
    }

    public Double getAmountPaymentsOne() {
        return amountPaymentsOne;
    }

    public void setAmountPaymentsOne(Double amountPaymentsOne) {
        this.amountPaymentsOne = amountPaymentsOne;
    }

    public Double getTotalBenefitsOne() {
        return totalBenefitsOne;
    }

    public void setTotalBenefitsOne(Double totalBenefitsOne) {
        this.totalBenefitsOne = totalBenefitsOne;
    }

    public Double getNumberTwo() {
        return numberTwo;
    }

    public void setNumberTwo(Double numberTwo) {
        this.numberTwo = numberTwo;
    }

    public Double getAmountPaymentsTwo() {
        return amountPaymentsTwo;
    }

    public void setAmountPaymentsTwo(Double amountPaymentsTwo) {
        this.amountPaymentsTwo = amountPaymentsTwo;
    }

    public Double getTotalBenefitsTwo() {
        return totalBenefitsTwo;
    }

    public void setTotalBenefitsTwo(Double totalBenefitsTwo) {
        this.totalBenefitsTwo = totalBenefitsTwo;
    }

    public Double getNumberWithOneDisabled() {
        return numberWithOneDisabled;
    }

    public void setNumberWithOneDisabled(Double numberWithOneDisabled) {
        this.numberWithOneDisabled = numberWithOneDisabled;
    }

    public Double getAmountBenefitsWithOneDisabled() {
        return amountBenefitsWithOneDisabled;
    }

    public void setAmountBenefitsWithOneDisabled(Double amountBenefitsWithOneDisabled) {
        this.amountBenefitsWithOneDisabled = amountBenefitsWithOneDisabled;
    }

    public Double getTotalBenefitsWithOneDisabled() {
        return totalBenefitsWithOneDisabled;
    }

    public void setTotalBenefitsWithOneDisabled(Double totalBenefitsWithOneDisabled) {
        this.totalBenefitsWithOneDisabled = totalBenefitsWithOneDisabled;
    }

    public Double getNumberWithSixDisabled() {
        return numberWithSixDisabled;
    }

    public void setNumberWithSixDisabled(Double numberWithSixDisabled) {
        this.numberWithSixDisabled = numberWithSixDisabled;
    }

    public Double getAmountBenefitsWithSixDisabled() {
        return amountBenefitsWithSixDisabled;
    }

    public void setAmountBenefitsWithSixDisabled(Double amountBenefitsWithSixDisabled) {
        this.amountBenefitsWithSixDisabled = amountBenefitsWithSixDisabled;
    }

    public Double getTotalBenefitsWithSixDisabled() {
        return totalBenefitsWithSixDisabled;
    }

    public void setTotalBenefitsWithSixDisabled(Double totalBenefitsWithSixDisabled) {
        this.totalBenefitsWithSixDisabled = totalBenefitsWithSixDisabled;
    }

    public Double getInvalidsTheFirstAmount() {
        return invalidsTheFirstAmount;
    }

    public void setInvalidsTheFirstAmount(Double invalidsTheFirstAmount) {
        this.invalidsTheFirstAmount = invalidsTheFirstAmount;
    }

    public Double getInvalidsFirstAmountTotal() {
        return invalidsFirstAmountTotal;
    }

    public void setInvalidsFirstAmountTotal(Double invalidsFirstAmountTotal) {
        this.invalidsFirstAmountTotal = invalidsFirstAmountTotal;
    }

    public Double getInvalidsSecond() {
        return invalidsSecond;
    }

    public void setInvalidsSecond(Double invalidsSecond) {
        this.invalidsSecond = invalidsSecond;
    }

    public Double getInvalidsSecondAmount() {
        return invalidsSecondAmount;
    }

    public void setInvalidsSecondAmount(Double invalidsSecondAmount) {
        this.invalidsSecondAmount = invalidsSecondAmount;
    }

    public Double getInvalidsSecondAmountTotal() {
        return invalidsSecondAmountTotal;
    }

    public void setInvalidsSecondAmountTotal(Double invalidsSecondAmountTotal) {
        this.invalidsSecondAmountTotal = invalidsSecondAmountTotal;
    }

    public Double getInvalidsThird() {
        return invalidsThird;
    }

    public void setInvalidsThird(Double invalidsThird) {
        this.invalidsThird = invalidsThird;
    }

    public Double getInvalidsThirdAmount() {
        return invalidsThirdAmount;
    }

    public void setInvalidsThirdAmount(Double invalidsThirdAmount) {
        this.invalidsThirdAmount = invalidsThirdAmount;
    }

    public Double getInvalidsThirdAmountAmountTotal() {
        return invalidsThirdAmountAmountTotal;
    }

    public void setInvalidsThirdAmountAmountTotal(Double invalidsThirdAmountAmountTotal) {
        this.invalidsThirdAmountAmountTotal = invalidsThirdAmountAmountTotal;
    }

    public Double getInvalidsChildren() {
        return invalidsChildren;
    }

    public void setInvalidsChildren(Double invalidsChildren) {
        this.invalidsChildren = invalidsChildren;
    }

    public Double getAmountInvalidsChildren() {
        return amountInvalidsChildren;
    }

    public void setAmountInvalidsChildren(Double amountInvalidsChildren) {
        this.amountInvalidsChildren = amountInvalidsChildren;
    }

    public Double getTotalInvalidsChildren() {
        return totalInvalidsChildren;
    }

    public void setTotalInvalidsChildren(Double totalInvalidsChildren) {
        this.totalInvalidsChildren = totalInvalidsChildren;
    }

    public Double getChildbirthAllowance() {
        return childbirthAllowance;
    }

    public void setChildbirthAllowance(Double childbirthAllowance) {
        this.childbirthAllowance = childbirthAllowance;
    }

    public Double getAmountMaternityBenefits() {
        return amountMaternityBenefits;
    }

    public void setAmountMaternityBenefits(Double amountMaternityBenefits) {
        this.amountMaternityBenefits = amountMaternityBenefits;
    }

    public Double getTotalAmountMaternityBenefits() {
        return totalAmountMaternityBenefits;
    }

    public void setTotalAmountMaternityBenefits(Double totalAmountMaternityBenefits) {
        this.totalAmountMaternityBenefits = totalAmountMaternityBenefits;
    }

    public Double getChildbirthAllowanceA() {
        return childbirthAllowanceA;
    }

    public void setChildbirthAllowanceA(Double childbirthAllowanceA) {
        this.childbirthAllowanceA = childbirthAllowanceA;
    }

    public Double getAmountMaternityBenefitsA() {
        return amountMaternityBenefitsA;
    }

    public void setAmountMaternityBenefitsA(Double amountMaternityBenefitsA) {
        this.amountMaternityBenefitsA = amountMaternityBenefitsA;
    }

    public Double getTotalAmountMaternityBenefitsA() {
        return totalAmountMaternityBenefitsA;
    }

    public void setTotalAmountMaternityBenefitsA(Double totalAmountMaternityBenefitsA) {
        this.totalAmountMaternityBenefitsA = totalAmountMaternityBenefitsA;
    }

    public Double getInvalidsFirstGroup() {
        return invalidsFirstGroup;
    }

    public void setInvalidsFirstGroup(Double invalidsFirstGroup) {
        this.invalidsFirstGroup = invalidsFirstGroup;
    }

    public Double getAmountInvalidsFirstGroup() {
        return amountInvalidsFirstGroup;
    }

    public void setAmountInvalidsFirstGroup(Double amountInvalidsFirstGroup) {
        this.amountInvalidsFirstGroup = amountInvalidsFirstGroup;
    }

    public Double getTotalInvalidsFirstGroup() {
        return totalInvalidsFirstGroup;
    }

    public void setTotalInvalidsFirstGroup(Double totalInvalidsFirstGroup) {
        this.totalInvalidsFirstGroup = totalInvalidsFirstGroup;
    }

    public Double getInvalidsSecondGroup() {
        return invalidsSecondGroup;
    }

    public void setInvalidsSecondGroup(Double invalidsSecondGroup) {
        this.invalidsSecondGroup = invalidsSecondGroup;
    }

    public Double getAmountInvalidsSecondGroup() {
        return amountInvalidsSecondGroup;
    }

    public void setAmountInvalidsSecondGroup(Double amountInvalidsSecondGroup) {
        this.amountInvalidsSecondGroup = amountInvalidsSecondGroup;
    }

    public Double getTotalInvalidsSecondGroup() {
        return totalInvalidsSecondGroup;
    }

    public void setTotalInvalidsSecondGroup(Double totalInvalidsSecondGroup) {
        this.totalInvalidsSecondGroup = totalInvalidsSecondGroup;
    }

    public Double getInvalidsThirdGroup() {
        return invalidsThirdGroup;
    }

    public void setInvalidsThirdGroup(Double invalidsThirdGroup) {
        this.invalidsThirdGroup = invalidsThirdGroup;
    }

    public Double getAmountInvalidsThirdGroup() {
        return amountInvalidsThirdGroup;
    }

    public void setAmountInvalidsThirdGroup(Double amountInvalidsThirdGroup) {
        this.amountInvalidsThirdGroup = amountInvalidsThirdGroup;
    }

    public Double getTotalInvalidsThirdGroup() {
        return totalInvalidsThirdGroup;
    }

    public void setTotalInvalidsThirdGroup(Double totalInvalidsThirdGroup) {
        this.totalInvalidsThirdGroup = totalInvalidsThirdGroup;
    }

    public Double getLargeMothersAllowance() {
        return largeMothersAllowance;
    }

    public void setLargeMothersAllowance(Double largeMothersAllowance) {
        this.largeMothersAllowance = largeMothersAllowance;
    }

    public Double getAmountLargeMothersAllowance() {
        return amountLargeMothersAllowance;
    }

    public void setAmountLargeMothersAllowance(Double amountLargeMothersAllowance) {
        this.amountLargeMothersAllowance = amountLargeMothersAllowance;
    }

    public Double getTotalLargeMothersAllowance() {
        return totalLargeMothersAllowance;
    }

    public void setTotalLargeMothersAllowance(Double totalLargeMothersAllowance) {
        this.totalLargeMothersAllowance = totalLargeMothersAllowance;
    }

    public Double getAllowanceRaisingDisabledChild() {
        return allowanceRaisingDisabledChild;
    }

    public void setAllowanceRaisingDisabledChild(Double allowanceRaisingDisabledChild) {
        this.allowanceRaisingDisabledChild = allowanceRaisingDisabledChild;
    }

    public Double getAmountAllowanceRaiSingDisabledChild() {
        return amountAllowanceRaiSingDisabledChild;
    }

    public void setAmountAllowanceRaiSingDisabledChild(Double amountAllowanceRaiSingDisabledChild) {
        this.amountAllowanceRaiSingDisabledChild = amountAllowanceRaiSingDisabledChild;
    }

    public Double getTotalAllowanceRaisingDisabledChild() {
        return totalAllowanceRaisingDisabledChild;
    }

    public void setTotalAllowanceRaisingDisabledChild(Double totalAllowanceRaisingDisabledChild) {
        this.totalAllowanceRaisingDisabledChild = totalAllowanceRaisingDisabledChild;
    }

    public Double getDisabledCareAllowance() {
        return disabledCareAllowance;
    }

    public void setDisabledCareAllowance(Double disabledCareAllowance) {
        this.disabledCareAllowance = disabledCareAllowance;
    }

    public Double getAmountDisabledCareAllowance() {
        return amountDisabledCareAllowance;
    }

    public void setAmountDisabledCareAllowance(Double amountDisabledCareAllowance) {
        this.amountDisabledCareAllowance = amountDisabledCareAllowance;
    }

    public Double getTotalDisabledCareAllowance() {
        return totalDisabledCareAllowance;
    }

    public void setTotalDisabledCareAllowance(Double totalDisabledCareAllowance) {
        this.totalDisabledCareAllowance = totalDisabledCareAllowance;
    }

    public Double getLargeFamilyAllowance4Children() {
        return largeFamilyAllowance4Children;
    }

    public void setLargeFamilyAllowance4Children(Double largeFamilyAllowance4Children) {
        this.largeFamilyAllowance4Children = largeFamilyAllowance4Children;
    }

    public Double getAmountLargeFamilyAllowance4Children() {
        return amountLargeFamilyAllowance4Children;
    }

    public void setAmountLargeFamilyAllowance4Children(Double amountLargeFamilyAllowance4Children) {
        this.amountLargeFamilyAllowance4Children = amountLargeFamilyAllowance4Children;
    }

    public Double getTotalLargeFamilyAllowance4Children() {
        return totalLargeFamilyAllowance4Children;
    }

    public void setTotalLargeFamilyAllowance4Children(Double totalLargeFamilyAllowance4Children) {
        this.totalLargeFamilyAllowance4Children = totalLargeFamilyAllowance4Children;
    }

    public Double getLargeFamilyAllowance5Children() {
        return largeFamilyAllowance5Children;
    }

    public void setLargeFamilyAllowance5Children(Double largeFamilyAllowance5Children) {
        this.largeFamilyAllowance5Children = largeFamilyAllowance5Children;
    }

    public Double getAmountLargeFamilyAllowance5Children() {
        return amountLargeFamilyAllowance5Children;
    }

    public void setAmountLargeFamilyAllowance5Children(Double amountLargeFamilyAllowance5Children) {
        this.amountLargeFamilyAllowance5Children = amountLargeFamilyAllowance5Children;
    }

    public Double getTotalLargeFamilyAllowance5Children() {
        return totalLargeFamilyAllowance5Children;
    }

    public void setTotalLargeFamilyAllowance5Children(Double totalLargeFamilyAllowance5Children) {
        this.totalLargeFamilyAllowance5Children = totalLargeFamilyAllowance5Children;
    }

    public Double getLargeFamilyAllowance6Children() {
        return largeFamilyAllowance6Children;
    }

    public void setLargeFamilyAllowance6Children(Double largeFamilyAllowance6Children) {
        this.largeFamilyAllowance6Children = largeFamilyAllowance6Children;
    }

    public Double getAmountLargeFamilyAllowance6Children() {
        return amountLargeFamilyAllowance6Children;
    }

    public void setAmountLargeFamilyAllowance6Children(Double amountLargeFamilyAllowance6Children) {
        this.amountLargeFamilyAllowance6Children = amountLargeFamilyAllowance6Children;
    }

    public Double getTotalLargeFamilyAllowance6Children() {
        return totalLargeFamilyAllowance6Children;
    }

    public void setTotalLargeFamilyAllowance6Children(Double totalLargeFamilyAllowance6Children) {
        this.totalLargeFamilyAllowance6Children = totalLargeFamilyAllowance6Children;
    }

    public Double getLargeFamilyAllowance7Children() {
        return largeFamilyAllowance7Children;
    }

    public void setLargeFamilyAllowance7Children(Double largeFamilyAllowance7Children) {
        this.largeFamilyAllowance7Children = largeFamilyAllowance7Children;
    }

    public Double getAmountLargeFamilyAllowance7Children() {
        return amountLargeFamilyAllowance7Children;
    }

    public void setAmountLargeFamilyAllowance7Children(Double amountLargeFamilyAllowance7Children) {
        this.amountLargeFamilyAllowance7Children = amountLargeFamilyAllowance7Children;
    }

    public Double getTotalLargeFamilyAllowance7Children() {
        return totalLargeFamilyAllowance7Children;
    }

    public void setTotalLargeFamilyAllowance7Children(Double totalLargeFamilyAllowance7Children) {
        this.totalLargeFamilyAllowance7Children = totalLargeFamilyAllowance7Children;
    }

    public Double getBurialGrants() {
        return burialGrants;
    }

    public void setBurialGrants(Double burialGrants) {
        this.burialGrants = burialGrants;
    }

    public Double getAmountBurialGrants() {
        return amountBurialGrants;
    }

    public void setAmountBurialGrants(Double amountBurialGrants) {
        this.amountBurialGrants = amountBurialGrants;
    }

    public Double getTotalBurialGrants() {
        return totalBurialGrants;
    }

    public void setTotalBurialGrants(Double totalBurialGrants) {
        this.totalBurialGrants = totalBurialGrants;
    }

    public Double getBurialAllowancesParticipantsVov() {
        return burialAllowancesParticipantsVov;
    }

    public void setBurialAllowancesParticipantsVov(Double burialAllowancesParticipantsVov) {
        this.burialAllowancesParticipantsVov = burialAllowancesParticipantsVov;
    }

    public Double getAmountBurialAllowancesParticipantsVov() {
        return amountBurialAllowancesParticipantsVov;
    }

    public void setAmountBurialAllowancesParticipantsVov(Double amountBurialAllowancesParticipantsVov) {
        this.amountBurialAllowancesParticipantsVov = amountBurialAllowancesParticipantsVov;
    }

    public Double getTotalBurialAllowancesParticipantsVov() {
        return totalBurialAllowancesParticipantsVov;
    }

    public void setTotalBurialAllowancesParticipantsVov(Double totalBurialAllowancesParticipantsVov) {
        this.totalBurialAllowancesParticipantsVov = totalBurialAllowancesParticipantsVov;
    }

    public Double getInvalidsParticipantsVov() {
        return invalidsParticipantsVov;
    }

    public void setInvalidsParticipantsVov(Double invalidsParticipantsVov) {
        this.invalidsParticipantsVov = invalidsParticipantsVov;
    }

    public Double getAmountInvalidsParticipantsVov() {
        return amountInvalidsParticipantsVov;
    }

    public void setAmountInvalidsParticipantsVov(Double amountInvalidsParticipantsVov) {
        this.amountInvalidsParticipantsVov = amountInvalidsParticipantsVov;
    }

    public Double getTotalInvalidsParticipantsVov() {
        return totalInvalidsParticipantsVov;
    }

    public void setTotalInvalidsParticipantsVov(Double totalInvalidsParticipantsVov) {
        this.totalInvalidsParticipantsVov = totalInvalidsParticipantsVov;
    }

    public Double getPersonsEquatedDisabledVov() {
        return personsEquatedDisabledVov;
    }

    public void setPersonsEquatedDisabledVov(Double personsEquatedDisabledVov) {
        this.personsEquatedDisabledVov = personsEquatedDisabledVov;
    }

    public Double getAmountPersonsEquatedDisabledVov() {
        return amountPersonsEquatedDisabledVov;
    }

    public void setAmountPersonsEquatedDisabledVov(Double amountPersonsEquatedDisabledVov) {
        this.amountPersonsEquatedDisabledVov = amountPersonsEquatedDisabledVov;
    }

    public Double getTotalPersonsEquatedDisabledVov() {
        return totalPersonsEquatedDisabledVov;
    }

    public void setTotalPersonsEquatedDisabledVov(Double totalPersonsEquatedDisabledVov) {
        this.totalPersonsEquatedDisabledVov = totalPersonsEquatedDisabledVov;
    }

    public Double getPersonsEquatedParticipants() {
        return personsEquatedParticipants;
    }

    public void setPersonsEquatedParticipants(Double personsEquatedParticipants) {
        this.personsEquatedParticipants = personsEquatedParticipants;
    }

    public Double getAmountPersonsEquatedParticipants() {
        return amountPersonsEquatedParticipants;
    }

    public void setAmountPersonsEquatedParticipants(Double amountPersonsEquatedParticipants) {
        this.amountPersonsEquatedParticipants = amountPersonsEquatedParticipants;
    }

    public Double getTotalPersonsEquatedParticipants() {
        return totalPersonsEquatedParticipants;
    }

    public void setTotalPersonsEquatedParticipants(Double totalPersonsEquatedParticipants) {
        this.totalPersonsEquatedParticipants = totalPersonsEquatedParticipants;
    }

    public Double getVdovamPogibshikh() {
        return vdovamPogibshikh;
    }

    public void setVdovamPogibshikh(Double vdovamPogibshikh) {
        this.vdovamPogibshikh = vdovamPogibshikh;
    }

    public Double getAmountVdovamPogibshikh() {
        return amountVdovamPogibshikh;
    }

    public void setAmountVdovamPogibshikh(Double amountVdovamPogibshikh) {
        this.amountVdovamPogibshikh = amountVdovamPogibshikh;
    }

    public Double getTotalVdovamPogibshikh() {
        return totalVdovamPogibshikh;
    }

    public void setTotalVdovamPogibshikh(Double totalVdovamPogibshikh) {
        this.totalVdovamPogibshikh = totalVdovamPogibshikh;
    }

    public Double getAbc() {
        return abc;
    }

    public void setAbc(Double abc) {
        this.abc = abc;
    }

    public Double getAmountAbc() {
        return amountAbc;
    }

    public void setAmountAbc(Double amountAbc) {
        this.amountAbc = amountAbc;
    }

    public Double getTotalAbc() {
        return totalAbc;
    }

    public void setTotalAbc(Double totalAbc) {
        this.totalAbc = totalAbc;
    }

    public Double getBenefitsFrontWorkers() {
        return benefitsFrontWorkers;
    }

    public void setBenefitsFrontWorkers(Double benefitsFrontWorkers) {
        this.benefitsFrontWorkers = benefitsFrontWorkers;
    }

    public Double getAmountBenefitsFrontWorkers() {
        return amountBenefitsFrontWorkers;
    }

    public void setAmountBenefitsFrontWorkers(Double amountBenefitsFrontWorkers) {
        this.amountBenefitsFrontWorkers = amountBenefitsFrontWorkers;
    }

    public Double getTotalBenefitsFrontWorkers() {
        return totalBenefitsFrontWorkers;
    }

    public void setTotalBenefitsFrontWorkers(Double totalBenefitsFrontWorkers) {
        this.totalBenefitsFrontWorkers = totalBenefitsFrontWorkers;
    }

    public Double getAbcd() {
        return abcd;
    }

    public void setAbcd(Double abcd) {
        this.abcd = abcd;
    }

    public Double getAmountAbcd() {
        return amountAbcd;
    }

    public void setAmountAbcd(Double amountAbcd) {
        this.amountAbcd = amountAbcd;
    }

    public Double getTotalAbcd() {
        return totalAbcd;
    }

    public void setTotalAbcd(Double totalAbcd) {
        this.totalAbcd = totalAbcd;
    }

    public Double getIndividualsAwardedTitles() {
        return individualsAwardedTitles;
    }

    public void setIndividualsAwardedTitles(Double individualsAwardedTitles) {
        this.individualsAwardedTitles = individualsAwardedTitles;
    }

    public Double getAmountIndividualsAwardedTitles() {
        return amountIndividualsAwardedTitles;
    }

    public void setAmountIndividualsAwardedTitles(Double amountIndividualsAwardedTitles) {
        this.amountIndividualsAwardedTitles = amountIndividualsAwardedTitles;
    }

    public Double getTotalIndividualsAwardedTitles() {
        return totalIndividualsAwardedTitles;
    }

    public void setTotalIndividualsAwardedTitles(Double totalIndividualsAwardedTitles) {
        this.totalIndividualsAwardedTitles = totalIndividualsAwardedTitles;
    }

    public Double getHeroesSocialistLabor() {
        return heroesSocialistLabor;
    }

    public void setHeroesSocialistLabor(Double heroesSocialistLabor) {
        this.heroesSocialistLabor = heroesSocialistLabor;
    }

    public Double getAmountHeroesSocialistLabor() {
        return amountHeroesSocialistLabor;
    }

    public void setAmountHeroesSocialistLabor(Double amountHeroesSocialistLabor) {
        this.amountHeroesSocialistLabor = amountHeroesSocialistLabor;
    }

    public Double getTotalHeroesSocialistLabor() {
        return totalHeroesSocialistLabor;
    }

    public void setTotalHeroesSocialistLabor(Double totalHeroesSocialistLabor) {
        this.totalHeroesSocialistLabor = totalHeroesSocialistLabor;
    }

    public Double getVictimsPoliticalRepression() {
        return victimsPoliticalRepression;
    }

    public void setVictimsPoliticalRepression(Double victimsPoliticalRepression) {
        this.victimsPoliticalRepression = victimsPoliticalRepression;
    }

    public Double getAmountVictimsPoliticalRepression() {
        return amountVictimsPoliticalRepression;
    }

    public void setAmountVictimsPoliticalRepression(Double amountVictimsPoliticalRepression) {
        this.amountVictimsPoliticalRepression = amountVictimsPoliticalRepression;
    }

    public Double getTotalVictimsPoliticalRepression() {
        return totalVictimsPoliticalRepression;
    }

    public void setTotalVictimsPoliticalRepression(Double totalVictimsPoliticalRepression) {
        this.totalVictimsPoliticalRepression = totalVictimsPoliticalRepression;
    }

    public Double getPersonalPensioners() {
        return personalPensioners;
    }

    public void setPersonalPensioners(Double personalPensioners) {
        this.personalPensioners = personalPensioners;
    }

    public Double getAmountPersonalPensioners() {
        return amountPersonalPensioners;
    }

    public void setAmountPersonalPensioners(Double amountPersonalPensioners) {
        this.amountPersonalPensioners = amountPersonalPensioners;
    }

    public Double getTotalPersonalPensioners() {
        return totalPersonalPensioners;
    }

    public void setTotalPersonalPensioners(Double totalPersonalPensioners) {
        this.totalPersonalPensioners = totalPersonalPensioners;
    }

    public Double getWorkPermitIssued() {
        return workPermitIssued;
    }

    public void setWorkPermitIssued(Double workPermitIssued) {
        this.workPermitIssued = workPermitIssued;
    }

    public Double getAllocatedQuotas() {
        return allocatedQuotas;
    }

    public void setAllocatedQuotas(Double allocatedQuotas) {
        this.allocatedQuotas = allocatedQuotas;
    }

    public Double getUnemploymentRate() {
        return unemploymentRate;
    }

    public void setUnemploymentRate(Double unemploymentRate) {
        this.unemploymentRate = unemploymentRate;
    }

    public Double getYouthUnemploymentRate() {
        return youthUnemploymentRate;
    }

    public void setYouthUnemploymentRate(Double youthUnemploymentRate) {
        this.youthUnemploymentRate = youthUnemploymentRate;
    }

    public Double getVacantNewJobs() {
        return vacantNewJobs;
    }

    public void setVacantNewJobs(Double vacantNewJobs) {
        this.vacantNewJobs = vacantNewJobs;
    }

    public Double getCountSocialJobs() {
        return countSocialJobs;
    }

    public void setCountSocialJobs(Double countSocialJobs) {
        this.countSocialJobs = countSocialJobs;
    }

    public Double getCountYouthPractice() {
        return countYouthPractice;
    }

    public void setCountYouthPractice(Double countYouthPractice) {
        this.countYouthPractice = countYouthPractice;
    }

    public Double getCountPublicWorks() {
        return countPublicWorks;
    }

    public void setCountPublicWorks(Double countPublicWorks) {
        this.countPublicWorks = countPublicWorks;
    }

    public Double getCountProgramParticipant() {
        return countProgramParticipant;
    }

    public void setCountProgramParticipant(Double countProgramParticipant) {
        this.countProgramParticipant = countProgramParticipant;
    }

    public Double getCountVocationalTraining() {
        return countVocationalTraining;
    }

    public void setCountVocationalTraining(Double countVocationalTraining) {
        this.countVocationalTraining = countVocationalTraining;
    }
}
