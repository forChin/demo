package com.example.demo.domain;

public class FizMonitoring {

    private String year;
    private String area;
    private Double numberSportsFacilities;
    private Double numberEmergencySportsfacilities;
    private Double sharePhysicalCulture;
    private Double numberDyussh;
    private Double attendanceDyussh;

    public String getYear() {
        return year;
    }

    public String getArea() {
        return area;
    }

    public Double getNumberSportsFacilities() {
        return numberSportsFacilities;
    }

    public Double getNumberEmergencySportsfacilities() {
        return numberEmergencySportsfacilities;
    }

    public Double getSharePhysicalCulture() {
        return sharePhysicalCulture;
    }

    public Double getNumberDyussh() {
        return numberDyussh;
    }

    public Double getAttendanceDyussh() {
        return attendanceDyussh;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setNumberSportsFacilities(Double numberSportsFacilities) {
        this.numberSportsFacilities = numberSportsFacilities;
    }

    public void setNumberEmergencySportsfacilities(Double numberEmergencySportsfacilities) {
        this.numberEmergencySportsfacilities = numberEmergencySportsfacilities;
    }

    public void setSharePhysicalCulture(Double sharePhysicalCulture) {
        this.sharePhysicalCulture = sharePhysicalCulture;
    }

    public void setNumberDyussh(Double numberDyussh) {
        this.numberDyussh = numberDyussh;
    }

    public void setAttendanceDyussh(Double attendanceDyussh) {
        this.attendanceDyussh = attendanceDyussh;
    }
}
