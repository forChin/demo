package com.example.demo.domain;

public class MenuKoordinationRaion {

    private String year;            //sed_y_koordin_raion
    private String area;
    private String region;
    private Double numberPopulation;
    private Double countLabour;
    private Double employedPopulation;
    private Double hiredWorker;
    private Double selfEmployedPopulation;
    private Double countDisabilitiers;
    private Double productivelyEmployed;
    private Double unproductiveBusy;
    private Double unemployedPopulation;
    private Double payUnemployedPopulation;
    private Double registeredUnemployedPopulation;
    private Double targetedSocialAssistance;
    private Double housingAssistance;
    private Double guaranteedSocialPackage;
    private Double totalConditionalCashaid;
    private Double totalUnconditionalCashaid;
    private Double housingAssistanceTotal;
    private Double guaranteedSocialPackageTotal;
    private Double conditionalCashaid;
    private Double unconditionalCashaid;
    private Double housingAssistanceAverageSize;
    private Double guaranteedSocialPackageAverageSize;
    private Double employedPermanentJobs;
    private Double aimedCommunityService;
    private Double aimedSocialJobs;
    private Double aimedYouthPractice;
    private Double altynAlka;
    private Double kumisAlka;
    private Double mothersManyChildren;
    private Double countPensioners;
    private Double countPensionersAge;
    private Double countPreferentialPension;
    private Double countPensionRecipients;
    private Double countVeteransVov;
    private Double countInvalidsVov;
    private Double countWidowsVov;
    private Double countVeteransAfghan;
    private Double countInvalidsAfghan;
    private Double countWidowsAfghans;
    private Double countInvalids;
    private Double countInvalidsA;
    private Double countInvalidsB;
    private Double countInvalidsC;
    private Double countInvalidsChildren;
    private Double invalidsPermanentJobs;
    private Double countInvalidsPublicWorks;
    private Double countInvalidsSocialWork;
    private Double countInvalidsStudy;
    private Double completedIpr;
    private Double fulfilledIpr;
    private Double developedIpr;
    private Double totalAmount;
    private Double totalEmployed;
    private Double youthPractice;
    private Double publicWork;
    private Double professionalEducation;
    private Double socialJobs;
    private Double permanentJobs;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Double getNumberPopulation() {
        return numberPopulation;
    }

    public void setNumberPopulation(Double numberPopulation) {
        this.numberPopulation = numberPopulation;
    }

    public Double getCountLabour() {
        return countLabour;
    }

    public void setCountLabour(Double countLabour) {
        this.countLabour = countLabour;
    }

    public Double getEmployedPopulation() {
        return employedPopulation;
    }

    public void setEmployedPopulation(Double employedPopulation) {
        this.employedPopulation = employedPopulation;
    }

    public Double getHiredWorker() {
        return hiredWorker;
    }

    public void setHiredWorker(Double hiredWorker) {
        this.hiredWorker = hiredWorker;
    }

    public Double getSelfEmployedPopulation() {
        return selfEmployedPopulation;
    }

    public void setSelfEmployedPopulation(Double selfEmployedPopulation) {
        this.selfEmployedPopulation = selfEmployedPopulation;
    }

    public Double getCountDisabilitiers() {
        return countDisabilitiers;
    }

    public void setCountDisabilitiers(Double countDisabilitiers) {
        this.countDisabilitiers = countDisabilitiers;
    }

    public Double getProductivelyEmployed() {
        return productivelyEmployed;
    }

    public void setProductivelyEmployed(Double productivelyEmployed) {
        this.productivelyEmployed = productivelyEmployed;
    }

    public Double getUnproductiveBusy() {
        return unproductiveBusy;
    }

    public void setUnproductiveBusy(Double unproductiveBusy) {
        this.unproductiveBusy = unproductiveBusy;
    }

    public Double getUnemployedPopulation() {
        return unemployedPopulation;
    }

    public void setUnemployedPopulation(Double unemployedPopulation) {
        this.unemployedPopulation = unemployedPopulation;
    }

    public Double getPayUnemployedPopulation() {
        return payUnemployedPopulation;
    }

    public void setPayUnemployedPopulation(Double payUnemployedPopulation) {
        this.payUnemployedPopulation = payUnemployedPopulation;
    }

    public Double getRegisteredUnemployedPopulation() {
        return registeredUnemployedPopulation;
    }

    public void setRegisteredUnemployedPopulation(Double registeredUnemployedPopulation) {
        this.registeredUnemployedPopulation = registeredUnemployedPopulation;
    }

    public Double getTargetedSocialAssistance() {
        return targetedSocialAssistance;
    }

    public void setTargetedSocialAssistance(Double targetedSocialAssistance) {
        this.targetedSocialAssistance = targetedSocialAssistance;
    }

    public Double getHousingAssistance() {
        return housingAssistance;
    }

    public void setHousingAssistance(Double housingAssistance) {
        this.housingAssistance = housingAssistance;
    }

    public Double getGuaranteedSocialPackage() {
        return guaranteedSocialPackage;
    }

    public void setGuaranteedSocialPackage(Double guaranteedSocialPackage) {
        this.guaranteedSocialPackage = guaranteedSocialPackage;
    }

    public Double getTotalConditionalCashaid() {
        return totalConditionalCashaid;
    }

    public void setTotalConditionalCashaid(Double totalConditionalCashaid) {
        this.totalConditionalCashaid = totalConditionalCashaid;
    }

    public Double getTotalUnconditionalCashaid() {
        return totalUnconditionalCashaid;
    }

    public void setTotalUnconditionalCashaid(Double totalUnconditionalCashaid) {
        this.totalUnconditionalCashaid = totalUnconditionalCashaid;
    }

    public Double getHousingAssistanceTotal() {
        return housingAssistanceTotal;
    }

    public void setHousingAssistanceTotal(Double housingAssistanceTotal) {
        this.housingAssistanceTotal = housingAssistanceTotal;
    }

    public Double getGuaranteedSocialPackageTotal() {
        return guaranteedSocialPackageTotal;
    }

    public void setGuaranteedSocialPackageTotal(Double guaranteedSocialPackageTotal) {
        this.guaranteedSocialPackageTotal = guaranteedSocialPackageTotal;
    }

    public Double getConditionalCashaid() {
        return conditionalCashaid;
    }

    public void setConditionalCashaid(Double conditionalCashaid) {
        this.conditionalCashaid = conditionalCashaid;
    }

    public Double getUnconditionalCashaid() {
        return unconditionalCashaid;
    }

    public void setUnconditionalCashaid(Double unconditionalCashaid) {
        this.unconditionalCashaid = unconditionalCashaid;
    }

    public Double getHousingAssistanceAverageSize() {
        return housingAssistanceAverageSize;
    }

    public void setHousingAssistanceAverageSize(Double housingAssistanceAverageSize) {
        this.housingAssistanceAverageSize = housingAssistanceAverageSize;
    }

    public Double getGuaranteedSocialPackageAverageSize() {
        return guaranteedSocialPackageAverageSize;
    }

    public void setGuaranteedSocialPackageAverageSize(Double guaranteedSocialPackageAverageSize) {
        this.guaranteedSocialPackageAverageSize = guaranteedSocialPackageAverageSize;
    }

    public Double getEmployedPermanentJobs() {
        return employedPermanentJobs;
    }

    public void setEmployedPermanentJobs(Double employedPermanentJobs) {
        this.employedPermanentJobs = employedPermanentJobs;
    }

    public Double getAimedCommunityService() {
        return aimedCommunityService;
    }

    public void setAimedCommunityService(Double aimedCommunityService) {
        this.aimedCommunityService = aimedCommunityService;
    }

    public Double getAimedSocialJobs() {
        return aimedSocialJobs;
    }

    public void setAimedSocialJobs(Double aimedSocialJobs) {
        this.aimedSocialJobs = aimedSocialJobs;
    }

    public Double getAimedYouthPractice() {
        return aimedYouthPractice;
    }

    public void setAimedYouthPractice(Double aimedYouthPractice) {
        this.aimedYouthPractice = aimedYouthPractice;
    }

    public Double getAltynAlka() {
        return altynAlka;
    }

    public void setAltynAlka(Double altynAlka) {
        this.altynAlka = altynAlka;
    }

    public Double getKumisAlka() {
        return kumisAlka;
    }

    public void setKumisAlka(Double kumisAlka) {
        this.kumisAlka = kumisAlka;
    }

    public Double getMothersManyChildren() {
        return mothersManyChildren;
    }

    public void setMothersManyChildren(Double mothersManyChildren) {
        this.mothersManyChildren = mothersManyChildren;
    }

    public Double getCountPensioners() {
        return countPensioners;
    }

    public void setCountPensioners(Double countPensioners) {
        this.countPensioners = countPensioners;
    }

    public Double getCountPensionersAge() {
        return countPensionersAge;
    }

    public void setCountPensionersAge(Double countPensionersAge) {
        this.countPensionersAge = countPensionersAge;
    }

    public Double getCountPreferentialPension() {
        return countPreferentialPension;
    }

    public void setCountPreferentialPension(Double countPreferentialPension) {
        this.countPreferentialPension = countPreferentialPension;
    }

    public Double getCountPensionRecipients() {
        return countPensionRecipients;
    }

    public void setCountPensionRecipients(Double countPensionRecipients) {
        this.countPensionRecipients = countPensionRecipients;
    }

    public Double getCountVeteransVov() {
        return countVeteransVov;
    }

    public void setCountVeteransVov(Double countVeteransVov) {
        this.countVeteransVov = countVeteransVov;
    }

    public Double getCountInvalidsVov() {
        return countInvalidsVov;
    }

    public void setCountInvalidsVov(Double countInvalidsVov) {
        this.countInvalidsVov = countInvalidsVov;
    }

    public Double getCountWidowsVov() {
        return countWidowsVov;
    }

    public void setCountWidowsVov(Double countWidowsVov) {
        this.countWidowsVov = countWidowsVov;
    }

    public Double getCountVeteransAfghan() {
        return countVeteransAfghan;
    }

    public void setCountVeteransAfghan(Double countVeteransAfghan) {
        this.countVeteransAfghan = countVeteransAfghan;
    }

    public Double getCountInvalidsAfghan() {
        return countInvalidsAfghan;
    }

    public void setCountInvalidsAfghan(Double countInvalidsAfghan) {
        this.countInvalidsAfghan = countInvalidsAfghan;
    }

    public Double getCountWidowsAfghans() {
        return countWidowsAfghans;
    }

    public void setCountWidowsAfghans(Double countWidowsAfghans) {
        this.countWidowsAfghans = countWidowsAfghans;
    }

    public Double getCountInvalids() {
        return countInvalids;
    }

    public void setCountInvalids(Double countInvalids) {
        this.countInvalids = countInvalids;
    }

    public Double getCountInvalidsA() {
        return countInvalidsA;
    }

    public void setCountInvalidsA(Double countInvalidsA) {
        this.countInvalidsA = countInvalidsA;
    }

    public Double getCountInvalidsB() {
        return countInvalidsB;
    }

    public void setCountInvalidsB(Double countInvalidsB) {
        this.countInvalidsB = countInvalidsB;
    }

    public Double getCountInvalidsC() {
        return countInvalidsC;
    }

    public void setCountInvalidsC(Double countInvalidsC) {
        this.countInvalidsC = countInvalidsC;
    }

    public Double getCountInvalidsChildren() {
        return countInvalidsChildren;
    }

    public void setCountInvalidsChildren(Double countInvalidsChildren) {
        this.countInvalidsChildren = countInvalidsChildren;
    }

    public Double getInvalidsPermanentJobs() {
        return invalidsPermanentJobs;
    }

    public void setInvalidsPermanentJobs(Double invalidsPermanentJobs) {
        this.invalidsPermanentJobs = invalidsPermanentJobs;
    }

    public Double getCountInvalidsPublicWorks() {
        return countInvalidsPublicWorks;
    }

    public void setCountInvalidsPublicWorks(Double countInvalidsPublicWorks) {
        this.countInvalidsPublicWorks = countInvalidsPublicWorks;
    }

    public Double getCountInvalidsSocialWork() {
        return countInvalidsSocialWork;
    }

    public void setCountInvalidsSocialWork(Double countInvalidsSocialWork) {
        this.countInvalidsSocialWork = countInvalidsSocialWork;
    }

    public Double getCountInvalidsStudy() {
        return countInvalidsStudy;
    }

    public void setCountInvalidsStudy(Double countInvalidsStudy) {
        this.countInvalidsStudy = countInvalidsStudy;
    }

    public Double getCompletedIpr() {
        return completedIpr;
    }

    public void setCompletedIpr(Double completedIpr) {
        this.completedIpr = completedIpr;
    }

    public Double getFulfilledIpr() {
        return fulfilledIpr;
    }

    public void setFulfilledIpr(Double fulfilledIpr) {
        this.fulfilledIpr = fulfilledIpr;
    }

    public Double getDevelopedIpr() {
        return developedIpr;
    }

    public void setDevelopedIpr(Double developedIpr) {
        this.developedIpr = developedIpr;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Double getTotalEmployed() {
        return totalEmployed;
    }

    public void setTotalEmployed(Double totalEmployed) {
        this.totalEmployed = totalEmployed;
    }

    public Double getYouthPractice() {
        return youthPractice;
    }

    public void setYouthPractice(Double youthPractice) {
        this.youthPractice = youthPractice;
    }

    public Double getPublicWork() {
        return publicWork;
    }

    public void setPublicWork(Double publicWork) {
        this.publicWork = publicWork;
    }

    public Double getProfessionalEducation() {
        return professionalEducation;
    }

    public void setProfessionalEducation(Double professionalEducation) {
        this.professionalEducation = professionalEducation;
    }

    public Double getSocialJobs() {
        return socialJobs;
    }

    public void setSocialJobs(Double socialJobs) {
        this.socialJobs = socialJobs;
    }

    public Double getPermanentJobs() {
        return permanentJobs;
    }

    public void setPermanentJobs(Double permanentJobs) {
        this.permanentJobs = permanentJobs;
    }
}
