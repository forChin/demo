package com.example.demo.domain;

public class MenuKoordinationRegionM {

    private String year;            //sed_m_koordin_region
    private String area;
    private Double jobFairsHeld;
    private Double employersTookPart;
    private Double unemployed;
    private Double vacanciesSubmitted;
    private Double permanentJobs;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Double getJobFairsHeld() {
        return jobFairsHeld;
    }

    public void setJobFairsHeld(Double jobFairsHeld) {
        this.jobFairsHeld = jobFairsHeld;
    }

    public Double getEmployersTookPart() {
        return employersTookPart;
    }

    public void setEmployersTookPart(Double employersTookPart) {
        this.employersTookPart = employersTookPart;
    }

    public Double getUnemployed() {
        return unemployed;
    }

    public void setUnemployed(Double unemployed) {
        this.unemployed = unemployed;
    }

    public Double getVacanciesSubmitted() {
        return vacanciesSubmitted;
    }

    public void setVacanciesSubmitted(Double vacanciesSubmitted) {
        this.vacanciesSubmitted = vacanciesSubmitted;
    }

    public Double getPermanentJobs() {
        return permanentJobs;
    }

    public void setPermanentJobs(Double permanentJobs) {
        this.permanentJobs = permanentJobs;
    }
}
