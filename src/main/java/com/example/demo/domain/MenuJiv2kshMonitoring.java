package com.example.demo.domain;

public class MenuJiv2kshMonitoring {

    private String year;            //ush_stat_jivotnovodstvo
    private String area;
    private String region;
    private Double numberLivestockPoultry;

    private Double numberCattleFarms;

    public String getYear() {
        return year;
    }

    public String getArea() {
        return area;
    }

    public String getRegion() {
        return region;
    }

    public Double getNumberLivestockPoultry() {
        return numberLivestockPoultry;
    }

    public Double getNumberCattleFarms() {
        return numberCattleFarms;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setNumberLivestockPoultry(Double numberLivestockPoultry) {
        this.numberLivestockPoultry = numberLivestockPoultry;
    }

    public void setNumberCattleFarms(Double numberCattleFarms) {
        this.numberCattleFarms = numberCattleFarms;
    }
}
