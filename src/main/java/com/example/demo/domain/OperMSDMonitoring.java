package com.example.demo.domain;

public class OperMSDMonitoring {
    //mon_sil_i_sred_dthcs

    private String year;
    private String period;
    private String month;
    private String region;
    private String ruralDistrict;
    private String address;
    private String dateEvent;
    private String typeE;
    private String descriptionSituation;
    private String numberLedgerInformation;
    private String acceptedMeasure;
    private String time;

    public String getYear() {
        return year;
    }

    public String getPeriod() {
        return period;
    }

    public String getMonth() {
        return month;
    }

    public String getRegion() {
        return region;
    }

    public String getRuralDistrict() {
        return ruralDistrict;
    }

    public String getAddress() {
        return address;
    }

    public String getDateEvent() {
        return dateEvent;
    }

    public String getTypeE() {
        return typeE;
    }

    public String getDescriptionSituation() {
        return descriptionSituation;
    }

    public String getNumberLedgerInformation() {
        return numberLedgerInformation;
    }

    public String getAcceptedMeasure() {
        return acceptedMeasure;
    }

    public String getTime() {
        return time;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setRuralDistrict(String ruralDistrict) {
        this.ruralDistrict = ruralDistrict;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setDateEvent(String dateEvent) {
        this.dateEvent = dateEvent;
    }

    public void setTypeE(String typeE) {
        this.typeE = typeE;
    }

    public void setDescriptionSituation(String descriptionSituation) {
        this.descriptionSituation = descriptionSituation;
    }

    public void setNumberLedgerInformation(String numberLedgerInformation) {
        this.numberLedgerInformation = numberLedgerInformation;
    }

    public void setAcceptedMeasure(String acceptedMeasure) {
        this.acceptedMeasure = acceptedMeasure;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
