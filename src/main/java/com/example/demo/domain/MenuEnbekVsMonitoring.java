package com.example.demo.domain;

public class MenuEnbekVsMonitoring {
    private String view;            //gp_enbek_vs
    private Double registeredEntities;
    private Double actors;

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }

    public Double getRegisteredEntities() {
        return registeredEntities;
    }

    public void setRegisteredEntities(Double registeredEntities) {
        this.registeredEntities = registeredEntities;
    }

    public Double getActors() {
        return actors;
    }

    public void setActors(Double actors) {
        this.actors = actors;
    }
}
