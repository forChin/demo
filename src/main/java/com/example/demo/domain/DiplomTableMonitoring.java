package com.example.demo.domain;

public class DiplomTableMonitoring {
    //diplom table

    private String year;
    private String data;
    private Double summ;
    private String unit;
    private Double sbid;

    public String getYear() {
        return year;
    }

    public String getData() {
        return data;
    }

    public Double getSumm() {
        return summ;
    }

    public String getUnit() {
        return unit;
    }

    public Double getSbid() {
        return sbid;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setSumm(Double summ) {
        this.summ = summ;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setSbid(Double sbid) {
        this.sbid = sbid;
    }
}
