package com.example.demo.domain;

public class MenuJiv2kshSGMonitoring {

    private String year;            //ush_stat_jivotnovodstvo
    private String area;
    private String region;
    private Double numberSheepgoatsFarms;

    public String getYear() {
        return year;
    }

    public String getArea() {
        return area;
    }

    public String getRegion() {
        return region;
    }

    public Double getNumberSheepgoatsFarms() {
        return numberSheepgoatsFarms;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setNumberSheepgoatsFarms(Double numberSheepgoatsFarms) {
        this.numberSheepgoatsFarms = numberSheepgoatsFarms;
    }
}
