package com.example.demo.domain;

import java.util.HashMap;
import java.util.Map;

public class NatRhyOlohMonitoring {

    private String year;

    private Map<String, Double> resultsTicket;
    private Map<String, Double> stateRegisContract;

    public NatRhyOlohMonitoring(){
        this.resultsTicket = new HashMap<>();
        this.stateRegisContract = new HashMap<>();
    }

    public String getYear() {
        return year;
    }

    public Map<String, Double> getResultsTicket() {
        return resultsTicket;
    }

    public Map<String, Double> getStateRegisContract() {
        return stateRegisContract;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setResultsTicket(Map<String, Double> resultsTicket) {
        this.resultsTicket = resultsTicket;
    }

    public void setStateRegisContract(Map<String, Double> stateRegisContract) {
        this.stateRegisContract = stateRegisContract;
    }
}
