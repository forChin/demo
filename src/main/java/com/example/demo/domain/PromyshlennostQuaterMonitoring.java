package com.example.demo.domain;

import java.util.HashMap;
import java.util.Map;

public class PromyshlennostQuaterMonitoring {

    private String year;

    private Map<String, Double> volumeProduction;
    private Map<String, Double> numberEmployees;
    private Map<String, Double> sharePopulation;

    public PromyshlennostQuaterMonitoring(){
        this.volumeProduction = new HashMap<>();
        this.numberEmployees = new HashMap<>();
        this.sharePopulation = new HashMap<>();
    }

    public String getYear() {
        return year;
    }

    public Map<String, Double> getVolumeProduction() {
        return volumeProduction;
    }

    public Map<String, Double> getNumberEmployees() {
        return numberEmployees;
    }

    public Map<String, Double> getSharePopulation() {
        return sharePopulation;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setVolumeProduction(Map<String, Double> volumeProduction) {
        this.volumeProduction = volumeProduction;
    }

    public void setNumberEmployees(Map<String, Double> numberEmployees) {
        this.numberEmployees = numberEmployees;
    }

    public void setSharePopulation(Map<String, Double> sharePopulation) {
        this.sharePopulation = sharePopulation;
    }
}
