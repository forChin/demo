package com.example.demo.domain;

public class EnergetikaIJkx2 {

    private String year;
    private String area;
    private String unit;

    private Double centralWatersupply;
    private Double centralWaterdisposal;
    private Double centraiGas;
    private String unitA;
    private Double centralWatersupplya;
    private Double centralWaterdisposala;
    private Double centraiGasa;
    private Double amountHeatenergy;
    private String unitB;
    private Double volumeGasconsumption;
    private String unitC;
    private Double electricityProduction;
    private String unitD;
    private Double volumeWaterconsumption;
    private String unitE;
    private Double consumptionDrinkingwater;
    private String unitF;
    private Double numberSettlements;
    private String unitG;
    private Double numberSettlementsGasified;
    private String unitH;
    private String unitI;
    private Double requiringMajorrepairs;
    private Double renovated;
    private Double emergency;
    private Double decrepit;
    private Double demolished;
    private String unitJ;
    private Integer too;
    private Integer kck;
    private Integer ip;
    private String unitK;
    private Integer electrosupplies;
    private Integer watersupply;
    private Integer gassupply;
    private Double totalLengthHeatnetworks;
    private String unitL;
    private Double totalLengthElectricnetworks;
    private String unitM;
    private Double totalLengthWaterseweragenetworks;
    private String unitN;

    public EnergetikaIJkx2(){

    }

    public String getYear() {
        return year;
    }

    public String getArea() {
        return area;
    }

    public String getUnit() {
        return unit;
    }

    public Double getCentralWatersupply() {
        return centralWatersupply;
    }

    public Double getCentralWaterdisposal() {
        return centralWaterdisposal;
    }

    public Double getCentraiGas() {
        return centraiGas;
    }

    public String getUnitA() {
        return unitA;
    }

    public Double getCentralWatersupplya() {
        return centralWatersupplya;
    }

    public Double getCentralWaterdisposala() {
        return centralWaterdisposala;
    }

    public Double getCentraiGasa() {
        return centraiGasa;
    }

    public Double getAmountHeatenergy() {
        return amountHeatenergy;
    }

    public String getUnitB() {
        return unitB;
    }

    public Double getVolumeGasconsumption() {
        return volumeGasconsumption;
    }

    public String getUnitC() {
        return unitC;
    }

    public Double getElectricityProduction() {
        return electricityProduction;
    }

    public String getUnitD() {
        return unitD;
    }

    public Double getVolumeWaterconsumption() {
        return volumeWaterconsumption;
    }

    public String getUnitE() {
        return unitE;
    }

    public Double getConsumptionDrinkingwater() {
        return consumptionDrinkingwater;
    }

    public String getUnitF() {
        return unitF;
    }

    public Double getNumberSettlements() {
        return numberSettlements;
    }

    public String getUnitG() {
        return unitG;
    }

    public Double getNumberSettlementsGasified() {
        return numberSettlementsGasified;
    }

    public String getUnitH() {
        return unitH;
    }

    public String getUnitI() {
        return unitI;
    }

    public Double getRequiringMajorrepairs() {
        return requiringMajorrepairs;
    }

    public Double getRenovated() {
        return renovated;
    }

    public Double getEmergency() {
        return emergency;
    }

    public Double getDecrepit() {
        return decrepit;
    }

    public Double getDemolished() {
        return demolished;
    }

    public String getUnitJ() {
        return unitJ;
    }

    public Integer getToo() {
        return too;
    }

    public Integer getKck() {
        return kck;
    }

    public Integer getIp() {
        return ip;
    }

    public String getUnitK() {
        return unitK;
    }

    public Integer getElectrosupplies() {
        return electrosupplies;
    }

    public Integer getWatersupply() {
        return watersupply;
    }

    public Integer getGassupply() {
        return gassupply;
    }

    public Double getTotalLengthHeatnetworks() {
        return totalLengthHeatnetworks;
    }

    public String getUnitL() {
        return unitL;
    }

    public Double getTotalLengthElectricnetworks() {
        return totalLengthElectricnetworks;
    }

    public String getUnitM() {
        return unitM;
    }

    public Double getTotalLengthWaterseweragenetworks() {
        return totalLengthWaterseweragenetworks;
    }

    public String getUnitN() {
        return unitN;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setCentralWatersupply(Double centralWatersupply) {
        this.centralWatersupply = centralWatersupply;
    }

    public void setCentralWaterdisposal(Double centralWaterdisposal) {
        this.centralWaterdisposal = centralWaterdisposal;
    }

    public void setCentraiGas(Double centraiGas) {
        this.centraiGas = centraiGas;
    }

    public void setUnitA(String unitA) {
        this.unitA = unitA;
    }

    public void setCentralWatersupplya(Double centralWatersupplya) {
        this.centralWatersupplya = centralWatersupplya;
    }

    public void setCentralWaterdisposala(Double centralWaterdisposala) {
        this.centralWaterdisposala = centralWaterdisposala;
    }

    public void setCentraiGasa(Double centraiGasa) {
        this.centraiGasa = centraiGasa;
    }

    public void setAmountHeatenergy(Double amountHeatenergy) {
        this.amountHeatenergy = amountHeatenergy;
    }

    public void setUnitB(String unitB) {
        this.unitB = unitB;
    }

    public void setVolumeGasconsumption(Double volumeGasconsumption) {
        this.volumeGasconsumption = volumeGasconsumption;
    }

    public void setUnitC(String unitC) {
        this.unitC = unitC;
    }

    public void setElectricityProduction(Double electricityProduction) {
        this.electricityProduction = electricityProduction;
    }

    public void setUnitD(String unitD) {
        this.unitD = unitD;
    }

    public void setVolumeWaterconsumption(Double volumeWaterconsumption) {
        this.volumeWaterconsumption = volumeWaterconsumption;
    }

    public void setUnitE(String unitE) {
        this.unitE = unitE;
    }

    public void setConsumptionDrinkingwater(Double consumptionDrinkingwater) {
        this.consumptionDrinkingwater = consumptionDrinkingwater;
    }

    public void setUnitF(String unitF) {
        this.unitF = unitF;
    }

    public void setNumberSettlements(Double numberSettlements) {
        this.numberSettlements = numberSettlements;
    }

    public void setUnitG(String unitG) {
        this.unitG = unitG;
    }

    public void setNumberSettlementsGasified(Double numberSettlementsGasified) {
        this.numberSettlementsGasified = numberSettlementsGasified;
    }

    public void setUnitH(String unitH) {
        this.unitH = unitH;
    }

    public void setUnitI(String unitI) {
        this.unitI = unitI;
    }

    public void setRequiringMajorrepairs(Double requiringMajorrepairs) {
        this.requiringMajorrepairs = requiringMajorrepairs;
    }

    public void setRenovated(Double renovated) {
        this.renovated = renovated;
    }

    public void setEmergency(Double emergency) {
        this.emergency = emergency;
    }

    public void setDecrepit(Double decrepit) {
        this.decrepit = decrepit;
    }

    public void setDemolished(Double demolished) {
        this.demolished = demolished;
    }

    public void setUnitJ(String unitJ) {
        this.unitJ = unitJ;
    }

    public void setToo(Integer too) {
        this.too = too;
    }

    public void setKck(Integer kck) {
        this.kck = kck;
    }

    public void setIp(Integer ip) {
        this.ip = ip;
    }

    public void setUnitK(String unitK) {
        this.unitK = unitK;
    }

    public void setElectrosupplies(Integer electrosupplies) {
        this.electrosupplies = electrosupplies;
    }

    public void setWatersupply(Integer watersupply) {
        this.watersupply = watersupply;
    }

    public void setGassupply(Integer gassupply) {
        this.gassupply = gassupply;
    }

    public void setTotalLengthHeatnetworks(Double totalLengthHeatnetworks) {
        this.totalLengthHeatnetworks = totalLengthHeatnetworks;
    }

    public void setUnitL(String unitL) {
        this.unitL = unitL;
    }

    public void setTotalLengthElectricnetworks(Double totalLengthElectricnetworks) {
        this.totalLengthElectricnetworks = totalLengthElectricnetworks;
    }

    public void setUnitM(String unitM) {
        this.unitM = unitM;
    }

    public void setTotalLengthWaterseweragenetworks(Double totalLengthWaterseweragenetworks) {
        this.totalLengthWaterseweragenetworks = totalLengthWaterseweragenetworks;
    }

    public void setUnitN(String unitN) {
        this.unitN = unitN;
    }
}
