package com.example.demo.domain;

import java.util.HashMap;
import java.util.Map;

public class MolPhyMonitoring {

    private String year;

    private Map<String, Double> numberSocialProjects;
    private Map<String, Double> numberYoungpeopleInvolvedTeams;
    private Map<String, Double> youthPractice;
    private Map<String, Double> jasOtau;
    private Map<String, Double> affordableHousing;
    private Map<String, Double> withDiplomaVillage;
    private Map<String, Double> shareYouthSocialOrder;
    private Map<String, Double> youthOutreachYouthPolicy;
    private Map<String, Double> numberYoungpeopleInvolvedWork;

    public MolPhyMonitoring(){
        this.affordableHousing = new HashMap<>();
        this.jasOtau = new HashMap<>();
        this.numberSocialProjects = new HashMap<>();
        this.numberYoungpeopleInvolvedTeams = new HashMap<>();
        this.numberYoungpeopleInvolvedWork = new HashMap<>();
        this.shareYouthSocialOrder = new HashMap<>();
        this.withDiplomaVillage = new HashMap<>();
        this.youthOutreachYouthPolicy = new HashMap<>();
        this.youthPractice = new HashMap<>();
    }

    public String getYear() {
        return year;
    }

    public Map<String, Double> getNumberSocialProjects() {
        return numberSocialProjects;
    }

    public Map<String, Double> getNumberYoungpeopleInvolvedTeams() {
        return numberYoungpeopleInvolvedTeams;
    }

    public Map<String, Double> getYouthPractice() {
        return youthPractice;
    }

    public Map<String, Double> getJasOtau() {
        return jasOtau;
    }

    public Map<String, Double> getAffordableHousing() {
        return affordableHousing;
    }

    public Map<String, Double> getWithDiplomaVillage() {
        return withDiplomaVillage;
    }

    public Map<String, Double> getShareYouthSocialOrder() {
        return shareYouthSocialOrder;
    }

    public Map<String, Double> getYouthOutreachYouthPolicy() {
        return youthOutreachYouthPolicy;
    }

    public Map<String, Double> getNumberYoungpeopleInvolvedWork() {
        return numberYoungpeopleInvolvedWork;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setNumberSocialProjects(Map<String, Double> numberSocialProjects) {
        this.numberSocialProjects = numberSocialProjects;
    }

    public void setNumberYoungpeopleInvolvedTeams(Map<String, Double> numberYoungpeopleInvolvedTeams) {
        this.numberYoungpeopleInvolvedTeams = numberYoungpeopleInvolvedTeams;
    }

    public void setYouthPractice(Map<String, Double> youthPractice) {
        this.youthPractice = youthPractice;
    }

    public void setJasOtau(Map<String, Double> jasOtau) {
        this.jasOtau = jasOtau;
    }

    public void setAffordableHousing(Map<String, Double> affordableHousing) {
        this.affordableHousing = affordableHousing;
    }

    public void setWithDiplomaVillage(Map<String, Double> withDiplomaVillage) {
        this.withDiplomaVillage = withDiplomaVillage;
    }

    public void setShareYouthSocialOrder(Map<String, Double> shareYouthSocialOrder) {
        this.shareYouthSocialOrder = shareYouthSocialOrder;
    }

    public void setYouthOutreachYouthPolicy(Map<String, Double> youthOutreachYouthPolicy) {
        this.youthOutreachYouthPolicy = youthOutreachYouthPolicy;
    }

    public void setNumberYoungpeopleInvolvedWork(Map<String, Double> numberYoungpeopleInvolvedWork) {
        this.numberYoungpeopleInvolvedWork = numberYoungpeopleInvolvedWork;
    }
}
