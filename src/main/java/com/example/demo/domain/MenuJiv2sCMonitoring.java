package com.example.demo.domain;

public class MenuJiv2sCMonitoring {

    private String year;            //ush_stat_jivotnovodstvo
    private String area;
    private String region;
    private Double numberCamelsAgricultural;

    public String getYear() {
        return year;
    }

    public String getArea() {
        return area;
    }

    public String getRegion() {
        return region;
    }

    public Double getNumberCamelsAgricultural() {
        return numberCamelsAgricultural;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setNumberCamelsAgricultural(Double numberCamelsAgricultural) {
        this.numberCamelsAgricultural = numberCamelsAgricultural;
    }
}
