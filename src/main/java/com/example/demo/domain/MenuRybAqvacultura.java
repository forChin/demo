package com.example.demo.domain;

public class MenuRybAqvacultura {
    private String year;            //ush_stat_ryb_i_aqva_2
    private String area;
    private String region;
    private Double volumeFishAquaculture;
    private Double volumeIfoFishAquaculture;
    private Double usedAllKindsMaterial;
    private Double fertilizerApplied;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Double getVolumeFishAquaculture() {
        return volumeFishAquaculture;
    }

    public void setVolumeFishAquaculture(Double volumeFishAquaculture) {
        this.volumeFishAquaculture = volumeFishAquaculture;
    }

    public Double getVolumeIfoFishAquaculture() {
        return volumeIfoFishAquaculture;
    }

    public void setVolumeIfoFishAquaculture(Double volumeIfoFishAquaculture) {
        this.volumeIfoFishAquaculture = volumeIfoFishAquaculture;
    }

    public Double getUsedAllKindsMaterial() {
        return usedAllKindsMaterial;
    }

    public void setUsedAllKindsMaterial(Double usedAllKindsMaterial) {
        this.usedAllKindsMaterial = usedAllKindsMaterial;
    }

    public Double getFertilizerApplied() {
        return fertilizerApplied;
    }

    public void setFertilizerApplied(Double fertilizerApplied) {
        this.fertilizerApplied = fertilizerApplied;
    }
}
