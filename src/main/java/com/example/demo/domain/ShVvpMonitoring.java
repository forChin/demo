package com.example.demo.domain;

public class ShVvpMonitoring {
    //sh_vvp_po_kategoriyam

    private String year;
    private String kategory;
    private String pokazatel;
    private String index;

    public String getYear() {
        return year;
    }

    public String getKategory() {
        return kategory;
    }

    public String getPokazatel() {
        return pokazatel;
    }

    public String getIndex() {
        return index;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setKategory(String kategory) {
        this.kategory = kategory;
    }

    public void setPokazatel(String pokazatel) {
        this.pokazatel = pokazatel;
    }

    public void setIndex(String index) {
        this.index = index;
    }
}
