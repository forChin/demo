package com.example.demo.domain;

public class BNumbersMonitoring {
    // new numbers

    private String area;
    private String ruralDistrict;
    private String snp;
    private Integer totalPopulation;
    private Integer totalMans;
    private Integer mans0w16;
    private Integer mans16w30;
    private Integer mans30w63;
    private Integer w63plus;
    private Integer totalWomans;
    private Integer womans0w16;
    private Integer womans16w30;
    private Integer womans30w57;
    private Integer womans57plus;
    private Integer totalKids;
    private Integer kids0w6;
    private Integer kids6w11;
    private Integer kids11w16;
    private Integer kids16plus;
    private Integer borns;
    private Integer deads;
    private Integer arrived;
    private Integer retired;

    public String getArea() {
        return area;
    }

    public String getRuralDistrict() {
        return ruralDistrict;
    }

    public String getSnp() {
        return snp;
    }

    public Integer getTotalPopulation() {
        return totalPopulation;
    }

    public Integer getTotalMans() {
        return totalMans;
    }

    public Integer getMans0w16() {
        return mans0w16;
    }

    public Integer getMans16w30() {
        return mans16w30;
    }

    public Integer getMans30w63() {
        return mans30w63;
    }

    public Integer getW63plus() {
        return w63plus;
    }

    public Integer getTotalWomans() {
        return totalWomans;
    }

    public Integer getWomans0w16() {
        return womans0w16;
    }

    public Integer getWomans16w30() {
        return womans16w30;
    }

    public Integer getWomans30w57() {
        return womans30w57;
    }

    public Integer getWomans57plus() {
        return womans57plus;
    }

    public Integer getTotalKids() {
        return totalKids;
    }

    public Integer getKids0w6() {
        return kids0w6;
    }

    public Integer getKids6w11() {
        return kids6w11;
    }

    public Integer getKids11w16() {
        return kids11w16;
    }

    public Integer getKids16plus() {
        return kids16plus;
    }

    public Integer getBorns() {
        return borns;
    }

    public Integer getDeads() {
        return deads;
    }

    public Integer getArrived() {
        return arrived;
    }

    public Integer getRetired() {
        return retired;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setRuralDistrict(String ruralDistrict) {
        this.ruralDistrict = ruralDistrict;
    }

    public void setSnp(String snp) {
        this.snp = snp;
    }

    public void setTotalPopulation(Integer totalPopulation) {
        this.totalPopulation = totalPopulation;
    }

    public void setTotalMans(Integer totalMans) {
        this.totalMans = totalMans;
    }

    public void setMans0w16(Integer mans0w16) {
        this.mans0w16 = mans0w16;
    }

    public void setMans16w30(Integer mans16w30) {
        this.mans16w30 = mans16w30;
    }

    public void setMans30w63(Integer mans30w63) {
        this.mans30w63 = mans30w63;
    }

    public void setW63plus(Integer w63plus) {
        this.w63plus = w63plus;
    }

    public void setTotalWomans(Integer totalWomans) {
        this.totalWomans = totalWomans;
    }

    public void setWomans0w16(Integer womans0w16) {
        this.womans0w16 = womans0w16;
    }

    public void setWomans16w30(Integer womans16w30) {
        this.womans16w30 = womans16w30;
    }

    public void setWomans30w57(Integer womans30w57) {
        this.womans30w57 = womans30w57;
    }

    public void setWomans57plus(Integer womans57plus) {
        this.womans57plus = womans57plus;
    }

    public void setTotalKids(Integer totalKids) {
        this.totalKids = totalKids;
    }

    public void setKids0w6(Integer kids0w6) {
        this.kids0w6 = kids0w6;
    }

    public void setKids6w11(Integer kids6w11) {
        this.kids6w11 = kids6w11;
    }

    public void setKids11w16(Integer kids11w16) {
        this.kids11w16 = kids11w16;
    }

    public void setKids16plus(Integer kids16plus) {
        this.kids16plus = kids16plus;
    }

    public void setBorns(Integer borns) {
        this.borns = borns;
    }

    public void setDeads(Integer deads) {
        this.deads = deads;
    }

    public void setArrived(Integer arrived) {
        this.arrived = arrived;
    }

    public void setRetired(Integer retired) {
        this.retired = retired;
    }
}
