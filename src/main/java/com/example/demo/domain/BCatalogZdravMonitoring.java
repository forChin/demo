package com.example.demo.domain;

public class BCatalogZdravMonitoring {
    // new catalog zdrav

    private Double ot;
    private Double doo;
    private Double number;
    private String object;

    public Double getOt() {
        return ot;
    }

    public Double getDoo() {
        return doo;
    }

    public Double getNumber() {
        return number;
    }

    public String getObject() {
        return object;
    }

    public void setOt(Double ot) {
        this.ot = ot;
    }

    public void setDoo(Double doo) {
        this.doo = doo;
    }

    public void setNumber(Double number) {
        this.number = number;
    }

    public void setObject(String object) {
        this.object = object;
    }
}
