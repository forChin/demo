package com.example.demo.domain;

public class StateLaborInspectorateOktoMonitoring {


    private String year;
    private String month;

    private Double administrativeFinesImposed; // o_administrative_fines_imposed | Наложено администратиынфх штрафов
	private Double identificationUntimelyPayment; // o_identification_untimely_payment | Выявление фактов несвоевременной выплаты заработной платы
	private Double socialTension; // o_social_tension | Социальная напряженность в трудовых коллективах

    public StateLaborInspectorateOktoMonitoring(){

    }

    public String getYear() {
        return year;
    }

    public String getMonth() {
        return month;
    }

    public Double getAdministrativeFinesImposed() {
        return administrativeFinesImposed;
    }

    public Double getIdentificationUntimelyPayment() {
        return identificationUntimelyPayment;
    }

    public Double getSocialTension() {
        return socialTension;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public void setAdministrativeFinesImposed(Double administrativeFinesImposed) {
        this.administrativeFinesImposed = administrativeFinesImposed;
    }

    public void setIdentificationUntimelyPayment(Double identificationUntimelyPayment) {
        this.identificationUntimelyPayment = identificationUntimelyPayment;
    }

    public void setSocialTension(Double socialTension) {
        this.socialTension = socialTension;
    }
}
