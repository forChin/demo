package com.example.demo.domain;

public class MenuVetTableMonitoring {
    private Double vac;         //ush_stat_veterinariya_table
    private String type;
    private String year;
    private String region;


    public Double getVac() {
        return vac;
    }

    public void setVac(Double vac) {
        this.vac = vac;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getYear() {
        return year;
    }

    public String getRegion() {
        return region;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setRegion(String region) {
        this.region = region;
    }
}
