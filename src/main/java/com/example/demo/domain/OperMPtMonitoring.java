package com.example.demo.domain;

public class OperMPtMonitoring {
    //oper_mon_prod_tov

    private String area;
    private String year;
    private String month;
    private String period;
    private String dateEvent;
    private String unit;
    private Integer wheatFlourFirstGrade;
    private Integer wheatBread;
    private Integer horns;
    private Integer buckwheat;
    private Integer polishedRice;
    private Integer potato;
    private Integer carrots;
    private Integer onion;
    private Integer cabbageWhite;
    private Integer sugar;
    private Integer oil;
    private Integer beef;
    private Integer chickenMeat;
    private Integer milk;
    private Integer kefir;
    private Integer butter;
    private Integer egg;
    private Integer salt;
    private Integer cottageCheese;

    public String getArea() {
        return area;
    }

    public String getYear() {
        return year;
    }

    public String getMonth() {
        return month;
    }

    public String getPeriod() {
        return period;
    }

    public String getDateEvent() {
        return dateEvent;
    }

    public String getUnit() {
        return unit;
    }

    public Integer getWheatFlourFirstGrade() {
        return wheatFlourFirstGrade;
    }

    public Integer getWheatBread() {
        return wheatBread;
    }

    public Integer getHorns() {
        return horns;
    }

    public Integer getBuckwheat() {
        return buckwheat;
    }

    public Integer getPolishedRice() {
        return polishedRice;
    }

    public Integer getPotato() {
        return potato;
    }

    public Integer getCarrots() {
        return carrots;
    }

    public Integer getOnion() {
        return onion;
    }

    public Integer getCabbageWhite() {
        return cabbageWhite;
    }

    public Integer getSugar() {
        return sugar;
    }

    public Integer getOil() {
        return oil;
    }

    public Integer getBeef() {
        return beef;
    }

    public Integer getChickenMeat() {
        return chickenMeat;
    }

    public Integer getMilk() {
        return milk;
    }

    public Integer getKefir() {
        return kefir;
    }

    public Integer getButter() {
        return butter;
    }

    public Integer getEgg() {
        return egg;
    }

    public Integer getSalt() {
        return salt;
    }

    public Integer getCottageCheese() {
        return cottageCheese;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public void setDateEvent(String dateEvent) {
        this.dateEvent = dateEvent;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setWheatFlourFirstGrade(Integer wheatFlourFirstGrade) {
        this.wheatFlourFirstGrade = wheatFlourFirstGrade;
    }

    public void setWheatBread(Integer wheatBread) {
        this.wheatBread = wheatBread;
    }

    public void setHorns(Integer horns) {
        this.horns = horns;
    }

    public void setBuckwheat(Integer buckwheat) {
        this.buckwheat = buckwheat;
    }

    public void setPolishedRice(Integer polishedRice) {
        this.polishedRice = polishedRice;
    }

    public void setPotato(Integer potato) {
        this.potato = potato;
    }

    public void setCarrots(Integer carrots) {
        this.carrots = carrots;
    }

    public void setOnion(Integer onion) {
        this.onion = onion;
    }

    public void setCabbageWhite(Integer cabbageWhite) {
        this.cabbageWhite = cabbageWhite;
    }

    public void setSugar(Integer sugar) {
        this.sugar = sugar;
    }

    public void setOil(Integer oil) {
        this.oil = oil;
    }

    public void setBeef(Integer beef) {
        this.beef = beef;
    }

    public void setChickenMeat(Integer chickenMeat) {
        this.chickenMeat = chickenMeat;
    }

    public void setMilk(Integer milk) {
        this.milk = milk;
    }

    public void setKefir(Integer kefir) {
        this.kefir = kefir;
    }

    public void setButter(Integer butter) {
        this.butter = butter;
    }

    public void setEgg(Integer egg) {
        this.egg = egg;
    }

    public void setSalt(Integer salt) {
        this.salt = salt;
    }

    public void setCottageCheese(Integer cottageCheese) {
        this.cottageCheese = cottageCheese;
    }
}
