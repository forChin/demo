package com.example.demo.domain;

public class MenuTKRT1allMonitoring {

    private Double inte;
    private String name;
    private String region;
    private String year;

    public String getYear() {
        return year;
    }

    public String getRegion() {
        return region;
    }

    public Double getInte() {
        return inte;
    }

    public String getName() {
        return name;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setInte(Double inte) {
        this.inte = inte;
    }

    public void setName(String name) {
        this.name = name;
    }
}
