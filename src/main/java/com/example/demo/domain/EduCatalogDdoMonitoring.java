package com.example.demo.domain;

public class EduCatalogDdoMonitoring {

    // new edu catalog ddo points

    private String index;
    private Double shareInfluence;
    private Double point;

    public String getIndex() {
        return index;
    }

    public Double getShareInfluence() {
        return shareInfluence;
    }

    public Double getPoint() {
        return point;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public void setShareInfluence(Double shareInfluence) {
        this.shareInfluence = shareInfluence;
    }

    public void setPoint(Double point) {
        this.point = point;
    }
}
