package com.example.demo.domain;

public class EduSchoolNeedMonitoring {

    // new edu school need

    private String year;
    private String raion;
    private String ruralDitrict;
    private String snp;
    private String schoolName;
    private String need;
    private Double count;
    private String languageInstruction;

    public String getYear() {
        return year;
    }

    public String getRaion() {
        return raion;
    }

    public String getRuralDitrict() {
        return ruralDitrict;
    }

    public String getSnp() {
        return snp;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public String getNeed() {
        return need;
    }

    public Double getCount() {
        return count;
    }

    public String getLanguageInstruction() {
        return languageInstruction;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setRaion(String raion) {
        this.raion = raion;
    }

    public void setRuralDitrict(String ruralDitrict) {
        this.ruralDitrict = ruralDitrict;
    }

    public void setSnp(String snp) {
        this.snp = snp;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public void setNeed(String need) {
        this.need = need;
    }

    public void setCount(Double count) {
        this.count = count;
    }

    public void setLanguageInstruction(String languageInstruction) {
        this.languageInstruction = languageInstruction;
    }
}
