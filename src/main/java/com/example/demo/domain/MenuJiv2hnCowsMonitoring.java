package com.example.demo.domain;

public class MenuJiv2hnCowsMonitoring {

    private String year;            //ush_stat_jivotnovodstvo
    private String area;
    private String region;
    private Double numberCowsHouseholds;

    public String getYear() {
        return year;
    }

    public String getArea() {
        return area;
    }

    public String getRegion() {
        return region;
    }

    public Double getNumberCowsHouseholds() {
        return numberCowsHouseholds;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setNumberCowsHouseholds(Double numberCowsHouseholds) {
        this.numberCowsHouseholds = numberCowsHouseholds;
    }
}
