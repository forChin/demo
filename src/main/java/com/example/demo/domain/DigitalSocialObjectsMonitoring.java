package com.example.demo.domain;

public class DigitalSocialObjectsMonitoring {
    // social objects

    private String name;
    private String industry;
    private String gategory;
    private String yearStart;
    private String statusProject;
    private String yearEnd;
    private Double power;
    private String unit;
    private Integer numerObjects;
    private String budgetProgram;
    private String customer;
    private String contractor;
    private String region;
    private String ruralDistrict;
    private String ruralSettlement;
    private String latitude;
    private String longitude;
    private Integer numberPopulation;
    private Integer numberWomen;
    private Integer numberMan;
    private Integer numerChildren;
    private Double totalProjectCost;
    private String unitProject;
    private String year;
    private String yearA;
    private Double localBudget;
    private Double localBudgetA;
    private Double republicanBudget;
    private Double republicanBudgetA;
    private Double other;
    private Double otherA;
    private String source;
    private String sourceA;
    private String unitA;
    private String unitAA;
    private String yearB;
    private String yearC;
    private Double localBudgetB;
    private Double localBudgetC;
    private Double republicanBudgetB;
    private Double republicanBudgetC;
    private Double otherB;
    private Double otherC;
    private String sourceB;
    private String sourceC;
    private String unitAB;
    private String unitAC;
    private String yearD;
    private String yearE;
    private Double localBudgetD;
    private Double localBudgetE;
    private Double republicanBudgetD;
    private Double republicanBudgetE;
    private Double otherD;
    private Double otherE;
    private String sourceD;
    private String sourceE;
    private String unitAD;
    private String unitAE;

    public void setYearA(String yearA) {
        this.yearA = yearA;
    }

    public void setLocalBudgetA(Double localBudgetA) {
        this.localBudgetA = localBudgetA;
    }

    public void setRepublicanBudgetA(Double republicanBudgetA) {
        this.republicanBudgetA = republicanBudgetA;
    }

    public void setOtherA(Double otherA) {
        this.otherA = otherA;
    }

    public void setSourceA(String sourceA) {
        this.sourceA = sourceA;
    }

    public void setUnitAA(String unitAA) {
        this.unitAA = unitAA;
    }

    public void setYearB(String yearB) {
        this.yearB = yearB;
    }

    public void setYearC(String yearC) {
        this.yearC = yearC;
    }

    public void setLocalBudgetB(Double localBudgetB) {
        this.localBudgetB = localBudgetB;
    }

    public void setLocalBudgetC(Double localBudgetC) {
        this.localBudgetC = localBudgetC;
    }

    public void setRepublicanBudgetB(Double republicanBudgetB) {
        this.republicanBudgetB = republicanBudgetB;
    }

    public void setRepublicanBudgetC(Double republicanBudgetC) {
        this.republicanBudgetC = republicanBudgetC;
    }

    public void setOtherB(Double otherB) {
        this.otherB = otherB;
    }

    public void setOtherC(Double otherC) {
        this.otherC = otherC;
    }

    public void setSourceB(String sourceB) {
        this.sourceB = sourceB;
    }

    public void setSourceC(String sourceC) {
        this.sourceC = sourceC;
    }

    public void setUnitAB(String unitAB) {
        this.unitAB = unitAB;
    }

    public void setUnitAC(String unitAC) {
        this.unitAC = unitAC;
    }

    public void setYearD(String yearD) {
        this.yearD = yearD;
    }

    public void setYearE(String yearE) {
        this.yearE = yearE;
    }

    public void setLocalBudgetD(Double localBudgetD) {
        this.localBudgetD = localBudgetD;
    }

    public void setLocalBudgetE(Double localBudgetE) {
        this.localBudgetE = localBudgetE;
    }

    public void setRepublicanBudgetD(Double republicanBudgetD) {
        this.republicanBudgetD = republicanBudgetD;
    }

    public void setRepublicanBudgetE(Double republicanBudgetE) {
        this.republicanBudgetE = republicanBudgetE;
    }

    public void setOtherD(Double otherD) {
        this.otherD = otherD;
    }

    public void setOtherE(Double otherE) {
        this.otherE = otherE;
    }

    public void setSourceD(String sourceD) {
        this.sourceD = sourceD;
    }

    public void setSourceE(String sourceE) {
        this.sourceE = sourceE;
    }

    public void setUnitAD(String unitAD) {
        this.unitAD = unitAD;
    }

    public void setUnitAE(String unitAE) {
        this.unitAE = unitAE;
    }

    public String getYearA() {
        return yearA;
    }

    public Double getLocalBudgetA() {
        return localBudgetA;
    }

    public Double getRepublicanBudgetA() {
        return republicanBudgetA;
    }

    public Double getOtherA() {
        return otherA;
    }

    public String getSourceA() {
        return sourceA;
    }

    public String getUnitAA() {
        return unitAA;
    }

    public String getYearB() {
        return yearB;
    }

    public String getYearC() {
        return yearC;
    }

    public Double getLocalBudgetB() {
        return localBudgetB;
    }

    public Double getLocalBudgetC() {
        return localBudgetC;
    }

    public Double getRepublicanBudgetB() {
        return republicanBudgetB;
    }

    public Double getRepublicanBudgetC() {
        return republicanBudgetC;
    }

    public Double getOtherB() {
        return otherB;
    }

    public Double getOtherC() {
        return otherC;
    }

    public String getSourceB() {
        return sourceB;
    }

    public String getSourceC() {
        return sourceC;
    }

    public String getUnitAB() {
        return unitAB;
    }

    public String getUnitAC() {
        return unitAC;
    }

    public String getYearD() {
        return yearD;
    }

    public String getYearE() {
        return yearE;
    }

    public Double getLocalBudgetD() {
        return localBudgetD;
    }

    public Double getLocalBudgetE() {
        return localBudgetE;
    }

    public Double getRepublicanBudgetD() {
        return republicanBudgetD;
    }

    public Double getRepublicanBudgetE() {
        return republicanBudgetE;
    }

    public Double getOtherD() {
        return otherD;
    }

    public Double getOtherE() {
        return otherE;
    }

    public String getSourceD() {
        return sourceD;
    }

    public String getSourceE() {
        return sourceE;
    }

    public String getUnitAD() {
        return unitAD;
    }

    public String getUnitAE() {
        return unitAE;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setLocalBudget(Double localBudget) {
        this.localBudget = localBudget;
    }

    public void setRepublicanBudget(Double republicanBudget) {
        this.republicanBudget = republicanBudget;
    }

    public void setOther(Double other) {
        this.other = other;
    }

    public void setUnitA(String unitA) {
        this.unitA = unitA;
    }

    public String getYear() {
        return year;
    }

    public Double getLocalBudget() {
        return localBudget;
    }

    public Double getRepublicanBudget() {
        return republicanBudget;
    }

    public Double getOther() {
        return other;
    }

    public String getUnitA() {
        return unitA;
    }

    public String getName() {
        return name;
    }

    public String getIndustry() {
        return industry;
    }

    public String getGategory() {
        return gategory;
    }

    public String getYearStart() {
        return yearStart;
    }

    public String getStatusProject() {
        return statusProject;
    }

    public String getYearEnd() {
        return yearEnd;
    }

    public Double getPower() {
        return power;
    }

    public String getUnit() {
        return unit;
    }

    public Integer getNumerObjects() {
        return numerObjects;
    }

    public String getBudgetProgram() {
        return budgetProgram;
    }

    public String getCustomer() {
        return customer;
    }

    public String getContractor() {
        return contractor;
    }

    public String getRegion() {
        return region;
    }

    public String getRuralDistrict() {
        return ruralDistrict;
    }

    public String getRuralSettlement() {
        return ruralSettlement;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public Integer getNumberPopulation() {
        return numberPopulation;
    }

    public Integer getNumberWomen() {
        return numberWomen;
    }

    public Integer getNumberMan() {
        return numberMan;
    }

    public Integer getNumerChildren() {
        return numerChildren;
    }

    public Double getTotalProjectCost() {
        return totalProjectCost;
    }

    public String getUnitProject() {
        return unitProject;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public void setGategory(String gategory) {
        this.gategory = gategory;
    }

    public void setYearStart(String yearStart) {
        this.yearStart = yearStart;
    }

    public void setStatusProject(String statusProject) {
        this.statusProject = statusProject;
    }

    public void setYearEnd(String yearEnd) {
        this.yearEnd = yearEnd;
    }

    public void setPower(Double power) {
        this.power = power;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setNumerObjects(Integer numerObjects) {
        this.numerObjects = numerObjects;
    }

    public void setBudgetProgram(String budgetProgram) {
        this.budgetProgram = budgetProgram;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public void setContractor(String contractor) {
        this.contractor = contractor;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setRuralDistrict(String ruralDistrict) {
        this.ruralDistrict = ruralDistrict;
    }

    public void setRuralSettlement(String ruralSettlement) {
        this.ruralSettlement = ruralSettlement;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setNumberPopulation(Integer numberPopulation) {
        this.numberPopulation = numberPopulation;
    }

    public void setNumberWomen(Integer numberWomen) {
        this.numberWomen = numberWomen;
    }

    public void setNumberMan(Integer numberMan) {
        this.numberMan = numberMan;
    }

    public void setNumerChildren(Integer numerChildren) {
        this.numerChildren = numerChildren;
    }

    public void setTotalProjectCost(Double totalProjectCost) {
        this.totalProjectCost = totalProjectCost;
    }

    public void setUnitProject(String unitProject) {
        this.unitProject = unitProject;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
