package com.example.demo.domain;

import java.util.HashMap;
import java.util.Map;

public class NaturalResQuarterOlohMonitoring {

    private String year;

    private Map<String, Double> issuanceTickets;

    public NaturalResQuarterOlohMonitoring(){
        this.issuanceTickets = new HashMap<>();
    }

    public String getYear() {
        return year;
    }

    public Map<String, Double> getIssuanceTickets() {
        return issuanceTickets;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setIssuanceTickets(Map<String, Double> issuanceTickets) {
        this.issuanceTickets = issuanceTickets;
    }
}
