package com.example.demo.domain;

public class NatMonthOnivhMonitoring {

    //sed_m_uprirp_onvh

    private String year;
    private String month;
    private Integer conclusionMinerals;


    public String getYear() {
        return year;
    }

    public String getMonth() {
        return month;
    }

    public Integer getConclusionMinerals() {
        return conclusionMinerals;
    }


    public void setYear(String year) {
        this.year = year;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public void setConclusionMinerals(Integer conclusionMinerals) {
        this.conclusionMinerals = conclusionMinerals;
    }
}
