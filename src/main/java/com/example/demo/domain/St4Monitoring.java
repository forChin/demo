package com.example.demo.domain;

public class St4Monitoring {
    //strateg_upr_4

    private String indikator1;
    private String indikator2;
    private String corr;
    private String stepenVl;
    private String napravVl;

    public String getIndikator1() {
        return indikator1;
    }

    public String getIndikator2() {
        return indikator2;
    }

    public String getCorr() {
        return corr;
    }

    public String getStepenVl() {
        return stepenVl;
    }

    public String getNapravVl() {
        return napravVl;
    }

    public void setIndikator1(String indikator1) {
        this.indikator1 = indikator1;
    }

    public void setIndikator2(String indikator2) {
        this.indikator2 = indikator2;
    }

    public void setCorr(String corr) {
        this.corr = corr;
    }

    public void setStepenVl(String stepenVl) {
        this.stepenVl = stepenVl;
    }

    public void setNapravVl(String napravVl) {
        this.napravVl = napravVl;
    }
}
