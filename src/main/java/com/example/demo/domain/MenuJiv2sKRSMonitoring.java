package com.example.demo.domain;

public class MenuJiv2sKRSMonitoring {

    private String year;            //ush_stat_jivotnovodstvo
    private String area;
    private String region;
    private Double numberCattleAgricultural;

    public String getYear() {
        return year;
    }

    public String getArea() {
        return area;
    }

    public String getRegion() {
        return region;
    }

    public Double getNumberCattleAgricultural() {
        return numberCattleAgricultural;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setNumberCattleAgricultural(Double numberCattleAgricultural) {
        this.numberCattleAgricultural = numberCattleAgricultural;
    }
}
