package com.example.demo.domain;

import java.util.HashMap;
import java.util.Map;

public class PromQuaterOropMonitoring {

    private String year;

    private Map<String, Double> growthNonoilExports;

    public PromQuaterOropMonitoring(){
        this.growthNonoilExports = new HashMap<>();
    }

    public String getYear() {
        return year;
    }

    public Map<String, Double> getGrowthNonoilExports() {
        return growthNonoilExports;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setGrowthNonoilExports(Map<String, Double> growthNonoilExports) {
        this.growthNonoilExports = growthNonoilExports;
    }

}
