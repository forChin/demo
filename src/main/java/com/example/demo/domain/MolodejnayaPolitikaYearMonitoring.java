package com.example.demo.domain;

public class MolodejnayaPolitikaYearMonitoring {

    private String year;
    private Integer youthPublicAssociationsRegion;
    private Integer youthOutreachActivities;
    private Integer youthInvolvedWork;
    private Integer numberYoungpeopleRegion;
    private Integer women;
    private Integer men;
    private Integer ruralUrbanYouth;
    private Integer urbanYouth;
    private Integer ruralYouth;
    private Double ruralYouthpercent;
    private Double urbanYouthPercent;

    public MolodejnayaPolitikaYearMonitoring(){

    }

    public String getYear() {
        return year;
    }

    public Integer getYouthPublicAssociationsRegion() {
        return youthPublicAssociationsRegion;
    }

    public Integer getYouthOutreachActivities() {
        return youthOutreachActivities;
    }

    public Integer getYouthInvolvedWork() {
        return youthInvolvedWork;
    }

    public Integer getNumberYoungpeopleRegion() {
        return numberYoungpeopleRegion;
    }

    public Integer getWomen() {
        return women;
    }

    public Integer getMen() {
        return men;
    }

    public Integer getRuralUrbanYouth() {
        return ruralUrbanYouth;
    }

    public Integer getUrbanYouth() {
        return urbanYouth;
    }

    public Integer getRuralYouth() {
        return ruralYouth;
    }

    public Double getRuralYouthpercent() {
        return ruralYouthpercent;
    }

    public Double getUrbanYouthPercent() {
        return urbanYouthPercent;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setYouthPublicAssociationsRegio(Integer youthPublicAssociationsRegion) {
        this.youthPublicAssociationsRegion = youthPublicAssociationsRegion;
    }

    public void setYouthOutreachActivities(Integer youthOutreachActivities) {
        this.youthOutreachActivities = youthOutreachActivities;
    }

    public void setYouthInvolvedWork(Integer youthInvolvedWork) {
        this.youthInvolvedWork = youthInvolvedWork;
    }

    public void setNumberYoungpeopleRegion(Integer numberYoungpeopleRegion) {
        this.numberYoungpeopleRegion = numberYoungpeopleRegion;
    }

    public void setWomen(Integer women) {
        this.women = women;
    }

    public void setMen(Integer men) {
        this.men = men;
    }

    public void setRuralUrbanYouth(Integer ruralUrbanYouth) {
        this.ruralUrbanYouth = ruralUrbanYouth;
    }

    public void setUrbanYouth(Integer urbanYouth) {
        this.urbanYouth = urbanYouth;
    }

    public void setRuralYouth(Integer ruralYouth) {
        this.ruralYouth = ruralYouth;
    }

    public void setRuralYouthpercent(Double ruralYouthpercent) {
        this.ruralYouthpercent = ruralYouthpercent;
    }

    public void setUrbanYouthPercent(Double urbanYouthPercent) {
        this.urbanYouthPercent = urbanYouthPercent;
    }

}
