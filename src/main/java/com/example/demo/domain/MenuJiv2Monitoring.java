package com.example.demo.domain;

public class MenuJiv2Monitoring {

    private String year;            //ush_stat_jivotnovodstvo
    private String area;
    private String region;
    private Double numberLivestockPoultry;

    private Double numberCattleFarms;
    private Double numberCowsFarms;
    private Double numberSheepgoatsFarms;
    private Double numberPigsFarms;
    private Double numberHorsesFarms;
    private Double numberCamelsFarms;
    private Double numberBirdsFarms;
    private Double calvesYieldFarms;
    private Double outputLambsFarms;
    private Double outletKidsFarms;
    private Double outputFoalsFarms;
    private Double outputCamelscoltsFarms;

    public String getYear() {
        return year;
    }

    public String getArea() {
        return area;
    }

    public String getRegion() {
        return region;
    }

    public Double getNumberLivestockPoultry() {
        return numberLivestockPoultry;
    }

    public Double getNumberCattleFarms() {
        return numberCattleFarms;
    }

    public Double getNumberCowsFarms() {
        return numberCowsFarms;
    }

    public Double getNumberSheepgoatsFarms() {
        return numberSheepgoatsFarms;
    }

    public Double getNumberPigsFarms() {
        return numberPigsFarms;
    }

    public Double getNumberHorsesFarms() {
        return numberHorsesFarms;
    }

    public Double getNumberCamelsFarms() {
        return numberCamelsFarms;
    }

    public Double getNumberBirdsFarms() {
        return numberBirdsFarms;
    }

    public Double getCalvesYieldFarms() {
        return calvesYieldFarms;
    }

    public Double getOutputLambsFarms() {
        return outputLambsFarms;
    }

    public Double getOutletKidsFarms() {
        return outletKidsFarms;
    }

    public Double getOutputFoalsFarms() {
        return outputFoalsFarms;
    }

    public Double getOutputCamelscoltsFarms() {
        return outputCamelscoltsFarms;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setNumberLivestockPoultry(Double numberLivestockPoultry) {
        this.numberLivestockPoultry = numberLivestockPoultry;
    }

    public void setNumberCattleFarms(Double numberCattleFarms) {
        this.numberCattleFarms = numberCattleFarms;
    }

    public void setNumberCowsFarms(Double numberCowsFarms) {
        this.numberCowsFarms = numberCowsFarms;
    }

    public void setNumberSheepgoatsFarms(Double numberSheepgoatsFarms) {
        this.numberSheepgoatsFarms = numberSheepgoatsFarms;
    }

    public void setNumberPigsFarms(Double numberPigsFarms) {
        this.numberPigsFarms = numberPigsFarms;
    }

    public void setNumberHorsesFarms(Double numberHorsesFarms) {
        this.numberHorsesFarms = numberHorsesFarms;
    }

    public void setNumberCamelsFarms(Double numberCamelsFarms) {
        this.numberCamelsFarms = numberCamelsFarms;
    }

    public void setNumberBirdsFarms(Double numberBirdsFarms) {
        this.numberBirdsFarms = numberBirdsFarms;
    }

    public void setCalvesYieldFarms(Double calvesYieldFarms) {
        this.calvesYieldFarms = calvesYieldFarms;
    }

    public void setOutputLambsFarms(Double outputLambsFarms) {
        this.outputLambsFarms = outputLambsFarms;
    }

    public void setOutletKidsFarms(Double outletKidsFarms) {
        this.outletKidsFarms = outletKidsFarms;
    }

    public void setOutputFoalsFarms(Double outputFoalsFarms) {
        this.outputFoalsFarms = outputFoalsFarms;
    }

    public void setOutputCamelscoltsFarms(Double outputCamelscoltsFarms) {
        this.outputCamelscoltsFarms = outputCamelscoltsFarms;
    }
}
