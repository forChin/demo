package com.example.demo.domain;

public class MenuBipDorogiMonitoring {

    private String nameProject;     //bip_dorogi
    private String statusProject;
    private Double totalProjectCoast;
    private Double allocatedAmountCurrentMoment;
    private Double length;
    private String unit;
    private String industry;
    private String instruction;
    private String imageProject;
    private String roadType;
    private String region;
    private String roadSnp;

    public String getNameProject() {
        return nameProject;
    }

    public void setNameProject(String nameProject) {
        this.nameProject = nameProject;
    }

    public String getStatusProject() {
        return statusProject;
    }

    public void setStatusProject(String statusProject) {
        this.statusProject = statusProject;
    }

    public Double getTotalProjectCoast() {
        return totalProjectCoast;
    }

    public void setTotalProjectCoast(Double totalProjectCoast) {
        this.totalProjectCoast = totalProjectCoast;
    }

    public Double getAllocatedAmountCurrentMoment() {
        return allocatedAmountCurrentMoment;
    }

    public void setAllocatedAmountCurrentMoment(Double allocatedAmountCurrentMoment) {
        this.allocatedAmountCurrentMoment = allocatedAmountCurrentMoment;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public String getImageProject() {
        return imageProject;
    }

    public void setImageProject(String imageProject) {
        this.imageProject = imageProject;
    }

    public String getRoadType() {
        return roadType;
    }

    public void setRoadType(String roadType) {
        this.roadType = roadType;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRoadSnp() {
        return roadSnp;
    }

    public void setRoadSnp(String roadSnp) {
        this.roadSnp = roadSnp;
    }
}
