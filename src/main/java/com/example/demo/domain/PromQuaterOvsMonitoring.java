package com.example.demo.domain;

import java.util.HashMap;
import java.util.Map;

public class PromQuaterOvsMonitoring {

    private String year;

    private Map<String, Double> retailTurnover;
    private Map<String, Double> foreignTradeTurnover;
    private Map<String, Double> export;
    private Map<String, Double> importt;
    private Map<String, Double> tradeWithCountries;
    private Map<String, Double> exportsToCountries;
    private Map<String, Double> importToCountries;

    public PromQuaterOvsMonitoring(){
        this.retailTurnover = new HashMap<>();
        this.foreignTradeTurnover = new HashMap<>();
        this.export = new HashMap<>();
        this.importt = new HashMap<>();
        this.tradeWithCountries = new HashMap<>();
        this.exportsToCountries = new HashMap<>();
        this.importToCountries = new HashMap<>();
    }

    public String getYear() {
        return year;
    }

    public Map<String, Double> getRetailTurnover() {
        return retailTurnover;
    }

    public Map<String, Double> getForeignTradeTurnover() {
        return foreignTradeTurnover;
    }

    public Map<String, Double> getExport() {
        return export;
    }

    public Map<String, Double> getImportt() {
        return importt;
    }

    public Map<String, Double> getTradeWithCountries() {
        return tradeWithCountries;
    }

    public Map<String, Double> getExportsToCountries() {
        return exportsToCountries;
    }

    public Map<String, Double> getImportToCountries() {
        return importToCountries;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setRetailTurnover(Map<String, Double> retailTurnover) {
        this.retailTurnover = retailTurnover;
    }

    public void setForeignTradeTurnover(Map<String, Double> foreignTradeTurnover) {
        this.foreignTradeTurnover = foreignTradeTurnover;
    }

    public void setExport(Map<String, Double> export) {
        this.export = export;
    }

    public void setImportt(Map<String, Double> importt) {
        this.importt = importt;
    }

    public void setTradeWithCountries(Map<String, Double> tradeWithCountries) {
        this.tradeWithCountries = tradeWithCountries;
    }

    public void setExportsToCountries(Map<String, Double> exportsToCountries) {
        this.exportsToCountries = exportsToCountries;
    }

    public void setImportToCountries(Map<String, Double> importToCountries) {
        this.importToCountries = importToCountries;
    }
}
