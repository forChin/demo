package com.example.demo.domain;

import java.util.HashMap;
import java.util.Map;

public class InternalPQMonitoring {

    private String year;

    private Map<String, Double> positiveTone;
    private Map<String, Double> negativeTone;
    private Map<String, Double> positiveToneA;
    private Map<String, Double> negativeToneA;
    private Map<String, Double> numberPositiveNewspaperArticles;

    public InternalPQMonitoring(){
        this.positiveTone = new HashMap<>();
        this.negativeTone = new HashMap<>();
        this.positiveToneA = new HashMap<>();
        this.negativeToneA = new HashMap<>();
        this.numberPositiveNewspaperArticles = new HashMap<>();
    }

    public String getYear() {
        return year;
    }

    public Map<String, Double> getPositiveTone() {
        return positiveTone;
    }

    public Map<String, Double> getNegativeTone() {
        return negativeTone;
    }

    public Map<String, Double> getPositiveToneA() {
        return positiveToneA;
    }

    public Map<String, Double> getNegativeToneA() {
        return negativeToneA;
    }

    public Map<String, Double> getNumberPositiveNewspaperArticles() {
        return numberPositiveNewspaperArticles;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setPositiveTone(Map<String, Double> positiveTone) {
        this.positiveTone = positiveTone;
    }

    public void setNegativeTone(Map<String, Double> negativeTone) {
        this.negativeTone = negativeTone;
    }

    public void setPositiveToneA(Map<String, Double> positiveToneA) {
        this.positiveToneA = positiveToneA;
    }

    public void setNegativeToneA(Map<String, Double> negativeToneA) {
        this.negativeToneA = negativeToneA;
    }

    public void setNumberPositiveNewspaperArticles(Map<String, Double> numberPositiveNewspaperArticles) {
        this.numberPositiveNewspaperArticles = numberPositiveNewspaperArticles;
    }
}
