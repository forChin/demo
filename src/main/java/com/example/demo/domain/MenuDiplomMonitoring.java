package com.example.demo.domain;

public class MenuDiplomMonitoring {

    private String lastname;            //diplom
    private String firstname;
    private String otchestvo;
    private String idPerson;
    private String pol;
    private Integer age;
    private String areaFrom;
    private String regionFrom;
    private String ruralDistrictFrom;
    private String typeEducation;
    private String profession;
    private String profession_rus;
    private String profession_kz;
    private String educationalInstruction;
    private String areaIn;
    private String regionIn;
    private String ruralDistrictIn;
    private String ruralSettlementIn;
    private String workSpace;
    private String fieldActivity;
    private String activity;
    private String statusEmployee;
    private String commissionDecision;
    private String contractNumber;
    private Double sumGrant;
    private String unit;
    private String needReturn;
    private Integer refund;
    private String reasonReturn;
    private Double contractSumm;
    private String unitCredit;
    private Double balanceCredit;
    private String reasonForRefund;
    private String reason_project_completion;
    private String contractNumberA;
    private String dateBirthday;
    private String dateGraduation;
    private String arrivalDate;
    private String dateDismissalTransfer;
    private String dateContract;
    private String dateReturn;
    private String dateAgreementLoan;

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getOtchestvo() {
        return otchestvo;
    }

    public void setOtchestvo(String otchestvo) {
        this.otchestvo = otchestvo;
    }

    public String getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(String idPerson) {
        this.idPerson = idPerson;
    }

    public String getPol() {
        return pol;
    }

    public void setPol(String pol) {
        this.pol = pol;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getAreaFrom() {
        return areaFrom;
    }

    public void setAreaFrom(String areaFrom) {
        this.areaFrom = areaFrom;
    }

    public String getRegionFrom() {
        return regionFrom;
    }

    public void setRegionFrom(String regionFrom) {
        this.regionFrom = regionFrom;
    }

    public String getRuralDistrictFrom() {
        return ruralDistrictFrom;
    }

    public void setRuralDistrictFrom(String ruralDistrictFrom) {
        this.ruralDistrictFrom = ruralDistrictFrom;
    }

    public String getTypeEducation() {
        return typeEducation;
    }

    public void setTypeEducation(String typeEducation) {
        this.typeEducation = typeEducation;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getProfession_rus() {
        return profession_rus;
    }

    public void setProfession_rus(String profession_rus) {
        this.profession_rus = profession_rus;
    }

    public String getProfession_kz() {
        return profession_kz;
    }

    public void setProfession_kz(String profession_kz) {
        this.profession_kz = profession_kz;
    }

    public String getEducationalInstruction() {
        return educationalInstruction;
    }

    public void setEducationalInstruction(String educationalInstruction) {
        this.educationalInstruction = educationalInstruction;
    }

    public String getAreaIn() {
        return areaIn;
    }

    public void setAreaIn(String areaIn) {
        this.areaIn = areaIn;
    }

    public String getRegionIn() {
        return regionIn;
    }

    public void setRegionIn(String regionIn) {
        this.regionIn = regionIn;
    }

    public String getRuralDistrictIn() {
        return ruralDistrictIn;
    }

    public void setRuralDistrictIn(String ruralDistrictIn) {
        this.ruralDistrictIn = ruralDistrictIn;
    }

    public String getRuralSettlementIn() {
        return ruralSettlementIn;
    }

    public void setRuralSettlementIn(String ruralSettlementIn) {
        this.ruralSettlementIn = ruralSettlementIn;
    }

    public String getWorkSpace() {
        return workSpace;
    }

    public void setWorkSpace(String workSpace) {
        this.workSpace = workSpace;
    }

    public String getFieldActivity() {
        return fieldActivity;
    }

    public void setFieldActivity(String fieldActivity) {
        this.fieldActivity = fieldActivity;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getStatusEmployee() {
        return statusEmployee;
    }

    public void setStatusEmployee(String statusEmployee) {
        this.statusEmployee = statusEmployee;
    }

    public String getCommissionDecision() {
        return commissionDecision;
    }

    public void setCommissionDecision(String commissionDecision) {
        this.commissionDecision = commissionDecision;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public Double getSumGrant() {
        return sumGrant;
    }

    public void setSumGrant(Double sumGrant) {
        this.sumGrant = sumGrant;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getNeedReturn() {
        return needReturn;
    }

    public void setNeedReturn(String needReturn) {
        this.needReturn = needReturn;
    }

    public Integer getRefund() {
        return refund;
    }

    public void setRefund(Integer refund) {
        this.refund = refund;
    }

    public String getReasonReturn() {
        return reasonReturn;
    }

    public void setReasonReturn(String reasonReturn) {
        this.reasonReturn = reasonReturn;
    }

    public Double getContractSumm() {
        return contractSumm;
    }

    public void setContractSumm(Double contractSumm) {
        this.contractSumm = contractSumm;
    }

    public String getUnitCredit() {
        return unitCredit;
    }

    public void setUnitCredit(String unitCredit) {
        this.unitCredit = unitCredit;
    }

    public Double getBalanceCredit() {
        return balanceCredit;
    }

    public void setBalanceCredit(Double balanceCredit) {
        this.balanceCredit = balanceCredit;
    }

    public String getReasonForRefund() {
        return reasonForRefund;
    }

    public void setReasonForRefund(String reasonForRefund) {
        this.reasonForRefund = reasonForRefund;
    }

    public String getReason_project_completion() {
        return reason_project_completion;
    }

    public void setReason_project_completion(String reason_project_completion) {
        this.reason_project_completion = reason_project_completion;
    }

    public String getContractNumberA() {
        return contractNumberA;
    }

    public void setContractNumberA(String contractNumberA) {
        this.contractNumberA = contractNumberA;
    }

    public String getDateBirthday() {
        return dateBirthday;
    }

    public void setDateBirthday(String dateBirthday) {
        this.dateBirthday = dateBirthday;
    }

    public String getDateGraduation() {
        return dateGraduation;
    }

    public void setDateGraduation(String dateGraduation) {
        this.dateGraduation = dateGraduation;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getDateDismissalTransfer() {
        return dateDismissalTransfer;
    }

    public void setDateDismissalTransfer(String dateDismissalTransfer) {
        this.dateDismissalTransfer = dateDismissalTransfer;
    }

    public String getDateContract() {
        return dateContract;
    }

    public void setDateContract(String dateContract) {
        this.dateContract = dateContract;
    }

    public String getDateReturn() {
        return dateReturn;
    }

    public void setDateReturn(String dateReturn) {
        this.dateReturn = dateReturn;
    }

    public String getDateAgreementLoan() {
        return dateAgreementLoan;
    }

    public void setDateAgreementLoan(String dateAgreementLoan) {
        this.dateAgreementLoan = dateAgreementLoan;
    }
}
