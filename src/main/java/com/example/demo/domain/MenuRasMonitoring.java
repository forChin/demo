package com.example.demo.domain;

public class MenuRasMonitoring {

    private String year;        //ush_stat_rastenievodstvo
    private String area;
    private String region;
    private Double latitude;
    private Double longitude;
    private Double totalAcreage;
    private Double volumeProduction;
    private Double ifoVolumeProduction;
    private Double grossHarvestAll;
    private Double watermelonGross;
    private Double potatoGross;
    private Double pepperGross;
    private Double eggplantGross;
    private Double onionGross;
    private Double tomatoesGross;
    private Double carrotGross;
    private Double melonsGross;
    private Double beetGross;
    private Double cabbageGross;
    private Double cucumbersGross;
    private Double sownAreaAll;
    private Double watermelonSownArea;
    private Double potatoSownArea;
    private Double pepperSownArea;
    private Double eggplantSownArea;
    private Double onionSownArea;
    private Double tomatoesSownArea;
    private Double carrotSownArea;
    private Double melonsSownArea;
    private Double beetSownArea;
    private Double cabbageSownArea;
    private Double cucumbersSownArea;
    private Double yieldAll;
    private Double watermelonYield;
    private Double potatoYield;
    private Double pepperYield;
    private Double eggplantYield;
    private Double onionYield;
    private Double tomatoesYield;
    private Double carrotYield;
    private Double melonsYield;
    private Double beetYield;
    private Double cabbageYield;
    private Double cucumbersYield;
    private Double cerealsLegumesPeasantFarmSownarea;
    private Double potatoPeasantFarmSownarea;
    private Double melonCulturesPeasantFarmSownarea;
    private Double forageCropsPeasantFarmSownarea;
    private Double cultureOlivesPeasantFarmSownarea;
    private Double vegetablesPeasantFarmSownarea;
    private Double cerealsLegumesAgriculturalenterpriseSownarea;
    private Double potatoAgriculturalenterpriseSownarea;
    private Double melonCulturesAgriculturalenterpriseSownarea;
    private Double forageCropsAgriculturalenterpriseSownarea;
    private Double cultureOlivesAgriculturalenterpriseSownarea;
    private Double vegetablesAgriculturalenterpriseSownarea;
    private Double cerealsLegumesHouseholdsSownarea;
    private Double potatoHouseholdsSownarea;
    private Double melonCulturesHouseholdsSownarea;
    private Double forageCropsHouseholdsSownarea;
    private Double cultureOlivesHouseholdsSownarea;
    private Double vegetablesHouseholdsSownarea;
    private Double cerealsLegumesPeasantFarmCleanedarea;
    private Double potatoPeasantFarmCleanedarea;
    private Double melonCulturesPeasantFarmCleanedarea;
    private Double forageCropsPeasantFarmCleanedarea;
    private Double cultureOlivesPeasantFarmCleanedarea;
    private Double vegetablesPeasantFarmCleanedarea;
    private Double cerealsLegumesAgriculturCleandarea;
    private Double potatoAgriculturCleandarea;
    private Double melonCulturesAgriculturCleandarea;
    private Double forageCropsAgriculturCleandarea;
    private Double cultureOlivesAgriculturCleandarea;
    private Double vegetablesAgriculturCleandarea;
    private Double cerealsLegumesHouseholdsCleandarea;
    private Double potatoHouseholdsCleandarea;
    private Double melonCulturesHouseholdsCleandarea;
    private Double forageCropsHouseholdsCleandarea;
    private Double cultureOlivesHouseholdsCleandarea;
    private Double vegetablesHouseholdsCleandarea;
    private Double cerealsLegumesPeasantFarmHarvest;
    private Double potatoPeasantFarmHarvest;
    private Double melonCulturesPeasantFarmHarvest;
    private Double forageCropsPeasantFarmHarvest;
    private Double cultureOlivesPeasantFarmHarvest;
    private Double vegetablesPeasantFarmHarvest;
    private Double cerealsLegumesAgriculturHarvest;
    private Double potatoAgriculturHarvest;
    private Double melonCulturesAgriculturHarvest;
    private Double forageCropsAgriculturHarvest;
    private Double cultureOlivesAgriculturHarvest;
    private Double vegetablesAgriculturHarvest;
    private Double cerealsLegumesHouseholdsHarvest;
    private Double potatoHouseholdsHarvest;
    private Double melonCulturesHouseholdsHarvest;
    private Double forageCropsHouseholdsHarvest;
    private Double cultureOlivesHouseholdsHarvest;
    private Double vegetablesHouseholdsHarvest;
    private Double cerealsLegumesSquareStructure;
    private Double potatoSquareStructure;
    private Double melonCulturesSquareStructure;
    private Double forageCropsSquareStructure;
    private Double cultureOlivesSquareStructure;
    private Double vegetablesSquareStructure;

    public String getYear() {
        return year;
    }

    public String getArea() {
        return area;
    }

    public String getRegion() {
        return region;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Double getTotalAcreage() {
        return totalAcreage;
    }

    public Double getVolumeProduction() {
        return volumeProduction;
    }

    public Double getIfoVolumeProduction() {
        return ifoVolumeProduction;
    }

    public Double getGrossHarvestAll() {
        return grossHarvestAll;
    }

    public Double getWatermelonGross() {
        return watermelonGross;
    }

    public Double getPotatoGross() {
        return potatoGross;
    }

    public Double getPepperGross() {
        return pepperGross;
    }

    public Double getEggplantGross() {
        return eggplantGross;
    }

    public Double getOnionGross() {
        return onionGross;
    }

    public Double getTomatoesGross() {
        return tomatoesGross;
    }

    public Double getCarrotGross() {
        return carrotGross;
    }

    public Double getMelonsGross() {
        return melonsGross;
    }

    public Double getBeetGross() {
        return beetGross;
    }

    public Double getCabbageGross() {
        return cabbageGross;
    }

    public Double getCucumbersGross() {
        return cucumbersGross;
    }

    public Double getSownAreaAll() {
        return sownAreaAll;
    }

    public Double getWatermelonSownArea() {
        return watermelonSownArea;
    }

    public Double getPotatoSownArea() {
        return potatoSownArea;
    }

    public Double getPepperSownArea() {
        return pepperSownArea;
    }

    public Double getEggplantSownArea() {
        return eggplantSownArea;
    }

    public Double getOnionSownArea() {
        return onionSownArea;
    }

    public Double getTomatoesSownArea() {
        return tomatoesSownArea;
    }

    public Double getCarrotSownArea() {
        return carrotSownArea;
    }

    public Double getMelonsSownArea() {
        return melonsSownArea;
    }

    public Double getBeetSownArea() {
        return beetSownArea;
    }

    public Double getCabbageSownArea() {
        return cabbageSownArea;
    }

    public Double getCucumbersSownArea() {
        return cucumbersSownArea;
    }

    public Double getYieldAll() {
        return yieldAll;
    }

    public Double getWatermelonYield() {
        return watermelonYield;
    }

    public Double getPotatoYield() {
        return potatoYield;
    }

    public Double getPepperYield() {
        return pepperYield;
    }

    public Double getEggplantYield() {
        return eggplantYield;
    }

    public Double getOnionYield() {
        return onionYield;
    }

    public Double getTomatoesYield() {
        return tomatoesYield;
    }

    public Double getCarrotYield() {
        return carrotYield;
    }

    public Double getMelonsYield() {
        return melonsYield;
    }

    public Double getBeetYield() {
        return beetYield;
    }

    public Double getCabbageYield() {
        return cabbageYield;
    }

    public Double getCucumbersYield() {
        return cucumbersYield;
    }

    public Double getCerealsLegumesPeasantFarmSownarea() {
        return cerealsLegumesPeasantFarmSownarea;
    }

    public Double getPotatoPeasantFarmSownarea() {
        return potatoPeasantFarmSownarea;
    }

    public Double getMelonCulturesPeasantFarmSownarea() {
        return melonCulturesPeasantFarmSownarea;
    }

    public Double getForageCropsPeasantFarmSownarea() {
        return forageCropsPeasantFarmSownarea;
    }

    public Double getCultureOlivesPeasantFarmSownarea() {
        return cultureOlivesPeasantFarmSownarea;
    }

    public Double getVegetablesPeasantFarmSownarea() {
        return vegetablesPeasantFarmSownarea;
    }

    public Double getCerealsLegumesAgriculturalenterpriseSownarea() {
        return cerealsLegumesAgriculturalenterpriseSownarea;
    }

    public Double getPotatoAgriculturalenterpriseSownarea() {
        return potatoAgriculturalenterpriseSownarea;
    }

    public Double getMelonCulturesAgriculturalenterpriseSownarea() {
        return melonCulturesAgriculturalenterpriseSownarea;
    }

    public Double getForageCropsAgriculturalenterpriseSownarea() {
        return forageCropsAgriculturalenterpriseSownarea;
    }

    public Double getCultureOlivesAgriculturalenterpriseSownarea() {
        return cultureOlivesAgriculturalenterpriseSownarea;
    }

    public Double getVegetablesAgriculturalenterpriseSownarea() {
        return vegetablesAgriculturalenterpriseSownarea;
    }

    public Double getCerealsLegumesHouseholdsSownarea() {
        return cerealsLegumesHouseholdsSownarea;
    }

    public Double getPotatoHouseholdsSownarea() {
        return potatoHouseholdsSownarea;
    }

    public Double getMelonCulturesHouseholdsSownarea() {
        return melonCulturesHouseholdsSownarea;
    }

    public Double getForageCropsHouseholdsSownarea() {
        return forageCropsHouseholdsSownarea;
    }

    public Double getCultureOlivesHouseholdsSownarea() {
        return cultureOlivesHouseholdsSownarea;
    }

    public Double getVegetablesHouseholdsSownarea() {
        return vegetablesHouseholdsSownarea;
    }

    public Double getCerealsLegumesPeasantFarmCleanedarea() {
        return cerealsLegumesPeasantFarmCleanedarea;
    }

    public Double getPotatoPeasantFarmCleanedarea() {
        return potatoPeasantFarmCleanedarea;
    }

    public Double getMelonCulturesPeasantFarmCleanedarea() {
        return melonCulturesPeasantFarmCleanedarea;
    }

    public Double getForageCropsPeasantFarmCleanedarea() {
        return forageCropsPeasantFarmCleanedarea;
    }

    public Double getCultureOlivesPeasantFarmCleanedarea() {
        return cultureOlivesPeasantFarmCleanedarea;
    }

    public Double getVegetablesPeasantFarmCleanedarea() {
        return vegetablesPeasantFarmCleanedarea;
    }

    public Double getCerealsLegumesAgriculturCleandarea() {
        return cerealsLegumesAgriculturCleandarea;
    }

    public Double getPotatoAgriculturCleandarea() {
        return potatoAgriculturCleandarea;
    }

    public Double getMelonCulturesAgriculturCleandarea() {
        return melonCulturesAgriculturCleandarea;
    }

    public Double getForageCropsAgriculturCleandarea() {
        return forageCropsAgriculturCleandarea;
    }

    public Double getCultureOlivesAgriculturCleandarea() {
        return cultureOlivesAgriculturCleandarea;
    }

    public Double getVegetablesAgriculturCleandarea() {
        return vegetablesAgriculturCleandarea;
    }

    public Double getCerealsLegumesHouseholdsCleandarea() {
        return cerealsLegumesHouseholdsCleandarea;
    }

    public Double getPotatoHouseholdsCleandarea() {
        return potatoHouseholdsCleandarea;
    }

    public Double getMelonCulturesHouseholdsCleandarea() {
        return melonCulturesHouseholdsCleandarea;
    }

    public Double getForageCropsHouseholdsCleandarea() {
        return forageCropsHouseholdsCleandarea;
    }

    public Double getCultureOlivesHouseholdsCleandarea() {
        return cultureOlivesHouseholdsCleandarea;
    }

    public Double getVegetablesHouseholdsCleandarea() {
        return vegetablesHouseholdsCleandarea;
    }

    public Double getCerealsLegumesPeasantFarmHarvest() {
        return cerealsLegumesPeasantFarmHarvest;
    }

    public Double getPotatoPeasantFarmHarvest() {
        return potatoPeasantFarmHarvest;
    }

    public Double getMelonCulturesPeasantFarmHarvest() {
        return melonCulturesPeasantFarmHarvest;
    }

    public Double getForageCropsPeasantFarmHarvest() {
        return forageCropsPeasantFarmHarvest;
    }

    public Double getCultureOlivesPeasantFarmHarvest() {
        return cultureOlivesPeasantFarmHarvest;
    }

    public Double getVegetablesPeasantFarmHarvest() {
        return vegetablesPeasantFarmHarvest;
    }

    public Double getCerealsLegumesAgriculturHarvest() {
        return cerealsLegumesAgriculturHarvest;
    }

    public Double getPotatoAgriculturHarvest() {
        return potatoAgriculturHarvest;
    }

    public Double getMelonCulturesAgriculturHarvest() {
        return melonCulturesAgriculturHarvest;
    }

    public Double getForageCropsAgriculturHarvest() {
        return forageCropsAgriculturHarvest;
    }

    public Double getCultureOlivesAgriculturHarvest() {
        return cultureOlivesAgriculturHarvest;
    }

    public Double getVegetablesAgriculturHarvest() {
        return vegetablesAgriculturHarvest;
    }

    public Double getCerealsLegumesHouseholdsHarvest() {
        return cerealsLegumesHouseholdsHarvest;
    }

    public Double getPotatoHouseholdsHarvest() {
        return potatoHouseholdsHarvest;
    }

    public Double getMelonCulturesHouseholdsHarvest() {
        return melonCulturesHouseholdsHarvest;
    }

    public Double getForageCropsHouseholdsHarvest() {
        return forageCropsHouseholdsHarvest;
    }

    public Double getCultureOlivesHouseholdsHarvest() {
        return cultureOlivesHouseholdsHarvest;
    }

    public Double getVegetablesHouseholdsHarvest() {
        return vegetablesHouseholdsHarvest;
    }

    public Double getCerealsLegumesSquareStructure() {
        return cerealsLegumesSquareStructure;
    }

    public Double getPotatoSquareStructure() {
        return potatoSquareStructure;
    }

    public Double getMelonCulturesSquareStructure() {
        return melonCulturesSquareStructure;
    }

    public Double getForageCropsSquareStructure() {
        return forageCropsSquareStructure;
    }

    public Double getCultureOlivesSquareStructure() {
        return cultureOlivesSquareStructure;
    }

    public Double getVegetablesSquareStructure() {
        return vegetablesSquareStructure;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public void setTotalAcreage(Double totalAcreage) {
        this.totalAcreage = totalAcreage;
    }

    public void setVolumeProduction(Double volumeProduction) {
        this.volumeProduction = volumeProduction;
    }

    public void setIfoVolumeProduction(Double ifoVolumeProduction) {
        this.ifoVolumeProduction = ifoVolumeProduction;
    }

    public void setGrossHarvestAll(Double grossHarvestAll) {
        this.grossHarvestAll = grossHarvestAll;
    }

    public void setWatermelonGross(Double watermelonGross) {
        this.watermelonGross = watermelonGross;
    }

    public void setPotatoGross(Double potatoGross) {
        this.potatoGross = potatoGross;
    }

    public void setPepperGross(Double pepperGross) {
        this.pepperGross = pepperGross;
    }

    public void setEggplantGross(Double eggplantGross) {
        this.eggplantGross = eggplantGross;
    }

    public void setOnionGross(Double onionGross) {
        this.onionGross = onionGross;
    }

    public void setTomatoesGross(Double tomatoesGross) {
        this.tomatoesGross = tomatoesGross;
    }

    public void setCarrotGross(Double carrotGross) {
        this.carrotGross = carrotGross;
    }

    public void setMelonsGross(Double melonsGross) {
        this.melonsGross = melonsGross;
    }

    public void setBeetGross(Double beetGross) {
        this.beetGross = beetGross;
    }

    public void setCabbageGross(Double cabbageGross) {
        this.cabbageGross = cabbageGross;
    }

    public void setCucumbersGross(Double cucumbersGross) {
        this.cucumbersGross = cucumbersGross;
    }

    public void setSownAreaAll(Double sownAreaAll) {
        this.sownAreaAll = sownAreaAll;
    }

    public void setWatermelonSownArea(Double watermelonSownArea) {
        this.watermelonSownArea = watermelonSownArea;
    }

    public void setPotatoSownArea(Double potatoSownArea) {
        this.potatoSownArea = potatoSownArea;
    }

    public void setPepperSownArea(Double pepperSownArea) {
        this.pepperSownArea = pepperSownArea;
    }

    public void setEggplantSownArea(Double eggplantSownArea) {
        this.eggplantSownArea = eggplantSownArea;
    }

    public void setOnionSownArea(Double onionSownArea) {
        this.onionSownArea = onionSownArea;
    }

    public void setTomatoesSownArea(Double tomatoesSownArea) {
        this.tomatoesSownArea = tomatoesSownArea;
    }

    public void setCarrotSownArea(Double carrotSownArea) {
        this.carrotSownArea = carrotSownArea;
    }

    public void setMelonsSownArea(Double melonsSownArea) {
        this.melonsSownArea = melonsSownArea;
    }

    public void setBeetSownArea(Double beetSownArea) {
        this.beetSownArea = beetSownArea;
    }

    public void setCabbageSownArea(Double cabbageSownArea) {
        this.cabbageSownArea = cabbageSownArea;
    }

    public void setCucumbersSownArea(Double cucumbersSownArea) {
        this.cucumbersSownArea = cucumbersSownArea;
    }

    public void setYieldAll(Double yieldAll) {
        this.yieldAll = yieldAll;
    }

    public void setWatermelonYield(Double watermelonYield) {
        this.watermelonYield = watermelonYield;
    }

    public void setPotatoYield(Double potatoYield) {
        this.potatoYield = potatoYield;
    }

    public void setPepperYield(Double pepperYield) {
        this.pepperYield = pepperYield;
    }

    public void setEggplantYield(Double eggplantYield) {
        this.eggplantYield = eggplantYield;
    }

    public void setOnionYield(Double onionYield) {
        this.onionYield = onionYield;
    }

    public void setTomatoesYield(Double tomatoesYield) {
        this.tomatoesYield = tomatoesYield;
    }

    public void setCarrotYield(Double carrotYield) {
        this.carrotYield = carrotYield;
    }

    public void setMelonsYield(Double melonsYield) {
        this.melonsYield = melonsYield;
    }

    public void setBeetYield(Double beetYield) {
        this.beetYield = beetYield;
    }

    public void setCabbageYield(Double cabbageYield) {
        this.cabbageYield = cabbageYield;
    }

    public void setCucumbersYield(Double cucumbersYield) {
        this.cucumbersYield = cucumbersYield;
    }

    public void setCerealsLegumesPeasantFarmSownarea(Double cerealsLegumesPeasantFarmSownarea) {
        this.cerealsLegumesPeasantFarmSownarea = cerealsLegumesPeasantFarmSownarea;
    }

    public void setPotatoPeasantFarmSownarea(Double potatoPeasantFarmSownarea) {
        this.potatoPeasantFarmSownarea = potatoPeasantFarmSownarea;
    }

    public void setMelonCulturesPeasantFarmSownarea(Double melonCulturesPeasantFarmSownarea) {
        this.melonCulturesPeasantFarmSownarea = melonCulturesPeasantFarmSownarea;
    }

    public void setForageCropsPeasantFarmSownarea(Double forageCropsPeasantFarmSownarea) {
        this.forageCropsPeasantFarmSownarea = forageCropsPeasantFarmSownarea;
    }

    public void setCultureOlivesPeasantFarmSownarea(Double cultureOlivesPeasantFarmSownarea) {
        this.cultureOlivesPeasantFarmSownarea = cultureOlivesPeasantFarmSownarea;
    }

    public void setVegetablesPeasantFarmSownarea(Double vegetablesPeasantFarmSownarea) {
        this.vegetablesPeasantFarmSownarea = vegetablesPeasantFarmSownarea;
    }

    public void setCerealsLegumesAgriculturalenterpriseSownarea(Double cerealsLegumesAgriculturalenterpriseSownarea) {
        this.cerealsLegumesAgriculturalenterpriseSownarea = cerealsLegumesAgriculturalenterpriseSownarea;
    }

    public void setPotatoAgriculturalenterpriseSownarea(Double potatoAgriculturalenterpriseSownarea) {
        this.potatoAgriculturalenterpriseSownarea = potatoAgriculturalenterpriseSownarea;
    }

    public void setMelonCulturesAgriculturalenterpriseSownarea(Double melonCulturesAgriculturalenterpriseSownarea) {
        this.melonCulturesAgriculturalenterpriseSownarea = melonCulturesAgriculturalenterpriseSownarea;
    }

    public void setForageCropsAgriculturalenterpriseSownarea(Double forageCropsAgriculturalenterpriseSownarea) {
        this.forageCropsAgriculturalenterpriseSownarea = forageCropsAgriculturalenterpriseSownarea;
    }

    public void setCultureOlivesAgriculturalenterpriseSownarea(Double cultureOlivesAgriculturalenterpriseSownarea) {
        this.cultureOlivesAgriculturalenterpriseSownarea = cultureOlivesAgriculturalenterpriseSownarea;
    }

    public void setVegetablesAgriculturalenterpriseSownarea(Double vegetablesAgriculturalenterpriseSownarea) {
        this.vegetablesAgriculturalenterpriseSownarea = vegetablesAgriculturalenterpriseSownarea;
    }

    public void setCerealsLegumesHouseholdsSownarea(Double cerealsLegumesHouseholdsSownarea) {
        this.cerealsLegumesHouseholdsSownarea = cerealsLegumesHouseholdsSownarea;
    }

    public void setPotatoHouseholdsSownarea(Double potatoHouseholdsSownarea) {
        this.potatoHouseholdsSownarea = potatoHouseholdsSownarea;
    }

    public void setMelonCulturesHouseholdsSownarea(Double melonCulturesHouseholdsSownarea) {
        this.melonCulturesHouseholdsSownarea = melonCulturesHouseholdsSownarea;
    }

    public void setForageCropsHouseholdsSownarea(Double forageCropsHouseholdsSownarea) {
        this.forageCropsHouseholdsSownarea = forageCropsHouseholdsSownarea;
    }

    public void setCultureOlivesHouseholdsSownarea(Double cultureOlivesHouseholdsSownarea) {
        this.cultureOlivesHouseholdsSownarea = cultureOlivesHouseholdsSownarea;
    }

    public void setVegetablesHouseholdsSownarea(Double vegetablesHouseholdsSownarea) {
        this.vegetablesHouseholdsSownarea = vegetablesHouseholdsSownarea;
    }

    public void setCerealsLegumesPeasantFarmCleanedarea(Double cerealsLegumesPeasantFarmCleanedarea) {
        this.cerealsLegumesPeasantFarmCleanedarea = cerealsLegumesPeasantFarmCleanedarea;
    }

    public void setPotatoPeasantFarmCleanedarea(Double potatoPeasantFarmCleanedarea) {
        this.potatoPeasantFarmCleanedarea = potatoPeasantFarmCleanedarea;
    }

    public void setMelonCulturesPeasantFarmCleanedarea(Double melonCulturesPeasantFarmCleanedarea) {
        this.melonCulturesPeasantFarmCleanedarea = melonCulturesPeasantFarmCleanedarea;
    }

    public void setForageCropsPeasantFarmCleanedarea(Double forageCropsPeasantFarmCleanedarea) {
        this.forageCropsPeasantFarmCleanedarea = forageCropsPeasantFarmCleanedarea;
    }

    public void setCultureOlivesPeasantFarmCleanedarea(Double cultureOlivesPeasantFarmCleanedarea) {
        this.cultureOlivesPeasantFarmCleanedarea = cultureOlivesPeasantFarmCleanedarea;
    }

    public void setVegetablesPeasantFarmCleanedarea(Double vegetablesPeasantFarmCleanedarea) {
        this.vegetablesPeasantFarmCleanedarea = vegetablesPeasantFarmCleanedarea;
    }

    public void setCerealsLegumesAgriculturCleandarea(Double cerealsLegumesAgriculturCleandarea) {
        this.cerealsLegumesAgriculturCleandarea = cerealsLegumesAgriculturCleandarea;
    }

    public void setPotatoAgriculturCleandarea(Double potatoAgriculturCleandarea) {
        this.potatoAgriculturCleandarea = potatoAgriculturCleandarea;
    }

    public void setMelonCulturesAgriculturCleandarea(Double melonCulturesAgriculturCleandarea) {
        this.melonCulturesAgriculturCleandarea = melonCulturesAgriculturCleandarea;
    }

    public void setForageCropsAgriculturCleandarea(Double forageCropsAgriculturCleandarea) {
        this.forageCropsAgriculturCleandarea = forageCropsAgriculturCleandarea;
    }

    public void setCultureOlivesAgriculturCleandarea(Double cultureOlivesAgriculturCleandarea) {
        this.cultureOlivesAgriculturCleandarea = cultureOlivesAgriculturCleandarea;
    }

    public void setVegetablesAgriculturCleandarea(Double vegetablesAgriculturCleandarea) {
        this.vegetablesAgriculturCleandarea = vegetablesAgriculturCleandarea;
    }

    public void setCerealsLegumesHouseholdsCleandarea(Double cerealsLegumesHouseholdsCleandarea) {
        this.cerealsLegumesHouseholdsCleandarea = cerealsLegumesHouseholdsCleandarea;
    }

    public void setPotatoHouseholdsCleandarea(Double potatoHouseholdsCleandarea) {
        this.potatoHouseholdsCleandarea = potatoHouseholdsCleandarea;
    }

    public void setMelonCulturesHouseholdsCleandarea(Double melonCulturesHouseholdsCleandarea) {
        this.melonCulturesHouseholdsCleandarea = melonCulturesHouseholdsCleandarea;
    }

    public void setForageCropsHouseholdsCleandarea(Double forageCropsHouseholdsCleandarea) {
        this.forageCropsHouseholdsCleandarea = forageCropsHouseholdsCleandarea;
    }

    public void setCultureOlivesHouseholdsCleandarea(Double cultureOlivesHouseholdsCleandarea) {
        this.cultureOlivesHouseholdsCleandarea = cultureOlivesHouseholdsCleandarea;
    }

    public void setVegetablesHouseholdsCleandarea(Double vegetablesHouseholdsCleandarea) {
        this.vegetablesHouseholdsCleandarea = vegetablesHouseholdsCleandarea;
    }

    public void setCerealsLegumesPeasantFarmHarvest(Double cerealsLegumesPeasantFarmHarvest) {
        this.cerealsLegumesPeasantFarmHarvest = cerealsLegumesPeasantFarmHarvest;
    }

    public void setPotatoPeasantFarmHarvest(Double potatoPeasantFarmHarvest) {
        this.potatoPeasantFarmHarvest = potatoPeasantFarmHarvest;
    }

    public void setMelonCulturesPeasantFarmHarvest(Double melonCulturesPeasantFarmHarvest) {
        this.melonCulturesPeasantFarmHarvest = melonCulturesPeasantFarmHarvest;
    }

    public void setForageCropsPeasantFarmHarvest(Double forageCropsPeasantFarmHarvest) {
        this.forageCropsPeasantFarmHarvest = forageCropsPeasantFarmHarvest;
    }

    public void setCultureOlivesPeasantFarmHarvest(Double cultureOlivesPeasantFarmHarvest) {
        this.cultureOlivesPeasantFarmHarvest = cultureOlivesPeasantFarmHarvest;
    }

    public void setVegetablesPeasantFarmHarvest(Double vegetablesPeasantFarmHarvest) {
        this.vegetablesPeasantFarmHarvest = vegetablesPeasantFarmHarvest;
    }

    public void setCerealsLegumesAgriculturHarvest(Double cerealsLegumesAgriculturHarvest) {
        this.cerealsLegumesAgriculturHarvest = cerealsLegumesAgriculturHarvest;
    }

    public void setPotatoAgriculturHarvest(Double potatoAgriculturHarvest) {
        this.potatoAgriculturHarvest = potatoAgriculturHarvest;
    }

    public void setMelonCulturesAgriculturHarvest(Double melonCulturesAgriculturHarvest) {
        this.melonCulturesAgriculturHarvest = melonCulturesAgriculturHarvest;
    }

    public void setForageCropsAgriculturHarvest(Double forageCropsAgriculturHarvest) {
        this.forageCropsAgriculturHarvest = forageCropsAgriculturHarvest;
    }

    public void setCultureOlivesAgriculturHarvest(Double cultureOlivesAgriculturHarvest) {
        this.cultureOlivesAgriculturHarvest = cultureOlivesAgriculturHarvest;
    }

    public void setVegetablesAgriculturHarvest(Double vegetablesAgriculturHarvest) {
        this.vegetablesAgriculturHarvest = vegetablesAgriculturHarvest;
    }

    public void setCerealsLegumesHouseholdsHarvest(Double cerealsLegumesHouseholdsHarvest) {
        this.cerealsLegumesHouseholdsHarvest = cerealsLegumesHouseholdsHarvest;
    }

    public void setPotatoHouseholdsHarvest(Double potatoHouseholdsHarvest) {
        this.potatoHouseholdsHarvest = potatoHouseholdsHarvest;
    }

    public void setMelonCulturesHouseholdsHarvest(Double melonCulturesHouseholdsHarvest) {
        this.melonCulturesHouseholdsHarvest = melonCulturesHouseholdsHarvest;
    }

    public void setForageCropsHouseholdsHarvest(Double forageCropsHouseholdsHarvest) {
        this.forageCropsHouseholdsHarvest = forageCropsHouseholdsHarvest;
    }

    public void setCultureOlivesHouseholdsHarvest(Double cultureOlivesHouseholdsHarvest) {
        this.cultureOlivesHouseholdsHarvest = cultureOlivesHouseholdsHarvest;
    }

    public void setVegetablesHouseholdsHarvest(Double vegetablesHouseholdsHarvest) {
        this.vegetablesHouseholdsHarvest = vegetablesHouseholdsHarvest;
    }

    public void setCerealsLegumesSquareStructure(Double cerealsLegumesSquareStructure) {
        this.cerealsLegumesSquareStructure = cerealsLegumesSquareStructure;
    }

    public void setPotatoSquareStructure(Double potatoSquareStructure) {
        this.potatoSquareStructure = potatoSquareStructure;
    }

    public void setMelonCulturesSquareStructure(Double melonCulturesSquareStructure) {
        this.melonCulturesSquareStructure = melonCulturesSquareStructure;
    }

    public void setForageCropsSquareStructure(Double forageCropsSquareStructure) {
        this.forageCropsSquareStructure = forageCropsSquareStructure;
    }

    public void setCultureOlivesSquareStructure(Double cultureOlivesSquareStructure) {
        this.cultureOlivesSquareStructure = cultureOlivesSquareStructure;
    }

    public void setVegetablesSquareStructure(Double vegetablesSquareStructure) {
        this.vegetablesSquareStructure = vegetablesSquareStructure;
    }
}
