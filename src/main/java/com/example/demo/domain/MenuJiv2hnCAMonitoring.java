package com.example.demo.domain;

public class MenuJiv2hnCAMonitoring {

    private String year;            //ush_stat_jivotnovodstvo
    private String area;
    private String region;
    private Double numberCamelsHouseholds;

    public String getYear() {
        return year;
    }

    public String getArea() {
        return area;
    }

    public String getRegion() {
        return region;
    }

    public Double getNumberCamelsHouseholds() {
        return numberCamelsHouseholds;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setNumberCamelsHouseholds(Double numberCamelsHouseholds) {
        this.numberCamelsHouseholds = numberCamelsHouseholds;
    }
}
