package com.example.demo.domain;

public class BlokEcologicalMonitoring {
    // blok ecological

    private String raion;
    private String ruralDistrict;
    private String ruralSettlement;
    private Double report;
    private Double report2;
    private Double itog;
    private String year;

    public String getRaion() {
        return raion;
    }

    public String getRuralDistrict() {
        return ruralDistrict;
    }

    public String getRuralSettlement() {
        return ruralSettlement;
    }

    public Double getReport() {
        return report;
    }

    public Double getReport2() {
        return report2;
    }

    public Double getItog() {
        return itog;
    }

    public String getYear() {
        return year;
    }

    public void setRaion(String raion) {
        this.raion = raion;
    }

    public void setRuralDistrict(String ruralDistrict) {
        this.ruralDistrict = ruralDistrict;
    }

    public void setRuralSettlement(String ruralSettlement) {
        this.ruralSettlement = ruralSettlement;
    }

    public void setReport(Double report) {
        this.report = report;
    }

    public void setReport2(Double report2) {
        this.report2 = report2;
    }

    public void setItog(Double itog) {
        this.itog = itog;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
