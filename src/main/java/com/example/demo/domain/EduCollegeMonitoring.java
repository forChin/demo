package com.example.demo.domain;

public class EduCollegeMonitoring {

    // new edu college

    private String raion;
    private String year;
    private String nameTipo;
    private Integer countTeachers;
    private Integer countAcceptedStudents;
    private Integer countGraduates;
    private Integer countReleasedStudents;
    private Integer countDesignCapacity;
    private Integer countCategoryC;
    private Integer countCategoryA;
    private Integer countCategoryB;
    private Integer countMagistrate;
    private Integer countDoctorEngineering;
    private Integer countCategoryD;
    private Integer countEducationHigher;
    private Integer countEducationColleges;
    private Integer countGeneralSecondaryEducation;

    public String getRaion() {
        return raion;
    }

    public String getYear() {
        return year;
    }

    public String getNameTipo() {
        return nameTipo;
    }

    public Integer getCountTeachers() {
        return countTeachers;
    }

    public Integer getCountAcceptedStudents() {
        return countAcceptedStudents;
    }

    public Integer getCountGraduates() {
        return countGraduates;
    }

    public Integer getCountReleasedStudents() {
        return countReleasedStudents;
    }

    public Integer getCountDesignCapacity() {
        return countDesignCapacity;
    }

    public Integer getCountCategoryC() {
        return countCategoryC;
    }

    public Integer getCountCategoryA() {
        return countCategoryA;
    }

    public Integer getCountCategoryB() {
        return countCategoryB;
    }

    public Integer getCountMagistrate() {
        return countMagistrate;
    }

    public Integer getCountDoctorEngineering() {
        return countDoctorEngineering;
    }

    public Integer getCountCategoryD() {
        return countCategoryD;
    }

    public Integer getCountEducationHigher() {
        return countEducationHigher;
    }

    public Integer getCountEducationColleges() {
        return countEducationColleges;
    }

    public Integer getCountGeneralSecondaryEducation() {
        return countGeneralSecondaryEducation;
    }

    public void setRaion(String raion) {
        this.raion = raion;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setNameTipo(String nameTipo) {
        this.nameTipo = nameTipo;
    }

    public void setCountTeachers(Integer countTeachers) {
        this.countTeachers = countTeachers;
    }

    public void setCountAcceptedStudents(Integer countAcceptedStudents) {
        this.countAcceptedStudents = countAcceptedStudents;
    }

    public void setCountGraduates(Integer countGraduates) {
        this.countGraduates = countGraduates;
    }

    public void setCountReleasedStudents(Integer countReleasedStudents) {
        this.countReleasedStudents = countReleasedStudents;
    }

    public void setCountDesignCapacity(Integer countDesignCapacity) {
        this.countDesignCapacity = countDesignCapacity;
    }

    public void setCountCategoryC(Integer countCategoryC) {
        this.countCategoryC = countCategoryC;
    }

    public void setCountCategoryA(Integer countCategoryA) {
        this.countCategoryA = countCategoryA;
    }

    public void setCountCategoryB(Integer countCategoryB) {
        this.countCategoryB = countCategoryB;
    }

    public void setCountMagistrate(Integer countMagistrate) {
        this.countMagistrate = countMagistrate;
    }

    public void setCountDoctorEngineering(Integer countDoctorEngineering) {
        this.countDoctorEngineering = countDoctorEngineering;
    }

    public void setCountCategoryD(Integer countCategoryD) {
        this.countCategoryD = countCategoryD;
    }

    public void setCountEducationHigher(Integer countEducationHigher) {
        this.countEducationHigher = countEducationHigher;
    }

    public void setCountEducationColleges(Integer countEducationColleges) {
        this.countEducationColleges = countEducationColleges;
    }

    public void setCountGeneralSecondaryEducation(Integer countGeneralSecondaryEducation) {
        this.countGeneralSecondaryEducation = countGeneralSecondaryEducation;
    }
}
