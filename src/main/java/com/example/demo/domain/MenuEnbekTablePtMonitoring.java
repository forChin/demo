package com.example.demo.domain;

public class MenuEnbekTablePtMonitoring {
    private String view;            //gp_enbek_table_pt
    private Double employedPopulation;
    private Double need;

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }

    public Double getEmployedPopulation() {
        return employedPopulation;
    }

    public void setEmployedPopulation(Double employedPopulation) {
        this.employedPopulation = employedPopulation;
    }

    public Double getNeed() {
        return need;
    }

    public void setNeed(Double need) {
        this.need = need;
    }
}
