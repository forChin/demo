package com.example.demo.domain;

public class BInflationMonitoring {
    // new inflation

    private String column;
    private String o2011;
    private String o2012;
    private String o2013;
    private String o2014;
    private String o2015;
    private String o2016;
    private String o2017;
    private String o2018;
    private String o2019;
    private String o2020;

    public String getColumn() {
        return column;
    }

    public String getO2011() {
        return o2011;
    }

    public String getO2012() {
        return o2012;
    }

    public String getO2013() {
        return o2013;
    }

    public String getO2014() {
        return o2014;
    }

    public String getO2015() {
        return o2015;
    }

    public String getO2016() {
        return o2016;
    }

    public String getO2017() {
        return o2017;
    }

    public String getO2018() {
        return o2018;
    }

    public String getO2019() {
        return o2019;
    }

    public String getO2020() {
        return o2020;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public void setO2011(String o2011) {
        this.o2011 = o2011;
    }

    public void setO2012(String o2012) {
        this.o2012 = o2012;
    }

    public void setO2013(String o2013) {
        this.o2013 = o2013;
    }

    public void setO2014(String o2014) {
        this.o2014 = o2014;
    }

    public void setO2015(String o2015) {
        this.o2015 = o2015;
    }

    public void setO2016(String o2016) {
        this.o2016 = o2016;
    }

    public void setO2017(String o2017) {
        this.o2017 = o2017;
    }

    public void setO2018(String o2018) {
        this.o2018 = o2018;
    }

    public void setO2019(String o2019) {
        this.o2019 = o2019;
    }

    public void setO2020(String o2020) {
        this.o2020 = o2020;
    }
}
