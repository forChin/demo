package com.example.demo.domain;

public class MenuJiv2hnPMonitoring {

    private String year;            //ush_stat_jivotnovodstvo
    private String area;
    private String region;
    private Double numberPigsHouseholds;

    public String getYear() {
        return year;
    }

    public String getArea() {
        return area;
    }

    public String getRegion() {
        return region;
    }

    public Double getNumberPigsHouseholds() {
        return numberPigsHouseholds;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setNumberPigsHouseholds(Double numberPigsHouseholds) {
        this.numberPigsHouseholds = numberPigsHouseholds;
    }
}
