package com.example.demo.domain;

public class MenuEnbekMonitoring {
    private String year;        //gp_enbek
    private String area;
    private String region;
    private Integer countLabour;
    private Integer unemployedPopulation;
    private Integer countNeedIabor;
    private Integer numberPopulation;
    private Double unemploymentRate;
    private Double youthUnemploymentRate;
    private Double femaleUnemploymentRate;
    private Integer selfEmployedPopulation;
    private Integer productivelyEmployed;
    private Integer unproductiveBusy;
    private Double unproductiveBusyA;
    private Integer countActiveSubject;
    private Integer countInactiveSubject;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Integer getCountLabour() {
        return countLabour;
    }

    public void setCountLabour(Integer countLabour) {
        this.countLabour = countLabour;
    }

    public Integer getUnemployedPopulation() {
        return unemployedPopulation;
    }

    public void setUnemployedPopulation(Integer unemployedPopulation) {
        this.unemployedPopulation = unemployedPopulation;
    }

    public Integer getCountNeedIabor() {
        return countNeedIabor;
    }

    public void setCountNeedIabor(Integer countNeedIabor) {
        this.countNeedIabor = countNeedIabor;
    }

    public Integer getNumberPopulation() {
        return numberPopulation;
    }

    public void setNumberPopulation(Integer numberPopulation) {
        this.numberPopulation = numberPopulation;
    }

    public Double getUnemploymentRate() {
        return unemploymentRate;
    }

    public void setUnemploymentRate(Double unemploymentRate) {
        this.unemploymentRate = unemploymentRate;
    }

    public Double getYouthUnemploymentRate() {
        return youthUnemploymentRate;
    }

    public void setYouthUnemploymentRate(Double youthUnemploymentRate) {
        this.youthUnemploymentRate = youthUnemploymentRate;
    }

    public Double getFemaleUnemploymentRate() {
        return femaleUnemploymentRate;
    }

    public void setFemaleUnemploymentRate(Double femaleUnemploymentRate) {
        this.femaleUnemploymentRate = femaleUnemploymentRate;
    }

    public Integer getSelfEmployedPopulation() {
        return selfEmployedPopulation;
    }

    public void setSelfEmployedPopulation(Integer selfEmployedPopulation) {
        this.selfEmployedPopulation = selfEmployedPopulation;
    }

    public Integer getProductivelyEmployed() {
        return productivelyEmployed;
    }

    public void setProductivelyEmployed(Integer productivelyEmployed) {
        this.productivelyEmployed = productivelyEmployed;
    }

    public Integer getUnproductiveBusy() {
        return unproductiveBusy;
    }

    public void setUnproductiveBusy(Integer unproductiveBusy) {
        this.unproductiveBusy = unproductiveBusy;
    }

    public Double getUnproductiveBusyA() {
        return unproductiveBusyA;
    }

    public void setUnproductiveBusyA(Double unproductiveBusyA) {
        this.unproductiveBusyA = unproductiveBusyA;
    }

    public Integer getCountActiveSubject() {
        return countActiveSubject;
    }

    public void setCountActiveSubject(Integer countActiveSubject) {
        this.countActiveSubject = countActiveSubject;
    }

    public Integer getCountInactiveSubject() {
        return countInactiveSubject;
    }

    public void setCountInactiveSubject(Integer countInactiveSubject) {
        this.countInactiveSubject = countInactiveSubject;
    }
}
