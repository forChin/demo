package com.example.demo.domain;

public class BDorogiPointsMonitoring {
    // new dorogi points

    private String index;
    private Double maxPoint;

    public String getIndex() {
        return index;
    }

    public Double getMaxPoint() {
        return maxPoint;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public void setMaxPoint(Double maxPoint) {
        this.maxPoint = maxPoint;
    }
}
