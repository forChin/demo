package com.example.demo.domain;

public class ShPpoPMonitoring {
    //sh_posevnaya_po_produkt

    private String year;
    private String name;
    private String raion;
    private String produkt;
    private String pokazatel;
    private String shirota;
    private String dolgota;

    public String getYear() {
        return year;
    }

    public String getName() {
        return name;
    }

    public String getRaion() {
        return raion;
    }

    public String getProdukt() {
        return produkt;
    }

    public String getPokazatel() {
        return pokazatel;
    }

    public String getShirota() {
        return shirota;
    }

    public String getDolgota() {
        return dolgota;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRaion(String raion) {
        this.raion = raion;
    }

    public void setProdukt(String produkt) {
        this.produkt = produkt;
    }

    public void setPokazatel(String pokazatel) {
        this.pokazatel = pokazatel;
    }

    public void setShirota(String shirota) {
        this.shirota = shirota;
    }

    public void setDolgota(String dolgota) {
        this.dolgota = dolgota;
    }
}
