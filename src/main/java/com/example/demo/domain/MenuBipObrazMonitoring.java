package com.example.demo.domain;

public class MenuBipObrazMonitoring {

    private String nameProject;     //bip_obraz
    private String statusProject;
    private Double totalProjectCost;
    private Double allocatedAmountCurrentMoment;
    private Double capacity;
    private String unit;
    private String industry;
    private String category;
    private String typeObject;
    private String instruction;
    private String imageProject;
    private String region;
    private String ruralDistrict;
    private String ruralSettlement;


//    private String latitude;
//    private String longitude;

    public String getNameProject() {
        return nameProject;
    }

    public void setNameProject(String nameProject) {
        this.nameProject = nameProject;
    }

    public String getStatusProject() {
        return statusProject;
    }

    public void setStatusProject(String statusProject) {
        this.statusProject = statusProject;
    }

    public Double getTotalProjectCost() {
        return totalProjectCost;
    }

    public void setTotalProjectCost(Double totalProjectCost) {
        this.totalProjectCost = totalProjectCost;
    }

    public Double getAllocatedAmountCurrentMoment() {
        return allocatedAmountCurrentMoment;
    }

    public void setAllocatedAmountCurrentMoment(Double allocatedAmountCurrentMoment) {
        this.allocatedAmountCurrentMoment = allocatedAmountCurrentMoment;
    }

    public Double getCapacity() {
        return capacity;
    }

    public void setCapacity(Double capacity) {
        this.capacity = capacity;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTypeObject() {
        return typeObject;
    }

    public void setTypeObject(String typeObject) {
        this.typeObject = typeObject;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public String getImageProject() {
        return imageProject;
    }

    public void setImageProject(String imageProject) {
        this.imageProject = imageProject;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRuralDistrict() {
        return ruralDistrict;
    }

    public void setRuralDistrict(String ruralDistrict) {
        this.ruralDistrict = ruralDistrict;
    }

    public String getRuralSettlement() {
        return ruralSettlement;
    }

    public void setRuralSettlement(String ruralSettlement) {
        this.ruralSettlement = ruralSettlement;
    }

//    public String getLatitude() {
//        return latitude;
//    }
//
//    public void setLatitude(String latitude) {
//        this.latitude = latitude;
//    }
//
//    public String getLongitude() {
//        return longitude;
//    }
//
//    public void setLongitude(String longitude) {
//        this.longitude = longitude;
//    }



}
