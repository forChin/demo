package com.example.demo.domain;

import java.util.HashMap;
import java.util.Map;

public class MolodejPQMonitoring {

    private  String year;

    private Map<String, Double> youthUnemploymentRate;

    public MolodejPQMonitoring(){
        this.youthUnemploymentRate = new HashMap<>();
    }

    public String getYear() {
        return year;
    }

    public Map<String, Double> getYouthUnemploymentRate() {
        return youthUnemploymentRate;
    }

    public void setYouthUnemploymentRate(Map<String, Double> youthUnemploymentRate) {
        this.youthUnemploymentRate = youthUnemploymentRate;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
