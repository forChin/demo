package com.example.demo.domain;

import java.util.HashMap;
import java.util.Map;

public class ArchiveHyMonitoring {

    private String year;

    private Map<String, Double> receptionDocumentsArchive;
    private Map<String, Double> translationArchivalDocuments;
    private Map<String, Double> proportionDocuments;
    private Map<String, Double> issuanceArchivalCertificates;
    private Map<String, Double> thematicRequests;
    private Map<String, Double> socialLegalInquiries;

    public ArchiveHyMonitoring(){
        this.issuanceArchivalCertificates = new HashMap<>();
        this.proportionDocuments = new HashMap<>();
        this.receptionDocumentsArchive = new HashMap<>();
        this.socialLegalInquiries = new HashMap<>();
        this.thematicRequests = new HashMap<>();
        this.translationArchivalDocuments = new HashMap<>();
    }

    public String getYear() {
        return year;
    }

    public Map<String, Double> getReceptionDocumentsArchive() {
        return receptionDocumentsArchive;
    }

    public Map<String, Double> getTranslationArchivalDocuments() {
        return translationArchivalDocuments;
    }

    public Map<String, Double> getProportionDocuments() {
        return proportionDocuments;
    }

    public Map<String, Double> getIssuanceArchivalCertificates() {
        return issuanceArchivalCertificates;
    }

    public Map<String, Double> getThematicRequests() {
        return thematicRequests;
    }

    public Map<String, Double> getSocialLegalInquiries() {
        return socialLegalInquiries;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setReceptionDocumentsArchive(Map<String, Double> receptionDocumentsArchive) {
        this.receptionDocumentsArchive = receptionDocumentsArchive;
    }

    public void setTranslationArchivalDocuments(Map<String, Double> translationArchivalDocuments) {
        this.translationArchivalDocuments = translationArchivalDocuments;
    }

    public void setProportionDocuments(Map<String, Double> proportionDocuments) {
        this.proportionDocuments = proportionDocuments;
    }

    public void setIssuanceArchivalCertificates(Map<String, Double> issuanceArchivalCertificates) {
        this.issuanceArchivalCertificates = issuanceArchivalCertificates;
    }

    public void setThematicRequests(Map<String, Double> thematicRequests) {
        this.thematicRequests = thematicRequests;
    }

    public void setSocialLegalInquiries(Map<String, Double> socialLegalInquiries) {
        this.socialLegalInquiries = socialLegalInquiries;
    }
}
