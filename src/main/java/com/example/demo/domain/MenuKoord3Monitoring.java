package com.example.demo.domain;

public class MenuKoord3Monitoring {

    //sed_y_koordin_region

    private Double amountPaymentsOne;
    private Double amountPaymentsTwo;
    private Double amountBenefitsWithOneDisabled;
    private Double amountBenefitsWithSixDisabled;
    private Double invalidsTheFirstAmount;
    private Double invalidsSecondAmount;
    private Double invalidsThirdAmount;
    private Double amountInvalidsChildren;
    private Double amountMaternityBenefits;
    private Double amountMaternityBenefitsA;
    private Double amountInvalidsFirstGroup;
    private Double amountInvalidsSecondGroup;
    private Double amountInvalidsThirdGroup;
    private Double amountLargeMothersAllowance;
    private Double amountAllowanceRaisingDisabledchild;
    private Double amountDisabledCareAllowance;
    private Double amountLargeFamilyAllowance4Children;
    private Double amountLargeFamilyAllowance5Children;
    private Double amountLargeFamilyAllowance6Children;
    private Double amountLargeFamilyAllowance7Children;
    private Double amountBurialGrants;
    private Double amountBurialAllowancesParticipantsVov;
    private Double amountInvalidsParticipantsVov;
    private Double amountPersonsEquatedDisabledVov;
    private Double amountPersonsEquatedParticipants;
    private Double amountVdovamPogibshikh;
    private Double amountAbc;
    private Double amountBenefitsFrontWorkers;
    private Double amountAbcd;
    private Double amountIndividualsAwardedTitles;
    private Double amountHeroesSocialistLabor;
    private Double amountVictimsPoliticalRepression;
    private Double amountPersonalPensioners;

    public Double getAmountPaymentsOne() {
        return amountPaymentsOne;
    }

    public Double getAmountPaymentsTwo() {
        return amountPaymentsTwo;
    }

    public Double getAmountBenefitsWithOneDisabled() {
        return amountBenefitsWithOneDisabled;
    }

    public Double getAmountBenefitsWithSixDisabled() {
        return amountBenefitsWithSixDisabled;
    }

    public Double getInvalidsTheFirstAmount() {
        return invalidsTheFirstAmount;
    }

    public Double getInvalidsSecondAmount() {
        return invalidsSecondAmount;
    }

    public Double getInvalidsThirdAmount() {
        return invalidsThirdAmount;
    }

    public Double getAmountInvalidsChildren() {
        return amountInvalidsChildren;
    }

    public Double getAmountMaternityBenefits() {
        return amountMaternityBenefits;
    }

    public Double getAmountMaternityBenefitsA() {
        return amountMaternityBenefitsA;
    }

    public Double getAmountInvalidsFirstGroup() {
        return amountInvalidsFirstGroup;
    }

    public Double getAmountInvalidsSecondGroup() {
        return amountInvalidsSecondGroup;
    }

    public Double getAmountInvalidsThirdGroup() {
        return amountInvalidsThirdGroup;
    }

    public Double getAmountLargeMothersAllowance() {
        return amountLargeMothersAllowance;
    }

    public Double getAmountAllowanceRaisingDisabledchild() {
        return amountAllowanceRaisingDisabledchild;
    }

    public Double getAmountDisabledCareAllowance() {
        return amountDisabledCareAllowance;
    }

    public Double getAmountLargeFamilyAllowance4Children() {
        return amountLargeFamilyAllowance4Children;
    }

    public Double getAmountLargeFamilyAllowance5Children() {
        return amountLargeFamilyAllowance5Children;
    }

    public Double getAmountLargeFamilyAllowance6Children() {
        return amountLargeFamilyAllowance6Children;
    }

    public Double getAmountLargeFamilyAllowance7Children() {
        return amountLargeFamilyAllowance7Children;
    }

    public Double getAmountBurialGrants() {
        return amountBurialGrants;
    }

    public Double getAmountBurialAllowancesParticipantsVov() {
        return amountBurialAllowancesParticipantsVov;
    }

    public Double getAmountInvalidsParticipantsVov() {
        return amountInvalidsParticipantsVov;
    }

    public Double getAmountPersonsEquatedDisabledVov() {
        return amountPersonsEquatedDisabledVov;
    }

    public Double getAmountPersonsEquatedParticipants() {
        return amountPersonsEquatedParticipants;
    }

    public Double getAmountVdovamPogibshikh() {
        return amountVdovamPogibshikh;
    }

    public Double getAmountAbc() {
        return amountAbc;
    }

    public Double getAmountBenefitsFrontWorkers() {
        return amountBenefitsFrontWorkers;
    }

    public Double getAmountAbcd() {
        return amountAbcd;
    }

    public Double getAmountIndividualsAwardedTitles() {
        return amountIndividualsAwardedTitles;
    }

    public Double getAmountHeroesSocialistLabor() {
        return amountHeroesSocialistLabor;
    }

    public Double getAmountVictimsPoliticalRepression() {
        return amountVictimsPoliticalRepression;
    }

    public Double getAmountPersonalPensioners() {
        return amountPersonalPensioners;
    }

    public void setAmountPaymentsOne(Double amountPaymentsOne) {
        this.amountPaymentsOne = amountPaymentsOne;
    }

    public void setAmountPaymentsTwo(Double amountPaymentsTwo) {
        this.amountPaymentsTwo = amountPaymentsTwo;
    }

    public void setAmountBenefitsWithOneDisabled(Double amountBenefitsWithOneDisabled) {
        this.amountBenefitsWithOneDisabled = amountBenefitsWithOneDisabled;
    }

    public void setAmountBenefitsWithSixDisabled(Double amountBenefitsWithSixDisabled) {
        this.amountBenefitsWithSixDisabled = amountBenefitsWithSixDisabled;
    }

    public void setInvalidsTheFirstAmount(Double invalidsTheFirstAmount) {
        this.invalidsTheFirstAmount = invalidsTheFirstAmount;
    }

    public void setInvalidsSecondAmount(Double invalidsSecondAmount) {
        this.invalidsSecondAmount = invalidsSecondAmount;
    }

    public void setInvalidsThirdAmount(Double invalidsThirdAmount) {
        this.invalidsThirdAmount = invalidsThirdAmount;
    }

    public void setAmountInvalidsChildren(Double amountInvalidsChildren) {
        this.amountInvalidsChildren = amountInvalidsChildren;
    }

    public void setAmountMaternityBenefits(Double amountMaternityBenefits) {
        this.amountMaternityBenefits = amountMaternityBenefits;
    }

    public void setAmountMaternityBenefitsA(Double amountMaternityBenefitsA) {
        this.amountMaternityBenefitsA = amountMaternityBenefitsA;
    }

    public void setAmountInvalidsFirstGroup(Double amountInvalidsFirstGroup) {
        this.amountInvalidsFirstGroup = amountInvalidsFirstGroup;
    }

    public void setAmountInvalidsSecondGroup(Double amountInvalidsSecondGroup) {
        this.amountInvalidsSecondGroup = amountInvalidsSecondGroup;
    }

    public void setAmountInvalidsThirdGroup(Double amountInvalidsThirdGroup) {
        this.amountInvalidsThirdGroup = amountInvalidsThirdGroup;
    }

    public void setAmountLargeMothersAllowance(Double amountLargeMothersAllowance) {
        this.amountLargeMothersAllowance = amountLargeMothersAllowance;
    }

    public void setAmountAllowanceRaisingDisabledchild(Double amountAllowanceRaisingDisabledchild) {
        this.amountAllowanceRaisingDisabledchild = amountAllowanceRaisingDisabledchild;
    }

    public void setAmountDisabledCareAllowance(Double amountDisabledCareAllowance) {
        this.amountDisabledCareAllowance = amountDisabledCareAllowance;
    }

    public void setAmountLargeFamilyAllowance4Children(Double amountLargeFamilyAllowance4Children) {
        this.amountLargeFamilyAllowance4Children = amountLargeFamilyAllowance4Children;
    }

    public void setAmountLargeFamilyAllowance5Children(Double amountLargeFamilyAllowance5Children) {
        this.amountLargeFamilyAllowance5Children = amountLargeFamilyAllowance5Children;
    }

    public void setAmountLargeFamilyAllowance6Children(Double amountLargeFamilyAllowance6Children) {
        this.amountLargeFamilyAllowance6Children = amountLargeFamilyAllowance6Children;
    }

    public void setAmountLargeFamilyAllowance7Children(Double amountLargeFamilyAllowance7Children) {
        this.amountLargeFamilyAllowance7Children = amountLargeFamilyAllowance7Children;
    }

    public void setAmountBurialGrants(Double amountBurialGrants) {
        this.amountBurialGrants = amountBurialGrants;
    }

    public void setAmountBurialAllowancesParticipantsVov(Double amountBurialAllowancesParticipantsVov) {
        this.amountBurialAllowancesParticipantsVov = amountBurialAllowancesParticipantsVov;
    }

    public void setAmountInvalidsParticipantsVov(Double amountInvalidsParticipantsVov) {
        this.amountInvalidsParticipantsVov = amountInvalidsParticipantsVov;
    }

    public void setAmountPersonsEquatedDisabledVov(Double amountPersonsEquatedDisabledVov) {
        this.amountPersonsEquatedDisabledVov = amountPersonsEquatedDisabledVov;
    }

    public void setAmountPersonsEquatedParticipants(Double amountPersonsEquatedParticipants) {
        this.amountPersonsEquatedParticipants = amountPersonsEquatedParticipants;
    }

    public void setAmountVdovamPogibshikh(Double amountVdovamPogibshikh) {
        this.amountVdovamPogibshikh = amountVdovamPogibshikh;
    }

    public void setAmountAbc(Double amountAbc) {
        this.amountAbc = amountAbc;
    }

    public void setAmountBenefitsFrontWorkers(Double amountBenefitsFrontWorkers) {
        this.amountBenefitsFrontWorkers = amountBenefitsFrontWorkers;
    }

    public void setAmountAbcd(Double amountAbcd) {
        this.amountAbcd = amountAbcd;
    }

    public void setAmountIndividualsAwardedTitles(Double amountIndividualsAwardedTitles) {
        this.amountIndividualsAwardedTitles = amountIndividualsAwardedTitles;
    }

    public void setAmountHeroesSocialistLabor(Double amountHeroesSocialistLabor) {
        this.amountHeroesSocialistLabor = amountHeroesSocialistLabor;
    }

    public void setAmountVictimsPoliticalRepression(Double amountVictimsPoliticalRepression) {
        this.amountVictimsPoliticalRepression = amountVictimsPoliticalRepression;
    }

    public void setAmountPersonalPensioners(Double amountPersonalPensioners) {
        this.amountPersonalPensioners = amountPersonalPensioners;
    }
}
