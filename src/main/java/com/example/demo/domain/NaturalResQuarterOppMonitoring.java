package com.example.demo.domain;

import java.util.HashMap;
import java.util.Map;

public class NaturalResQuarterOppMonitoring {

    private String year;

    private Map<String, Double> implementationActionplan;
    private Map<String, Double> exterminationWolves;

    public NaturalResQuarterOppMonitoring(){
        this.implementationActionplan = new HashMap<>();
        this.exterminationWolves = new HashMap<>();
    }

    public String getYear() {
        return year;
    }

    public Map<String, Double> getImplementationActionplan() {
        return implementationActionplan;
    }

    public Map<String, Double> getExterminationWolves() {
        return exterminationWolves;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setImplementationActionplan(Map<String, Double> implementationActionplan) {
        this.implementationActionplan = implementationActionplan;
    }

    public void setExterminationWolves(Map<String, Double> exterminationWolves) {
        this.exterminationWolves = exterminationWolves;
    }
}
