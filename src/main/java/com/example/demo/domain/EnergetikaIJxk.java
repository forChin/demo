package com.example.demo.domain;

import java.util.function.DoubleUnaryOperator;

public class EnergetikaIJxk {

    private String area;
    private String year;
    private String month;
    private Double constructionGasmainpipelines;
    private String unit;
    private Double constructionWaterfacilities;
    private String unitB;
    private Double constructionDistributionfacilities;
    private String unitC;
    private Double powerAirconditioning;
    private String unitD;
    private Double waterWastedistribution;
    private String unitE;
    private Double emergencyHeatsupply;
    private String unitF;
    private Double emergencyPoweroutages;
    private String unitG;
    private Double emergencyGassupply;
    private String unitH;
    private Double emergencySupplynetworks;
    private String unitI;
    private Double totalBoilerrooms;
    private String unitJ;
    private Double renovated;
    private String unitK;
    private Double educations;
    private String unitL;
    private Double healths;
    private String unitM;
    private Double residentialBuilding;
    private String unitN;
    private Double coal;
    private String unitO;
    private Double liquidFuel;
    private String unitP;
    private Double totalLengthHeatnetworks;
    private String unitQ;
    private Double totalLengthElectricnetworks;
    private String unitR;
    private Double totalLengthWaterseweragenetworks;
    private String unitS;


    public EnergetikaIJxk() {

    }


    public String getArea() {
        return area;
    }

    public String getYear() {
        return year;
    }

    public String getMonth() {
        return month;
    }

    public Double getConstructionGasmainpipelines() {
        return constructionGasmainpipelines;
    }

    public String getUnit() {
        return unit;
    }

    public Double getConstructionWaterfacilities() {
        return constructionWaterfacilities;
    }

    public String getUnitB() {
        return unitB;
    }

    public Double getConstructionDistributionfacilities() {
        return constructionDistributionfacilities;
    }

    public String getUnitC() {
        return unitC;
    }

    public Double getPowerAirconditioning() {
        return powerAirconditioning;
    }

    public String getUnitD() {
        return unitD;
    }

    public Double getWaterWastedistribution() {
        return waterWastedistribution;
    }

    public String getUnitE() {
        return unitE;
    }

    public Double getEmergencyHeatsupply() {
        return emergencyHeatsupply;
    }

    public String getUnitF() {
        return unitF;
    }

    public Double getEmergencyPoweroutages() {
        return emergencyPoweroutages;
    }

    public String getUnitG() {
        return unitG;
    }

    public Double getEmergencyGassupply() {
        return emergencyGassupply;
    }

    public String getUnitH() {
        return unitH;
    }

    public Double getEmergencySupplynetworks() {
        return emergencySupplynetworks;
    }

    public String getUnitI() {
        return unitI;
    }

    public Double getTotalBoilerrooms() {
        return totalBoilerrooms;
    }

    public String getUnitJ() {
        return unitJ;
    }

    public Double getRenovated() {
        return renovated;
    }

    public String getUnitK() {
        return unitK;
    }

    public Double getEducations() {
        return educations;
    }

    public String getUnitL() {
        return unitL;
    }

    public Double getHealths() {
        return healths;
    }

    public String getUnitM() {
        return unitM;
    }

    public Double getResidentialBuilding() {
        return residentialBuilding;
    }

    public String getUnitN() {
        return unitN;
    }

    public Double getCoal() {
        return coal;
    }

    public String getUnitO() {
        return unitO;
    }

    public Double getLiquidFuel() {
        return liquidFuel;
    }

    public String getUnitP() {
        return unitP;
    }

    public Double getTotalLengthHeatnetworks() {
        return totalLengthHeatnetworks;
    }

    public String getUnitQ() {
        return unitQ;
    }

    public Double getTotalLengthElectricnetworks() {
        return totalLengthElectricnetworks;
    }

    public String getUnitR() {
        return unitR;
    }

    public Double getTotalLengthWaterseweragenetworks() {
        return totalLengthWaterseweragenetworks;
    }

    public String getUnitS() {
        return unitS;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public void setConstructionGasmainpipelines(Double constructionGasmainpipelines) {
        this.constructionGasmainpipelines = constructionGasmainpipelines;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setConstructionWaterfacilities(Double constructionWaterfacilities) {
        this.constructionWaterfacilities = constructionWaterfacilities;
    }

    public void setUnitB(String unitB) {
        this.unitB = unitB;
    }

    public void setConstructionDistributionfacilities(Double constructionDistributionfacilities) {
        this.constructionDistributionfacilities = constructionDistributionfacilities;
    }

    public void setUnitC(String unitC) {
        this.unitC = unitC;
    }

    public void setPowerAirconditioning(Double powerAirconditioning) {
        this.powerAirconditioning = powerAirconditioning;
    }

    public void setUnitD(String unitD) {
        this.unitD = unitD;
    }

    public void setWaterWastedistribution(Double waterWastedistribution) {
        this.waterWastedistribution = waterWastedistribution;
    }

    public void setUnitE(String unitE) {
        this.unitE = unitE;
    }

    public void setEmergencyHeatsupply(Double emergencyHeatsupply) {
        this.emergencyHeatsupply = emergencyHeatsupply;
    }

    public void setUnitF(String unitF) {
        this.unitF = unitF;
    }

    public void setEmergencyPoweroutages(Double emergencyPoweroutages) {
        this.emergencyPoweroutages = emergencyPoweroutages;
    }

    public void setUnitG(String unitG) {
        this.unitG = unitG;
    }

    public void setEmergencyGassupply(Double emergencyGassupply) {
        this.emergencyGassupply = emergencyGassupply;
    }

    public void setUnitH(String unitH) {
        this.unitH = unitH;
    }

    public void setEmergencySupplynetworks(Double emergencySupplynetworks) {
        this.emergencySupplynetworks = emergencySupplynetworks;
    }

    public void setUnitI(String unitI) {
        this.unitI = unitI;
    }

    public void setTotalBoilerrooms(Double totalBoilerrooms) {
        this.totalBoilerrooms = totalBoilerrooms;
    }

    public void setUnitJ(String unitJ) {
        this.unitJ = unitJ;
    }

    public void setRenovated(Double renovated) {
        this.renovated = renovated;
    }

    public void setUnitK(String unitK) {
        this.unitK = unitK;
    }

    public void setEducations(Double educations) {
        this.educations = educations;
    }

    public void setUnitL(String unitL) {
        this.unitL = unitL;
    }

    public void setHealths(Double healths) {
        this.healths = healths;
    }

    public void setUnitM(String unitM) {
        this.unitM = unitM;
    }

    public void setResidentialBuilding(Double residentialBuilding) {
        this.residentialBuilding = residentialBuilding;
    }

    public void setUnitN(String unitN) {
        this.unitN = unitN;
    }

    public void setCoal(Double coal) {
        this.coal = coal;
    }

    public void setUnitO(String unitO) {
        this.unitO = unitO;
    }

    public void setLiquidFuel(Double liquidFuel) {
        this.liquidFuel = liquidFuel;
    }

    public void setUnitP(String unitP) {
        this.unitP = unitP;
    }

    public void setTotalLengthHeatnetworks(Double totalLengthHeatnetworks) {
        this.totalLengthHeatnetworks = totalLengthHeatnetworks;
    }

    public void setUnitQ(String unitQ) {
        this.unitQ = unitQ;
    }

    public void setTotalLengthElectricnetworks(Double totalLengthElectricnetworks) {
        this.totalLengthElectricnetworks = totalLengthElectricnetworks;
    }

    public void setUnitR(String unitR) {
        this.unitR = unitR;
    }

    public void setTotalLengthWaterseweragenetworks(Double totalLengthWaterseweragenetworks) {
        this.totalLengthWaterseweragenetworks = totalLengthWaterseweragenetworks;
    }

    public void setUnitS(String unitS) {
        this.unitS = unitS;
    }
}
