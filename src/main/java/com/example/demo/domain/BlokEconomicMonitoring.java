package com.example.demo.domain;

public class BlokEconomicMonitoring {
    // blok economic

    private String raion;
    private String ruralDistrict;
    private String ruralSettlement;
    private Double soilFertility;
    private Double distanceFromMarkets;
    private Integer presenceLargeEnterprise;
    private Integer developmentAgriculturalProducts;
    private Integer countTractors;
    private Integer countCombine;
    private Double availabilityIrrigationsSystems;
    private Integer arable;
    private Integer pasturesNatural;
    private Integer pasturesForageCrops;
    private Integer farmingA;
    private Integer farmingB;
    private Integer total;
    private String year;

    public String getRaion() {
        return raion;
    }

    public String getRuralDistrict() {
        return ruralDistrict;
    }

    public String getRuralSettlement() {
        return ruralSettlement;
    }

    public Double getSoilFertility() {
        return soilFertility;
    }

    public Double getDistanceFromMarkets() {
        return distanceFromMarkets;
    }

    public Integer getPresenceLargeEnterprise() {
        return presenceLargeEnterprise;
    }

    public Integer getDevelopmentAgriculturalProducts() {
        return developmentAgriculturalProducts;
    }

    public Integer getCountTractors() {
        return countTractors;
    }

    public Integer getCountCombine() {
        return countCombine;
    }

    public Double getAvailabilityIrrigationsSystems() {
        return availabilityIrrigationsSystems;
    }

    public Integer getArable() {
        return arable;
    }

    public Integer getPasturesNatural() {
        return pasturesNatural;
    }

    public Integer getPasturesForageCrops() {
        return pasturesForageCrops;
    }

    public Integer getFarmingA() {
        return farmingA;
    }

    public Integer getFarmingB() {
        return farmingB;
    }

    public Integer getTotal() {
        return total;
    }

    public String getYear() {
        return year;
    }

    public void setRaion(String raion) {
        this.raion = raion;
    }

    public void setRuralDistrict(String ruralDistrict) {
        this.ruralDistrict = ruralDistrict;
    }

    public void setRuralSettlement(String ruralSettlement) {
        this.ruralSettlement = ruralSettlement;
    }

    public void setSoilFertility(Double soilFertility) {
        this.soilFertility = soilFertility;
    }

    public void setDistanceFromMarkets(Double distanceFromMarkets) {
        this.distanceFromMarkets = distanceFromMarkets;
    }

    public void setPresenceLargeEnterprise(Integer presenceLargeEnterprise) {
        this.presenceLargeEnterprise = presenceLargeEnterprise;
    }

    public void setDevelopmentAgriculturalProducts(Integer developmentAgriculturalProducts) {
        this.developmentAgriculturalProducts = developmentAgriculturalProducts;
    }

    public void setCountTractors(Integer countTractors) {
        this.countTractors = countTractors;
    }

    public void setCountCombine(Integer countCombine) {
        this.countCombine = countCombine;
    }

    public void setAvailabilityIrrigationsSystems(Double availabilityIrrigationsSystems) {
        this.availabilityIrrigationsSystems = availabilityIrrigationsSystems;
    }

    public void setArable(Integer arable) {
        this.arable = arable;
    }

    public void setPasturesNatural(Integer pasturesNatural) {
        this.pasturesNatural = pasturesNatural;
    }

    public void setPasturesForageCrops(Integer pasturesForageCrops) {
        this.pasturesForageCrops = pasturesForageCrops;
    }

    public void setFarmingA(Integer farmingA) {
        this.farmingA = farmingA;
    }

    public void setFarmingB(Integer farmingB) {
        this.farmingB = farmingB;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
