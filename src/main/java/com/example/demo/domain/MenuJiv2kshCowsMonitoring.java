package com.example.demo.domain;

public class MenuJiv2kshCowsMonitoring {

    private String year;            //ush_stat_jivotnovodstvo
    private String area;
    private String region;
    private Double numberLivestockPoultry;

    private Double numberCowsFarms;

    public String getYear() {
        return year;
    }

    public String getArea() {
        return area;
    }

    public String getRegion() {
        return region;
    }

    public Double getNumberLivestockPoultry() {
        return numberLivestockPoultry;
    }

    public Double getNumberCowsFarms() {
        return numberCowsFarms;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setNumberLivestockPoultry(Double numberLivestockPoultry) {
        this.numberLivestockPoultry = numberLivestockPoultry;
    }

    public void setNumberCowsFarms(Double numberCowsFarms) {
        this.numberCowsFarms = numberCowsFarms;
    }
}
