package com.example.demo.domain;

public class BSpisokShkolyMonitoring {
    // new spisok shkoly

    private String schoolName;
    private String area;
    private String raion;
    private String ruralDistruct;
    private String snp;
    private String adress;
    private String yearConstruction;
    private String yearCaprenovation;
    private Double accidentRate;

    public String getSchoolName() {
        return schoolName;
    }

    public String getArea() {
        return area;
    }

    public String getRaion() {
        return raion;
    }

    public String getRuralDistruct() {
        return ruralDistruct;
    }

    public String getSnp() {
        return snp;
    }

    public String getAdress() {
        return adress;
    }

    public String getYearConstruction() {
        return yearConstruction;
    }

    public String getYearCaprenovation() {
        return yearCaprenovation;
    }

    public Double getAccidentRate() {
        return accidentRate;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setRaion(String raion) {
        this.raion = raion;
    }

    public void setRuralDistruct(String ruralDistruct) {
        this.ruralDistruct = ruralDistruct;
    }

    public void setSnp(String snp) {
        this.snp = snp;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public void setYearConstruction(String yearConstruction) {
        this.yearConstruction = yearConstruction;
    }

    public void setYearCaprenovation(String yearCaprenovation) {
        this.yearCaprenovation = yearCaprenovation;
    }

    public void setAccidentRate(Double accidentRate) {
        this.accidentRate = accidentRate;
    }
}
