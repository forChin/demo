package com.example.demo.domain;

public class MenuEduDdoMonitoring {
    private String year;        //edu_ddo
    private String area;
    private String region;
    private String ruralDistrict;
    private String nameDdo;
    private Integer numberChildren;
    private Integer numberFrames;
    private Integer numberPackages;
    private Integer numberWaitingLists;
    private Integer age;
    private Integer ageA;
    private Integer ageB;
    private Integer ageC;
    private Integer ageD;
    private Integer experience;
    private Integer experienceA;
    private Integer experienceB;
    private Integer experienceC;
    private Integer education;
    private Integer educationA;
    private String adress;
    private String yearA;
    private String workingTime;
    private String contactNumber;
    private String workingTimeA;
    private String lastName;
    private String firstName;
    private String otchestvo;
    private String ruralSettlement;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRuralDistrict() {
        return ruralDistrict;
    }

    public void setRuralDistrict(String ruralDistrict) {
        this.ruralDistrict = ruralDistrict;
    }

    public String getNameDdo() {
        return nameDdo;
    }

    public void setNameDdo(String nameDdo) {
        this.nameDdo = nameDdo;
    }

    public Integer getNumberChildren() {
        return numberChildren;
    }

    public void setNumberChildren(Integer numberChildren) {
        this.numberChildren = numberChildren;
    }

    public Integer getNumberFrames() {
        return numberFrames;
    }

    public void setNumberFrames(Integer numberFrames) {
        this.numberFrames = numberFrames;
    }

    public Integer getNumberPackages() {
        return numberPackages;
    }

    public void setNumberPackages(Integer numberPackages) {
        this.numberPackages = numberPackages;
    }

    public Integer getNumberWaitingLists() {
        return numberWaitingLists;
    }

    public void setNumberWaitingLists(Integer numberWaitingLists) {
        this.numberWaitingLists = numberWaitingLists;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getAgeA() {
        return ageA;
    }

    public void setAgeA(Integer ageA) {
        this.ageA = ageA;
    }

    public Integer getAgeB() {
        return ageB;
    }

    public void setAgeB(Integer ageB) {
        this.ageB = ageB;
    }

    public Integer getAgeC() {
        return ageC;
    }

    public void setAgeC(Integer ageC) {
        this.ageC = ageC;
    }

    public Integer getAgeD() {
        return ageD;
    }

    public void setAgeD(Integer ageD) {
        this.ageD = ageD;
    }

    public Integer getExperience() {
        return experience;
    }

    public void setExperience(Integer experience) {
        this.experience = experience;
    }

    public Integer getExperienceA() {
        return experienceA;
    }

    public void setExperienceA(Integer experienceA) {
        this.experienceA = experienceA;
    }

    public Integer getExperienceB() {
        return experienceB;
    }

    public void setExperienceB(Integer experienceB) {
        this.experienceB = experienceB;
    }

    public Integer getExperienceC() {
        return experienceC;
    }

    public void setExperienceC(Integer experienceC) {
        this.experienceC = experienceC;
    }

    public Integer getEducation() {
        return education;
    }

    public void setEducation(Integer education) {
        this.education = education;
    }

    public Integer getEducationA() {
        return educationA;
    }

    public void setEducationA(Integer educationA) {
        this.educationA = educationA;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getYearA() {
        return yearA;
    }

    public void setYearA(String yearA) {
        this.yearA = yearA;
    }

    public String getWorkingTime() {
        return workingTime;
    }

    public void setWorkingTime(String workingTime) {
        this.workingTime = workingTime;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getWorkingTimeA() {
        return workingTimeA;
    }

    public void setWorkingTimeA(String workingTimeA) {
        this.workingTimeA = workingTimeA;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getOtchestvo() {
        return otchestvo;
    }

    public void setOtchestvo(String otchestvo) {
        this.otchestvo = otchestvo;
    }

    public String getRuralSettlement() {
        return ruralSettlement;
    }

    public void setRuralSettlement(String ruralSettlement) {
        this.ruralSettlement = ruralSettlement;
    }
}
