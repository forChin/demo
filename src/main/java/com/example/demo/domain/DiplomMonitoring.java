package com.example.demo.domain;

public class DiplomMonitoring {
    // diplom

    private String lastname;
    private String firstname;
    private String otchestvo;
    private String idPerson;
    private String pol;
    private Integer age;
    private String areaFrom;
    private String regionFrom;
    private String ruralDistrictFrom;
    private String typeEducation;
    private String profession;
    private String professionRus;
    private String professionKz;
    private String educationalInstitution;
    private String areaIn;
    private String regionIn;
    private String ruralDistrictIn;
    private String ruralSettlementIn;
    private String workspace;
    private String fieldActivity;
    private String activity;
    private String statusEmployee;
    private String commissionDecision;
    private String contractNumber;
    private Double sumGrant;
    private String unit;
    private String needReturn;
    private Integer refund;
    private String reasonReturn;
    private Double contractSumm;
    private String unitCredit;
    private Double balanceCredit;
    private String reasonForRefund;
    private String reasonProjectCompletion;
    private String contractNumberA;
    private String dateBirthday;
    private String dateGraduation;
    private String arrivalDate;
    private String dateDismissalTransfer;
    private String dataContract;
    private String dateReturn;
    private String dateAgreementLoan;
    private Double sbid;

    public Double getSbid() {
        return sbid;
    }

    public void setSbid(Double sbid) {
        this.sbid = sbid;
    }

    public String getLastname() {
        return lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getOtchestvo() {
        return otchestvo;
    }

    public String getIdPerson() {
        return idPerson;
    }

    public String getPol() {
        return pol;
    }

    public Integer getAge() {
        return age;
    }

    public String getAreaFrom() {
        return areaFrom;
    }

    public String getRegionFrom() {
        return regionFrom;
    }

    public String getRuralDistrictFrom() {
        return ruralDistrictFrom;
    }

    public String getTypeEducation() {
        return typeEducation;
    }

    public String getProfession() {
        return profession;
    }

    public String getProfessionRus() {
        return professionRus;
    }

    public String getProfessionKz() {
        return professionKz;
    }

    public String getEducationalInstitution() {
        return educationalInstitution;
    }

    public String getAreaIn() {
        return areaIn;
    }

    public String getRegionIn() {
        return regionIn;
    }

    public String getRuralDistrictIn() {
        return ruralDistrictIn;
    }

    public String getRuralSettlementIn() {
        return ruralSettlementIn;
    }

    public String getWorkspace() {
        return workspace;
    }

    public String getFieldActivity() {
        return fieldActivity;
    }

    public String getActivity() {
        return activity;
    }

    public String getStatusEmployee() {
        return statusEmployee;
    }

    public String getCommissionDecision() {
        return commissionDecision;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public Double getSumGrant() {
        return sumGrant;
    }

    public String getUnit() {
        return unit;
    }

    public String getNeedReturn() {
        return needReturn;
    }

    public Integer getRefund() {
        return refund;
    }

    public String getReasonReturn() {
        return reasonReturn;
    }

    public Double getContractSumm() {
        return contractSumm;
    }

    public String getUnitCredit() {
        return unitCredit;
    }

    public Double getBalanceCredit() {
        return balanceCredit;
    }

    public String getReasonForRefund() {
        return reasonForRefund;
    }

    public String getReasonProjectCompletion() {
        return reasonProjectCompletion;
    }

    public String getContractNumberA() {
        return contractNumberA;
    }

    public String getDateBirthday() {
        return dateBirthday;
    }

    public String getDateGraduation() {
        return dateGraduation;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public String getDateDismissalTransfer() {
        return dateDismissalTransfer;
    }

    public String getDataContract() {
        return dataContract;
    }

    public String getDateReturn() {
        return dateReturn;
    }

    public String getDateAgreementLoan() {
        return dateAgreementLoan;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setOtchestvo(String otchestvo) {
        this.otchestvo = otchestvo;
    }

    public void setIdPerson(String idPerson) {
        this.idPerson = idPerson;
    }

    public void setPol(String pol) {
        this.pol = pol;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setAreaFrom(String areaFrom) {
        this.areaFrom = areaFrom;
    }

    public void setRegionFrom(String regionFrom) {
        this.regionFrom = regionFrom;
    }

    public void setRuralDistrictFrom(String ruralDistrictFrom) {
        this.ruralDistrictFrom = ruralDistrictFrom;
    }

    public void setTypeEducation(String typeEducation) {
        this.typeEducation = typeEducation;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public void setProfessionRus(String professionRus) {
        this.professionRus = professionRus;
    }

    public void setProfessionKz(String professionKz) {
        this.professionKz = professionKz;
    }

    public void setEducationalInstitution(String educationalInstitution) {
        this.educationalInstitution = educationalInstitution;
    }

    public void setAreaIn(String areaIn) {
        this.areaIn = areaIn;
    }

    public void setRegionIn(String regionIn) {
        this.regionIn = regionIn;
    }

    public void setRuralDistrictIn(String ruralDistrictIn) {
        this.ruralDistrictIn = ruralDistrictIn;
    }

    public void setRuralSettlementIn(String ruralSettlementIn) {
        this.ruralSettlementIn = ruralSettlementIn;
    }

    public void setWorkspace(String workspace) {
        this.workspace = workspace;
    }

    public void setFieldActivity(String fieldActivity) {
        this.fieldActivity = fieldActivity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public void setStatusEmployee(String statusEmployee) {
        this.statusEmployee = statusEmployee;
    }

    public void setCommissionDecision(String commissionDecision) {
        this.commissionDecision = commissionDecision;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public void setSumGrant(Double sumGrant) {
        this.sumGrant = sumGrant;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setNeedReturn(String needReturn) {
        this.needReturn = needReturn;
    }

    public void setRefund(Integer refund) {
        this.refund = refund;
    }

    public void setReasonReturn(String reasonReturn) {
        this.reasonReturn = reasonReturn;
    }

    public void setContractSumm(Double contractSumm) {
        this.contractSumm = contractSumm;
    }

    public void setUnitCredit(String unitCredit) {
        this.unitCredit = unitCredit;
    }

    public void setBalanceCredit(Double balanceCredit) {
        this.balanceCredit = balanceCredit;
    }

    public void setReasonForRefund(String reasonForRefund) {
        this.reasonForRefund = reasonForRefund;
    }

    public void setReasonProjectCompletion(String reasonProjectCompletion) {
        this.reasonProjectCompletion = reasonProjectCompletion;
    }

    public void setContractNumberA(String contractNumberA) {
        this.contractNumberA = contractNumberA;
    }

    public void setDateBirthday(String dateBirthday) {
        this.dateBirthday = dateBirthday;
    }

    public void setDateGraduation(String dateGraduation) {
        this.dateGraduation = dateGraduation;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public void setDateDismissalTransfer(String dateDismissalTransfer) {
        this.dateDismissalTransfer = dateDismissalTransfer;
    }

    public void setDataContract(String dataContract) {
        this.dataContract = dataContract;
    }

    public void setDateReturn(String dateReturn) {
        this.dateReturn = dateReturn;
    }

    public void setDateAgreementLoan(String dateAgreementLoan) {
        this.dateAgreementLoan = dateAgreementLoan;
    }
}
