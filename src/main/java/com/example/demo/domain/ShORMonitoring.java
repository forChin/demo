package com.example.demo.domain;

public class ShORMonitoring {
    //sh_obyem_ryby

    private String obyem;
    private String index;
    private String year;

    public String getObyem() {
        return obyem;
    }

    public String getIndex() {
        return index;
    }

    public String getYear() {
        return year;
    }

    public void setObyem(String obyem) {
        this.obyem = obyem;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
