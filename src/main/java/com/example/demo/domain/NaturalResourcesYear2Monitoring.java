package com.example.demo.domain;

public class NaturalResourcesYear2Monitoring {

    private String year;
    private Double registrationContracts;

    public NaturalResourcesYear2Monitoring(){

    }

    public String getYear() {
        return year;
    }

    public Double getRegistrationContracts() {
        return registrationContracts;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setRegistrationContracts(Double registrationContracts) {
        this.registrationContracts = registrationContracts;
    }
}
