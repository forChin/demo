package com.example.demo.domain;

public class MenuRybAqva2TableMonitoring {
    private String country;     //ush_stat_ryb_i_aqva_2_table
    private Double exportTonna;
    private String year;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Double getExportTonna() {
        return exportTonna;
    }

    public void setExportTonna(Double exportTonna) {
        this.exportTonna = exportTonna;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
