package com.example.demo.domain;

public class MenuJiv2hnFMonitoring {

    private String year;            //ush_stat_jivotnovodstvo
    private String area;
    private String region;
    private Double outputFoalsHouseholds;

    public String getYear() {
        return year;
    }

    public String getArea() {
        return area;
    }

    public String getRegion() {
        return region;
    }

    public Double getOutputFoalsHouseholds() {
        return outputFoalsHouseholds;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setOutputFoalsHouseholds(Double outputFoalsHouseholds) {
        this.outputFoalsHouseholds = outputFoalsHouseholds;
    }
}
