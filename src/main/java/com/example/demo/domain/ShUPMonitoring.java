package com.example.demo.domain;

public class ShUPMonitoring {
    //sh_ubrannaya_pl

    private String kultura;
    private String year;
    private String raion;
    private String kategory;
    private String pokazatel;

    public String getKultura() {
        return kultura;
    }

    public String getYear() {
        return year;
    }

    public String getRaion() {
        return raion;
    }

    public String getKategory() {
        return kategory;
    }

    public String getPokazatel() {
        return pokazatel;
    }

    public void setKultura(String kultura) {
        this.kultura = kultura;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setRaion(String raion) {
        this.raion = raion;
    }

    public void setKategory(String kategory) {
        this.kategory = kategory;
    }

    public void setPokazatel(String pokazatel) {
        this.pokazatel = pokazatel;
    }
}
