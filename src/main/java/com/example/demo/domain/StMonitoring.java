package com.example.demo.domain;

public class StMonitoring {
    //strateg_upr_2

    private String raion;
    private String napravlenie;
    private String razdel;
    private String indikator;
    private String edIzm;
    private Integer number;
    private String data;
    private String fact;
    private String klPokazatel;
    private String tendenciya;
    private String year;
    private Integer nPeriod;
    private String periodi4nost;
    private String period;
    private String plan;
    private Integer raspred;


    public void setTendenciya(String tendenciya) {
        this.tendenciya = tendenciya;
    }

    public void setPeriodi4nost(String periodi4nost) {
        this.periodi4nost = periodi4nost;
    }

    public void setRaspred(Integer raspred) {
        this.raspred = raspred;
    }

    public String getTendenciya() {
        return tendenciya;
    }

    public String getPeriodi4nost() {
        return periodi4nost;
    }

    public Integer getRaspred() {
        return raspred;
    }

    public String getRaion() {
        return raion;
    }

    public String getNapravlenie() {
        return napravlenie;
    }

    public String getRazdel() {
        return razdel;
    }

    public String getIndikator() {
        return indikator;
    }

    public String getEdIzm() {
        return edIzm;
    }

    public Integer getNumber() {
        return number;
    }

    public String getData() {
        return data;
    }

    public String getFact() {
        return fact;
    }

    public String getKlPokazatel() {
        return klPokazatel;
    }

    public String getYear() {
        return year;
    }

    public Integer getnPeriod() {
        return nPeriod;
    }

    public String getPeriod() {
        return period;
    }

    public String getPlan() {
        return plan;
    }

    public void setRaion(String raion) {
        this.raion = raion;
    }

    public void setNapravlenie(String napravlenie) {
        this.napravlenie = napravlenie;
    }

    public void setRazdel(String razdel) {
        this.razdel = razdel;
    }

    public void setIndikator(String indikator) {
        this.indikator = indikator;
    }

    public void setEdIzm(String edIzm) {
        this.edIzm = edIzm;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setFact(String fact) {
        this.fact = fact;
    }

    public void setKlPokazatel(String klPokazatel) {
        this.klPokazatel = klPokazatel;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setnPeriod(Integer nPeriod) {
        this.nPeriod = nPeriod;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }
}
