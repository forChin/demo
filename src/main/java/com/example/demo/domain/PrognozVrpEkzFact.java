package com.example.demo.domain;

public class PrognozVrpEkzFact {

    private String year;
    private Double oilPrice;
    private Double inflation;
    private Double exchangeRate;
    private Double population;
    private Double populationAtyrau;
    private Double populationZhylyoi;
    private Double populationInder;
    private Double populationIsatai;
    private Double populationKurmangazy;
    private Double populationKzylkoga;
    private Double populationMakat;
    private Double populationMakhambet;
    private Double oil;
    private Double oilAtyrau;
    private Double oilZhylyoi;
    private Double oilInder;
    private Double oilIsatai;
    private Double oilKurmangazy;
    private Double oilKzylkoga;
    private Double oilMakat;
    private Double oilMakhambet;
    private Double grain;
    private Double grainAtyrau;
    private Double grainZhylyoi;
    private Double grainInder;
    private Double grainIsatai;
    private Double grainKurmangazy;
    private Double grainKzylkoga;
    private Double grainMakat;
    private Double grainMakhambet;
    private Double potato;
    private Double potatoAtyrau;
    private Double potatoZhylyoi;
    private Double potatoInder;
    private Double potatoIsatai;
    private Double potatoKurmangazy;
    private Double potatoKzylkoga;
    private Double potatoMakat;
    private Double potatoMakhambet;
    private Double vegetables;
    private Double vegetablesAtyrau;
    private Double vegetablesZhylyoi;
    private Double vegetablesInder;
    private Double vegetablesIsatai;
    private Double vegetablesKurmangazy;
    private Double vegetablesKzylkoga;
    private Double vegetablesMakat;
    private Double vegetablesMakhambet;
    private Double cattle;
    private Double cattleAtyrau;
    private Double cattleZhylyoi;
    private Double cattleInder;
    private Double cattleIsatai;
    private Double cattleKurmangazy;
    private Double cattleKzylkoga;
    private Double cattleMakat;
    private Double cattleMakhambet;
    private Double goats;
    private Double goatsAtyrau;
    private Double goatsZhylyoi;
    private Double goatsInder;
    private Double goatsIsatai;
    private Double goatsKurmangazy;
    private Double goatsKzylkoga;
    private Double goatsMakat;
    private Double goatsMakhambet;
    private Double horses;
    private Double horsesAtyrau;
    private Double horsesZhylyoi;
    private Double horsesInder;
    private Double horsesIsatai;
    private Double horsesKurmangazy;
    private Double horsesKzylkoga;
    private Double horsesMakat;
    private Double horsesMakhambet;
    private Double poultry;
    private Double poultryAtyrau;
    private Double poultryZhylyoi;
    private Double poultryInder;
    private Double poultryIsatai;
    private Double poultryKurmangazy;
    private Double poultryKzylkoga;
    private Double poultryMakat;
    private Double poultryMakhambet;
    private Double weightCattle;
    private Double weightGoats;
    private Double weightHorses;
    private Double weightPoultry;
    private Double housing;
    private Double housingAtyrau;
    private Double housingZhylyoi;
    private Double housingInder;
    private Double housingIsatai;
    private Double housingKurmangazy;
    private Double housingKzylkoga;
    private Double housingMakat;
    private Double housingMakhambet;
    private Double buildingInvest;
    private Double prodAgro;
    private Double prodAgroAtyrau;
    private Double prodAgroZhylyoi;
    private Double prodAgroInder;
    private Double prodAgroIsatai;
    private Double prodAgroKurmangazy;
    private Double prodAgroKzylkoga;
    private Double prodAgroMakat;
    private Double prodAgroMakhambet;
    private Double prodMining;
    private Double prodMiningAtyrau;
    private Double prodMiningZhylyoi;
    private Double prodMiningInder;
    private Double prodMiningIsatai;
    private Double prodMiningKurmangazy;
    private Double prodMiningKzylkoga;
    private Double prodMiningMakat;
    private Double prodMiningMakhambet;
    private Double prodMan;
    private Double prodManAtyrau;
    private Double prodManZhylyoi;
    private Double prodManInder;
    private Double prodManIsatai;
    private Double prodManKurmangazy;
    private Double prodManKzylkoga;
    private Double prodManMakat;
    private Double prodManMakhambet;
    private Double prodEnergo;
    private Double prodEnergoAtyrau;
    private Double prodEnergoZhylyoi;
    private Double prodEnergoInder;
    private Double prodEnergoIsatai;
    private Double prodEnergoKurmangazy;
    private Double prodEnergoKzylkoga;
    private Double prodEnergoMakat;
    private Double prodEnergoMakhambet;
    private Double prodWater;
    private Double prodWaterAtyrau;
    private Double prodWaterZhylyoi;
    private Double prodWaterInder;
    private Double prodWaterIsatai;
    private Double prodWaterKurmangazy;
    private Double prodWaterKzylkoga;
    private Double prodWaterMakat;
    private Double prodWaterMakhambet;
    private Double prodBuilding;
    private Double prodBuildingAtyrau;
    private Double prodBuildingZhylyoi;
    private Double prodBuildingInder;
    private Double prodBuildingIsatai;
    private Double prodBuildingKurmangazy;
    private Double prodBuildingKzylkoga;
    private Double prodBuildingMakat;
    private Double prodBuildingMakhambet;
    private Double prodRetail;
    private Double prodRetailAtyrau;
    private Double prodRetailZhylyoi;
    private Double prodRetailInder;
    private Double prodRetailIsatai;
    private Double prodRetailKurmangazy;
    private Double prodRetailKzylkoga;
    private Double prodRetailMakat;
    private Double prodRetailMakhambet;
    private Double prodTrade;
    private Double prodTradeAtyrau;
    private Double prodTradeZhylyoi;
    private Double prodTradeInder;
    private Double prodTradeIsatai;
    private Double prodTradeKurmangazy;
    private Double prodTradeKzylkoga;
    private Double prodTradeMakat;
    private Double prodTradeMakhambet;
    private Double prodTransp;
    private Double prodTranspAtyrau;
    private Double prodTranspZhylyoi;
    private Double prodTranspInder;
    private Double prodTranspIsatai;
    private Double prodTranspKurmangazy;
    private Double prodTranspKzylkoga;
    private Double prodTranspMakat;
    private Double prodTranspMakhambet;
    private Double prodInfo;
    private Double prodInfoAtyrau;
    private Double prodInfoZhylyoi;
    private Double prodInfoInder;
    private Double prodInfoIsatai;
    private Double prodInfoKurmangazy;
    private Double prodInfoKzylkoga;
    private Double prodInfoMakat;
    private Double prodInfoMakhambet;
    private Double prodEstate;
    private Double prodEstateAtyrau;
    private Double prodEstateZhylyoi;
    private Double prodEstateInder;
    private Double prodEstateIsatai;
    private Double prodEstateKurmangazy;
    private Double prodEstateKzylkoga;
    private Double prodEstateMakat;
    private Double prodEstateMakhambet;
    private Double prodOther;
    private Double prodOtherAtyrau;
    private Double prodOtherZhylyoi;
    private Double prodOtherInder;
    private Double prodOtherIsatai;
    private Double prodOtherKurmangazy;
    private Double prodOtherKzylkoga;
    private Double prodOtherMakat;
    private Double prodOtherMakhambet;
    private Double ifo;
    private Double ifoAgro;
    private Double ifoAgroAtyrau;
    private Double ifoAgroZhylyoi;
    private Double ifoAgroInder;
    private Double ifoAgroIsatai;
    private Double ifoAgroKurmangazy;
    private Double ifoAgroKzylkoga;
    private Double ifoAgroMakat;
    private Double ifoAgroMakhambet;
    private Double ifoMining;
    private Double ifoMiningAtyrau;
    private Double ifoMiningZhylyoi;
    private Double ifoMiningInder;
    private Double ifoMiningIsatai;
    private Double ifoMiningKurmangazy;
    private Double ifoMiningKzylkoga;
    private Double ifoMiningMakat;
    private Double ifoMiningMakhambet;
    private Double ifoMan;
    private Double ifoManAtyrau;
    private Double ifoManZhylyoi;
    private Double ifoManInder;
    private Double ifoManIsatai;
    private Double ifoManKurmangazy;
    private Double ifoManKzylkoga;
    private Double ifoManMakat;
    private Double ifoManMakhambet;
    private Double ifoEnergo;
    private Double ifoEnergoAtyrau;
    private Double ifoEnergoZhylyoi;
    private Double ifoEnergoInder;
    private Double ifoEnergoIsatai;
    private Double ifoEnergoKurmangazy;
    private Double ifoEnergoKzylkoga;
    private Double ifoEnergoMakat;
    private Double ifoEnergoMakhambet;
    private Double ifoWater;
    private Double ifoWaterAtyrau;
    private Double ifoWaterZhylyoi;
    private Double ifoWaterInder;
    private Double ifoWaterIsatai;
    private Double ifoWaterKurmangazy;
    private Double ifoWaterKzylkoga;
    private Double ifoWaterMakat;
    private Double ifoWaterMakhambet;
    private Double ifoBuilding;
    private Double ifoBuildingAtyrau;
    private Double ifoBuildingZhylyoi;
    private Double ifoBuildingInder;
    private Double ifoBuildingIsatai;
    private Double ifoBuildingKurmangazy;
    private Double ifoBuildingKzylkoga;
    private Double ifoBuildingMakat;
    private Double ifoBuildingMakhambet;
    private Double ifoRetail;
    private Double ifoRetailAtyrau;
    private Double ifoRetailZhylyoi;
    private Double ifoRetailInder;
    private Double ifoRetailIsatai;
    private Double ifoRetailKurmangazy;
    private Double ifoRetailKzylkoga;
    private Double ifoRetailMakat;
    private Double ifoRetailMakhambet;
    private Double ifoTrade;
    private Double ifoTradeAtyrau;
    private Double ifoTradeZhylyoi;
    private Double ifoTradeInder;
    private Double ifoTradeIsatai;
    private Double ifoTradeKurmangazy;
    private Double ifoTradeKzylkoga;
    private Double ifoTradeMakat;
    private Double ifoTradeMakhambet;
    private Double ifoTransp;
    private Double ifoTranspAtyrau;
    private Double ifoTranspZhylyoi;
    private Double ifoTranspInder;
    private Double ifoTranspIsatai;
    private Double ifoTranspKurmangazy;
    private Double ifoTranspKzylkoga;
    private Double ifoTranspMakat;
    private Double ifoTranspMakhambet;
    private Double ifoInfo;
    private Double ifoInfoAtyrau;
    private Double ifoInfoZhylyoi;
    private Double ifoInfoInder;
    private Double ifoInfoIsatai;
    private Double ifoInfoKurmangazy;
    private Double ifoInfoKzylkoga;
    private Double ifoInfoMakat;
    private Double ifoInfoMakhambet;
    private Double ifoEstate;
    private Double ifoEstateAtyrau;
    private Double ifoEstateZhylyoi;
    private Double ifoEstateInder;
    private Double ifoEstateIsatai;
    private Double ifoEstateKurmangazy;
    private Double ifoEstateKzylkoga;
    private Double ifoEstateMakat;
    private Double ifoEstateMakhambet;
    private Double ifoOther;
    private Double ifoOtherAtyrau;
    private Double ifoOtherZhylyoi;
    private Double ifoOtherInder;
    private Double ifoOtherIsatai;
    private Double ifoOtherKurmangazy;
    private Double ifoOtherKzylkoga;
    private Double ifoOtherMakat;
    private Double ifoOtherMakhambet;

    public String getYear() {
        return year;
    }

    public Double getOilPrice() {
        return oilPrice;
    }

    public Double getInflation() {
        return inflation;
    }

    public Double getExchangeRate() {
        return exchangeRate;
    }

    public Double getPopulation() {
        return population;
    }

    public Double getPopulationAtyrau() {
        return populationAtyrau;
    }

    public Double getPopulationZhylyoi() {
        return populationZhylyoi;
    }

    public Double getPopulationInder() {
        return populationInder;
    }

    public Double getPopulationIsatai() {
        return populationIsatai;
    }

    public Double getPopulationKurmangazy() {
        return populationKurmangazy;
    }

    public Double getPopulationKzylkoga() {
        return populationKzylkoga;
    }

    public Double getPopulationMakat() {
        return populationMakat;
    }

    public Double getPopulationMakhambet() {
        return populationMakhambet;
    }

    public Double getOil() {
        return oil;
    }

    public Double getOilAtyrau() {
        return oilAtyrau;
    }

    public Double getOilZhylyoi() {
        return oilZhylyoi;
    }

    public Double getOilInder() {
        return oilInder;
    }

    public Double getOilIsatai() {
        return oilIsatai;
    }

    public Double getOilKurmangazy() {
        return oilKurmangazy;
    }

    public Double getOilKzylkoga() {
        return oilKzylkoga;
    }

    public Double getOilMakat() {
        return oilMakat;
    }

    public Double getOilMakhambet() {
        return oilMakhambet;
    }

    public Double getGrain() {
        return grain;
    }

    public Double getGrainAtyrau() {
        return grainAtyrau;
    }

    public Double getGrainZhylyoi() {
        return grainZhylyoi;
    }

    public Double getGrainInder() {
        return grainInder;
    }

    public Double getGrainIsatai() {
        return grainIsatai;
    }

    public Double getGrainKurmangazy() {
        return grainKurmangazy;
    }

    public Double getGrainKzylkoga() {
        return grainKzylkoga;
    }

    public Double getGrainMakat() {
        return grainMakat;
    }

    public Double getGrainMakhambet() {
        return grainMakhambet;
    }

    public Double getPotato() {
        return potato;
    }

    public Double getPotatoAtyrau() {
        return potatoAtyrau;
    }

    public Double getPotatoZhylyoi() {
        return potatoZhylyoi;
    }

    public Double getPotatoInder() {
        return potatoInder;
    }

    public Double getPotatoIsatai() {
        return potatoIsatai;
    }

    public Double getPotatoKurmangazy() {
        return potatoKurmangazy;
    }

    public Double getPotatoKzylkoga() {
        return potatoKzylkoga;
    }

    public Double getPotatoMakat() {
        return potatoMakat;
    }

    public Double getPotatoMakhambet() {
        return potatoMakhambet;
    }

    public Double getVegetables() {
        return vegetables;
    }

    public Double getVegetablesAtyrau() {
        return vegetablesAtyrau;
    }

    public Double getVegetablesZhylyoi() {
        return vegetablesZhylyoi;
    }

    public Double getVegetablesInder() {
        return vegetablesInder;
    }

    public Double getVegetablesIsatai() {
        return vegetablesIsatai;
    }

    public Double getVegetablesKurmangazy() {
        return vegetablesKurmangazy;
    }

    public Double getVegetablesKzylkoga() {
        return vegetablesKzylkoga;
    }

    public Double getVegetablesMakat() {
        return vegetablesMakat;
    }

    public Double getVegetablesMakhambet() {
        return vegetablesMakhambet;
    }

    public Double getCattle() {
        return cattle;
    }

    public Double getCattleAtyrau() {
        return cattleAtyrau;
    }

    public Double getCattleZhylyoi() {
        return cattleZhylyoi;
    }

    public Double getCattleInder() {
        return cattleInder;
    }

    public Double getCattleIsatai() {
        return cattleIsatai;
    }

    public Double getCattleKurmangazy() {
        return cattleKurmangazy;
    }

    public Double getCattleKzylkoga() {
        return cattleKzylkoga;
    }

    public Double getCattleMakat() {
        return cattleMakat;
    }

    public Double getCattleMakhambet() {
        return cattleMakhambet;
    }

    public Double getGoats() {
        return goats;
    }

    public Double getGoatsAtyrau() {
        return goatsAtyrau;
    }

    public Double getGoatsZhylyoi() {
        return goatsZhylyoi;
    }

    public Double getGoatsInder() {
        return goatsInder;
    }

    public Double getGoatsIsatai() {
        return goatsIsatai;
    }

    public Double getGoatsKurmangazy() {
        return goatsKurmangazy;
    }

    public Double getGoatsKzylkoga() {
        return goatsKzylkoga;
    }

    public Double getGoatsMakat() {
        return goatsMakat;
    }

    public Double getGoatsMakhambet() {
        return goatsMakhambet;
    }

    public Double getHorses() {
        return horses;
    }

    public Double getHorsesAtyrau() {
        return horsesAtyrau;
    }

    public Double getHorsesZhylyoi() {
        return horsesZhylyoi;
    }

    public Double getHorsesInder() {
        return horsesInder;
    }

    public Double getHorsesIsatai() {
        return horsesIsatai;
    }

    public Double getHorsesKurmangazy() {
        return horsesKurmangazy;
    }

    public Double getHorsesKzylkoga() {
        return horsesKzylkoga;
    }

    public Double getHorsesMakat() {
        return horsesMakat;
    }

    public Double getHorsesMakhambet() {
        return horsesMakhambet;
    }

    public Double getPoultry() {
        return poultry;
    }

    public Double getPoultryAtyrau() {
        return poultryAtyrau;
    }

    public Double getPoultryZhylyoi() {
        return poultryZhylyoi;
    }

    public Double getPoultryInder() {
        return poultryInder;
    }

    public Double getPoultryIsatai() {
        return poultryIsatai;
    }

    public Double getPoultryKurmangazy() {
        return poultryKurmangazy;
    }

    public Double getPoultryKzylkoga() {
        return poultryKzylkoga;
    }

    public Double getPoultryMakat() {
        return poultryMakat;
    }

    public Double getPoultryMakhambet() {
        return poultryMakhambet;
    }

    public Double getWeightCattle() {
        return weightCattle;
    }

    public Double getWeightGoats() {
        return weightGoats;
    }

    public Double getWeightHorses() {
        return weightHorses;
    }

    public Double getWeightPoultry() {
        return weightPoultry;
    }

    public Double getHousing() {
        return housing;
    }

    public Double getHousingAtyrau() {
        return housingAtyrau;
    }

    public Double getHousingZhylyoi() {
        return housingZhylyoi;
    }

    public Double getHousingInder() {
        return housingInder;
    }

    public Double getHousingIsatai() {
        return housingIsatai;
    }

    public Double getHousingKurmangazy() {
        return housingKurmangazy;
    }

    public Double getHousingKzylkoga() {
        return housingKzylkoga;
    }

    public Double getHousingMakat() {
        return housingMakat;
    }

    public Double getHousingMakhambet() {
        return housingMakhambet;
    }

    public Double getBuildingInvest() {
        return buildingInvest;
    }

    public Double getProdAgro() {
        return prodAgro;
    }

    public Double getProdAgroAtyrau() {
        return prodAgroAtyrau;
    }

    public Double getProdAgroZhylyoi() {
        return prodAgroZhylyoi;
    }

    public Double getProdAgroInder() {
        return prodAgroInder;
    }

    public Double getProdAgroIsatai() {
        return prodAgroIsatai;
    }

    public Double getProdAgroKurmangazy() {
        return prodAgroKurmangazy;
    }

    public Double getProdAgroKzylkoga() {
        return prodAgroKzylkoga;
    }

    public Double getProdAgroMakat() {
        return prodAgroMakat;
    }

    public Double getProdAgroMakhambet() {
        return prodAgroMakhambet;
    }

    public Double getProdMining() {
        return prodMining;
    }

    public Double getProdMiningAtyrau() {
        return prodMiningAtyrau;
    }

    public Double getProdMiningZhylyoi() {
        return prodMiningZhylyoi;
    }

    public Double getProdMiningInder() {
        return prodMiningInder;
    }

    public Double getProdMiningIsatai() {
        return prodMiningIsatai;
    }

    public Double getProdMiningKurmangazy() {
        return prodMiningKurmangazy;
    }

    public Double getProdMiningKzylkoga() {
        return prodMiningKzylkoga;
    }

    public Double getProdMiningMakat() {
        return prodMiningMakat;
    }

    public Double getProdMiningMakhambet() {
        return prodMiningMakhambet;
    }

    public Double getProdMan() {
        return prodMan;
    }

    public Double getProdManAtyrau() {
        return prodManAtyrau;
    }

    public Double getProdManZhylyoi() {
        return prodManZhylyoi;
    }

    public Double getProdManInder() {
        return prodManInder;
    }

    public Double getProdManIsatai() {
        return prodManIsatai;
    }

    public Double getProdManKurmangazy() {
        return prodManKurmangazy;
    }

    public Double getProdManKzylkoga() {
        return prodManKzylkoga;
    }

    public Double getProdManMakat() {
        return prodManMakat;
    }

    public Double getProdManMakhambet() {
        return prodManMakhambet;
    }

    public Double getProdEnergo() {
        return prodEnergo;
    }

    public Double getProdEnergoAtyrau() {
        return prodEnergoAtyrau;
    }

    public Double getProdEnergoZhylyoi() {
        return prodEnergoZhylyoi;
    }

    public Double getProdEnergoInder() {
        return prodEnergoInder;
    }

    public Double getProdEnergoIsatai() {
        return prodEnergoIsatai;
    }

    public Double getProdEnergoKurmangazy() {
        return prodEnergoKurmangazy;
    }

    public Double getProdEnergoKzylkoga() {
        return prodEnergoKzylkoga;
    }

    public Double getProdEnergoMakat() {
        return prodEnergoMakat;
    }

    public Double getProdEnergoMakhambet() {
        return prodEnergoMakhambet;
    }

    public Double getProdWater() {
        return prodWater;
    }

    public Double getProdWaterAtyrau() {
        return prodWaterAtyrau;
    }

    public Double getProdWaterZhylyoi() {
        return prodWaterZhylyoi;
    }

    public Double getProdWaterInder() {
        return prodWaterInder;
    }

    public Double getProdWaterIsatai() {
        return prodWaterIsatai;
    }

    public Double getProdWaterKurmangazy() {
        return prodWaterKurmangazy;
    }

    public Double getProdWaterKzylkoga() {
        return prodWaterKzylkoga;
    }

    public Double getProdWaterMakat() {
        return prodWaterMakat;
    }

    public Double getProdWaterMakhambet() {
        return prodWaterMakhambet;
    }

    public Double getProdBuilding() {
        return prodBuilding;
    }

    public Double getProdBuildingAtyrau() {
        return prodBuildingAtyrau;
    }

    public Double getProdBuildingZhylyoi() {
        return prodBuildingZhylyoi;
    }

    public Double getProdBuildingInder() {
        return prodBuildingInder;
    }

    public Double getProdBuildingIsatai() {
        return prodBuildingIsatai;
    }

    public Double getProdBuildingKurmangazy() {
        return prodBuildingKurmangazy;
    }

    public Double getProdBuildingKzylkoga() {
        return prodBuildingKzylkoga;
    }

    public Double getProdBuildingMakat() {
        return prodBuildingMakat;
    }

    public Double getProdBuildingMakhambet() {
        return prodBuildingMakhambet;
    }

    public Double getProdRetail() {
        return prodRetail;
    }

    public Double getProdRetailAtyrau() {
        return prodRetailAtyrau;
    }

    public Double getProdRetailZhylyoi() {
        return prodRetailZhylyoi;
    }

    public Double getProdRetailInder() {
        return prodRetailInder;
    }

    public Double getProdRetailIsatai() {
        return prodRetailIsatai;
    }

    public Double getProdRetailKurmangazy() {
        return prodRetailKurmangazy;
    }

    public Double getProdRetailKzylkoga() {
        return prodRetailKzylkoga;
    }

    public Double getProdRetailMakat() {
        return prodRetailMakat;
    }

    public Double getProdRetailMakhambet() {
        return prodRetailMakhambet;
    }

    public Double getProdTrade() {
        return prodTrade;
    }

    public Double getProdTradeAtyrau() {
        return prodTradeAtyrau;
    }

    public Double getProdTradeZhylyoi() {
        return prodTradeZhylyoi;
    }

    public Double getProdTradeInder() {
        return prodTradeInder;
    }

    public Double getProdTradeIsatai() {
        return prodTradeIsatai;
    }

    public Double getProdTradeKurmangazy() {
        return prodTradeKurmangazy;
    }

    public Double getProdTradeKzylkoga() {
        return prodTradeKzylkoga;
    }

    public Double getProdTradeMakat() {
        return prodTradeMakat;
    }

    public Double getProdTradeMakhambet() {
        return prodTradeMakhambet;
    }

    public Double getProdTransp() {
        return prodTransp;
    }

    public Double getProdTranspAtyrau() {
        return prodTranspAtyrau;
    }

    public Double getProdTranspZhylyoi() {
        return prodTranspZhylyoi;
    }

    public Double getProdTranspInder() {
        return prodTranspInder;
    }

    public Double getProdTranspIsatai() {
        return prodTranspIsatai;
    }

    public Double getProdTranspKurmangazy() {
        return prodTranspKurmangazy;
    }

    public Double getProdTranspKzylkoga() {
        return prodTranspKzylkoga;
    }

    public Double getProdTranspMakat() {
        return prodTranspMakat;
    }

    public Double getProdTranspMakhambet() {
        return prodTranspMakhambet;
    }

    public Double getProdInfo() {
        return prodInfo;
    }

    public Double getProdInfoAtyrau() {
        return prodInfoAtyrau;
    }

    public Double getProdInfoZhylyoi() {
        return prodInfoZhylyoi;
    }

    public Double getProdInfoInder() {
        return prodInfoInder;
    }

    public Double getProdInfoIsatai() {
        return prodInfoIsatai;
    }

    public Double getProdInfoKurmangazy() {
        return prodInfoKurmangazy;
    }

    public Double getProdInfoKzylkoga() {
        return prodInfoKzylkoga;
    }

    public Double getProdInfoMakat() {
        return prodInfoMakat;
    }

    public Double getProdInfoMakhambet() {
        return prodInfoMakhambet;
    }

    public Double getProdEstate() {
        return prodEstate;
    }

    public Double getProdEstateAtyrau() {
        return prodEstateAtyrau;
    }

    public Double getProdEstateZhylyoi() {
        return prodEstateZhylyoi;
    }

    public Double getProdEstateInder() {
        return prodEstateInder;
    }

    public Double getProdEstateIsatai() {
        return prodEstateIsatai;
    }

    public Double getProdEstateKurmangazy() {
        return prodEstateKurmangazy;
    }

    public Double getProdEstateKzylkoga() {
        return prodEstateKzylkoga;
    }

    public Double getProdEstateMakat() {
        return prodEstateMakat;
    }

    public Double getProdEstateMakhambet() {
        return prodEstateMakhambet;
    }

    public Double getProdOther() {
        return prodOther;
    }

    public Double getProdOtherAtyrau() {
        return prodOtherAtyrau;
    }

    public Double getProdOtherZhylyoi() {
        return prodOtherZhylyoi;
    }

    public Double getProdOtherInder() {
        return prodOtherInder;
    }

    public Double getProdOtherIsatai() {
        return prodOtherIsatai;
    }

    public Double getProdOtherKurmangazy() {
        return prodOtherKurmangazy;
    }

    public Double getProdOtherKzylkoga() {
        return prodOtherKzylkoga;
    }

    public Double getProdOtherMakat() {
        return prodOtherMakat;
    }

    public Double getProdOtherMakhambet() {
        return prodOtherMakhambet;
    }

    public Double getIfo() {
        return ifo;
    }

    public Double getIfoAgro() {
        return ifoAgro;
    }

    public Double getIfoAgroAtyrau() {
        return ifoAgroAtyrau;
    }

    public Double getIfoAgroZhylyoi() {
        return ifoAgroZhylyoi;
    }

    public Double getIfoAgroInder() {
        return ifoAgroInder;
    }

    public Double getIfoAgroIsatai() {
        return ifoAgroIsatai;
    }

    public Double getIfoAgroKurmangazy() {
        return ifoAgroKurmangazy;
    }

    public Double getIfoAgroKzylkoga() {
        return ifoAgroKzylkoga;
    }

    public Double getIfoAgroMakat() {
        return ifoAgroMakat;
    }

    public Double getIfoAgroMakhambet() {
        return ifoAgroMakhambet;
    }

    public Double getIfoMining() {
        return ifoMining;
    }

    public Double getIfoMiningAtyrau() {
        return ifoMiningAtyrau;
    }

    public Double getIfoMiningZhylyoi() {
        return ifoMiningZhylyoi;
    }

    public Double getIfoMiningInder() {
        return ifoMiningInder;
    }

    public Double getIfoMiningIsatai() {
        return ifoMiningIsatai;
    }

    public Double getIfoMiningKurmangazy() {
        return ifoMiningKurmangazy;
    }

    public Double getIfoMiningKzylkoga() {
        return ifoMiningKzylkoga;
    }

    public Double getIfoMiningMakat() {
        return ifoMiningMakat;
    }

    public Double getIfoMiningMakhambet() {
        return ifoMiningMakhambet;
    }

    public Double getIfoMan() {
        return ifoMan;
    }

    public Double getIfoManAtyrau() {
        return ifoManAtyrau;
    }

    public Double getIfoManZhylyoi() {
        return ifoManZhylyoi;
    }

    public Double getIfoManInder() {
        return ifoManInder;
    }

    public Double getIfoManIsatai() {
        return ifoManIsatai;
    }

    public Double getIfoManKurmangazy() {
        return ifoManKurmangazy;
    }

    public Double getIfoManKzylkoga() {
        return ifoManKzylkoga;
    }

    public Double getIfoManMakat() {
        return ifoManMakat;
    }

    public Double getIfoManMakhambet() {
        return ifoManMakhambet;
    }

    public Double getIfoEnergo() {
        return ifoEnergo;
    }

    public Double getIfoEnergoAtyrau() {
        return ifoEnergoAtyrau;
    }

    public Double getIfoEnergoZhylyoi() {
        return ifoEnergoZhylyoi;
    }

    public Double getIfoEnergoInder() {
        return ifoEnergoInder;
    }

    public Double getIfoEnergoIsatai() {
        return ifoEnergoIsatai;
    }

    public Double getIfoEnergoKurmangazy() {
        return ifoEnergoKurmangazy;
    }

    public Double getIfoEnergoKzylkoga() {
        return ifoEnergoKzylkoga;
    }

    public Double getIfoEnergoMakat() {
        return ifoEnergoMakat;
    }

    public Double getIfoEnergoMakhambet() {
        return ifoEnergoMakhambet;
    }

    public Double getIfoWater() {
        return ifoWater;
    }

    public Double getIfoWaterAtyrau() {
        return ifoWaterAtyrau;
    }

    public Double getIfoWaterZhylyoi() {
        return ifoWaterZhylyoi;
    }

    public Double getIfoWaterInder() {
        return ifoWaterInder;
    }

    public Double getIfoWaterIsatai() {
        return ifoWaterIsatai;
    }

    public Double getIfoWaterKurmangazy() {
        return ifoWaterKurmangazy;
    }

    public Double getIfoWaterKzylkoga() {
        return ifoWaterKzylkoga;
    }

    public Double getIfoWaterMakat() {
        return ifoWaterMakat;
    }

    public Double getIfoWaterMakhambet() {
        return ifoWaterMakhambet;
    }

    public Double getIfoBuilding() {
        return ifoBuilding;
    }

    public Double getIfoBuildingAtyrau() {
        return ifoBuildingAtyrau;
    }

    public Double getIfoBuildingZhylyoi() {
        return ifoBuildingZhylyoi;
    }

    public Double getIfoBuildingInder() {
        return ifoBuildingInder;
    }

    public Double getIfoBuildingIsatai() {
        return ifoBuildingIsatai;
    }

    public Double getIfoBuildingKurmangazy() {
        return ifoBuildingKurmangazy;
    }

    public Double getIfoBuildingKzylkoga() {
        return ifoBuildingKzylkoga;
    }

    public Double getIfoBuildingMakat() {
        return ifoBuildingMakat;
    }

    public Double getIfoBuildingMakhambet() {
        return ifoBuildingMakhambet;
    }

    public Double getIfoRetail() {
        return ifoRetail;
    }

    public Double getIfoRetailAtyrau() {
        return ifoRetailAtyrau;
    }

    public Double getIfoRetailZhylyoi() {
        return ifoRetailZhylyoi;
    }

    public Double getIfoRetailInder() {
        return ifoRetailInder;
    }

    public Double getIfoRetailIsatai() {
        return ifoRetailIsatai;
    }

    public Double getIfoRetailKurmangazy() {
        return ifoRetailKurmangazy;
    }

    public Double getIfoRetailKzylkoga() {
        return ifoRetailKzylkoga;
    }

    public Double getIfoRetailMakat() {
        return ifoRetailMakat;
    }

    public Double getIfoRetailMakhambet() {
        return ifoRetailMakhambet;
    }

    public Double getIfoTrade() {
        return ifoTrade;
    }

    public Double getIfoTradeAtyrau() {
        return ifoTradeAtyrau;
    }

    public Double getIfoTradeZhylyoi() {
        return ifoTradeZhylyoi;
    }

    public Double getIfoTradeInder() {
        return ifoTradeInder;
    }

    public Double getIfoTradeIsatai() {
        return ifoTradeIsatai;
    }

    public Double getIfoTradeKurmangazy() {
        return ifoTradeKurmangazy;
    }

    public Double getIfoTradeKzylkoga() {
        return ifoTradeKzylkoga;
    }

    public Double getIfoTradeMakat() {
        return ifoTradeMakat;
    }

    public Double getIfoTradeMakhambet() {
        return ifoTradeMakhambet;
    }

    public Double getIfoTransp() {
        return ifoTransp;
    }

    public Double getIfoTranspAtyrau() {
        return ifoTranspAtyrau;
    }

    public Double getIfoTranspZhylyoi() {
        return ifoTranspZhylyoi;
    }

    public Double getIfoTranspInder() {
        return ifoTranspInder;
    }

    public Double getIfoTranspIsatai() {
        return ifoTranspIsatai;
    }

    public Double getIfoTranspKurmangazy() {
        return ifoTranspKurmangazy;
    }

    public Double getIfoTranspKzylkoga() {
        return ifoTranspKzylkoga;
    }

    public Double getIfoTranspMakat() {
        return ifoTranspMakat;
    }

    public Double getIfoTranspMakhambet() {
        return ifoTranspMakhambet;
    }

    public Double getIfoInfo() {
        return ifoInfo;
    }

    public Double getIfoInfoAtyrau() {
        return ifoInfoAtyrau;
    }

    public Double getIfoInfoZhylyoi() {
        return ifoInfoZhylyoi;
    }

    public Double getIfoInfoInder() {
        return ifoInfoInder;
    }

    public Double getIfoInfoIsatai() {
        return ifoInfoIsatai;
    }

    public Double getIfoInfoKurmangazy() {
        return ifoInfoKurmangazy;
    }

    public Double getIfoInfoKzylkoga() {
        return ifoInfoKzylkoga;
    }

    public Double getIfoInfoMakat() {
        return ifoInfoMakat;
    }

    public Double getIfoInfoMakhambet() {
        return ifoInfoMakhambet;
    }

    public Double getIfoEstate() {
        return ifoEstate;
    }

    public Double getIfoEstateAtyrau() {
        return ifoEstateAtyrau;
    }

    public Double getIfoEstateZhylyoi() {
        return ifoEstateZhylyoi;
    }

    public Double getIfoEstateInder() {
        return ifoEstateInder;
    }

    public Double getIfoEstateIsatai() {
        return ifoEstateIsatai;
    }

    public Double getIfoEstateKurmangazy() {
        return ifoEstateKurmangazy;
    }

    public Double getIfoEstateKzylkoga() {
        return ifoEstateKzylkoga;
    }

    public Double getIfoEstateMakat() {
        return ifoEstateMakat;
    }

    public Double getIfoEstateMakhambet() {
        return ifoEstateMakhambet;
    }

    public Double getIfoOther() {
        return ifoOther;
    }

    public Double getIfoOtherAtyrau() {
        return ifoOtherAtyrau;
    }

    public Double getIfoOtherZhylyoi() {
        return ifoOtherZhylyoi;
    }

    public Double getIfoOtherInder() {
        return ifoOtherInder;
    }

    public Double getIfoOtherIsatai() {
        return ifoOtherIsatai;
    }

    public Double getIfoOtherKurmangazy() {
        return ifoOtherKurmangazy;
    }

    public Double getIfoOtherKzylkoga() {
        return ifoOtherKzylkoga;
    }

    public Double getIfoOtherMakat() {
        return ifoOtherMakat;
    }

    public Double getIfoOtherMakhambet() {
        return ifoOtherMakhambet;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setOilPrice(Double oilPrice) {
        this.oilPrice = oilPrice;
    }

    public void setInflation(Double inflation) {
        this.inflation = inflation;
    }

    public void setExchangeRate(Double exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public void setPopulation(Double population) {
        this.population = population;
    }

    public void setPopulationAtyrau(Double populationAtyrau) {
        this.populationAtyrau = populationAtyrau;
    }

    public void setPopulationZhylyoi(Double populationZhylyoi) {
        this.populationZhylyoi = populationZhylyoi;
    }

    public void setPopulationInder(Double populationInder) {
        this.populationInder = populationInder;
    }

    public void setPopulationIsatai(Double populationIsatai) {
        this.populationIsatai = populationIsatai;
    }

    public void setPopulationKurmangazy(Double populationKurmangazy) {
        this.populationKurmangazy = populationKurmangazy;
    }

    public void setPopulationKzylkoga(Double populationKzylkoga) {
        this.populationKzylkoga = populationKzylkoga;
    }

    public void setPopulationMakat(Double populationMakat) {
        this.populationMakat = populationMakat;
    }

    public void setPopulationMakhambet(Double populationMakhambet) {
        this.populationMakhambet = populationMakhambet;
    }

    public void setOil(Double oil) {
        this.oil = oil;
    }

    public void setOilAtyrau(Double oilAtyrau) {
        this.oilAtyrau = oilAtyrau;
    }

    public void setOilZhylyoi(Double oilZhylyoi) {
        this.oilZhylyoi = oilZhylyoi;
    }

    public void setOilInder(Double oilInder) {
        this.oilInder = oilInder;
    }

    public void setOilIsatai(Double oilIsatai) {
        this.oilIsatai = oilIsatai;
    }

    public void setOilKurmangazy(Double oilKurmangazy) {
        this.oilKurmangazy = oilKurmangazy;
    }

    public void setOilKzylkoga(Double oilKzylkoga) {
        this.oilKzylkoga = oilKzylkoga;
    }

    public void setOilMakat(Double oilMakat) {
        this.oilMakat = oilMakat;
    }

    public void setOilMakhambet(Double oilMakhambet) {
        this.oilMakhambet = oilMakhambet;
    }

    public void setGrain(Double grain) {
        this.grain = grain;
    }

    public void setGrainAtyrau(Double grainAtyrau) {
        this.grainAtyrau = grainAtyrau;
    }

    public void setGrainZhylyoi(Double grainZhylyoi) {
        this.grainZhylyoi = grainZhylyoi;
    }

    public void setGrainInder(Double grainInder) {
        this.grainInder = grainInder;
    }

    public void setGrainIsatai(Double grainIsatai) {
        this.grainIsatai = grainIsatai;
    }

    public void setGrainKurmangazy(Double grainKurmangazy) {
        this.grainKurmangazy = grainKurmangazy;
    }

    public void setGrainKzylkoga(Double grainKzylkoga) {
        this.grainKzylkoga = grainKzylkoga;
    }

    public void setGrainMakat(Double grainMakat) {
        this.grainMakat = grainMakat;
    }

    public void setGrainMakhambet(Double grainMakhambet) {
        this.grainMakhambet = grainMakhambet;
    }

    public void setPotato(Double potato) {
        this.potato = potato;
    }

    public void setPotatoAtyrau(Double potatoAtyrau) {
        this.potatoAtyrau = potatoAtyrau;
    }

    public void setPotatoZhylyoi(Double potatoZhylyoi) {
        this.potatoZhylyoi = potatoZhylyoi;
    }

    public void setPotatoInder(Double potatoInder) {
        this.potatoInder = potatoInder;
    }

    public void setPotatoIsatai(Double potatoIsatai) {
        this.potatoIsatai = potatoIsatai;
    }

    public void setPotatoKurmangazy(Double potatoKurmangazy) {
        this.potatoKurmangazy = potatoKurmangazy;
    }

    public void setPotatoKzylkoga(Double potatoKzylkoga) {
        this.potatoKzylkoga = potatoKzylkoga;
    }

    public void setPotatoMakat(Double potatoMakat) {
        this.potatoMakat = potatoMakat;
    }

    public void setPotatoMakhambet(Double potatoMakhambet) {
        this.potatoMakhambet = potatoMakhambet;
    }

    public void setVegetables(Double vegetables) {
        this.vegetables = vegetables;
    }

    public void setVegetablesAtyrau(Double vegetablesAtyrau) {
        this.vegetablesAtyrau = vegetablesAtyrau;
    }

    public void setVegetablesZhylyoi(Double vegetablesZhylyoi) {
        this.vegetablesZhylyoi = vegetablesZhylyoi;
    }

    public void setVegetablesInder(Double vegetablesInder) {
        this.vegetablesInder = vegetablesInder;
    }

    public void setVegetablesIsatai(Double vegetablesIsatai) {
        this.vegetablesIsatai = vegetablesIsatai;
    }

    public void setVegetablesKurmangazy(Double vegetablesKurmangazy) {
        this.vegetablesKurmangazy = vegetablesKurmangazy;
    }

    public void setVegetablesKzylkoga(Double vegetablesKzylkoga) {
        this.vegetablesKzylkoga = vegetablesKzylkoga;
    }

    public void setVegetablesMakat(Double vegetablesMakat) {
        this.vegetablesMakat = vegetablesMakat;
    }

    public void setVegetablesMakhambet(Double vegetablesMakhambet) {
        this.vegetablesMakhambet = vegetablesMakhambet;
    }

    public void setCattle(Double cattle) {
        this.cattle = cattle;
    }

    public void setCattleAtyrau(Double cattleAtyrau) {
        this.cattleAtyrau = cattleAtyrau;
    }

    public void setCattleZhylyoi(Double cattleZhylyoi) {
        this.cattleZhylyoi = cattleZhylyoi;
    }

    public void setCattleInder(Double cattleInder) {
        this.cattleInder = cattleInder;
    }

    public void setCattleIsatai(Double cattleIsatai) {
        this.cattleIsatai = cattleIsatai;
    }

    public void setCattleKurmangazy(Double cattleKurmangazy) {
        this.cattleKurmangazy = cattleKurmangazy;
    }

    public void setCattleKzylkoga(Double cattleKzylkoga) {
        this.cattleKzylkoga = cattleKzylkoga;
    }

    public void setCattleMakat(Double cattleMakat) {
        this.cattleMakat = cattleMakat;
    }

    public void setCattleMakhambet(Double cattleMakhambet) {
        this.cattleMakhambet = cattleMakhambet;
    }

    public void setGoats(Double goats) {
        this.goats = goats;
    }

    public void setGoatsAtyrau(Double goatsAtyrau) {
        this.goatsAtyrau = goatsAtyrau;
    }

    public void setGoatsZhylyoi(Double goatsZhylyoi) {
        this.goatsZhylyoi = goatsZhylyoi;
    }

    public void setGoatsInder(Double goatsInder) {
        this.goatsInder = goatsInder;
    }

    public void setGoatsIsatai(Double goatsIsatai) {
        this.goatsIsatai = goatsIsatai;
    }

    public void setGoatsKurmangazy(Double goatsKurmangazy) {
        this.goatsKurmangazy = goatsKurmangazy;
    }

    public void setGoatsKzylkoga(Double goatsKzylkoga) {
        this.goatsKzylkoga = goatsKzylkoga;
    }

    public void setGoatsMakat(Double goatsMakat) {
        this.goatsMakat = goatsMakat;
    }

    public void setGoatsMakhambet(Double goatsMakhambet) {
        this.goatsMakhambet = goatsMakhambet;
    }

    public void setHorses(Double horses) {
        this.horses = horses;
    }

    public void setHorsesAtyrau(Double horsesAtyrau) {
        this.horsesAtyrau = horsesAtyrau;
    }

    public void setHorsesZhylyoi(Double horsesZhylyoi) {
        this.horsesZhylyoi = horsesZhylyoi;
    }

    public void setHorsesInder(Double horsesInder) {
        this.horsesInder = horsesInder;
    }

    public void setHorsesIsatai(Double horsesIsatai) {
        this.horsesIsatai = horsesIsatai;
    }

    public void setHorsesKurmangazy(Double horsesKurmangazy) {
        this.horsesKurmangazy = horsesKurmangazy;
    }

    public void setHorsesKzylkoga(Double horsesKzylkoga) {
        this.horsesKzylkoga = horsesKzylkoga;
    }

    public void setHorsesMakat(Double horsesMakat) {
        this.horsesMakat = horsesMakat;
    }

    public void setHorsesMakhambet(Double horsesMakhambet) {
        this.horsesMakhambet = horsesMakhambet;
    }

    public void setPoultry(Double poultry) {
        this.poultry = poultry;
    }

    public void setPoultryAtyrau(Double poultryAtyrau) {
        this.poultryAtyrau = poultryAtyrau;
    }

    public void setPoultryZhylyoi(Double poultryZhylyoi) {
        this.poultryZhylyoi = poultryZhylyoi;
    }

    public void setPoultryInder(Double poultryInder) {
        this.poultryInder = poultryInder;
    }

    public void setPoultryIsatai(Double poultryIsatai) {
        this.poultryIsatai = poultryIsatai;
    }

    public void setPoultryKurmangazy(Double poultryKurmangazy) {
        this.poultryKurmangazy = poultryKurmangazy;
    }

    public void setPoultryKzylkoga(Double poultryKzylkoga) {
        this.poultryKzylkoga = poultryKzylkoga;
    }

    public void setPoultryMakat(Double poultryMakat) {
        this.poultryMakat = poultryMakat;
    }

    public void setPoultryMakhambet(Double poultryMakhambet) {
        this.poultryMakhambet = poultryMakhambet;
    }

    public void setWeightCattle(Double weightCattle) {
        this.weightCattle = weightCattle;
    }

    public void setWeightGoats(Double weightGoats) {
        this.weightGoats = weightGoats;
    }

    public void setWeightHorses(Double weightHorses) {
        this.weightHorses = weightHorses;
    }

    public void setWeightPoultry(Double weightPoultry) {
        this.weightPoultry = weightPoultry;
    }

    public void setHousing(Double housing) {
        this.housing = housing;
    }

    public void setHousingAtyrau(Double housingAtyrau) {
        this.housingAtyrau = housingAtyrau;
    }

    public void setHousingZhylyoi(Double housingZhylyoi) {
        this.housingZhylyoi = housingZhylyoi;
    }

    public void setHousingInder(Double housingInder) {
        this.housingInder = housingInder;
    }

    public void setHousingIsatai(Double housingIsatai) {
        this.housingIsatai = housingIsatai;
    }

    public void setHousingKurmangazy(Double housingKurmangazy) {
        this.housingKurmangazy = housingKurmangazy;
    }

    public void setHousingKzylkoga(Double housingKzylkoga) {
        this.housingKzylkoga = housingKzylkoga;
    }

    public void setHousingMakat(Double housingMakat) {
        this.housingMakat = housingMakat;
    }

    public void setHousingMakhambet(Double housingMakhambet) {
        this.housingMakhambet = housingMakhambet;
    }

    public void setBuildingInvest(Double buildingInvest) {
        this.buildingInvest = buildingInvest;
    }

    public void setProdAgro(Double prodAgro) {
        this.prodAgro = prodAgro;
    }

    public void setProdAgroAtyrau(Double prodAgroAtyrau) {
        this.prodAgroAtyrau = prodAgroAtyrau;
    }

    public void setProdAgroZhylyoi(Double prodAgroZhylyoi) {
        this.prodAgroZhylyoi = prodAgroZhylyoi;
    }

    public void setProdAgroInder(Double prodAgroInder) {
        this.prodAgroInder = prodAgroInder;
    }

    public void setProdAgroIsatai(Double prodAgroIsatai) {
        this.prodAgroIsatai = prodAgroIsatai;
    }

    public void setProdAgroKurmangazy(Double prodAgroKurmangazy) {
        this.prodAgroKurmangazy = prodAgroKurmangazy;
    }

    public void setProdAgroKzylkoga(Double prodAgroKzylkoga) {
        this.prodAgroKzylkoga = prodAgroKzylkoga;
    }

    public void setProdAgroMakat(Double prodAgroMakat) {
        this.prodAgroMakat = prodAgroMakat;
    }

    public void setProdAgroMakhambet(Double prodAgroMakhambet) {
        this.prodAgroMakhambet = prodAgroMakhambet;
    }

    public void setProdMining(Double prodMining) {
        this.prodMining = prodMining;
    }

    public void setProdMiningAtyrau(Double prodMiningAtyrau) {
        this.prodMiningAtyrau = prodMiningAtyrau;
    }

    public void setProdMiningZhylyoi(Double prodMiningZhylyoi) {
        this.prodMiningZhylyoi = prodMiningZhylyoi;
    }

    public void setProdMiningInder(Double prodMiningInder) {
        this.prodMiningInder = prodMiningInder;
    }

    public void setProdMiningIsatai(Double prodMiningIsatai) {
        this.prodMiningIsatai = prodMiningIsatai;
    }

    public void setProdMiningKurmangazy(Double prodMiningKurmangazy) {
        this.prodMiningKurmangazy = prodMiningKurmangazy;
    }

    public void setProdMiningKzylkoga(Double prodMiningKzylkoga) {
        this.prodMiningKzylkoga = prodMiningKzylkoga;
    }

    public void setProdMiningMakat(Double prodMiningMakat) {
        this.prodMiningMakat = prodMiningMakat;
    }

    public void setProdMiningMakhambet(Double prodMiningMakhambet) {
        this.prodMiningMakhambet = prodMiningMakhambet;
    }

    public void setProdMan(Double prodMan) {
        this.prodMan = prodMan;
    }

    public void setProdManAtyrau(Double prodManAtyrau) {
        this.prodManAtyrau = prodManAtyrau;
    }

    public void setProdManZhylyoi(Double prodManZhylyoi) {
        this.prodManZhylyoi = prodManZhylyoi;
    }

    public void setProdManInder(Double prodManInder) {
        this.prodManInder = prodManInder;
    }

    public void setProdManIsatai(Double prodManIsatai) {
        this.prodManIsatai = prodManIsatai;
    }

    public void setProdManKurmangazy(Double prodManKurmangazy) {
        this.prodManKurmangazy = prodManKurmangazy;
    }

    public void setProdManKzylkoga(Double prodManKzylkoga) {
        this.prodManKzylkoga = prodManKzylkoga;
    }

    public void setProdManMakat(Double prodManMakat) {
        this.prodManMakat = prodManMakat;
    }

    public void setProdManMakhambet(Double prodManMakhambet) {
        this.prodManMakhambet = prodManMakhambet;
    }

    public void setProdEnergo(Double prodEnergo) {
        this.prodEnergo = prodEnergo;
    }

    public void setProdEnergoAtyrau(Double prodEnergoAtyrau) {
        this.prodEnergoAtyrau = prodEnergoAtyrau;
    }

    public void setProdEnergoZhylyoi(Double prodEnergoZhylyoi) {
        this.prodEnergoZhylyoi = prodEnergoZhylyoi;
    }

    public void setProdEnergoInder(Double prodEnergoInder) {
        this.prodEnergoInder = prodEnergoInder;
    }

    public void setProdEnergoIsatai(Double prodEnergoIsatai) {
        this.prodEnergoIsatai = prodEnergoIsatai;
    }

    public void setProdEnergoKurmangazy(Double prodEnergoKurmangazy) {
        this.prodEnergoKurmangazy = prodEnergoKurmangazy;
    }

    public void setProdEnergoKzylkoga(Double prodEnergoKzylkoga) {
        this.prodEnergoKzylkoga = prodEnergoKzylkoga;
    }

    public void setProdEnergoMakat(Double prodEnergoMakat) {
        this.prodEnergoMakat = prodEnergoMakat;
    }

    public void setProdEnergoMakhambet(Double prodEnergoMakhambet) {
        this.prodEnergoMakhambet = prodEnergoMakhambet;
    }

    public void setProdWater(Double prodWater) {
        this.prodWater = prodWater;
    }

    public void setProdWaterAtyrau(Double prodWaterAtyrau) {
        this.prodWaterAtyrau = prodWaterAtyrau;
    }

    public void setProdWaterZhylyoi(Double prodWaterZhylyoi) {
        this.prodWaterZhylyoi = prodWaterZhylyoi;
    }

    public void setProdWaterInder(Double prodWaterInder) {
        this.prodWaterInder = prodWaterInder;
    }

    public void setProdWaterIsatai(Double prodWaterIsatai) {
        this.prodWaterIsatai = prodWaterIsatai;
    }

    public void setProdWaterKurmangazy(Double prodWaterKurmangazy) {
        this.prodWaterKurmangazy = prodWaterKurmangazy;
    }

    public void setProdWaterKzylkoga(Double prodWaterKzylkoga) {
        this.prodWaterKzylkoga = prodWaterKzylkoga;
    }

    public void setProdWaterMakat(Double prodWaterMakat) {
        this.prodWaterMakat = prodWaterMakat;
    }

    public void setProdWaterMakhambet(Double prodWaterMakhambet) {
        this.prodWaterMakhambet = prodWaterMakhambet;
    }

    public void setProdBuilding(Double prodBuilding) {
        this.prodBuilding = prodBuilding;
    }

    public void setProdBuildingAtyrau(Double prodBuildingAtyrau) {
        this.prodBuildingAtyrau = prodBuildingAtyrau;
    }

    public void setProdBuildingZhylyoi(Double prodBuildingZhylyoi) {
        this.prodBuildingZhylyoi = prodBuildingZhylyoi;
    }

    public void setProdBuildingInder(Double prodBuildingInder) {
        this.prodBuildingInder = prodBuildingInder;
    }

    public void setProdBuildingIsatai(Double prodBuildingIsatai) {
        this.prodBuildingIsatai = prodBuildingIsatai;
    }

    public void setProdBuildingKurmangazy(Double prodBuildingKurmangazy) {
        this.prodBuildingKurmangazy = prodBuildingKurmangazy;
    }

    public void setProdBuildingKzylkoga(Double prodBuildingKzylkoga) {
        this.prodBuildingKzylkoga = prodBuildingKzylkoga;
    }

    public void setProdBuildingMakat(Double prodBuildingMakat) {
        this.prodBuildingMakat = prodBuildingMakat;
    }

    public void setProdBuildingMakhambet(Double prodBuildingMakhambet) {
        this.prodBuildingMakhambet = prodBuildingMakhambet;
    }

    public void setProdRetail(Double prodRetail) {
        this.prodRetail = prodRetail;
    }

    public void setProdRetailAtyrau(Double prodRetailAtyrau) {
        this.prodRetailAtyrau = prodRetailAtyrau;
    }

    public void setProdRetailZhylyoi(Double prodRetailZhylyoi) {
        this.prodRetailZhylyoi = prodRetailZhylyoi;
    }

    public void setProdRetailInder(Double prodRetailInder) {
        this.prodRetailInder = prodRetailInder;
    }

    public void setProdRetailIsatai(Double prodRetailIsatai) {
        this.prodRetailIsatai = prodRetailIsatai;
    }

    public void setProdRetailKurmangazy(Double prodRetailKurmangazy) {
        this.prodRetailKurmangazy = prodRetailKurmangazy;
    }

    public void setProdRetailKzylkoga(Double prodRetailKzylkoga) {
        this.prodRetailKzylkoga = prodRetailKzylkoga;
    }

    public void setProdRetailMakat(Double prodRetailMakat) {
        this.prodRetailMakat = prodRetailMakat;
    }

    public void setProdRetailMakhambet(Double prodRetailMakhambet) {
        this.prodRetailMakhambet = prodRetailMakhambet;
    }

    public void setProdTrade(Double prodTrade) {
        this.prodTrade = prodTrade;
    }

    public void setProdTradeAtyrau(Double prodTradeAtyrau) {
        this.prodTradeAtyrau = prodTradeAtyrau;
    }

    public void setProdTradeZhylyoi(Double prodTradeZhylyoi) {
        this.prodTradeZhylyoi = prodTradeZhylyoi;
    }

    public void setProdTradeInder(Double prodTradeInder) {
        this.prodTradeInder = prodTradeInder;
    }

    public void setProdTradeIsatai(Double prodTradeIsatai) {
        this.prodTradeIsatai = prodTradeIsatai;
    }

    public void setProdTradeKurmangazy(Double prodTradeKurmangazy) {
        this.prodTradeKurmangazy = prodTradeKurmangazy;
    }

    public void setProdTradeKzylkoga(Double prodTradeKzylkoga) {
        this.prodTradeKzylkoga = prodTradeKzylkoga;
    }

    public void setProdTradeMakat(Double prodTradeMakat) {
        this.prodTradeMakat = prodTradeMakat;
    }

    public void setProdTradeMakhambet(Double prodTradeMakhambet) {
        this.prodTradeMakhambet = prodTradeMakhambet;
    }

    public void setProdTransp(Double prodTransp) {
        this.prodTransp = prodTransp;
    }

    public void setProdTranspAtyrau(Double prodTranspAtyrau) {
        this.prodTranspAtyrau = prodTranspAtyrau;
    }

    public void setProdTranspZhylyoi(Double prodTranspZhylyoi) {
        this.prodTranspZhylyoi = prodTranspZhylyoi;
    }

    public void setProdTranspInder(Double prodTranspInder) {
        this.prodTranspInder = prodTranspInder;
    }

    public void setProdTranspIsatai(Double prodTranspIsatai) {
        this.prodTranspIsatai = prodTranspIsatai;
    }

    public void setProdTranspKurmangazy(Double prodTranspKurmangazy) {
        this.prodTranspKurmangazy = prodTranspKurmangazy;
    }

    public void setProdTranspKzylkoga(Double prodTranspKzylkoga) {
        this.prodTranspKzylkoga = prodTranspKzylkoga;
    }

    public void setProdTranspMakat(Double prodTranspMakat) {
        this.prodTranspMakat = prodTranspMakat;
    }

    public void setProdTranspMakhambet(Double prodTranspMakhambet) {
        this.prodTranspMakhambet = prodTranspMakhambet;
    }

    public void setProdInfo(Double prodInfo) {
        this.prodInfo = prodInfo;
    }

    public void setProdInfoAtyrau(Double prodInfoAtyrau) {
        this.prodInfoAtyrau = prodInfoAtyrau;
    }

    public void setProdInfoZhylyoi(Double prodInfoZhylyoi) {
        this.prodInfoZhylyoi = prodInfoZhylyoi;
    }

    public void setProdInfoInder(Double prodInfoInder) {
        this.prodInfoInder = prodInfoInder;
    }

    public void setProdInfoIsatai(Double prodInfoIsatai) {
        this.prodInfoIsatai = prodInfoIsatai;
    }

    public void setProdInfoKurmangazy(Double prodInfoKurmangazy) {
        this.prodInfoKurmangazy = prodInfoKurmangazy;
    }

    public void setProdInfoKzylkoga(Double prodInfoKzylkoga) {
        this.prodInfoKzylkoga = prodInfoKzylkoga;
    }

    public void setProdInfoMakat(Double prodInfoMakat) {
        this.prodInfoMakat = prodInfoMakat;
    }

    public void setProdInfoMakhambet(Double prodInfoMakhambet) {
        this.prodInfoMakhambet = prodInfoMakhambet;
    }

    public void setProdEstate(Double prodEstate) {
        this.prodEstate = prodEstate;
    }

    public void setProdEstateAtyrau(Double prodEstateAtyrau) {
        this.prodEstateAtyrau = prodEstateAtyrau;
    }

    public void setProdEstateZhylyoi(Double prodEstateZhylyoi) {
        this.prodEstateZhylyoi = prodEstateZhylyoi;
    }

    public void setProdEstateInder(Double prodEstateInder) {
        this.prodEstateInder = prodEstateInder;
    }

    public void setProdEstateIsatai(Double prodEstateIsatai) {
        this.prodEstateIsatai = prodEstateIsatai;
    }

    public void setProdEstateKurmangazy(Double prodEstateKurmangazy) {
        this.prodEstateKurmangazy = prodEstateKurmangazy;
    }

    public void setProdEstateKzylkoga(Double prodEstateKzylkoga) {
        this.prodEstateKzylkoga = prodEstateKzylkoga;
    }

    public void setProdEstateMakat(Double prodEstateMakat) {
        this.prodEstateMakat = prodEstateMakat;
    }

    public void setProdEstateMakhambet(Double prodEstateMakhambet) {
        this.prodEstateMakhambet = prodEstateMakhambet;
    }

    public void setProdOther(Double prodOther) {
        this.prodOther = prodOther;
    }

    public void setProdOtherAtyrau(Double prodOtherAtyrau) {
        this.prodOtherAtyrau = prodOtherAtyrau;
    }

    public void setProdOtherZhylyoi(Double prodOtherZhylyoi) {
        this.prodOtherZhylyoi = prodOtherZhylyoi;
    }

    public void setProdOtherInder(Double prodOtherInder) {
        this.prodOtherInder = prodOtherInder;
    }

    public void setProdOtherIsatai(Double prodOtherIsatai) {
        this.prodOtherIsatai = prodOtherIsatai;
    }

    public void setProdOtherKurmangazy(Double prodOtherKurmangazy) {
        this.prodOtherKurmangazy = prodOtherKurmangazy;
    }

    public void setProdOtherKzylkoga(Double prodOtherKzylkoga) {
        this.prodOtherKzylkoga = prodOtherKzylkoga;
    }

    public void setProdOtherMakat(Double prodOtherMakat) {
        this.prodOtherMakat = prodOtherMakat;
    }

    public void setProdOtherMakhambet(Double prodOtherMakhambet) {
        this.prodOtherMakhambet = prodOtherMakhambet;
    }

    public void setIfo(Double ifo) {
        this.ifo = ifo;
    }

    public void setIfoAgro(Double ifoAgro) {
        this.ifoAgro = ifoAgro;
    }

    public void setIfoAgroAtyrau(Double ifoAgroAtyrau) {
        this.ifoAgroAtyrau = ifoAgroAtyrau;
    }

    public void setIfoAgroZhylyoi(Double ifoAgroZhylyoi) {
        this.ifoAgroZhylyoi = ifoAgroZhylyoi;
    }

    public void setIfoAgroInder(Double ifoAgroInder) {
        this.ifoAgroInder = ifoAgroInder;
    }

    public void setIfoAgroIsatai(Double ifoAgroIsatai) {
        this.ifoAgroIsatai = ifoAgroIsatai;
    }

    public void setIfoAgroKurmangazy(Double ifoAgroKurmangazy) {
        this.ifoAgroKurmangazy = ifoAgroKurmangazy;
    }

    public void setIfoAgroKzylkoga(Double ifoAgroKzylkoga) {
        this.ifoAgroKzylkoga = ifoAgroKzylkoga;
    }

    public void setIfoAgroMakat(Double ifoAgroMakat) {
        this.ifoAgroMakat = ifoAgroMakat;
    }

    public void setIfoAgroMakhambet(Double ifoAgroMakhambet) {
        this.ifoAgroMakhambet = ifoAgroMakhambet;
    }

    public void setIfoMining(Double ifoMining) {
        this.ifoMining = ifoMining;
    }

    public void setIfoMiningAtyrau(Double ifoMiningAtyrau) {
        this.ifoMiningAtyrau = ifoMiningAtyrau;
    }

    public void setIfoMiningZhylyoi(Double ifoMiningZhylyoi) {
        this.ifoMiningZhylyoi = ifoMiningZhylyoi;
    }

    public void setIfoMiningInder(Double ifoMiningInder) {
        this.ifoMiningInder = ifoMiningInder;
    }

    public void setIfoMiningIsatai(Double ifoMiningIsatai) {
        this.ifoMiningIsatai = ifoMiningIsatai;
    }

    public void setIfoMiningKurmangazy(Double ifoMiningKurmangazy) {
        this.ifoMiningKurmangazy = ifoMiningKurmangazy;
    }

    public void setIfoMiningKzylkoga(Double ifoMiningKzylkoga) {
        this.ifoMiningKzylkoga = ifoMiningKzylkoga;
    }

    public void setIfoMiningMakat(Double ifoMiningMakat) {
        this.ifoMiningMakat = ifoMiningMakat;
    }

    public void setIfoMiningMakhambet(Double ifoMiningMakhambet) {
        this.ifoMiningMakhambet = ifoMiningMakhambet;
    }

    public void setIfoMan(Double ifoMan) {
        this.ifoMan = ifoMan;
    }

    public void setIfoManAtyrau(Double ifoManAtyrau) {
        this.ifoManAtyrau = ifoManAtyrau;
    }

    public void setIfoManZhylyoi(Double ifoManZhylyoi) {
        this.ifoManZhylyoi = ifoManZhylyoi;
    }

    public void setIfoManInder(Double ifoManInder) {
        this.ifoManInder = ifoManInder;
    }

    public void setIfoManIsatai(Double ifoManIsatai) {
        this.ifoManIsatai = ifoManIsatai;
    }

    public void setIfoManKurmangazy(Double ifoManKurmangazy) {
        this.ifoManKurmangazy = ifoManKurmangazy;
    }

    public void setIfoManKzylkoga(Double ifoManKzylkoga) {
        this.ifoManKzylkoga = ifoManKzylkoga;
    }

    public void setIfoManMakat(Double ifoManMakat) {
        this.ifoManMakat = ifoManMakat;
    }

    public void setIfoManMakhambet(Double ifoManMakhambet) {
        this.ifoManMakhambet = ifoManMakhambet;
    }

    public void setIfoEnergo(Double ifoEnergo) {
        this.ifoEnergo = ifoEnergo;
    }

    public void setIfoEnergoAtyrau(Double ifoEnergoAtyrau) {
        this.ifoEnergoAtyrau = ifoEnergoAtyrau;
    }

    public void setIfoEnergoZhylyoi(Double ifoEnergoZhylyoi) {
        this.ifoEnergoZhylyoi = ifoEnergoZhylyoi;
    }

    public void setIfoEnergoInder(Double ifoEnergoInder) {
        this.ifoEnergoInder = ifoEnergoInder;
    }

    public void setIfoEnergoIsatai(Double ifoEnergoIsatai) {
        this.ifoEnergoIsatai = ifoEnergoIsatai;
    }

    public void setIfoEnergoKurmangazy(Double ifoEnergoKurmangazy) {
        this.ifoEnergoKurmangazy = ifoEnergoKurmangazy;
    }

    public void setIfoEnergoKzylkoga(Double ifoEnergoKzylkoga) {
        this.ifoEnergoKzylkoga = ifoEnergoKzylkoga;
    }

    public void setIfoEnergoMakat(Double ifoEnergoMakat) {
        this.ifoEnergoMakat = ifoEnergoMakat;
    }

    public void setIfoEnergoMakhambet(Double ifoEnergoMakhambet) {
        this.ifoEnergoMakhambet = ifoEnergoMakhambet;
    }

    public void setIfoWater(Double ifoWater) {
        this.ifoWater = ifoWater;
    }

    public void setIfoWaterAtyrau(Double ifoWaterAtyrau) {
        this.ifoWaterAtyrau = ifoWaterAtyrau;
    }

    public void setIfoWaterZhylyoi(Double ifoWaterZhylyoi) {
        this.ifoWaterZhylyoi = ifoWaterZhylyoi;
    }

    public void setIfoWaterInder(Double ifoWaterInder) {
        this.ifoWaterInder = ifoWaterInder;
    }

    public void setIfoWaterIsatai(Double ifoWaterIsatai) {
        this.ifoWaterIsatai = ifoWaterIsatai;
    }

    public void setIfoWaterKurmangazy(Double ifoWaterKurmangazy) {
        this.ifoWaterKurmangazy = ifoWaterKurmangazy;
    }

    public void setIfoWaterKzylkoga(Double ifoWaterKzylkoga) {
        this.ifoWaterKzylkoga = ifoWaterKzylkoga;
    }

    public void setIfoWaterMakat(Double ifoWaterMakat) {
        this.ifoWaterMakat = ifoWaterMakat;
    }

    public void setIfoWaterMakhambet(Double ifoWaterMakhambet) {
        this.ifoWaterMakhambet = ifoWaterMakhambet;
    }

    public void setIfoBuilding(Double ifoBuilding) {
        this.ifoBuilding = ifoBuilding;
    }

    public void setIfoBuildingAtyrau(Double ifoBuildingAtyrau) {
        this.ifoBuildingAtyrau = ifoBuildingAtyrau;
    }

    public void setIfoBuildingZhylyoi(Double ifoBuildingZhylyoi) {
        this.ifoBuildingZhylyoi = ifoBuildingZhylyoi;
    }

    public void setIfoBuildingInder(Double ifoBuildingInder) {
        this.ifoBuildingInder = ifoBuildingInder;
    }

    public void setIfoBuildingIsatai(Double ifoBuildingIsatai) {
        this.ifoBuildingIsatai = ifoBuildingIsatai;
    }

    public void setIfoBuildingKurmangazy(Double ifoBuildingKurmangazy) {
        this.ifoBuildingKurmangazy = ifoBuildingKurmangazy;
    }

    public void setIfoBuildingKzylkoga(Double ifoBuildingKzylkoga) {
        this.ifoBuildingKzylkoga = ifoBuildingKzylkoga;
    }

    public void setIfoBuildingMakat(Double ifoBuildingMakat) {
        this.ifoBuildingMakat = ifoBuildingMakat;
    }

    public void setIfoBuildingMakhambet(Double ifoBuildingMakhambet) {
        this.ifoBuildingMakhambet = ifoBuildingMakhambet;
    }

    public void setIfoRetail(Double ifoRetail) {
        this.ifoRetail = ifoRetail;
    }

    public void setIfoRetailAtyrau(Double ifoRetailAtyrau) {
        this.ifoRetailAtyrau = ifoRetailAtyrau;
    }

    public void setIfoRetailZhylyoi(Double ifoRetailZhylyoi) {
        this.ifoRetailZhylyoi = ifoRetailZhylyoi;
    }

    public void setIfoRetailInder(Double ifoRetailInder) {
        this.ifoRetailInder = ifoRetailInder;
    }

    public void setIfoRetailIsatai(Double ifoRetailIsatai) {
        this.ifoRetailIsatai = ifoRetailIsatai;
    }

    public void setIfoRetailKurmangazy(Double ifoRetailKurmangazy) {
        this.ifoRetailKurmangazy = ifoRetailKurmangazy;
    }

    public void setIfoRetailKzylkoga(Double ifoRetailKzylkoga) {
        this.ifoRetailKzylkoga = ifoRetailKzylkoga;
    }

    public void setIfoRetailMakat(Double ifoRetailMakat) {
        this.ifoRetailMakat = ifoRetailMakat;
    }

    public void setIfoRetailMakhambet(Double ifoRetailMakhambet) {
        this.ifoRetailMakhambet = ifoRetailMakhambet;
    }

    public void setIfoTrade(Double ifoTrade) {
        this.ifoTrade = ifoTrade;
    }

    public void setIfoTradeAtyrau(Double ifoTradeAtyrau) {
        this.ifoTradeAtyrau = ifoTradeAtyrau;
    }

    public void setIfoTradeZhylyoi(Double ifoTradeZhylyoi) {
        this.ifoTradeZhylyoi = ifoTradeZhylyoi;
    }

    public void setIfoTradeInder(Double ifoTradeInder) {
        this.ifoTradeInder = ifoTradeInder;
    }

    public void setIfoTradeIsatai(Double ifoTradeIsatai) {
        this.ifoTradeIsatai = ifoTradeIsatai;
    }

    public void setIfoTradeKurmangazy(Double ifoTradeKurmangazy) {
        this.ifoTradeKurmangazy = ifoTradeKurmangazy;
    }

    public void setIfoTradeKzylkoga(Double ifoTradeKzylkoga) {
        this.ifoTradeKzylkoga = ifoTradeKzylkoga;
    }

    public void setIfoTradeMakat(Double ifoTradeMakat) {
        this.ifoTradeMakat = ifoTradeMakat;
    }

    public void setIfoTradeMakhambet(Double ifoTradeMakhambet) {
        this.ifoTradeMakhambet = ifoTradeMakhambet;
    }

    public void setIfoTransp(Double ifoTransp) {
        this.ifoTransp = ifoTransp;
    }

    public void setIfoTranspAtyrau(Double ifoTranspAtyrau) {
        this.ifoTranspAtyrau = ifoTranspAtyrau;
    }

    public void setIfoTranspZhylyoi(Double ifoTranspZhylyoi) {
        this.ifoTranspZhylyoi = ifoTranspZhylyoi;
    }

    public void setIfoTranspInder(Double ifoTranspInder) {
        this.ifoTranspInder = ifoTranspInder;
    }

    public void setIfoTranspIsatai(Double ifoTranspIsatai) {
        this.ifoTranspIsatai = ifoTranspIsatai;
    }

    public void setIfoTranspKurmangazy(Double ifoTranspKurmangazy) {
        this.ifoTranspKurmangazy = ifoTranspKurmangazy;
    }

    public void setIfoTranspKzylkoga(Double ifoTranspKzylkoga) {
        this.ifoTranspKzylkoga = ifoTranspKzylkoga;
    }

    public void setIfoTranspMakat(Double ifoTranspMakat) {
        this.ifoTranspMakat = ifoTranspMakat;
    }

    public void setIfoTranspMakhambet(Double ifoTranspMakhambet) {
        this.ifoTranspMakhambet = ifoTranspMakhambet;
    }

    public void setIfoInfo(Double ifoInfo) {
        this.ifoInfo = ifoInfo;
    }

    public void setIfoInfoAtyrau(Double ifoInfoAtyrau) {
        this.ifoInfoAtyrau = ifoInfoAtyrau;
    }

    public void setIfoInfoZhylyoi(Double ifoInfoZhylyoi) {
        this.ifoInfoZhylyoi = ifoInfoZhylyoi;
    }

    public void setIfoInfoInder(Double ifoInfoInder) {
        this.ifoInfoInder = ifoInfoInder;
    }

    public void setIfoInfoIsatai(Double ifoInfoIsatai) {
        this.ifoInfoIsatai = ifoInfoIsatai;
    }

    public void setIfoInfoKurmangazy(Double ifoInfoKurmangazy) {
        this.ifoInfoKurmangazy = ifoInfoKurmangazy;
    }

    public void setIfoInfoKzylkoga(Double ifoInfoKzylkoga) {
        this.ifoInfoKzylkoga = ifoInfoKzylkoga;
    }

    public void setIfoInfoMakat(Double ifoInfoMakat) {
        this.ifoInfoMakat = ifoInfoMakat;
    }

    public void setIfoInfoMakhambet(Double ifoInfoMakhambet) {
        this.ifoInfoMakhambet = ifoInfoMakhambet;
    }

    public void setIfoEstate(Double ifoEstate) {
        this.ifoEstate = ifoEstate;
    }

    public void setIfoEstateAtyrau(Double ifoEstateAtyrau) {
        this.ifoEstateAtyrau = ifoEstateAtyrau;
    }

    public void setIfoEstateZhylyoi(Double ifoEstateZhylyoi) {
        this.ifoEstateZhylyoi = ifoEstateZhylyoi;
    }

    public void setIfoEstateInder(Double ifoEstateInder) {
        this.ifoEstateInder = ifoEstateInder;
    }

    public void setIfoEstateIsatai(Double ifoEstateIsatai) {
        this.ifoEstateIsatai = ifoEstateIsatai;
    }

    public void setIfoEstateKurmangazy(Double ifoEstateKurmangazy) {
        this.ifoEstateKurmangazy = ifoEstateKurmangazy;
    }

    public void setIfoEstateKzylkoga(Double ifoEstateKzylkoga) {
        this.ifoEstateKzylkoga = ifoEstateKzylkoga;
    }

    public void setIfoEstateMakat(Double ifoEstateMakat) {
        this.ifoEstateMakat = ifoEstateMakat;
    }

    public void setIfoEstateMakhambet(Double ifoEstateMakhambet) {
        this.ifoEstateMakhambet = ifoEstateMakhambet;
    }

    public void setIfoOther(Double ifoOther) {
        this.ifoOther = ifoOther;
    }

    public void setIfoOtherAtyrau(Double ifoOtherAtyrau) {
        this.ifoOtherAtyrau = ifoOtherAtyrau;
    }

    public void setIfoOtherZhylyoi(Double ifoOtherZhylyoi) {
        this.ifoOtherZhylyoi = ifoOtherZhylyoi;
    }

    public void setIfoOtherInder(Double ifoOtherInder) {
        this.ifoOtherInder = ifoOtherInder;
    }

    public void setIfoOtherIsatai(Double ifoOtherIsatai) {
        this.ifoOtherIsatai = ifoOtherIsatai;
    }

    public void setIfoOtherKurmangazy(Double ifoOtherKurmangazy) {
        this.ifoOtherKurmangazy = ifoOtherKurmangazy;
    }

    public void setIfoOtherKzylkoga(Double ifoOtherKzylkoga) {
        this.ifoOtherKzylkoga = ifoOtherKzylkoga;
    }

    public void setIfoOtherMakat(Double ifoOtherMakat) {
        this.ifoOtherMakat = ifoOtherMakat;
    }

    public void setIfoOtherMakhambet(Double ifoOtherMakhambet) {
        this.ifoOtherMakhambet = ifoOtherMakhambet;
    }
}
