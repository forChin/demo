package com.example.demo.domain;

import java.util.HashMap;
import java.util.Map;

public class InternalPhyMonitoring {

    private String year;

    private Map<String, Double> disruptionsProvisionResidents;
    private Map<String, Double> levelPublicConfidenceAkimobl;
    private Map<String, Double> levelPublicSupport;
    private Map<String, Double> levelAwarenessPopulation;
    private Map<String, Double> numberApplicationsRallies;
    private Map<String, Double> mapHotbedsSocialTension;

    public InternalPhyMonitoring(){
        this.disruptionsProvisionResidents = new HashMap<>();
        this.levelAwarenessPopulation = new HashMap<>();
        this.levelPublicConfidenceAkimobl = new HashMap<>();
        this.levelPublicSupport = new HashMap<>();
        this.mapHotbedsSocialTension = new HashMap<>();
        this.numberApplicationsRallies = new HashMap<>();
    }

    public String getYear() {
        return year;
    }

    public Map<String, Double> getDisruptionsProvisionResidents() {
        return disruptionsProvisionResidents;
    }

    public Map<String, Double> getLevelPublicConfidenceAkimobl() {
        return levelPublicConfidenceAkimobl;
    }

    public Map<String, Double> getLevelPublicSupport() {
        return levelPublicSupport;
    }

    public Map<String, Double> getLevelAwarenessPopulation() {
        return levelAwarenessPopulation;
    }

    public Map<String, Double> getNumberApplicationsRallies() {
        return numberApplicationsRallies;
    }

    public Map<String, Double> getMapHotbedsSocialTension() {
        return mapHotbedsSocialTension;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setDisruptionsProvisionResidents(Map<String, Double> disruptionsProvisionResidents) {
        this.disruptionsProvisionResidents = disruptionsProvisionResidents;
    }

    public void setLevelPublicConfidenceAkimobl(Map<String, Double> levelPublicConfidenceAkimobl) {
        this.levelPublicConfidenceAkimobl = levelPublicConfidenceAkimobl;
    }

    public void setLevelPublicSupport(Map<String, Double> levelPublicSupport) {
        this.levelPublicSupport = levelPublicSupport;
    }

    public void setLevelAwarenessPopulation(Map<String, Double> levelAwarenessPopulation) {
        this.levelAwarenessPopulation = levelAwarenessPopulation;
    }

    public void setNumberApplicationsRallies(Map<String, Double> numberApplicationsRallies) {
        this.numberApplicationsRallies = numberApplicationsRallies;
    }

    public void setMapHotbedsSocialTension(Map<String, Double> mapHotbedsSocialTension) {
        this.mapHotbedsSocialTension = mapHotbedsSocialTension;
    }
}
