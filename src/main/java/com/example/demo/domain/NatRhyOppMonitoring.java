package com.example.demo.domain;

import java.util.HashMap;
import java.util.Map;

public class NatRhyOppMonitoring {

    private String year;

    private Map<String, Double> volumeWaste;
    private Map<String, Double> ilmplementationActionPlan;
    private Map<String, Double> exterminationWolvesNature;

    public NatRhyOppMonitoring(){
        this.exterminationWolvesNature = new HashMap<>();
        this.ilmplementationActionPlan = new HashMap<>();
        this.volumeWaste = new HashMap<>();
    }

    public String getYear() {
        return year;
    }

    public Map<String, Double> getVolumeWaste() {
        return volumeWaste;
    }

    public Map<String, Double> getIlmplementationActionPlan() {
        return ilmplementationActionPlan;
    }

    public Map<String, Double> getExterminationWolvesNature() {
        return exterminationWolvesNature;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setVolumeWaste(Map<String, Double> volumeWaste) {
        this.volumeWaste = volumeWaste;
    }

    public void setIlmplementationActionPlan(Map<String, Double> ilmplementationActionPlan) {
        this.ilmplementationActionPlan = ilmplementationActionPlan;
    }

    public void setExterminationWolvesNature(Map<String, Double> exterminationWolvesNature) {
        this.exterminationWolvesNature = exterminationWolvesNature;
    }
}
