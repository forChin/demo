package com.example.demo.domain;

import java.util.HashMap;
import java.util.Map;

public class PrognozVrpTable {

    private String region;
    private String year;
    private String indicator;
    private String scenario;

    private Map<String, Double> industry = new HashMap<>();

    public PrognozVrpTable(){

    }

    public String getRegion() {
        return region;
    }

    public String getYear() {
        return year;
    }

    public Map<String, Double> getIndustry() {
        return industry;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setIndustry(Map<String, Double> industry) {
        this.industry = industry;
    }

    public String getIndicator() {
        return indicator;
    }

    public String getScenario() {
        return scenario;
    }

    public void setIndicator(String indicator) {
        this.indicator = indicator;
    }

    public void setScenario(String scenario) {
        this.scenario = scenario;
    }
}
