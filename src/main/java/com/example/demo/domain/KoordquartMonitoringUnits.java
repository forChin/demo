package com.example.demo.domain;

import java.util.Map;

public class KoordquartMonitoringUnits {

    private String mortalityRateUnit; // o_unit
    private String numberPopulationUnit;
    private String employedPopulationUnit;
    private String hiredWorkerUnit;
    private String selfEmployedUnit;
    private String numberUnemployedUnit;
    private String averageMonthlyWageUnit;
    private String numberPensionersUnit;
    private String proportionPopulationUnit;
    private String numberRecipientsAspUnit;
    private String shareWithDisabilitiesUnit;
    private String subsistenceUnit;

    public KoordquartMonitoringUnits(){

    }

    public String getMortalityRateUnit() {
        return mortalityRateUnit;
    }

    public String getNumberPopulationUnit() {
        return numberPopulationUnit;
    }

    public String getEmployedPopulationUnit() {
        return employedPopulationUnit;
    }

    public String getHiredWorkerUnit() {
        return hiredWorkerUnit;
    }

    public String getSelfEmployedUnit() {
        return selfEmployedUnit;
    }

    public String getNumberUnemployedUnit() {
        return numberUnemployedUnit;
    }

    public String getAverageMonthlyWageUnit() {
        return averageMonthlyWageUnit;
    }

    public String getNumberPensionersUnit() {
        return numberPensionersUnit;
    }

    public String getProportionPopulationUnit() {
        return proportionPopulationUnit;
    }

    public String getNumberRecipientsAspUnit() {
        return numberRecipientsAspUnit;
    }

    public String getShareWithDisabilitiesUnit() {
        return shareWithDisabilitiesUnit;
    }

    public String getSubsistenceUnit() {
        return subsistenceUnit;
    }

    public void setMortalityRateUnit(String mortalityRateUnit) {
        this.mortalityRateUnit = mortalityRateUnit;
    }

    public void setNumberPopulationUnit(String numberPopulationUnit) {
        this.numberPopulationUnit = numberPopulationUnit;
    }

    public void setEmployedPopulationUnit(String employedPopulationUnit) {
        this.employedPopulationUnit = employedPopulationUnit;
    }

    public void setHiredWorkerUnit(String hiredWorkerUnit) {
        this.hiredWorkerUnit = hiredWorkerUnit;
    }

    public void setSelfEmployedUnit(String selfEmployedUnit) {
        this.selfEmployedUnit = selfEmployedUnit;
    }

    public void setNumberUnemployedUnit(String numberUnemployedUnit) {
        this.numberUnemployedUnit = numberUnemployedUnit;
    }

    public void setAverageMonthlyWageUnit(String averageMonthlyWageUnit) {
        this.averageMonthlyWageUnit = averageMonthlyWageUnit;
    }

    public void setNumberPensionersUnit(String numberPensionersUnit) {
        this.numberPensionersUnit = numberPensionersUnit;
    }

    public void setProportionPopulationUnit(String proportionPopulationUnit) {
        this.proportionPopulationUnit = proportionPopulationUnit;
    }

    public void setNumberRecipientsAspUnit(String numberRecipientsAspUnit) {
        this.numberRecipientsAspUnit = numberRecipientsAspUnit;
    }

    public void setShareWithDisabilitiesUnit(String shareWithDisabilitiesUnit) {
        this.shareWithDisabilitiesUnit = shareWithDisabilitiesUnit;
    }

    public void setSubsistenceUnit(String subsistenceUnit) {
        this.subsistenceUnit = subsistenceUnit;
    }
}
