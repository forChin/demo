package com.example.demo.domain;

public class MenuRybAqvaTable {
    private String viewFish;        //ush_stat_ryb_i_aqva_table
    private Double fishCatch;

    public String getViewFish() {
        return viewFish;
    }

    public void setViewFish(String viewFish) {
        this.viewFish = viewFish;
    }

    public Double getFishCatch() {
        return fishCatch;
    }

    public void setFishCatch(Double fishCatch) {
        this.fishCatch = fishCatch;
    }
}
