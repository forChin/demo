package com.example.demo.domain;

public class LanguagesDevelopmentYearMonitoring {

    private String year;
    private Double populationSpeakingLanguage;

    public LanguagesDevelopmentYearMonitoring(){

    }

    public String getYear() {
        return year;
    }

    public Double getPopulationSpeakingLanguage() {
        return populationSpeakingLanguage;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setPopulationSpeakingLanguage(Double populationSpeakingLanguage) {
        this.populationSpeakingLanguage = populationSpeakingLanguage;
    }
}
