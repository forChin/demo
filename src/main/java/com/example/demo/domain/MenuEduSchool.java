package com.example.demo.domain;

public class MenuEduSchool {
    private String year;            //edu_school
    private String area;
    private String region;
    private String ruralDistrict;
    private String ruralSettlement;
    private String nameSchool;
    private Integer integer1;
    private Integer numberStudents;
    private Integer countGraduates;
    private Integer countParticipantsUnt;
    private Integer countTeachers;
    private Integer countCategoryC;
    private Integer countCategoryA;
    private Integer countCategoryB;
    private Integer countCategoryD;
    private Integer countAgeA;
    private Integer countAgeB;
    private Integer countAgeC;
    private Integer countAgeD;
    private Integer countAgeE;
    private Integer countAgeF;
    private Integer countEducationHigher;
    private Integer countEducationColleges;
    private Integer generalSecondaryEducation;
    private Integer countGoldMedal;
    private Integer countCertificateDistinction;
    private Integer countUnsatisfactoryEducation;
    private Integer countTheLowestRating;
    private String schoolAddress;
    private String yearB;
    private String contactNumber;
    private String workingHours;
    private String workingHoursA;
    private String lastName;
    private String firstName;
    private String otchestvo;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRuralDistrict() {
        return ruralDistrict;
    }

    public void setRuralDistrict(String ruralDistrict) {
        this.ruralDistrict = ruralDistrict;
    }

    public String getRuralSettlement() {
        return ruralSettlement;
    }

    public void setRuralSettlement(String ruralSettlement) {
        this.ruralSettlement = ruralSettlement;
    }

    public String getNameSchool() {
        return nameSchool;
    }

    public void setNameSchool(String nameSchool) {
        this.nameSchool = nameSchool;
    }

    public Integer getInteger1() {
        return integer1;
    }

    public void setInteger1(Integer integer1) {
        this.integer1 = integer1;
    }

    public Integer getNumberStudents() {
        return numberStudents;
    }

    public void setNumberStudents(Integer numberStudents) {
        this.numberStudents = numberStudents;
    }

    public Integer getCountGraduates() {
        return countGraduates;
    }

    public void setCountGraduates(Integer countGraduates) {
        this.countGraduates = countGraduates;
    }

    public Integer getCountParticipantsUnt() {
        return countParticipantsUnt;
    }

    public void setCountParticipantsUnt(Integer countParticipantsUnt) {
        this.countParticipantsUnt = countParticipantsUnt;
    }

    public Integer getCountTeachers() {
        return countTeachers;
    }

    public void setCountTeachers(Integer countTeachers) {
        this.countTeachers = countTeachers;
    }

    public Integer getCountCategoryC() {
        return countCategoryC;
    }

    public void setCountCategoryC(Integer countCategoryC) {
        this.countCategoryC = countCategoryC;
    }

    public Integer getCountCategoryA() {
        return countCategoryA;
    }

    public void setCountCategoryA(Integer countCategoryA) {
        this.countCategoryA = countCategoryA;
    }

    public Integer getCountCategoryB() {
        return countCategoryB;
    }

    public void setCountCategoryB(Integer countCategoryB) {
        this.countCategoryB = countCategoryB;
    }

    public Integer getCountCategoryD() {
        return countCategoryD;
    }

    public void setCountCategoryD(Integer countCategoryD) {
        this.countCategoryD = countCategoryD;
    }

    public Integer getCountAgeA() {
        return countAgeA;
    }

    public void setCountAgeA(Integer countAgeA) {
        this.countAgeA = countAgeA;
    }

    public Integer getCountAgeB() {
        return countAgeB;
    }

    public void setCountAgeB(Integer countAgeB) {
        this.countAgeB = countAgeB;
    }

    public Integer getCountAgeC() {
        return countAgeC;
    }

    public void setCountAgeC(Integer countAgeC) {
        this.countAgeC = countAgeC;
    }

    public Integer getCountAgeD() {
        return countAgeD;
    }

    public void setCountAgeD(Integer countAgeD) {
        this.countAgeD = countAgeD;
    }

    public Integer getCountAgeE() {
        return countAgeE;
    }

    public void setCountAgeE(Integer countAgeE) {
        this.countAgeE = countAgeE;
    }

    public Integer getCountAgeF() {
        return countAgeF;
    }

    public void setCountAgeF(Integer countAgeF) {
        this.countAgeF = countAgeF;
    }

    public Integer getCountEducationHigher() {
        return countEducationHigher;
    }

    public void setCountEducationHigher(Integer countEducationHigher) {
        this.countEducationHigher = countEducationHigher;
    }

    public Integer getCountEducationColleges() {
        return countEducationColleges;
    }

    public void setCountEducationColleges(Integer countEducationColleges) {
        this.countEducationColleges = countEducationColleges;
    }

    public Integer getGeneralSecondaryEducation() {
        return generalSecondaryEducation;
    }

    public void setGeneralSecondaryEducation(Integer generalSecondaryEducation) {
        this.generalSecondaryEducation = generalSecondaryEducation;
    }

    public Integer getCountGoldMedal() {
        return countGoldMedal;
    }

    public void setCountGoldMedal(Integer countGoldMedal) {
        this.countGoldMedal = countGoldMedal;
    }

    public Integer getCountCertificateDistinction() {
        return countCertificateDistinction;
    }

    public void setCountCertificateDistinction(Integer countCertificateDistinction) {
        this.countCertificateDistinction = countCertificateDistinction;
    }

    public Integer getCountUnsatisfactoryEducation() {
        return countUnsatisfactoryEducation;
    }

    public void setCountUnsatisfactoryEducation(Integer countUnsatisfactoryEducation) {
        this.countUnsatisfactoryEducation = countUnsatisfactoryEducation;
    }

    public Integer getCountTheLowestRating() {
        return countTheLowestRating;
    }

    public void setCountTheLowestRating(Integer countTheLowestRating) {
        this.countTheLowestRating = countTheLowestRating;
    }

    public String getSchoolAddress() {
        return schoolAddress;
    }

    public void setSchoolAddress(String schoolAddress) {
        this.schoolAddress = schoolAddress;
    }

    public String getYearB() {
        return yearB;
    }

    public void setYearB(String yearB) {
        this.yearB = yearB;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getWorkingHours() {
        return workingHours;
    }

    public void setWorkingHours(String workingHours) {
        this.workingHours = workingHours;
    }

    public String getWorkingHoursA() {
        return workingHoursA;
    }

    public void setWorkingHoursA(String workingHoursA) {
        this.workingHoursA = workingHoursA;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getOtchestvo() {
        return otchestvo;
    }

    public void setOtchestvo(String otchestvo) {
        this.otchestvo = otchestvo;
    }
}
