package com.example.demo.domain;

public class BSpisokDorogiMonitoring {
    // new spisok dorogi

    private String roadType;
    private String raion;
    private String roadName;
    private String raiRoad;
    private String oblRoad;
    private String extent;
    private String extentHor;
    private String extentUd;
    private String extentNeud;
    private String state;
    private String stateHor;
    private String stateUd;
    private String stateNeud;

    public String getRoadType() {
        return roadType;
    }

    public String getRaion() {
        return raion;
    }

    public String getRoadName() {
        return roadName;
    }

    public String getRaiRoad() {
        return raiRoad;
    }

    public String getOblRoad() {
        return oblRoad;
    }

    public String getExtent() {
        return extent;
    }

    public String getExtentHor() {
        return extentHor;
    }

    public String getExtentUd() {
        return extentUd;
    }

    public String getExtentNeud() {
        return extentNeud;
    }

    public String getState() {
        return state;
    }

    public String getStateHor() {
        return stateHor;
    }

    public String getStateUd() {
        return stateUd;
    }

    public String getStateNeud() {
        return stateNeud;
    }

    public void setRoadType(String roadType) {
        this.roadType = roadType;
    }

    public void setRaion(String raion) {
        this.raion = raion;
    }

    public void setRoadName(String roadName) {
        this.roadName = roadName;
    }

    public void setRaiRoad(String raiRoad) {
        this.raiRoad = raiRoad;
    }

    public void setOblRoad(String oblRoad) {
        this.oblRoad = oblRoad;
    }

    public void setExtent(String extent) {
        this.extent = extent;
    }

    public void setExtentHor(String extentHor) {
        this.extentHor = extentHor;
    }

    public void setExtentUd(String extentUd) {
        this.extentUd = extentUd;
    }

    public void setExtentNeud(String extentNeud) {
        this.extentNeud = extentNeud;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setStateHor(String stateHor) {
        this.stateHor = stateHor;
    }

    public void setStateUd(String stateUd) {
        this.stateUd = stateUd;
    }

    public void setStateNeud(String stateNeud) {
        this.stateNeud = stateNeud;
    }
}
