package com.example.demo.domain;

public class ZdravKCatalogMonitoring {

//new_zdravooh_catalog_zdrav

    private String index;
    private Double share;
    private Double point;
    private String source;
    private String soursPoints;

    public String getIndex() {
        return index;
    }

    public Double getShare() {
        return share;
    }

    public Double getPoint() {
        return point;
    }

    public String getSource() {
        return source;
    }

    public String getSoursPoints() {
        return soursPoints;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public void setShare(Double share) {
        this.share = share;
    }

    public void setPoint(Double point) {
        this.point = point;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setSoursPoints(String soursPoints) {
        this.soursPoints = soursPoints;
    }
}
