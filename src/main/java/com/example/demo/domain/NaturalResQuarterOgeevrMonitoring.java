package com.example.demo.domain;

import java.util.HashMap;
import java.util.Map;

public class NaturalResQuarterOgeevrMonitoring {

    private String year;

    private Map<String, Double> conductingPublicHearings;

    public NaturalResQuarterOgeevrMonitoring(){
        this.conductingPublicHearings = new HashMap<>();
    }

    public String getYear() {
        return year;
    }

    public Map<String, Double> getConductingPublicHearings() {
        return conductingPublicHearings;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setConductingPublicHearings(Map<String, Double> conductingPublicHearings) {
        this.conductingPublicHearings = conductingPublicHearings;
    }
}
