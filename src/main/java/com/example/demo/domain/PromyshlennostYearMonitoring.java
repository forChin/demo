package com.example.demo.domain;

public class PromyshlennostYearMonitoring {
    private String year;
    private Double shareOutput;

    public PromyshlennostYearMonitoring(){

    }

    public String getYear() {
        return year;
    }

    public Double getShareOutput() {
        return shareOutput;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setShareOutput(Double shareOutput) {
        this.shareOutput = shareOutput;
    }
}
