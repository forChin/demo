package com.example.demo.domain;

import java.util.HashMap;
import java.util.Map;

public class KoordquartMonitoring {

    private String year;
//    private String quater;
    private Map<String, Double> nemploymentRate;
    private Map<String, Double> numberPopulation;
    private Map<String, Double> employedPopulation;
    private Map<String, Double> hiredWorker;
    private Map<String, Double> selfEmployed;
    private Map<String, Double> numberUnemployed;
    private Map<String, Double> averageMonthlyWage;
    private Map<String, Double> numberPensioners;
    private Map<String, Double> proportionPopulation;
    private Map<String, Double> numberRecipientsAsp;
    private Map<String, Double> shareWithDisabilities;
    private Map<String, Double> subsistence;

    public KoordquartMonitoring(){
        this.nemploymentRate = new HashMap<>();
        this.numberPopulation = new HashMap<>();
        this.employedPopulation = new HashMap<>();
        this.hiredWorker = new HashMap<>();
        this.selfEmployed = new HashMap<>();
        this.numberUnemployed = new HashMap<>();
        this.averageMonthlyWage = new HashMap<>();
        this.numberPensioners = new HashMap<>();
        this.proportionPopulation = new HashMap<>();
        this.numberRecipientsAsp = new HashMap<>();
        this.shareWithDisabilities = new HashMap<>();
        this.subsistence = new HashMap<>();
    }

    public String getYear() {
        return year;
    }

    public Map<String, Double> getNemploymentRate() {
        return nemploymentRate;
    }

    public Map<String, Double> getNumberPopulation() {
        return numberPopulation;
    }

    public Map<String, Double> getEmployedPopulation() {
        return employedPopulation;
    }

    public Map<String, Double> getHiredWorker() {
        return hiredWorker;
    }

    public Map<String, Double> getSelfEmployed() {
        return selfEmployed;
    }

    public Map<String, Double> getNumberUnemployed() {
        return numberUnemployed;
    }

    public Map<String, Double> getAverageMonthlyWage() {
        return averageMonthlyWage;
    }

    public Map<String, Double> getNumberPensioners() {
        return numberPensioners;
    }

    public Map<String, Double> getProportionPopulation() {
        return proportionPopulation;
    }

    public Map<String, Double> getNumberRecipientsAsp() {
        return numberRecipientsAsp;
    }

    public Map<String, Double> getShareWithDisabilities() {
        return shareWithDisabilities;
    }

    public Map<String, Double> getSubsistence() {
        return subsistence;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setNemploymentRate(Map<String, Double> nemploymentRate) {
        this.nemploymentRate = nemploymentRate;
    }

    public void setNumberPopulation(Map<String, Double> numberPopulation) {
        this.numberPopulation = numberPopulation;
    }

    public void setEmployedPopulation(Map<String, Double> employedPopulation) {
        this.employedPopulation = employedPopulation;
    }

    public void setHiredWorker(Map<String, Double> hiredWorker) {
        this.hiredWorker = hiredWorker;
    }

    public void setSelfEmployed(Map<String, Double> selfEmployed) {
        this.selfEmployed = selfEmployed;
    }

    public void setNumberUnemployed(Map<String, Double> numberUnemployed) {
        this.numberUnemployed = numberUnemployed;
    }

    public void setAverageMonthlyWage(Map<String, Double> averageMonthlyWage) {
        this.averageMonthlyWage = averageMonthlyWage;
    }

    public void setNumberPensioners(Map<String, Double> numberPensioners) {
        this.numberPensioners = numberPensioners;
    }

    public void setProportionPopulation(Map<String, Double> proportionPopulation) {
        this.proportionPopulation = proportionPopulation;
    }

    public void setNumberRecipientsAsp(Map<String, Double> numberRecipientsAsp) {
        this.numberRecipientsAsp = numberRecipientsAsp;
    }

    public void setShareWithDisabilities(Map<String, Double> shareWithDisabilities) {
        this.shareWithDisabilities = shareWithDisabilities;
    }

    public void setSubsistence(Map<String, Double> subsistence) {
        this.subsistence = subsistence;
    }
}
