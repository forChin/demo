package com.example.demo.domain;

public class ShRKMonitoring {
    //sh_raion_kat

    private String raion;
    private String kategory;

    public String getRaion() {
        return raion;
    }

    public String getKategory() {
        return kategory;
    }

    public void setRaion(String raion) {
        this.raion = raion;
    }

    public void setKategory(String kategory) {
        this.kategory = kategory;
    }
}
