package com.example.demo.domain;

import java.util.HashMap;
import java.util.Map;

public class PromMMonitoring {

    private String year;

    private Map<String, Double> volumeInvestments = new HashMap<>();
    private Map<String, Double> ifo = new HashMap<>();
    private Map<String, Double> rbMb = new HashMap<>();
    private Map<String, Double> own = new HashMap<>();
    private Map<String, Double> bankLoan = new HashMap<>();
    private Map<String, Double> otherBorrowedFunds = new HashMap<>();
    private Map<String, Double> externalLinks = new HashMap<>();
    private Map<String, Double> fixedManufacturing = new HashMap<>();
    private Map<String, Double> growthRate = new HashMap<>();
    private Map<String, Double> volumeIndustrialProduction = new HashMap<>();
    private Map<String, Double> miningIndustry = new HashMap<>();
    private Map<String, Double> oilProduction = new HashMap<>();
    private Map<String, Double> internalInvestment = new HashMap<>();
    private Map<String, Double> productionAssociatedGas = new HashMap<>();
    private Map<String, Double> manufacturingIndustry = new HashMap<>();
    private Map<String, Double> foodProduction = new HashMap<>();
    private Map<String, Double> beverageIndustry = new HashMap<>();
    private Map<String, Double> clothesIndustry = new HashMap<>();
    private Map<String, Double> productionPetroleumProducts = new HashMap<>();
    private Map<String, Double> chemicalIndustry = new HashMap<>();
    private Map<String, Double> manufactureRubberPlastic = new HashMap<>();
    private Map<String, Double> metallurgicalIndustry = new HashMap<>();
    private Map<String, Double> productionOtherProducts = new HashMap<>();
    private Map<String, Double> engineering = new HashMap<>();
    private Map<String, Double> lightIndustry = new HashMap<>();

    public PromMMonitoring(){

    }

    public String getYear() {
        return year;
    }

    public Map<String, Double> getVolumeInvestments() {
        return volumeInvestments;
    }

    public Map<String, Double> getIfo() {
        return ifo;
    }

    public Map<String, Double> getRbMb() {
        return rbMb;
    }

    public Map<String, Double> getOwn() {
        return own;
    }

    public Map<String, Double> getBankLoan() {
        return bankLoan;
    }

    public Map<String, Double> getOtherBorrowedFunds() {
        return otherBorrowedFunds;
    }

    public Map<String, Double> getExternalLinks() {
        return externalLinks;
    }

    public Map<String, Double> getFixedManufacturing() {
        return fixedManufacturing;
    }

    public Map<String, Double> getGrowthRate() {
        return growthRate;
    }

    public Map<String, Double> getVolumeIndustrialProduction() {
        return volumeIndustrialProduction;
    }

    public Map<String, Double> getMiningIndustry() {
        return miningIndustry;
    }

    public Map<String, Double> getOilProduction() {
        return oilProduction;
    }

    public Map<String, Double> getInternalInvestment() {
        return internalInvestment;
    }

    public Map<String, Double> getProductionAssociatedGas() {
        return productionAssociatedGas;
    }

    public Map<String, Double> getManufacturingIndustry() {
        return manufacturingIndustry;
    }

    public Map<String, Double> getFoodProduction() {
        return foodProduction;
    }

    public Map<String, Double> getBeverageIndustry() {
        return beverageIndustry;
    }

    public Map<String, Double> getClothesIndustry() {
        return clothesIndustry;
    }

    public Map<String, Double> getProductionPetroleumProducts() {
        return productionPetroleumProducts;
    }

    public Map<String, Double> getChemicalIndustry() {
        return chemicalIndustry;
    }

    public Map<String, Double> getManufactureRubberPlastic() {
        return manufactureRubberPlastic;
    }

    public Map<String, Double> getMetallurgicalIndustry() {
        return metallurgicalIndustry;
    }

    public Map<String, Double> getProductionOtherProducts() {
        return productionOtherProducts;
    }

    public Map<String, Double> getEngineering() {
        return engineering;
    }

    public Map<String, Double> getLightIndustry() {
        return lightIndustry;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setVolumeInvestments(Map<String, Double> volumeInvestments) {
        this.volumeInvestments = volumeInvestments;
    }

    public void setIfo(Map<String, Double> ifo) {
        this.ifo = ifo;
    }

    public void setRbMb(Map<String, Double> rbMb) {
        this.rbMb = rbMb;
    }

    public void setOwn(Map<String, Double> own) {
        this.own = own;
    }

    public void setBankLoan(Map<String, Double> bankLoan) {
        this.bankLoan = bankLoan;
    }

    public void setOtherBorrowedFunds(Map<String, Double> otherBorrowedFunds) {
        this.otherBorrowedFunds = otherBorrowedFunds;
    }

    public void setExternalLinks(Map<String, Double> externalLinks) {
        this.externalLinks = externalLinks;
    }

    public void setFixedManufacturing(Map<String, Double> fixedManufacturing) {
        this.fixedManufacturing = fixedManufacturing;
    }

    public void setGrowthRate(Map<String, Double> growthRate) {
        this.growthRate = growthRate;
    }

    public void setVolumeIndustrialProduction(Map<String, Double> volumeIndustrialProduction) {
        this.volumeIndustrialProduction = volumeIndustrialProduction;
    }

    public void setMiningIndustry(Map<String, Double> miningIndustry) {
        this.miningIndustry = miningIndustry;
    }

    public void setOilProduction(Map<String, Double> oilProduction) {
        this.oilProduction = oilProduction;
    }

    public void setInternalInvestment(Map<String, Double> internalInvestment) {
        this.internalInvestment = internalInvestment;
    }

    public void setProductionAssociatedGas(Map<String, Double> productionAssociatedGas) {
        this.productionAssociatedGas = productionAssociatedGas;
    }

    public void setManufacturingIndustry(Map<String, Double> manufacturingIndustry) {
        this.manufacturingIndustry = manufacturingIndustry;
    }

    public void setFoodProduction(Map<String, Double> foodProduction) {
        this.foodProduction = foodProduction;
    }

    public void setBeverageIndustry(Map<String, Double> beverageIndustry) {
        this.beverageIndustry = beverageIndustry;
    }

    public void setClothesIndustry(Map<String, Double> clothesIndustry) {
        this.clothesIndustry = clothesIndustry;
    }

    public void setProductionPetroleumProducts(Map<String, Double> productionPetroleumProducts) {
        this.productionPetroleumProducts = productionPetroleumProducts;
    }

    public void setChemicalIndustry(Map<String, Double> chemicalIndustry) {
        this.chemicalIndustry = chemicalIndustry;
    }

    public void setManufactureRubberPlastic(Map<String, Double> manufactureRubberPlastic) {
        this.manufactureRubberPlastic = manufactureRubberPlastic;
    }

    public void setMetallurgicalIndustry(Map<String, Double> metallurgicalIndustry) {
        this.metallurgicalIndustry = metallurgicalIndustry;
    }

    public void setProductionOtherProducts(Map<String, Double> productionOtherProducts) {
        this.productionOtherProducts = productionOtherProducts;
    }

    public void setEngineering(Map<String, Double> engineering) {
        this.engineering = engineering;
    }

    public void setLightIndustry(Map<String, Double> lightIndustry) {
        this.lightIndustry = lightIndustry;
    }
}
