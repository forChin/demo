package com.example.demo.domain;

public class BCategoryWeightMonitoring {
    // new category weight

    private String type;
    private Double weight;

    public String getType() {
        return type;
    }

    public Double getWeight() {
        return weight;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }
}
