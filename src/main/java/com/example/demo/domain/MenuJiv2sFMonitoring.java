package com.example.demo.domain;

public class MenuJiv2sFMonitoring {

    private String year;            //ush_stat_jivotnovodstvo
    private String area;
    private String region;
    private Double outputFoalsAgricultural;

    public String getYear() {
        return year;
    }

    public String getArea() {
        return area;
    }

    public String getRegion() {
        return region;
    }

    public Double getOutputFoalsAgricultural() {
        return outputFoalsAgricultural;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setOutputFoalsAgricultural(Double outputFoalsAgricultural) {
        this.outputFoalsAgricultural = outputFoalsAgricultural;
    }
}
