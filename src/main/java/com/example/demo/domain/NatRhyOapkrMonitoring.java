package com.example.demo.domain;

import java.util.HashMap;
import java.util.Map;

public class NatRhyOapkrMonitoring {

    private String year;

    private Map<String, Double> appeal;

    public NatRhyOapkrMonitoring(){
        this.appeal = new HashMap<>();
    }

    public String getYear() {
        return year;
    }

    public Map<String, Double> getAppeal() {
        return appeal;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setAppeal(Map<String, Double> appeal) {
        this.appeal = appeal;
    }
}
