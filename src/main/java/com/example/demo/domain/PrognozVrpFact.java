package com.example.demo.domain;

public class PrognozVrpFact {

    private String year;
    private Double vrpAgro;
    private Double vrpMining;
    private Double vrpMan;
    private Double vrpEnergo;
    private Double vrpWater;
    private Double vrpBuilding;
    private Double vrpRetail;
    private Double vrpTrade;
    private Double vrpTransp;
    private Double vrpInfo;
    private Double vrpEstate;
    private Double vrpOther;
    private Double vrpTax;
    private Double ifo;
    private Double ifoAgro;
    private Double ifoInd;
    private Double ifoMining;
    private Double ifoMan;
    private Double ifoEnergo;
    private Double ifoWater;
    private Double ifoBuilding;
    private Double ifoAllTrade;
    private Double ifoTransp;
    private Double ifoInfo;
    private Double ifoEstate;
    private Double ifoOther;

    public String getYear() {
        return year;
    }

    public Double getVrpAgro() {
        return vrpAgro;
    }

    public Double getVrpMining() {
        return vrpMining;
    }

    public Double getVrpMan() {
        return vrpMan;
    }

    public Double getVrpEnergo() {
        return vrpEnergo;
    }

    public Double getVrpWater() {
        return vrpWater;
    }

    public Double getVrpBuilding() {
        return vrpBuilding;
    }

    public Double getVrpRetail() {
        return vrpRetail;
    }

    public Double getVrpTrade() {
        return vrpTrade;
    }

    public Double getVrpTransp() {
        return vrpTransp;
    }

    public Double getVrpInfo() {
        return vrpInfo;
    }

    public Double getVrpEstate() {
        return vrpEstate;
    }

    public Double getVrpOther() {
        return vrpOther;
    }

    public Double getVrpTax() {
        return vrpTax;
    }

    public Double getIfo() {
        return ifo;
    }

    public Double getIfoAgro() {
        return ifoAgro;
    }

    public Double getIfoInd() {
        return ifoInd;
    }

    public Double getIfoMining() {
        return ifoMining;
    }

    public Double getIfoMan() {
        return ifoMan;
    }

    public Double getIfoEnergo() {
        return ifoEnergo;
    }

    public Double getIfoWater() {
        return ifoWater;
    }

    public Double getIfoBuilding() {
        return ifoBuilding;
    }

    public Double getIfoAllTrade() {
        return ifoAllTrade;
    }

    public Double getIfoTransp() {
        return ifoTransp;
    }

    public Double getIfoInfo() {
        return ifoInfo;
    }

    public Double getIfoEstate() {
        return ifoEstate;
    }

    public Double getIfoOther() {
        return ifoOther;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setVrpAgro(Double vrpAgro) {
        this.vrpAgro = vrpAgro;
    }

    public void setVrpMining(Double vrpMining) {
        this.vrpMining = vrpMining;
    }

    public void setVrpMan(Double vrpMan) {
        this.vrpMan = vrpMan;
    }

    public void setVrpEnergo(Double vrpEnergo) {
        this.vrpEnergo = vrpEnergo;
    }

    public void setVrpWater(Double vrpWater) {
        this.vrpWater = vrpWater;
    }

    public void setVrpBuilding(Double vrpBuilding) {
        this.vrpBuilding = vrpBuilding;
    }

    public void setVrpRetail(Double vrpRetail) {
        this.vrpRetail = vrpRetail;
    }

    public void setVrpTrade(Double vrpTrade) {
        this.vrpTrade = vrpTrade;
    }

    public void setVrpTransp(Double vrpTransp) {
        this.vrpTransp = vrpTransp;
    }

    public void setVrpInfo(Double vrpInfo) {
        this.vrpInfo = vrpInfo;
    }

    public void setVrpEstate(Double vrpEstate) {
        this.vrpEstate = vrpEstate;
    }

    public void setVrpOther(Double vrpOther) {
        this.vrpOther = vrpOther;
    }

    public void setVrpTax(Double vrpTax) {
        this.vrpTax = vrpTax;
    }

    public void setIfo(Double ifo) {
        this.ifo = ifo;
    }

    public void setIfoAgro(Double ifoAgro) {
        this.ifoAgro = ifoAgro;
    }

    public void setIfoInd(Double ifoInd) {
        this.ifoInd = ifoInd;
    }

    public void setIfoMining(Double ifoMining) {
        this.ifoMining = ifoMining;
    }

    public void setIfoMan(Double ifoMan) {
        this.ifoMan = ifoMan;
    }

    public void setIfoEnergo(Double ifoEnergo) {
        this.ifoEnergo = ifoEnergo;
    }

    public void setIfoWater(Double ifoWater) {
        this.ifoWater = ifoWater;
    }

    public void setIfoBuilding(Double ifoBuilding) {
        this.ifoBuilding = ifoBuilding;
    }

    public void setIfoAllTrade(Double ifoAllTrade) {
        this.ifoAllTrade = ifoAllTrade;
    }

    public void setIfoTransp(Double ifoTransp) {
        this.ifoTransp = ifoTransp;
    }

    public void setIfoInfo(Double ifoInfo) {
        this.ifoInfo = ifoInfo;
    }

    public void setIfoEstate(Double ifoEstate) {
        this.ifoEstate = ifoEstate;
    }

    public void setIfoOther(Double ifoOther) {
        this.ifoOther = ifoOther;
    }
}
