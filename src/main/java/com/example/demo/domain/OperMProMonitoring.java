package com.example.demo.domain;

public class OperMProMonitoring {
    //oper_mon_proishestvie

    private String month;
    private String dateEvent;
    private String period;
    private String region;
    private Integer totalEmergency;
    private String unitA;
    private Integer lostA;
    private String unitB;
    private Integer hospitalizedA;
    private String unitC;
    private Integer evacuatedA;
    private String unitD;
    private Integer rescuedA;
    private String unitE;
    private Integer firesA;
    private String unitF;
    private Integer totalEmergencyB;
    private String unitAA;
    private Integer lostB;
    private String unitBB;
    private Integer hospitalizedB;
    private String unitCC;
    private Integer evacuatedB;
    private String unitDD;
    private Integer rescuedB;
    private String unitEE;
    private Integer firesB;
    private String unitFF;

    public String getMonth() {
        return month;
    }

    public String getDateEvent() {
        return dateEvent;
    }

    public String getPeriod() {
        return period;
    }

    public String getRegion() {
        return region;
    }

    public Integer getTotalEmergency() {
        return totalEmergency;
    }


    public Integer getLostA() {
        return lostA;
    }

    public String getUnitB() {
        return unitB;
    }

    public Integer getHospitalizedA() {
        return hospitalizedA;
    }

    public String getUnitC() {
        return unitC;
    }

    public Integer getEvacuatedA() {
        return evacuatedA;
    }

    public String getUnitD() {
        return unitD;
    }

    public Integer getRescuedA() {
        return rescuedA;
    }

    public String getUnitE() {
        return unitE;
    }

    public Integer getFiresA() {
        return firesA;
    }

    public String getUnitF() {
        return unitF;
    }

    public Integer getTotalEmergencyB() {
        return totalEmergencyB;
    }

    public String getUnitAA() {
        return unitAA;
    }

    public Integer getLostB() {
        return lostB;
    }

    public String getUnitBB() {
        return unitBB;
    }

    public Integer getHospitalizedB() {
        return hospitalizedB;
    }

    public String getUnitCC() {
        return unitCC;
    }

    public Integer getEvacuatedB() {
        return evacuatedB;
    }

    public String getUnitDD() {
        return unitDD;
    }

    public Integer getRescuedB() {
        return rescuedB;
    }

    public String getUnitEE() {
        return unitEE;
    }

    public Integer getFiresB() {
        return firesB;
    }

    public String getUnitFF() {
        return unitFF;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public void setDateEvent(String dateEvent) {
        this.dateEvent = dateEvent;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setTotalEmergency(Integer totalEmergency) {
        this.totalEmergency = totalEmergency;
    }


    public void setLostA(Integer lostA) {
        this.lostA = lostA;
    }

    public void setUnitB(String unitB) {
        this.unitB = unitB;
    }

    public void setHospitalizedA(Integer hospitalizedA) {
        this.hospitalizedA = hospitalizedA;
    }

    public void setUnitC(String unitC) {
        this.unitC = unitC;
    }

    public void setEvacuatedA(Integer evacuatedA) {
        this.evacuatedA = evacuatedA;
    }

    public void setUnitD(String unitD) {
        this.unitD = unitD;
    }

    public void setRescuedA(Integer rescuedA) {
        this.rescuedA = rescuedA;
    }

    public void setUnitE(String unitE) {
        this.unitE = unitE;
    }

    public void setFiresA(Integer firesA) {
        this.firesA = firesA;
    }

    public void setUnitF(String unitF) {
        this.unitF = unitF;
    }

    public void setTotalEmergencyB(Integer totalEmergencyB) {
        this.totalEmergencyB = totalEmergencyB;
    }

    public void setUnitAA(String unitAA) {
        this.unitAA = unitAA;
    }

    public void setLostB(Integer lostB) {
        this.lostB = lostB;
    }

    public void setUnitBB(String unitBB) {
        this.unitBB = unitBB;
    }

    public void setHospitalizedB(Integer hospitalizedB) {
        this.hospitalizedB = hospitalizedB;
    }

    public void setUnitCC(String unitCC) {
        this.unitCC = unitCC;
    }

    public void setEvacuatedB(Integer evacuatedB) {
        this.evacuatedB = evacuatedB;
    }

    public void setUnitDD(String unitDD) {
        this.unitDD = unitDD;
    }

    public void setRescuedB(Integer rescuedB) {
        this.rescuedB = rescuedB;
    }

    public void setUnitEE(String unitEE) {
        this.unitEE = unitEE;
    }

    public void setFiresB(Integer firesB) {
        this.firesB = firesB;
    }

    public void setUnitFF(String unitFF) {
        this.unitFF = unitFF;
    }

    public String getUnitA() {
        return unitA;
    }

    public void setUnitA(String unitA) {
        this.unitA = unitA;
    }
}
