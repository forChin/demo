package com.example.demo.domain;

public class DiplomSnpMonitoring {
    // snp bd

    private String dname;
    private String dregion;
    private String draion;
    private String druralDistrict;
    private String dlatitude;
    private String dlongitude;

    public String getDname() {
        return dname;
    }

    public String getDregion() {
        return dregion;
    }

    public String getDraion() {
        return draion;
    }

    public String getDruralDistrict() {
        return druralDistrict;
    }

    public String getDlatitude() {
        return dlatitude;
    }

    public String getDlongitude() {
        return dlongitude;
    }

    public void setDname(String dname) {
        this.dname = dname;
    }

    public void setDregion(String dregion) {
        this.dregion = dregion;
    }

    public void setDraion(String draion) {
        this.draion = draion;
    }

    public void setDruralDistrict(String druralDistrict) {
        this.druralDistrict = druralDistrict;
    }

    public void setDlatitude(String dlatitude) {
        this.dlatitude = dlatitude;
    }

    public void setDlongitude(String dlongitude) {
        this.dlongitude = dlongitude;
    }
}
