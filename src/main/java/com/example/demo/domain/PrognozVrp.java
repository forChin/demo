package com.example.demo.domain;

public class PrognozVrp {

    private String year; //o_year
    private String script;
    private Double oilPrice;
    private Double inflation;
    private Double exchangeRate;
    private Double population;
    private Double populationAtyrau;
    private Double populationZhylyoi;
    private Double populationInder;
    private Double populationIsatai;
    private Double populationKurmangazy;
    private Double populationKzylkoga;
    private Double populationMakat;
    private Double populationMakhambet;
    private Double oil;
    private Double oilAtyrau;
    private Double oilZhylyoi;
    private Double oilInder;
    private Double oilIsatai;
    private Double oilKurmangazy;
    private Double oilKzylkoga;
    private Double oilMakat;
    private Double oilMakhambet;
    private Double grain;
    private Double grainAtyrau;
    private Double grainZhylyoi;
    private Double grainInder;
    private Double grainIsatai;
    private Double grainKurmangazy;
    private Double grainKzylkoga;
    private Double grainMakat;
    private Double grainMakhambet;
    private Double potato;
    private Double potatoAtyrau;
    private Double potatoZhylyoi;
    private Double potatoInder;
    private Double potatoIsatai;
    private Double potatoKurmangazy;
    private Double potatoKzylkoga;
    private Double potatoMakat;
    private Double potatoMakhambet;
    private Double vegetables;
    private Double vegetablesAtyrau;
    private Double vegetablesZhylyoi;
    private Double vegetablesInder;
    private Double vegetablesIsatai;
    private Double vegetablesKurmangazy;
    private Double vegetablesKzylkoga;
    private Double vegetablesMakat;
    private Double vegetablesMakhambet;
    private Double cattle;
    private Double cattleAtyrau;
    private Double cattleZhylyoi;
    private Double cattleInder;
    private Double cattleIsatai;
    private Double cattleKurmangazy;
    private Double cattleKzylkoga;
    private Double cattleMakat;
    private Double cattleMakhambet;
    private Double goats;
    private Double goatsAtyrau;
    private Double goatsZhylyoi;
    private Double goatsInder;
    private Double goatsIsatai;
    private Double goatsKurmangazy;
    private Double goatsKzylkoga;
    private Double goatsMakat;
    private Double goatsMakhambet;
    private Double horses;
    private Double horsesAtyrau;
    private Double horsesZhylyoi;
    private Double horsesInder;
    private Double horsesIsatai;
    private Double horsesKurmangazy;
    private Double horsesKzylkoga;
    private Double horsesMakat;
    private Double horsesMakhambet;
    private Double poultry;
    private Double poultryAtyrau;
    private Double poultryZhylyoi;
    private Double poultryInder;
    private Double poultryIsatai;
    private Double poultryKurmangazy;
    private Double poultryKzylkoga;
    private Double poultryMakat;
    private Double poultryMakhambet;
    private Double weightCattle;
    private Double weightGoats;
    private Double weightHorses;
    private Double weightPoultry;
    private Double housing;
    private Double housingAtyrau;
    private Double housingZhylyoi;
    private Double housingInder;
    private Double housingIsatai;
    private Double housingKurmangazy;
    private Double housingKzylkoga;
    private Double housingMakat;
    private Double housingMakhambet;
    private Double buildingInvest;


    public String getYear() {
        return year;
    }

    public String getScript() {
        return script;
    }

    public Double getOilPrice() {
        return oilPrice;
    }

    public Double getInflation() {
        return inflation;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public void setOilPrice(Double oilPrice) {
        this.oilPrice = oilPrice;
    }

    public void setInflation(Double inflation) {
        this.inflation = inflation;
    }

    public void setExchangeRate(Double exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public void setPopulation(Double population) {
        this.population = population;
    }

    public void setPopulationAtyrau(Double populationAtyrau) {
        this.populationAtyrau = populationAtyrau;
    }

    public void setPopulationZhylyoi(Double populationZhylyoi) {
        this.populationZhylyoi = populationZhylyoi;
    }

    public void setPopulationInder(Double populationInder) {
        this.populationInder = populationInder;
    }

    public void setPopulationIsatai(Double populationIsatai) {
        this.populationIsatai = populationIsatai;
    }

    public void setPopulationKurmangazy(Double populationKurmangazy) {
        this.populationKurmangazy = populationKurmangazy;
    }

    public void setPopulationKzylkoga(Double populationKzylkoga) {
        this.populationKzylkoga = populationKzylkoga;
    }

    public void setPopulationMakat(Double populationMakat) {
        this.populationMakat = populationMakat;
    }

    public void setPopulationMakhambet(Double populationMakhambet) {
        this.populationMakhambet = populationMakhambet;
    }

    public void setOil(Double oil) {
        this.oil = oil;
    }

    public void setOilAtyrau(Double oilAtyrau) {
        this.oilAtyrau = oilAtyrau;
    }

    public void setOilZhylyoi(Double oilZhylyoi) {
        this.oilZhylyoi = oilZhylyoi;
    }

    public void setOilInder(Double oilInder) {
        this.oilInder = oilInder;
    }

    public void setOilIsatai(Double oilIsatai) {
        this.oilIsatai = oilIsatai;
    }

    public void setOilKurmangazy(Double oilKurmangazy) {
        this.oilKurmangazy = oilKurmangazy;
    }

    public void setOilKzylkoga(Double oilKzylkoga) {
        this.oilKzylkoga = oilKzylkoga;
    }

    public void setOilMakat(Double oilMakat) {
        this.oilMakat = oilMakat;
    }

    public void setOilMakhambet(Double oilMakhambet) {
        this.oilMakhambet = oilMakhambet;
    }

    public void setGrain(Double grain) {
        this.grain = grain;
    }

    public void setGrainAtyrau(Double grainAtyrau) {
        this.grainAtyrau = grainAtyrau;
    }

    public void setGrainZhylyoi(Double grainZhylyoi) {
        this.grainZhylyoi = grainZhylyoi;
    }

    public void setGrainInder(Double grainInder) {
        this.grainInder = grainInder;
    }

    public void setGrainIsatai(Double grainIsatai) {
        this.grainIsatai = grainIsatai;
    }

    public void setGrainKurmangazy(Double grainKurmangazy) {
        this.grainKurmangazy = grainKurmangazy;
    }

    public void setGrainKzylkoga(Double grainKzylkoga) {
        this.grainKzylkoga = grainKzylkoga;
    }

    public void setGrainMakat(Double grainMakat) {
        this.grainMakat = grainMakat;
    }

    public void setGrainMakhambet(Double grainMakhambet) {
        this.grainMakhambet = grainMakhambet;
    }

    public void setPotato(Double potato) {
        this.potato = potato;
    }

    public void setPotatoAtyrau(Double potatoAtyrau) {
        this.potatoAtyrau = potatoAtyrau;
    }

    public void setPotatoZhylyoi(Double potatoZhylyoi) {
        this.potatoZhylyoi = potatoZhylyoi;
    }

    public void setPotatoInder(Double potatoInder) {
        this.potatoInder = potatoInder;
    }

    public void setPotatoIsatai(Double potatoIsatai) {
        this.potatoIsatai = potatoIsatai;
    }

    public void setPotatoKurmangazy(Double potatoKurmangazy) {
        this.potatoKurmangazy = potatoKurmangazy;
    }

    public void setPotatoKzylkoga(Double potatoKzylkoga) {
        this.potatoKzylkoga = potatoKzylkoga;
    }

    public void setPotatoMakat(Double potatoMakat) {
        this.potatoMakat = potatoMakat;
    }

    public void setPotatoMakhambet(Double potatoMakhambet) {
        this.potatoMakhambet = potatoMakhambet;
    }

    public void setVegetables(Double vegetables) {
        this.vegetables = vegetables;
    }

    public void setVegetablesAtyrau(Double vegetablesAtyrau) {
        this.vegetablesAtyrau = vegetablesAtyrau;
    }

    public void setVegetablesZhylyoi(Double vegetablesZhylyoi) {
        this.vegetablesZhylyoi = vegetablesZhylyoi;
    }

    public void setVegetablesInder(Double vegetablesInder) {
        this.vegetablesInder = vegetablesInder;
    }

    public void setVegetablesIsatai(Double vegetablesIsatai) {
        this.vegetablesIsatai = vegetablesIsatai;
    }

    public void setVegetablesKurmangazy(Double vegetablesKurmangazy) {
        this.vegetablesKurmangazy = vegetablesKurmangazy;
    }

    public void setVegetablesKzylkoga(Double vegetablesKzylkoga) {
        this.vegetablesKzylkoga = vegetablesKzylkoga;
    }

    public void setVegetablesMakat(Double vegetablesMakat) {
        this.vegetablesMakat = vegetablesMakat;
    }

    public void setVegetablesMakhambet(Double vegetablesMakhambet) {
        this.vegetablesMakhambet = vegetablesMakhambet;
    }

    public void setCattle(Double cattle) {
        this.cattle = cattle;
    }

    public void setCattleAtyrau(Double cattleAtyrau) {
        this.cattleAtyrau = cattleAtyrau;
    }

    public void setCattleZhylyoi(Double cattleZhylyoi) {
        this.cattleZhylyoi = cattleZhylyoi;
    }

    public void setCattleInder(Double cattleInder) {
        this.cattleInder = cattleInder;
    }

    public void setCattleIsatai(Double cattleIsatai) {
        this.cattleIsatai = cattleIsatai;
    }

    public void setCattleKurmangazy(Double cattleKurmangazy) {
        this.cattleKurmangazy = cattleKurmangazy;
    }

    public void setCattleKzylkoga(Double cattleKzylkoga) {
        this.cattleKzylkoga = cattleKzylkoga;
    }

    public void setCattleMakat(Double cattleMakat) {
        this.cattleMakat = cattleMakat;
    }

    public void setCattleMakhambet(Double cattleMakhambet) {
        this.cattleMakhambet = cattleMakhambet;
    }

    public void setGoats(Double goats) {
        this.goats = goats;
    }

    public void setGoatsAtyrau(Double goatsAtyrau) {
        this.goatsAtyrau = goatsAtyrau;
    }

    public void setGoatsZhylyoi(Double goatsZhylyoi) {
        this.goatsZhylyoi = goatsZhylyoi;
    }

    public void setGoatsInder(Double goatsInder) {
        this.goatsInder = goatsInder;
    }

    public void setGoatsIsatai(Double goatsIsatai) {
        this.goatsIsatai = goatsIsatai;
    }

    public void setGoatsKurmangazy(Double goatsKurmangazy) {
        this.goatsKurmangazy = goatsKurmangazy;
    }

    public void setGoatsKzylkoga(Double goatsKzylkoga) {
        this.goatsKzylkoga = goatsKzylkoga;
    }

    public void setGoatsMakat(Double goatsMakat) {
        this.goatsMakat = goatsMakat;
    }

    public void setGoatsMakhambet(Double goatsMakhambet) {
        this.goatsMakhambet = goatsMakhambet;
    }

    public void setHorses(Double horses) {
        this.horses = horses;
    }

    public void setHorsesAtyrau(Double horsesAtyrau) {
        this.horsesAtyrau = horsesAtyrau;
    }

    public void setHorsesZhylyoi(Double horsesZhylyoi) {
        this.horsesZhylyoi = horsesZhylyoi;
    }

    public void setHorsesInder(Double horsesInder) {
        this.horsesInder = horsesInder;
    }

    public void setHorsesIsatai(Double horsesIsatai) {
        this.horsesIsatai = horsesIsatai;
    }

    public void setHorsesKurmangazy(Double horsesKurmangazy) {
        this.horsesKurmangazy = horsesKurmangazy;
    }

    public void setHorsesKzylkoga(Double horsesKzylkoga) {
        this.horsesKzylkoga = horsesKzylkoga;
    }

    public void setHorsesMakat(Double horsesMakat) {
        this.horsesMakat = horsesMakat;
    }

    public void setHorsesMakhambet(Double horsesMakhambet) {
        this.horsesMakhambet = horsesMakhambet;
    }

    public void setPoultry(Double poultry) {
        this.poultry = poultry;
    }

    public void setPoultryAtyrau(Double poultryAtyrau) {
        this.poultryAtyrau = poultryAtyrau;
    }

    public void setPoultryZhylyoi(Double poultryZhylyoi) {
        this.poultryZhylyoi = poultryZhylyoi;
    }

    public void setPoultryInder(Double poultryInder) {
        this.poultryInder = poultryInder;
    }

    public void setPoultryIsatai(Double poultryIsatai) {
        this.poultryIsatai = poultryIsatai;
    }

    public void setPoultryKurmangazy(Double poultryKurmangazy) {
        this.poultryKurmangazy = poultryKurmangazy;
    }

    public void setPoultryKzylkoga(Double poultryKzylkoga) {
        this.poultryKzylkoga = poultryKzylkoga;
    }

    public void setPoultryMakat(Double poultryMakat) {
        this.poultryMakat = poultryMakat;
    }

    public void setPoultryMakhambet(Double poultryMakhambet) {
        this.poultryMakhambet = poultryMakhambet;
    }

    public void setWeightCattle(Double weightCattle) {
        this.weightCattle = weightCattle;
    }

    public void setWeightGoats(Double weightGoats) {
        this.weightGoats = weightGoats;
    }

    public void setWeightHorses(Double weightHorses) {
        this.weightHorses = weightHorses;
    }

    public void setWeightPoultry(Double weightPoultry) {
        this.weightPoultry = weightPoultry;
    }

    public void setHousing(Double housing) {
        this.housing = housing;
    }

    public void setHousingAtyrau(Double housingAtyrau) {
        this.housingAtyrau = housingAtyrau;
    }

    public void setHousingZhylyoi(Double housingZhylyoi) {
        this.housingZhylyoi = housingZhylyoi;
    }

    public void setHousingInder(Double housingInder) {
        this.housingInder = housingInder;
    }

    public void setHousingIsatai(Double housingIsatai) {
        this.housingIsatai = housingIsatai;
    }

    public void setHousingKurmangazy(Double housingKurmangazy) {
        this.housingKurmangazy = housingKurmangazy;
    }

    public void setHousingKzylkoga(Double housingKzylkoga) {
        this.housingKzylkoga = housingKzylkoga;
    }

    public void setHousingMakat(Double housingMakat) {
        this.housingMakat = housingMakat;
    }

    public void setHousingMakhambet(Double housingMakhambet) {
        this.housingMakhambet = housingMakhambet;
    }

    public void setBuildingInvest(Double buildingInvest) {
        this.buildingInvest = buildingInvest;
    }

    public Double getExchangeRate() {
        return exchangeRate;
    }

    public Double getPopulation() {
        return population;
    }

    public Double getPopulationAtyrau() {
        return populationAtyrau;
    }

    public Double getPopulationZhylyoi() {
        return populationZhylyoi;
    }

    public Double getPopulationInder() {
        return populationInder;
    }

    public Double getPopulationIsatai() {
        return populationIsatai;
    }

    public Double getPopulationKurmangazy() {
        return populationKurmangazy;
    }

    public Double getPopulationKzylkoga() {
        return populationKzylkoga;
    }

    public Double getPopulationMakat() {
        return populationMakat;
    }

    public Double getPopulationMakhambet() {
        return populationMakhambet;
    }

    public Double getOil() {
        return oil;
    }

    public Double getOilAtyrau() {
        return oilAtyrau;
    }

    public Double getOilZhylyoi() {
        return oilZhylyoi;
    }

    public Double getOilInder() {
        return oilInder;
    }

    public Double getOilIsatai() {
        return oilIsatai;
    }

    public Double getOilKurmangazy() {
        return oilKurmangazy;
    }

    public Double getOilKzylkoga() {
        return oilKzylkoga;
    }

    public Double getOilMakat() {
        return oilMakat;
    }

    public Double getOilMakhambet() {
        return oilMakhambet;
    }

    public Double getGrain() {
        return grain;
    }

    public Double getGrainAtyrau() {
        return grainAtyrau;
    }

    public Double getGrainZhylyoi() {
        return grainZhylyoi;
    }

    public Double getGrainInder() {
        return grainInder;
    }

    public Double getGrainIsatai() {
        return grainIsatai;
    }

    public Double getGrainKurmangazy() {
        return grainKurmangazy;
    }

    public Double getGrainKzylkoga() {
        return grainKzylkoga;
    }

    public Double getGrainMakat() {
        return grainMakat;
    }

    public Double getGrainMakhambet() {
        return grainMakhambet;
    }

    public Double getPotato() {
        return potato;
    }

    public Double getPotatoAtyrau() {
        return potatoAtyrau;
    }

    public Double getPotatoZhylyoi() {
        return potatoZhylyoi;
    }

    public Double getPotatoInder() {
        return potatoInder;
    }

    public Double getPotatoIsatai() {
        return potatoIsatai;
    }

    public Double getPotatoKurmangazy() {
        return potatoKurmangazy;
    }

    public Double getPotatoKzylkoga() {
        return potatoKzylkoga;
    }

    public Double getPotatoMakat() {
        return potatoMakat;
    }

    public Double getPotatoMakhambet() {
        return potatoMakhambet;
    }

    public Double getVegetables() {
        return vegetables;
    }

    public Double getVegetablesAtyrau() {
        return vegetablesAtyrau;
    }

    public Double getVegetablesZhylyoi() {
        return vegetablesZhylyoi;
    }

    public Double getVegetablesInder() {
        return vegetablesInder;
    }

    public Double getVegetablesIsatai() {
        return vegetablesIsatai;
    }

    public Double getVegetablesKurmangazy() {
        return vegetablesKurmangazy;
    }

    public Double getVegetablesKzylkoga() {
        return vegetablesKzylkoga;
    }

    public Double getVegetablesMakat() {
        return vegetablesMakat;
    }

    public Double getVegetablesMakhambet() {
        return vegetablesMakhambet;
    }

    public Double getCattle() {
        return cattle;
    }

    public Double getCattleAtyrau() {
        return cattleAtyrau;
    }

    public Double getCattleZhylyoi() {
        return cattleZhylyoi;
    }

    public Double getCattleInder() {
        return cattleInder;
    }

    public Double getCattleIsatai() {
        return cattleIsatai;
    }

    public Double getCattleKurmangazy() {
        return cattleKurmangazy;
    }

    public Double getCattleKzylkoga() {
        return cattleKzylkoga;
    }

    public Double getCattleMakat() {
        return cattleMakat;
    }

    public Double getCattleMakhambet() {
        return cattleMakhambet;
    }

    public Double getGoats() {
        return goats;
    }

    public Double getGoatsAtyrau() {
        return goatsAtyrau;
    }

    public Double getGoatsZhylyoi() {
        return goatsZhylyoi;
    }

    public Double getGoatsInder() {
        return goatsInder;
    }

    public Double getGoatsIsatai() {
        return goatsIsatai;
    }

    public Double getGoatsKurmangazy() {
        return goatsKurmangazy;
    }

    public Double getGoatsKzylkoga() {
        return goatsKzylkoga;
    }

    public Double getGoatsMakat() {
        return goatsMakat;
    }

    public Double getGoatsMakhambet() {
        return goatsMakhambet;
    }

    public Double getHorses() {
        return horses;
    }

    public Double getHorsesAtyrau() {
        return horsesAtyrau;
    }

    public Double getHorsesZhylyoi() {
        return horsesZhylyoi;
    }

    public Double getHorsesInder() {
        return horsesInder;
    }

    public Double getHorsesIsatai() {
        return horsesIsatai;
    }

    public Double getHorsesKurmangazy() {
        return horsesKurmangazy;
    }

    public Double getHorsesKzylkoga() {
        return horsesKzylkoga;
    }

    public Double getHorsesMakat() {
        return horsesMakat;
    }

    public Double getHorsesMakhambet() {
        return horsesMakhambet;
    }

    public Double getPoultry() {
        return poultry;
    }

    public Double getPoultryAtyrau() {
        return poultryAtyrau;
    }

    public Double getPoultryZhylyoi() {
        return poultryZhylyoi;
    }

    public Double getPoultryInder() {
        return poultryInder;
    }

    public Double getPoultryIsatai() {
        return poultryIsatai;
    }

    public Double getPoultryKurmangazy() {
        return poultryKurmangazy;
    }

    public Double getPoultryKzylkoga() {
        return poultryKzylkoga;
    }

    public Double getPoultryMakat() {
        return poultryMakat;
    }

    public Double getPoultryMakhambet() {
        return poultryMakhambet;
    }

    public Double getWeightCattle() {
        return weightCattle;
    }

    public Double getWeightGoats() {
        return weightGoats;
    }

    public Double getWeightHorses() {
        return weightHorses;
    }

    public Double getWeightPoultry() {
        return weightPoultry;
    }

    public Double getHousing() {
        return housing;
    }

    public Double getHousingAtyrau() {
        return housingAtyrau;
    }

    public Double getHousingZhylyoi() {
        return housingZhylyoi;
    }

    public Double getHousingInder() {
        return housingInder;
    }

    public Double getHousingIsatai() {
        return housingIsatai;
    }

    public Double getHousingKurmangazy() {
        return housingKurmangazy;
    }

    public Double getHousingKzylkoga() {
        return housingKzylkoga;
    }

    public Double getHousingMakat() {
        return housingMakat;
    }

    public Double getHousingMakhambet() {
        return housingMakhambet;
    }

    public Double getBuildingInvest() {
        return buildingInvest;
    }
}
