package com.example.demo.domain;

public class MenuRybAqva2Monitoring {
    private String year;        //ush_stat_ryb_i_aqva_2
    private String area;
    private String month;
    private Double allocatedQuota;
    private Double developmentQuota;
    private Double riverKigach;
    private Double riverUral;
    private Double caspianSea;
    private Double total;
    private Double uralAtyrauHatchery;
    private Double uralAtyrauUterine;
    private Double atyrauHatchery;
    private Double atyrauUterine;
    private String note;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public Double getAllocatedQuota() {
        return allocatedQuota;
    }

    public void setAllocatedQuota(Double allocatedQuota) {
        this.allocatedQuota = allocatedQuota;
    }

    public Double getDevelopmentQuota() {
        return developmentQuota;
    }

    public void setDevelopmentQuota(Double developmentQuota) {
        this.developmentQuota = developmentQuota;
    }

    public Double getRiverKigach() {
        return riverKigach;
    }

    public void setRiverKigach(Double riverKigach) {
        this.riverKigach = riverKigach;
    }

    public Double getRiverUral() {
        return riverUral;
    }

    public void setRiverUral(Double riverUral) {
        this.riverUral = riverUral;
    }

    public Double getCaspianSea() {
        return caspianSea;
    }

    public void setCaspianSea(Double caspianSea) {
        this.caspianSea = caspianSea;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getUralAtyrauHatchery() {
        return uralAtyrauHatchery;
    }

    public void setUralAtyrauHatchery(Double uralAtyrauHatchery) {
        this.uralAtyrauHatchery = uralAtyrauHatchery;
    }

    public Double getUralAtyrauUterine() {
        return uralAtyrauUterine;
    }

    public void setUralAtyrauUterine(Double uralAtyrauUterine) {
        this.uralAtyrauUterine = uralAtyrauUterine;
    }

    public Double getAtyrauHatchery() {
        return atyrauHatchery;
    }

    public void setAtyrauHatchery(Double atyrauHatchery) {
        this.atyrauHatchery = atyrauHatchery;
    }

    public Double getAtyrauUterine() {
        return atyrauUterine;
    }

    public void setAtyrauUterine(Double atyrauUterine) {
        this.atyrauUterine = atyrauUterine;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
