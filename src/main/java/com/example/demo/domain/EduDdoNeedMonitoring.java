package com.example.demo.domain;

public class EduDdoNeedMonitoring {

    // new edu ddo need

    private String year;
    private String raion;
    private String ruralDitrict;
    private String snp;
    private String ddoName;
    private String candidate;
    private Double count;
    private String languageInstruction;

    public String getYear() {
        return year;
    }

    public String getRaion() {
        return raion;
    }

    public String getRuralDitrict() {
        return ruralDitrict;
    }

    public String getSnp() {
        return snp;
    }

    public String getDdoName() {
        return ddoName;
    }

    public String getCandidate() {
        return candidate;
    }

    public Double getCount() {
        return count;
    }

    public String getLanguageInstruction() {
        return languageInstruction;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setRaion(String raion) {
        this.raion = raion;
    }

    public void setRuralDitrict(String ruralDitrict) {
        this.ruralDitrict = ruralDitrict;
    }

    public void setSnp(String snp) {
        this.snp = snp;
    }

    public void setDdoName(String ddoName) {
        this.ddoName = ddoName;
    }

    public void setCandidate(String candidate) {
        this.candidate = candidate;
    }

    public void setCount(Double count) {
        this.count = count;
    }

    public void setLanguageInstruction(String languageInstruction) {
        this.languageInstruction = languageInstruction;
    }
}
