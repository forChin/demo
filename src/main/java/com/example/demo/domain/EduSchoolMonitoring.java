package com.example.demo.domain;

public class EduSchoolMonitoring {
    // new edu school

    private String raion;
    private String ruralDistrict;
    private String snp;
    private String year;
    private String nameSchool;
    private Integer countPlace;
    private Integer countStudents;
    private Integer countGraduates;
    private Integer countParticipantsUnt;
    private Integer countTeachers;
    private Integer categoryHigh;
    private Integer category1;
    private Integer category2;
    private Integer categoryNot;
    private Integer w025;
    private Integer w2534;
    private Integer w3544;
    private Integer w4554;
    private Integer w5564;
    private Integer w64up;
    private Integer countEducationHigher;
    private Integer countEducationColleges;
    private Integer generalSecondaryEducation;
    private Integer countGoldMedal;
    private Integer countCertificateDistinction;
    private Integer countUnsatisfactoryEvaluation;
    private Integer countTheLowestRating;
    private String schoolAddress;
    private String typeInstitution;
    private String yearConstruction;
    private String yearKapRenovation;
    private String contactNumber;
    private String workingHours;
    private String workingHoursA;
    private String lastname;
    private String firstname;
    private String otchestvo;

    public String getRaion() {
        return raion;
    }

    public String getRuralDistrict() {
        return ruralDistrict;
    }

    public String getSnp() {
        return snp;
    }

    public String getYear() {
        return year;
    }

    public String getNameSchool() {
        return nameSchool;
    }

    public Integer getCountPlace() {
        return countPlace;
    }

    public Integer getCountStudents() {
        return countStudents;
    }

    public Integer getCountGraduates() {
        return countGraduates;
    }

    public Integer getCountParticipantsUnt() {
        return countParticipantsUnt;
    }

    public Integer getCountTeachers() {
        return countTeachers;
    }

    public Integer getCategoryHigh() {
        return categoryHigh;
    }

    public Integer getCategory1() {
        return category1;
    }

    public Integer getCategory2() {
        return category2;
    }

    public Integer getCategoryNot() {
        return categoryNot;
    }

    public Integer getW025() {
        return w025;
    }

    public Integer getW2534() {
        return w2534;
    }

    public Integer getW3544() {
        return w3544;
    }

    public Integer getW4554() {
        return w4554;
    }

    public Integer getW5564() {
        return w5564;
    }

    public Integer getW64up() {
        return w64up;
    }

    public Integer getCountEducationHigher() {
        return countEducationHigher;
    }

    public Integer getCountEducationColleges() {
        return countEducationColleges;
    }

    public Integer getGeneralSecondaryEducation() {
        return generalSecondaryEducation;
    }

    public Integer getCountGoldMedal() {
        return countGoldMedal;
    }

    public Integer getCountCertificateDistinction() {
        return countCertificateDistinction;
    }

    public Integer getCountUnsatisfactoryEvaluation() {
        return countUnsatisfactoryEvaluation;
    }

    public Integer getCountTheLowestRating() {
        return countTheLowestRating;
    }

    public String getSchoolAddress() {
        return schoolAddress;
    }

    public String getTypeInstitution() {
        return typeInstitution;
    }

    public String getYearConstruction() {
        return yearConstruction;
    }

    public String getYearKapRenovation() {
        return yearKapRenovation;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public String getWorkingHours() {
        return workingHours;
    }

    public String getWorkingHoursA() {
        return workingHoursA;
    }

    public String getLastname() {
        return lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getOtchestvo() {
        return otchestvo;
    }

    public void setRaion(String raion) {
        this.raion = raion;
    }

    public void setRuralDistrict(String ruralDistrict) {
        this.ruralDistrict = ruralDistrict;
    }

    public void setSnp(String snp) {
        this.snp = snp;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setNameSchool(String nameSchool) {
        this.nameSchool = nameSchool;
    }

    public void setCountPlace(Integer countPlace) {
        this.countPlace = countPlace;
    }

    public void setCountStudents(Integer countStudents) {
        this.countStudents = countStudents;
    }

    public void setCountGraduates(Integer countGraduates) {
        this.countGraduates = countGraduates;
    }

    public void setCountParticipantsUnt(Integer countParticipantsUnt) {
        this.countParticipantsUnt = countParticipantsUnt;
    }

    public void setCountTeachers(Integer countTeachers) {
        this.countTeachers = countTeachers;
    }

    public void setCategoryHigh(Integer categoryHigh) {
        this.categoryHigh = categoryHigh;
    }

    public void setCategory1(Integer category1) {
        this.category1 = category1;
    }

    public void setCategory2(Integer category2) {
        this.category2 = category2;
    }

    public void setCategoryNot(Integer categoryNot) {
        this.categoryNot = categoryNot;
    }

    public void setW025(Integer w025) {
        this.w025 = w025;
    }

    public void setW2534(Integer w2534) {
        this.w2534 = w2534;
    }

    public void setW3544(Integer w3544) {
        this.w3544 = w3544;
    }

    public void setW4554(Integer w4554) {
        this.w4554 = w4554;
    }

    public void setW5564(Integer w5564) {
        this.w5564 = w5564;
    }

    public void setW64up(Integer w64up) {
        this.w64up = w64up;
    }

    public void setCountEducationHigher(Integer countEducationHigher) {
        this.countEducationHigher = countEducationHigher;
    }

    public void setCountEducationColleges(Integer countEducationColleges) {
        this.countEducationColleges = countEducationColleges;
    }

    public void setGeneralSecondaryEducation(Integer generalSecondaryEducation) {
        this.generalSecondaryEducation = generalSecondaryEducation;
    }

    public void setCountGoldMedal(Integer countGoldMedal) {
        this.countGoldMedal = countGoldMedal;
    }

    public void setCountCertificateDistinction(Integer countCertificateDistinction) {
        this.countCertificateDistinction = countCertificateDistinction;
    }

    public void setCountUnsatisfactoryEvaluation(Integer countUnsatisfactoryEvaluation) {
        this.countUnsatisfactoryEvaluation = countUnsatisfactoryEvaluation;
    }

    public void setCountTheLowestRating(Integer countTheLowestRating) {
        this.countTheLowestRating = countTheLowestRating;
    }

    public void setSchoolAddress(String schoolAddress) {
        this.schoolAddress = schoolAddress;
    }

    public void setTypeInstitution(String typeInstitution) {
        this.typeInstitution = typeInstitution;
    }

    public void setYearConstruction(String yearConstruction) {
        this.yearConstruction = yearConstruction;
    }

    public void setYearKapRenovation(String yearKapRenovation) {
        this.yearKapRenovation = yearKapRenovation;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public void setWorkingHours(String workingHours) {
        this.workingHours = workingHours;
    }

    public void setWorkingHoursA(String workingHoursA) {
        this.workingHoursA = workingHoursA;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setOtchestvo(String otchestvo) {
        this.otchestvo = otchestvo;
    }
}
