package com.example.demo.domain;

import java.util.HashMap;
import java.util.Map;

public class NatRhyOgeevrMonitoring {

    private String year;

    private Map<String, Double> resultsStateExpertise;
    private Map<String, Double> issuancePermitsEmissions;
    private Map<String, Double> conductingPublicHearings;

    public NatRhyOgeevrMonitoring(){
        this.conductingPublicHearings = new HashMap<>();
        this.issuancePermitsEmissions = new HashMap<>();
        this.resultsStateExpertise = new HashMap<>();
    }

    public String getYear() {
        return year;
    }

    public Map<String, Double> getResultsStateExpertise() {
        return resultsStateExpertise;
    }

    public Map<String, Double> getIssuancePermitsEmissions() {
        return issuancePermitsEmissions;
    }

    public Map<String, Double> getConductingPublicHearings() {
        return conductingPublicHearings;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setResultsStateExpertise(Map<String, Double> resultsStateExpertise) {
        this.resultsStateExpertise = resultsStateExpertise;
    }

    public void setIssuancePermitsEmissions(Map<String, Double> issuancePermitsEmissions) {
        this.issuancePermitsEmissions = issuancePermitsEmissions;
    }

    public void setConductingPublicHearings(Map<String, Double> conductingPublicHearings) {
        this.conductingPublicHearings = conductingPublicHearings;
    }
}
