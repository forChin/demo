package com.example.demo.domain;

public class MenuKoordinationRegionM2 {

    private String year;            //sed_m_koordin_region
    private String area;
    private Double jobFairsHeld;
    private Double employersTookPart;
    private Double unemployed;
    private Double vacanciesSubmitted;
    private Double permanentJobs;
    private String month;

    public String getYear() {
        return year;
    }

    public String getArea() {
        return area;
    }

    public Double getJobFairsHeld() {
        return jobFairsHeld;
    }

    public Double getEmployersTookPart() {
        return employersTookPart;
    }

    public Double getUnemployed() {
        return unemployed;
    }

    public Double getVacanciesSubmitted() {
        return vacanciesSubmitted;
    }

    public Double getPermanentJobs() {
        return permanentJobs;
    }

    public String getMonth() {
        return month;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setJobFairsHeld(Double jobFairsHeld) {
        this.jobFairsHeld = jobFairsHeld;
    }

    public void setEmployersTookPart(Double employersTookPart) {
        this.employersTookPart = employersTookPart;
    }

    public void setUnemployed(Double unemployed) {
        this.unemployed = unemployed;
    }

    public void setVacanciesSubmitted(Double vacanciesSubmitted) {
        this.vacanciesSubmitted = vacanciesSubmitted;
    }

    public void setPermanentJobs(Double permanentJobs) {
        this.permanentJobs = permanentJobs;
    }

    public void setMonth(String month) {
        this.month = month;
    }
}
