package com.example.demo.domain;

public class MenuEnbekNewMonitoring {
    private String year;        //gp_enbek_new
    private String month;
    private String area;
    private String region;
    private Double numberApplicants;
    private Double numberIncluded;
    private Double ppUnemployed;
    private Double ppJobSeekers;
    private Double ppPersonOutsideTipo;
    private Double ppSelfEmployed;
    private Double numberParticipantsOne;
    private Double numberParticipantsTwo;
    private Double numberParticipantsThree;
    private Double traineesPlanTipo;
    private Double completedTipo;
    private Double contractsTipo;
    private Double traineesPlanCratcos;
    private Double directedCratcos;
    private Double completedCratcos;
    private Double employedCratcos;
    private Double contractsCratcos;
    private Double traineesPlanBastaubusness;
    private Double directedBastaubusnessFact;
    private Double completedBastaubusnessFact;
    private Double planLending;
    private Double receivedMicroLoansLending;
    private Double numberMicroloansIssued;
    private Double undergoTrainingOp;
    private Double openOwnBusiness;
    private Double guaranteesMicrooans;
    private Double receivedGuarantees;
    private Double numberGrantRecipients;
    private Double receiveGrant;
    private Double planSrm;
    private Double EmployedSrm;
    private Double contractsSrm;
    private Double completedSrm;
    private Double permanentJobsSrm;
    private Double salarySrm;
    private Double planMp;
    private Double employedMp;
    private Double contractsMp;
    private Double completedMp;
    private Double permanentJobsMp;
    private Double salaryMp;
    private Double planOor;
    private Double employedOor;
    private Double completedOor;
    private Double permanentJobsOor;
    private Double salaryOor;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Double getNumberApplicants() {
        return numberApplicants;
    }

    public void setNumberApplicants(Double numberApplicants) {
        this.numberApplicants = numberApplicants;
    }

    public Double getNumberIncluded() {
        return numberIncluded;
    }

    public void setNumberIncluded(Double numberIncluded) {
        this.numberIncluded = numberIncluded;
    }

    public Double getPpUnemployed() {
        return ppUnemployed;
    }

    public void setPpUnemployed(Double ppUnemployed) {
        this.ppUnemployed = ppUnemployed;
    }

    public Double getPpJobSeekers() {
        return ppJobSeekers;
    }

    public void setPpJobSeekers(Double ppJobSeekers) {
        this.ppJobSeekers = ppJobSeekers;
    }

    public Double getPpPersonOutsideTipo() {
        return ppPersonOutsideTipo;
    }

    public void setPpPersonOutsideTipo(Double ppPersonOutsideTipo) {
        this.ppPersonOutsideTipo = ppPersonOutsideTipo;
    }

    public Double getPpSelfEmployed() {
        return ppSelfEmployed;
    }

    public void setPpSelfEmployed(Double ppSelfEmployed) {
        this.ppSelfEmployed = ppSelfEmployed;
    }

    public Double getNumberParticipantsOne() {
        return numberParticipantsOne;
    }

    public void setNumberParticipantsOne(Double numberParticipantsOne) {
        this.numberParticipantsOne = numberParticipantsOne;
    }

    public Double getNumberParticipantsTwo() {
        return numberParticipantsTwo;
    }

    public void setNumberParticipantsTwo(Double numberParticipantsTwo) {
        this.numberParticipantsTwo = numberParticipantsTwo;
    }

    public Double getNumberParticipantsThree() {
        return numberParticipantsThree;
    }

    public void setNumberParticipantsThree(Double numberParticipantsThree) {
        this.numberParticipantsThree = numberParticipantsThree;
    }

    public Double getTraineesPlanTipo() {
        return traineesPlanTipo;
    }

    public void setTraineesPlanTipo(Double traineesPlanTipo) {
        this.traineesPlanTipo = traineesPlanTipo;
    }

    public Double getCompletedTipo() {
        return completedTipo;
    }

    public void setCompletedTipo(Double completedTipo) {
        this.completedTipo = completedTipo;
    }

    public Double getContractsTipo() {
        return contractsTipo;
    }

    public void setContractsTipo(Double contractsTipo) {
        this.contractsTipo = contractsTipo;
    }

    public Double getTraineesPlanCratcos() {
        return traineesPlanCratcos;
    }

    public void setTraineesPlanCratcos(Double traineesPlanCratcos) {
        this.traineesPlanCratcos = traineesPlanCratcos;
    }

    public Double getDirectedCratcos() {
        return directedCratcos;
    }

    public void setDirectedCratcos(Double directedCratcos) {
        this.directedCratcos = directedCratcos;
    }

    public Double getCompletedCratcos() {
        return completedCratcos;
    }

    public void setCompletedCratcos(Double completedCratcos) {
        this.completedCratcos = completedCratcos;
    }

    public Double getEmployedCratcos() {
        return employedCratcos;
    }

    public void setEmployedCratcos(Double employedCratcos) {
        this.employedCratcos = employedCratcos;
    }

    public Double getContractsCratcos() {
        return contractsCratcos;
    }

    public void setContractsCratcos(Double contractsCratcos) {
        this.contractsCratcos = contractsCratcos;
    }

    public Double getTraineesPlanBastaubusness() {
        return traineesPlanBastaubusness;
    }

    public void setTraineesPlanBastaubusness(Double traineesPlanBastaubusness) {
        this.traineesPlanBastaubusness = traineesPlanBastaubusness;
    }

    public Double getDirectedBastaubusnessFact() {
        return directedBastaubusnessFact;
    }

    public void setDirectedBastaubusnessFact(Double directedBastaubusnessFact) {
        this.directedBastaubusnessFact = directedBastaubusnessFact;
    }

    public Double getCompletedBastaubusnessFact() {
        return completedBastaubusnessFact;
    }

    public void setCompletedBastaubusnessFact(Double completedBastaubusnessFact) {
        this.completedBastaubusnessFact = completedBastaubusnessFact;
    }

    public Double getPlanLending() {
        return planLending;
    }

    public void setPlanLending(Double planLending) {
        this.planLending = planLending;
    }

    public Double getReceivedMicroLoansLending() {
        return receivedMicroLoansLending;
    }

    public void setReceivedMicroLoansLending(Double receivedMicroLoansLending) {
        this.receivedMicroLoansLending = receivedMicroLoansLending;
    }

    public Double getNumberMicroloansIssued() {
        return numberMicroloansIssued;
    }

    public void setNumberMicroloansIssued(Double numberMicroloansIssued) {
        this.numberMicroloansIssued = numberMicroloansIssued;
    }

    public Double getUndergoTrainingOp() {
        return undergoTrainingOp;
    }

    public void setUndergoTrainingOp(Double undergoTrainingOp) {
        this.undergoTrainingOp = undergoTrainingOp;
    }

    public Double getOpenOwnBusiness() {
        return openOwnBusiness;
    }

    public void setOpenOwnBusiness(Double openOwnBusiness) {
        this.openOwnBusiness = openOwnBusiness;
    }

    public Double getGuaranteesMicrooans() {
        return guaranteesMicrooans;
    }

    public void setGuaranteesMicrooans(Double guaranteesMicrooans) {
        this.guaranteesMicrooans = guaranteesMicrooans;
    }

    public Double getReceivedGuarantees() {
        return receivedGuarantees;
    }

    public void setReceivedGuarantees(Double receivedGuarantees) {
        this.receivedGuarantees = receivedGuarantees;
    }

    public Double getNumberGrantRecipients() {
        return numberGrantRecipients;
    }

    public void setNumberGrantRecipients(Double numberGrantRecipients) {
        this.numberGrantRecipients = numberGrantRecipients;
    }

    public Double getReceiveGrant() {
        return receiveGrant;
    }

    public void setReceiveGrant(Double receiveGrant) {
        this.receiveGrant = receiveGrant;
    }

    public Double getPlanSrm() {
        return planSrm;
    }

    public void setPlanSrm(Double planSrm) {
        this.planSrm = planSrm;
    }

    public Double getEmployedSrm() {
        return EmployedSrm;
    }

    public void setEmployedSrm(Double employedSrm) {
        EmployedSrm = employedSrm;
    }

    public Double getContractsSrm() {
        return contractsSrm;
    }

    public void setContractsSrm(Double contractsSrm) {
        this.contractsSrm = contractsSrm;
    }

    public Double getCompletedSrm() {
        return completedSrm;
    }

    public void setCompletedSrm(Double completedSrm) {
        this.completedSrm = completedSrm;
    }

    public Double getPermanentJobsSrm() {
        return permanentJobsSrm;
    }

    public void setPermanentJobsSrm(Double permanentJobsSrm) {
        this.permanentJobsSrm = permanentJobsSrm;
    }

    public Double getSalarySrm() {
        return salarySrm;
    }

    public void setSalarySrm(Double salarySrm) {
        this.salarySrm = salarySrm;
    }

    public Double getPlanMp() {
        return planMp;
    }

    public void setPlanMp(Double planMp) {
        this.planMp = planMp;
    }

    public Double getEmployedMp() {
        return employedMp;
    }

    public void setEmployedMp(Double employedMp) {
        this.employedMp = employedMp;
    }

    public Double getContractsMp() {
        return contractsMp;
    }

    public void setContractsMp(Double contractsMp) {
        this.contractsMp = contractsMp;
    }

    public Double getCompletedMp() {
        return completedMp;
    }

    public void setCompletedMp(Double completedMp) {
        this.completedMp = completedMp;
    }

    public Double getPermanentJobsMp() {
        return permanentJobsMp;
    }

    public void setPermanentJobsMp(Double permanentJobsMp) {
        this.permanentJobsMp = permanentJobsMp;
    }

    public Double getSalaryMp() {
        return salaryMp;
    }

    public void setSalaryMp(Double salaryMp) {
        this.salaryMp = salaryMp;
    }

    public Double getPlanOor() {
        return planOor;
    }

    public void setPlanOor(Double planOor) {
        this.planOor = planOor;
    }

    public Double getEmployedOor() {
        return employedOor;
    }

    public void setEmployedOor(Double employedOor) {
        this.employedOor = employedOor;
    }

    public Double getCompletedOor() {
        return completedOor;
    }

    public void setCompletedOor(Double completedOor) {
        this.completedOor = completedOor;
    }

    public Double getPermanentJobsOor() {
        return permanentJobsOor;
    }

    public void setPermanentJobsOor(Double permanentJobsOor) {
        this.permanentJobsOor = permanentJobsOor;
    }

    public Double getSalaryOor() {
        return salaryOor;
    }

    public void setSalaryOor(Double salaryOor) {
        this.salaryOor = salaryOor;
    }
}
