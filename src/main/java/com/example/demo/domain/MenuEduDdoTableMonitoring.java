package com.example.demo.domain;

public class MenuEduDdoTableMonitoring {
    private String candidat;            //edu_ddo_table
    private Double amount;

    public String getCandidat() {
        return candidat;
    }

    public void setCandidat(String candidat) {
        this.candidat = candidat;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}
