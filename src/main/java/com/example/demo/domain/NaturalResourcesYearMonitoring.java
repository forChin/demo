package com.example.demo.domain;

public class NaturalResourcesYearMonitoring {

    private String year;
    private Double forestPlantingGlf;
    private Double regenerationForests;
    private Double transferForestcultures;
    private Double deviceMineralizedstrips;
    private Double animalWorld;

    public NaturalResourcesYearMonitoring(){

    }

    public String getYear() {
        return year;
    }

    public Double getForestPlantingGlf() {
        return forestPlantingGlf;
    }

    public Double getRegenerationForests() {
        return regenerationForests;
    }

    public Double getTransferForestcultures() {
        return transferForestcultures;
    }

    public Double getDeviceMineralizedstrips() {
        return deviceMineralizedstrips;
    }

    public Double getAnimalWorld() {
        return animalWorld;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setForestPlantingGlf(Double forestPlantingGlf) {
        this.forestPlantingGlf = forestPlantingGlf;
    }

    public void setRegenerationForests(Double regenerationForests) {
        this.regenerationForests = regenerationForests;
    }

    public void setTransferForestcultures(Double transferForestcultures) {
        this.transferForestcultures = transferForestcultures;
    }

    public void setDeviceMineralizedstrips(Double deviceMineralizedstrips) {
        this.deviceMineralizedstrips = deviceMineralizedstrips;
    }

    public void setAnimalWorld(Double animalWorld) {
        this.animalWorld = animalWorld;
    }

}
