package com.example.demo.domain;

public class Architectura2 {

    private String year;
    private String area;
    private Double numberMasterPlansdeveloped;
    private Double areaTerritory;

    public Architectura2(){

    }

    public String getYear() {
        return year;
    }

    public String getArea() {
        return area;
    }

    public Double getNumberMasterPlansdeveloped() {
        return numberMasterPlansdeveloped;
    }

    public Double getAreaTerritory() {
        return areaTerritory;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setNumberMasterPlansdeveloped(Double numberMasterPlansdeveloped) {
        this.numberMasterPlansdeveloped = numberMasterPlansdeveloped;
    }

    public void setAreaTerritory(Double areaTerritory) {
        this.areaTerritory = areaTerritory;
    }
}
