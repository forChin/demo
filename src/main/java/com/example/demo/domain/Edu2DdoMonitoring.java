package com.example.demo.domain;

public class Edu2DdoMonitoring {

    // new edu ddo

    private String year;
    private String region;
    private String ruralDistrict;
    private String snp;
    private String ddoName;
    private Integer amountKids;
    private Integer amountFrames;
    private Integer amountPlace;
    private Integer numberA;
    private Integer numberB;
    private Integer numberC;
    private Integer numberD;
    private Integer numberE;
    private Integer numberF;
    private Integer numberG;
    private Integer numberH;
    private Integer numberI;
    private Integer vuz;
    private Integer ssuz;
    private String adres;
    private String yearContruction;
    private String contractNumber;
    private String workTimeS;
    private String workTimeDo;
    private String surname;
    private String name;
    private String middlName;
    private String typeDdo;
    private String yearKapRenovation;

    public void setYear(String year) {
        this.year = year;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setRuralDistrict(String ruralDistrict) {
        this.ruralDistrict = ruralDistrict;
    }

    public void setSnp(String snp) {
        this.snp = snp;
    }

    public void setDdoName(String ddoName) {
        this.ddoName = ddoName;
    }

    public void setAmountKids(Integer amountKids) {
        this.amountKids = amountKids;
    }

    public void setAmountFrames(Integer amountFrames) {
        this.amountFrames = amountFrames;
    }

    public void setAmountPlace(Integer amountPlace) {
        this.amountPlace = amountPlace;
    }

    public void setNumberA(Integer numberA) {
        this.numberA = numberA;
    }

    public void setNumberB(Integer numberB) {
        this.numberB = numberB;
    }

    public void setNumberC(Integer numberC) {
        this.numberC = numberC;
    }

    public void setNumberD(Integer numberD) {
        this.numberD = numberD;
    }

    public void setNumberE(Integer numberE) {
        this.numberE = numberE;
    }

    public void setNumberF(Integer numberF) {
        this.numberF = numberF;
    }

    public void setNumberG(Integer numberG) {
        this.numberG = numberG;
    }

    public void setNumberH(Integer numberH) {
        this.numberH = numberH;
    }

    public void setNumberI(Integer numberI) {
        this.numberI = numberI;
    }

    public void setVuz(Integer vuz) {
        this.vuz = vuz;
    }

    public void setSsuz(Integer ssuz) {
        this.ssuz = ssuz;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }

    public void setYearContruction(String yearContruction) {
        this.yearContruction = yearContruction;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public void setWorkTimeS(String workTimeS) {
        this.workTimeS = workTimeS;
    }

    public void setWorkTimeDo(String workTimeDo) {
        this.workTimeDo = workTimeDo;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMiddlName(String middlName) {
        this.middlName = middlName;
    }

    public void setTypeDdo(String typeDdo) {
        this.typeDdo = typeDdo;
    }

    public void setYearKapRenovation(String yearKapRenovation) {
        this.yearKapRenovation = yearKapRenovation;
    }

    public String getYear() {
        return year;
    }

    public String getRegion() {
        return region;
    }

    public String getRuralDistrict() {
        return ruralDistrict;
    }

    public String getSnp() {
        return snp;
    }

    public String getDdoName() {
        return ddoName;
    }

    public Integer getAmountKids() {
        return amountKids;
    }

    public Integer getAmountFrames() {
        return amountFrames;
    }

    public Integer getAmountPlace() {
        return amountPlace;
    }

    public Integer getNumberA() {
        return numberA;
    }

    public Integer getNumberB() {
        return numberB;
    }

    public Integer getNumberC() {
        return numberC;
    }

    public Integer getNumberD() {
        return numberD;
    }

    public Integer getNumberE() {
        return numberE;
    }

    public Integer getNumberF() {
        return numberF;
    }

    public Integer getNumberG() {
        return numberG;
    }

    public Integer getNumberH() {
        return numberH;
    }

    public Integer getNumberI() {
        return numberI;
    }

    public Integer getVuz() {
        return vuz;
    }

    public Integer getSsuz() {
        return ssuz;
    }

    public String getAdres() {
        return adres;
    }

    public String getYearContruction() {
        return yearContruction;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public String getWorkTimeS() {
        return workTimeS;
    }

    public String getWorkTimeDo() {
        return workTimeDo;
    }

    public String getSurname() {
        return surname;
    }

    public String getName() {
        return name;
    }

    public String getMiddlName() {
        return middlName;
    }

    public String getTypeDdo() {
        return typeDdo;
    }

    public String getYearKapRenovation() {
        return yearKapRenovation;
    }
}
