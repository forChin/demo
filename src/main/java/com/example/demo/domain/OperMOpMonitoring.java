package com.example.demo.domain;

public class OperMOpMonitoring {
    //oper_mon_oil_price

    private String year;
    private String month;
    private String dateEvent;
    private String period;
    private String area;
    private String unit;
    private Double ai92;
    private Double ai95;
    private Double dieselWinter;
    private Double dieselFuelOffseason;

    public String getYear() {
        return year;
    }

    public String getMonth() {
        return month;
    }

    public String getDateEvent() {
        return dateEvent;
    }

    public String getPeriod() {
        return period;
    }

    public String getArea() {
        return area;
    }

    public String getUnit() {
        return unit;
    }

    public Double getAi92() {
        return ai92;
    }

    public Double getAi95() {
        return ai95;
    }

    public Double getDieselWinter() {
        return dieselWinter;
    }

    public Double getDieselFuelOffseason() {
        return dieselFuelOffseason;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public void setDateEvent(String dateEvent) {
        this.dateEvent = dateEvent;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setAi92(Double ai92) {
        this.ai92 = ai92;
    }

    public void setAi95(Double ai95) {
        this.ai95 = ai95;
    }

    public void setDieselWinter(Double dieselWinter) {
        this.dieselWinter = dieselWinter;
    }

    public void setDieselFuelOffseason(Double dieselFuelOffseason) {
        this.dieselFuelOffseason = dieselFuelOffseason;
    }
}
