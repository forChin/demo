package com.example.demo.domain;

import java.util.HashMap;
import java.util.Map;

public class NatRhyOnvhMonitoring {

    private String year;

    private Map<String, Double> conclusionRegistration;
    private Map<String, Double> lssuanceWrittenTheopi;

    public NatRhyOnvhMonitoring(){
        this.conclusionRegistration = new HashMap<>();
        this.lssuanceWrittenTheopi = new HashMap<>();
    }

    public String getYear() {
        return year;
    }

    public Map<String, Double> getConclusionRegistration() {
        return conclusionRegistration;
    }

    public Map<String, Double> getLssuanceWrittenTheopi() {
        return lssuanceWrittenTheopi;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setConclusionRegistration(Map<String, Double> conclusionRegistration) {
        this.conclusionRegistration = conclusionRegistration;
    }

    public void setLssuanceWrittenTheopi(Map<String, Double> lssuanceWrittenTheopi) {
        this.lssuanceWrittenTheopi = lssuanceWrittenTheopi;
    }
}
