package com.example.demo.domain;

public class EduCatalogSchoolMonitoring {

    //new edu catalog school points

    private String index;
    private Double shareInfluence;
    private Double point;
    private String column4;

    public String getIndex() {
        return index;
    }

    public Double getShareInfluence() {
        return shareInfluence;
    }

    public Double getPoint() {
        return point;
    }

    public String getColumn4() {
        return column4;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public void setShareInfluence(Double shareInfluence) {
        this.shareInfluence = shareInfluence;
    }

    public void setPoint(Double point) {
        this.point = point;
    }

    public void setColumn4(String column4) {
        this.column4 = column4;
    }
}
