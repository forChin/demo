package com.example.demo.domain;

public class MenuJiv2sLMonitoring {

    private String year;            //ush_stat_jivotnovodstvo
    private String area;
    private String region;
    private Double outputLambsAgricultural;

    public String getYear() {
        return year;
    }

    public String getArea() {
        return area;
    }

    public String getRegion() {
        return region;
    }

    public Double getOutputLambsAgricultural() {
        return outputLambsAgricultural;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setOutputLambsAgricultural(Double outputLambsAgricultural) {
        this.outputLambsAgricultural = outputLambsAgricultural;
    }
}
