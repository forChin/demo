package com.example.demo.domain;

public class BSpisokDdoMonitoring {
    // new spisok ddo

    private String nameDdo;
    private String area;
    private String raion;
    private String ruralDistrict;
    private String snp;
    private String adres;
    private String yearConstruction;
    private String accidentRate;

    public String getNameDdo() {
        return nameDdo;
    }

    public String getArea() {
        return area;
    }

    public String getRaion() {
        return raion;
    }

    public String getRuralDistrict() {
        return ruralDistrict;
    }

    public String getSnp() {
        return snp;
    }

    public String getAdres() {
        return adres;
    }

    public String getYearConstruction() {
        return yearConstruction;
    }

    public String getAccidentRate() {
        return accidentRate;
    }

    public void setNameDdo(String nameDdo) {
        this.nameDdo = nameDdo;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setRaion(String raion) {
        this.raion = raion;
    }

    public void setRuralDistrict(String ruralDistrict) {
        this.ruralDistrict = ruralDistrict;
    }

    public void setSnp(String snp) {
        this.snp = snp;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }

    public void setYearConstruction(String yearConstruction) {
        this.yearConstruction = yearConstruction;
    }

    public void setAccidentRate(String accidentRate) {
        this.accidentRate = accidentRate;
    }
}
