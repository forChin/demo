package com.example.demo.domain;

public class PrognozVrpOblastYear {

    private String region;
    private String indicator;
    private String industry;
    private String year;
    private String type;
    private Double value;
    private String scenario;
    private String investments;

    public String getRegion() {
        return region;
    }

    public String getIndicator() {
        return indicator;
    }

    public String getIndustry() {
        return industry;
    }

    public String getYear() {
        return year;
    }

    public String getType() {
        return type;
    }

    public Double getValue() {
        return value;
    }

    public String getScenario() {
        return scenario;
    }

    public String getInvestments() {
        return investments;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setIndicator(String indicator) {
        this.indicator = indicator;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public void setScenario(String scenario) {
        this.scenario = scenario;
    }

    public void setInvestments(String investments) {
        this.investments = investments;
    }
}
