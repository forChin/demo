package com.example.demo.domain;

public class MenuSnpMonitoring {

    private String name;        //snp_bd
    private String region;
    private String raion;
    private String ruralDistrict;
    private String latitude;
    private String longitude;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRaion() {
        return raion;
    }

    public void setRaion(String raion) {
        this.raion = raion;
    }

    public String getRuralDistrict() {
        return ruralDistrict;
    }

    public void setRuralDistrict(String ruralDistrict) {
        this.ruralDistrict = ruralDistrict;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
