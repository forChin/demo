package com.example.demo.domain;

public class BlokEngineeringMonitoring {
    //blok engineering

    private String raion;
    private String ruralDistrict;
    private String ruralSettlement;
    private Integer water2;
    private Double roadDistrictCenter;
    private Integer gas;
    private Integer electro;
    private Integer telephoneCommunication;
    private Integer itog;
    private String year;

    public String getRaion() {
        return raion;
    }

    public String getRuralDistrict() {
        return ruralDistrict;
    }

    public String getRuralSettlement() {
        return ruralSettlement;
    }

    public Integer getWater2() {
        return water2;
    }

    public Double getRoadDistrictCenter() {
        return roadDistrictCenter;
    }

    public Integer getGas() {
        return gas;
    }

    public Integer getElectro() {
        return electro;
    }

    public Integer getTelephoneCommunication() {
        return telephoneCommunication;
    }

    public Integer getItog() {
        return itog;
    }

    public String getYear() {
        return year;
    }

    public void setRaion(String raion) {
        this.raion = raion;
    }

    public void setRuralDistrict(String ruralDistrict) {
        this.ruralDistrict = ruralDistrict;
    }

    public void setRuralSettlement(String ruralSettlement) {
        this.ruralSettlement = ruralSettlement;
    }

    public void setWater2(Integer water2) {
        this.water2 = water2;
    }

    public void setRoadDistrictCenter(Double roadDistrictCenter) {
        this.roadDistrictCenter = roadDistrictCenter;
    }

    public void setGas(Integer gas) {
        this.gas = gas;
    }

    public void setElectro(Integer electro) {
        this.electro = electro;
    }

    public void setTelephoneCommunication(Integer telephoneCommunication) {
        this.telephoneCommunication = telephoneCommunication;
    }

    public void setItog(Integer itog) {
        this.itog = itog;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
