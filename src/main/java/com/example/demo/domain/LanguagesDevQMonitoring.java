package com.example.demo.domain;

import java.util.HashMap;
import java.util.Map;

public class LanguagesDevQMonitoring {

    private String year;

    private Map<String, Double> shareIncomingDocuments;
    private Map<String, Double> shareOutgoingDocuments;
    private Map<String, Double> shareDomesticOutgoingDocuments;

    public LanguagesDevQMonitoring(){
        this.shareIncomingDocuments = new HashMap<>();
        this.shareOutgoingDocuments = new HashMap<>();
        this.shareDomesticOutgoingDocuments = new HashMap<>();
    }

    public String getYear() {
        return year;
    }

    public Map<String, Double> getShareIncomingDocuments() {
        return shareIncomingDocuments;
    }

    public Map<String, Double> getShareOutgoingDocuments() {
        return shareOutgoingDocuments;
    }

    public Map<String, Double> getShareDomesticOutgoingDocuments() {
        return shareDomesticOutgoingDocuments;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setShareIncomingDocuments(Map<String, Double> shareIncomingDocuments) {
        this.shareIncomingDocuments = shareIncomingDocuments;
    }

    public void setShareOutgoingDocuments(Map<String, Double> shareOutgoingDocuments) {
        this.shareOutgoingDocuments = shareOutgoingDocuments;
    }

    public void setShareDomesticOutgoingDocuments(Map<String, Double> shareDomesticOutgoingDocuments) {
        this.shareDomesticOutgoingDocuments = shareDomesticOutgoingDocuments;
    }
}
