package com.example.demo.domain;

public class MenuEnbekTrMonitoring {
    private String name;            //gp_enbek_tr
    private Double higher;
    private Double averageTotal;
    private Double secondaryVocational;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getHigher() {
        return higher;
    }

    public void setHigher(Double higher) {
        this.higher = higher;
    }

    public Double getAverageTotal() {
        return averageTotal;
    }

    public void setAverageTotal(Double averageTotal) {
        this.averageTotal = averageTotal;
    }

    public Double getSecondaryVocational() {
        return secondaryVocational;
    }

    public void setSecondaryVocational(Double secondaryVocational) {
        this.secondaryVocational = secondaryVocational;
    }
}
