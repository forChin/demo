package com.example.demo.domain;

public class KoordyearMonitoring {

    private String year;
    private Double minimumWage;
    private Double monthlyCalculationIndex;
    private Double minimumPension;

    public KoordyearMonitoring(){

    }

    public String getYear() {
        return year;
    }

    public Double getMinimumWage() {
        return minimumWage;
    }

    public Double getMonthlyCalculationIndex() {
        return monthlyCalculationIndex;
    }

    public Double getMinimumPension() {
        return minimumPension;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setMinimumWage(Double minimumWage) {
        this.minimumWage = minimumWage;
    }

    public void setMonthlyCalculationIndex(Double monthlyCalculationIndex) {
        this.monthlyCalculationIndex = monthlyCalculationIndex;
    }

    public void setMinimumPension(Double minimumPension) {
        this.minimumPension = minimumPension;
    }


}
