package com.example.demo.domain;

public class ShPMonitoring {
    //sh_pogolovya

    private String year;
    private String name;
    private String pokazatel;

    public String getYear() {
        return year;
    }

    public String getName() {
        return name;
    }

    public String getPokazatel() {
        return pokazatel;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPokazatel(String pokazatel) {
        this.pokazatel = pokazatel;
    }
}
