package com.example.demo.domain;

public class NatMonthOapikrMonitoring {

//sed_m_uprirp_oapkr

    private String year;
    private String month;
    private Integer appeal;


    public String getYear() {
        return year;
    }

    public String getMonth() {
        return month;
    }

    public Integer getAppeal() {
        return appeal;
    }


    public void setYear(String year) {
        this.year = year;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public void setAppeal(Integer appeal) {
        this.appeal = appeal;
    }
}
