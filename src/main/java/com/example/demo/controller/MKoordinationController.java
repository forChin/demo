package com.example.demo.controller;

import com.example.demo.domain.*;
import com.example.demo.service.MenuKoordinationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/statistics/krdn")

public class MKoordinationController {

    @Autowired
    MenuKoordinationService menuKoordinationService;

    @GetMapping(value = "/kreg", params = {"year"})
    public List<MenuKoordinationMonitoring> getMenuKoordinationMonitoringByYear(String year){
        return menuKoordinationService.getMenuKoordinationMonitoringByYear(year);
    }

    @GetMapping(value = "/krai", params = {"year"})
    public List<MenuKoordinationRaion> getMenuKoordinationRaionByYear(String year){
        return menuKoordinationService.getMenuKoordinationRaionByYear(year);
    }

    @GetMapping(value = "/krt1", params = {"year"})
    public List<MenuKoordinationMonitoringT1> getMenuKoordinationMonitoringT1ByYear(String year){
        return menuKoordinationService.getMenuKoordinationMonitoringT1ByYear(year);
    }

    @GetMapping(value = "/krt2", params = {"year"})
    public List<MenuKoordinationMonitoringT2> getMenuKoordinationMonitoringT2ByYear(String year){
        return menuKoordinationService.getMenuKoordinationMonitoringT2ByYear(year);
    }

    @GetMapping(value = "/krat1", params = {"year"})
    public List<MenuKoordinationRaionT1> getMenuKoordinationRaionT1ByYear(String year){
        return menuKoordinationService.getMenuKoordinationRaionT1ByYear(year);
    }

    @GetMapping(value = "/krat2", params = {"year"})
    public List<MenuKoordinationRaionT2> getMenuKoordinationRaionT2ByYear(String year){
        return menuKoordinationService.getMenuKoordinationRaionT2ByYear(year);
    }

    @GetMapping(value = "/mrk", params = {"year"})
    public List<MenuKoordinationRegionM> getMenuKoordinationRegionMByYear(String year){
        return menuKoordinationService.getMenuKoordinationRegionMByYear(year);
    }

    @GetMapping("/kmonitoring/years")
    public List<String> menukoordinationmonitoringYears(){
        return menuKoordinationService.getMenuKoordinationMonitoringYears();
    }

    @GetMapping("/kraion/years")
    public List<String> menukoordinationraionYears(){
        return menuKoordinationService.getMenuKoordinationRaionYears();
    }

    @GetMapping("/kmt1/years")
    public List<String> menukoordinationmonitoringt1Years(){
        return menuKoordinationService.getMenuKoordinationMonitoringT1Years();
    }

    @GetMapping("/kmt2/years")
    public List<String> menukoordinationmonitoringt2Years(){
        return menuKoordinationService.getMenuKoordinationMonitoringT2Years();
    }

    @GetMapping("/krt1/years")
    public List<String> menukoordinationraiont1Years(){
        return menuKoordinationService.getMenuKoordinationRaionT1Years();
    }

    @GetMapping("/krt2/years")
    public List<String> menukoordinationraiont2Years(){
        return menuKoordinationService.getMenuKoordinationRaionT2Years();
    }

    @GetMapping("/krm/years")
    public List<String> menukoordinationregionmYears(){
        return menuKoordinationService.getMenuKoordinationRegionMYears();
    }

    //Table

    @GetMapping(value = "/ktables", params = {"year", "region"})
    public List<MenuTKRT1Monitoring> getMenuTKRT1MonitoringYear(
            @RequestParam("year") String year,
            @RequestParam("region") String region
    ){
        return menuKoordinationService.getMenuTKRT1MonitoringByYear(year, region);
    }

    @GetMapping(value = "/ykr2", params = {"year"})
    public List<MenuKoord2Monitoring> getMenuKoord2MonitoringByYear(String year){
        return menuKoordinationService.getMenuKoord2MonitoringByYear(year);
    }

    @GetMapping(value = "/ykr3", params = {"year"})
    public List<MenuKoord3Monitoring> getMenuKoord3MonitoringByYear(String year){
        return menuKoordinationService.getMenuKoord3MonitoringByYear(year);
    }

    @GetMapping(value = "/ykr4", params = {"year"})
    public List<MenuKoord4Monitoring> getMenuKoord4MonitoringByYear(String year){
        return menuKoordinationService.getMenuKoord4MonitoringByYear(year);
    }

    @GetMapping(value = "/ktall")
    public List<MenuTKRT1allMonitoring> getMenuTKRT1allMonitoringByAll(){
        return menuKoordinationService.getMenuTKRT1allMonitoringByAll();
    }

    @GetMapping(value = "/mkral")
    public List<MenuKoordinationRegionM2> getMenuKoordinationRegionMByAll(){
        return menuKoordinationService.getMenuKoordinationRegionMByAll();
    }



}
