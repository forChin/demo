package com.example.demo.controller;

import com.example.demo.domain.*;
import com.example.demo.service.DigitalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/statistics/digital")


public class DigitalController {

    @Autowired
    DigitalService digitalService;

    @GetMapping(value = "/dig", params = {"yearEnd", "industry", "source"})
    public List<DigitalSocialObjectsMonitoring> getDigitalSocialObjectsMonitoringByYear(
            @RequestParam("yearEnd") String yearEnd,
            @RequestParam("industry") String industry,
            @RequestParam("source") String source
    ){
        return digitalService.getDigitalSocialObjectsMonitoringByYear(yearEnd, industry, source);
    }

    @GetMapping("/dig/yearEnd")
    public List<String> digitalsocialobjectsmonitoringYearEnd() {
        return digitalService.getDigitalSocialObjectsMonitoringYearEnd();
    }

    @GetMapping("/dig/industry")
    public List<String> digitalsocialobjectsmonitoringIndustry() {
        return digitalService.getDigitalSocialObjectsMonitoringIndustry();
    }

    @GetMapping("/dig/source")
    public List<String> digitalsocialobjectsmonitoringSource() {
        return digitalService.getDigitalSocialObjectsMonitoringSource();
    }

    @GetMapping("/digit")
    public List<DigitalSocialObjectsMonitoring> getDigitalSocialObjectsMonitoring(){
        return digitalService.getDigitalSocialObjectsMonitoring();
    }

    @GetMapping("/dip")
    public List<DiplomMonitoring> getDiplomMonitoring(){
        return digitalService.getDiplomMonitoring();
    }

    @GetMapping("/diptable")
    public List<DiplomTableMonitoring> getDiplomTableMonitoring(){
        return digitalService.getDiplomTableMonitoring();
    }

    @GetMapping("/snp")
    public List<DiplomSnpMonitoring> getDiplomSnpMonitoring() {
        return digitalService.getDiplomSnpMonitoring();
    }

    @GetMapping(value = "/diplomsp", params = {"regionIn", "fieldActivity", "typeEducation", "statusEmployee", "pol"})
    public List<DiplomMonitoring> getDiplomMonitoringByRegion(
            @RequestParam("regionIn") String regionIn,
            @RequestParam("fieldActivity") String fieldActivity,
            @RequestParam("typeEducation") String typeEducation,
            @RequestParam("statusEmployee") String statusEmployee,
            @RequestParam("pol") String pol
    ){
        return digitalService.getDiplomMonitoringByRegion(regionIn, fieldActivity, typeEducation, statusEmployee, pol);
    }

    @GetMapping("/dipl/regionIn")
    public List<String> diplommonitoringRegionIn() {
        return digitalService.getDiplomMonitoringRegionIn();
    }

    @GetMapping("/dipl/fieldA")
    public List<String> diplommonitoringFieldActivity() {
        return digitalService.getDiplomMonitoringFieldActivity();
    }

    @GetMapping("/dipl/typeE")
    public List<String> diplommonitoringTypeEducation() {
        return digitalService.getDiplomMonitoringTypeEducation();
    }

    @GetMapping("/dipl/statusE")
    public List<String> diplommonitoringStatusEmployee() {
        return digitalService.getDiplomMonitoringStatusEmployee();
    }

    @GetMapping("/dipl/pol")
    public List<String> diplommonitoringPol() {
        return digitalService.getDiplomMonitoringPol();
    }

    @GetMapping("/ecolog")
    public List<BlokEcologicalMonitoring> getBlokEcologicalMonitoring() {
        return digitalService.getBlokEcologicalMonitoring();
    }

    @GetMapping("/econom")
    public List<BlokEconomicMonitoring> getBlokEconomicMonitoring() {
        return digitalService.getBlokEconomicMonitoring();
    }

    @GetMapping("/engineer")
    public List<BlokEngineeringMonitoring> getBlokEngineeringMonitoring() {
        return digitalService.getBlokEngineeringMonitoring();
    }

    @GetMapping("/social")
    public List<BlokSocialMonitoring> getBlokSocialMonitoring() {
        return digitalService.getBlokSocialMonitoring();
    }

    @GetMapping("/atyrau")
    public List<IdaMonitoring> getIdaMonitoring() {
        return digitalService.getIdaMonitoring();
    }

    @GetMapping("/inder")
    public List<IdinMonitoring> getIdinMonitoring() {
        return digitalService.getIdinMonitoring();
    }

    @GetMapping("/isatay")
    public List<IdisMonitoring> getIdisMonitoring() {
        return digitalService.getIdisMonitoring();
    }

    @GetMapping("/kurmangazy")
    public List<IdkuMonitoring> getIdkuMonitoring() {
        return digitalService.getIdkuMonitoring();
    }

    @GetMapping("/kyzylkoga")
    public List<IdkyMonitoring> getIdkyMonitoring() {
        return digitalService.getIdkyMonitoring();
    }

    @GetMapping("/mahambet")
    public List<IdmahMonitoring> getIdmahMonitoring() {
        return digitalService.getIdmahMonitoring();
    }

    @GetMapping("/makat")
    public List<IdmakMonitoring> getIdmakMonitoring() {
        return digitalService.getIdmakMonitoring();
    }

    @GetMapping("/zhylyoi")
    public List<IdzhMonitoring> getIdzhMonitoring() {
        return digitalService.getIdzhMonitoring();
    }


    @GetMapping("/mon_snp")
    public List<MenuSnpMonitoring> getMenuSnpMonitoring() {
        return digitalService.getMenuSnpMonitoring();
    }



}
