package com.example.demo.controller;

import com.example.demo.domain.*;
import com.example.demo.service.EduService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/statistics/edu-ocenka")

public class EduController {

    @Autowired
    EduService eduService;

    @GetMapping(value = "/catalog_college")
    public List<EduddoMonitoring> getEduddoMonitoring(){
        return eduService.getEduddoMonitoring();
    }

    @GetMapping(value = "/catalog_ddo")
    public List<EduCatalogDdoMonitoring> getEduCatalogDdoMonitoring(){
        return eduService.getEduCatalogDdoMonitoring();
    }

    @GetMapping(value = "/catalog_school")
    public List<EduCatalogSchoolMonitoring> getEduCatalogSchoolMonitoring(){
        return eduService.getEduCatalogSchoolMonitoring();
    }

    @GetMapping(value = "/edu_college")
    public List<EduCollegeMonitoring> getEduCollegeMonitoring(){
        return eduService.getEduCollegeMonitoring();
    }

    @GetMapping(value = "/edu_ddo")
    public List<Edu2DdoMonitoring> getEdu2DdoMonitoring(){
        return eduService.getEdu2DdoMonitoring();
    }

    @GetMapping(value = "/edu_ddoneed")
    public List<EduDdoNeedMonitoring> getEduDdoNeedMonitoring(){
        return eduService.getEduDdoNeedMonitoring();
    }

    @GetMapping(value = "/edu_number")
    public List<EduNumbersMonitoring> getEduNumbersMonitoring(){
        return eduService.getEduNumbersMonitoring();
    }

    @GetMapping(value = "/edu_queue")
    public List<EduQueueDdoMonitoring> getEduQueueDdoMonitoring(){
        return eduService.getEduQueueDdoMonitoring();
    }

    @GetMapping(value = "/edu_school")
    public List<EduSchoolMonitoring> getEduSchoolMonitoring(){
        return eduService.getEduSchoolMonitoring();
    }

    @GetMapping(value = "/edu_schoolneed")
    public List<EduSchoolNeedMonitoring> getEduSchoolNeedMonitoring(){
        return eduService.getEduSchoolNeedMonitoring();
    }


}
