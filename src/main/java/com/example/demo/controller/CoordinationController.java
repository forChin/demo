package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.domain.ArchitectureMonitoring;
import com.example.demo.domain.Coordination;
import com.example.demo.service.CoordinationService;
import com.example.demo.service.MonitoringService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/statistics/coordination")
public class CoordinationController {
	
	@Autowired
	CoordinationService coordinationService;
	
	@GetMapping(value="/labour-market", params = "year")
	public List<Coordination> getLabourMarket(@RequestParam String year) {
		return coordinationService.getLabourMarketByYear(year);
	}
	
}

