package com.example.demo.controller;

import com.example.demo.domain.*;
import com.example.demo.service.MenuJivService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/statistics/sh")

public class MenuJivController {

    @Autowired
    MenuJivService menuJivService;

    @GetMapping(value = "/jiv", params = {"region"})
    public List<MenuJivMonitoring> getMenuJivMonitoringByYear(String region){
        return menuJivService.getMenuJivMonitoringByYear(region);
    }

    @GetMapping("/jiv/regions")
    public List<String> menujivmonitoringYears(){
        return menuJivService.getMenuJivMonitoringYears();
    }

    @GetMapping(value = "/ras", params = {"year"})
    public List<MenuRasMonitoring> getMenuRasMonitoringByYear(String year){
        return menuJivService.getMenuRasMonitoringByYear(year);
    }

    @GetMapping(value = "/ras", params = {"region"})
    public List<MenuRasMonitoring> getMenuRasMonitoringByRegion(String region){
        return menuJivService.getMenuRasMonitoringByRegion(region);
    }

    @GetMapping("/ras/years")
    public List<String> menurasmonitoringYears(){
        return menuJivService.getMenuRasMonitoringYears();
    }

    @GetMapping("/ras/regions")
    public List<String> menurasmonitoringRegions(){
        return menuJivService.getMenuRasMonitoringRegions();
    }

    @GetMapping(value = "/ryb", params = {"year"})
    public List<MenuRybAqva2Monitoring> getMenuRybAqva2MonitoringByYear(String year){
        return menuJivService.getMenuRybAqva2MonitoringByYear(year);
    }

    @GetMapping("/ryb/years")
    public List<String> menurybaqva2monitoringYears(){
        return menuJivService.getMenuRybAqva2MonitoringYears();
    }

    @GetMapping(value = "/ria", params = {"view"})
    public List<MenuRybAqvaTable> getMenuRybAqvaTableByView(String view){
        return menuJivService.getMenuRybAqvaTableByView(view);
    }

//-----------------

    @GetMapping(value = "/ryball")
    public List<MenuRybAqvaTable> getMenu2RybAqvaTableByView(){
        return menuJivService.getMenu2RybAqvaTableByView();
    }


    @GetMapping("/ria/views")
    public List<String> menurybaqvatableYears(){
        return menuJivService.getMenuRybAqvaTableViews();
    }

    @GetMapping(value = "/ria2", params = {"year"})
    public List<MenuRybAqva2TableMonitoring> getMenuRybAqva2TableMonitoringByYear(String year){
        return menuJivService.getMenuRybAqva2TableMonitoringByYear(year);
    }

    @GetMapping("/ria2/years")
    public List<String> menurybaqva2tablemonitoringYears(){
        return menuJivService.getMenuRybAqva2TableYears();
    }

    @GetMapping(value = "/vet", params = {"year"})
    public List<MenuVetTableMonitoring> getMenuVetTableMonitoringByYear(String year){
        return menuJivService.getMenuVetTableMonitoringByYear(year);
    }

    @GetMapping(value = "/vet", params = {"region"})
    public List<MenuVetTableMonitoring> getMenuVetTableMonitoringByRegion(String region){
        return menuJivService.getMenuVetTableMonitoringByRegion(region);
    }

    @GetMapping(value = "/vet", params = {"type"})
    public List<MenuVetTableMonitoring> getMenuVetTableMonitoringByType(String type){
        return menuJivService.getMenuVetTableMonitoringByType(type);
    }

    @GetMapping("/vet/years")
    public List<String> menuvettablemonitoringYears(){
        return menuJivService.getMenuVetTableMonitoringYears();
    }

    @GetMapping("/vet/types")
    public List<String> menuvettablemonitoringTypes(){
        return menuJivService.getMenuVetTableMonitoringTypes();
    }

    @GetMapping(value = "/jivot")
    public List<MenuJiv2Monitoring> getMenuJiv2MonitoringByYear(){
        return menuJivService.getMenuJiv2MonitoringByYear();
    }

    @GetMapping(value = "/jivot/krs")
    public List<MenuJiv2kshMonitoring> getMenuJiv2kshMonitoringByYear(){
        return menuJivService.getMenuJiv2kshMonitoringByYear();
    }

    @GetMapping(value = "/jivot/cows")
    public List<MenuJiv2kshCowsMonitoring> getMenuJiv2kshCowsMonitoringByYear(){
        return menuJivService.getMenuJiv2kshCowsMonitoringByYear();
    }

    @GetMapping(value = "/jivot/sheeps")
    public List<MenuJiv2kshSGMonitoring> getMenuJiv2kshSGMonitoringByYear(){
        return menuJivService.getMenuJiv2kshSGMonitoringByYear();
    }

    @GetMapping(value = "/jivot/pigs")
    public List<MenuJiv2kshPMonitoring> getMenuJiv2kshPMonitoringByYear(){
        return menuJivService.getMenuJiv2kshPMonitoringByYear();
    }

    @GetMapping(value = "/jivot/horses")
    public List<MenuJiv2kshHMonitoring> getMenuJiv2kshHMonitoringByYear(){
        return menuJivService.getMenuJiv2kshHMonitoringByYear();
    }

    @GetMapping(value = "/jivot/camels")
    public List<MenuJiv2kshCMonitoring> getMenuJiv2kshCMonitoringByYear(){
        return menuJivService.getMenuJiv2kshCMonitoringByYear();
    }

    @GetMapping(value = "/jivot/birds")
    public List<MenuJiv2kshBMonitoring> getMenuJiv2kshBMonitoringByYear(){
        return menuJivService.getMenuJiv2kshBMonitoringByYear();
    }

    @GetMapping(value = "/jivot/yield")
    public List<MenuJiv2kshYMonitoring> getMenuJiv2kshYMonitoringByYear(){
        return menuJivService.getMenuJiv2kshYMonitoringByYear();
    }

    @GetMapping(value = "/jivot/lambs")
    public List<MenuJiv2kshLMonitoring> getMenuJiv2kshLMonitoringByYear(){
        return menuJivService.getMenuJiv2kshLMonitoringByYear();
    }

    @GetMapping(value = "/jivot/kids")
    public List<MenuJiv2kshKMonitoring> getMenuJiv2kshKMonitoringByYear(){
        return menuJivService.getMenuJiv2kshKMonitoringByYear();
    }

    @GetMapping(value = "/jivot/foals")
    public List<MenuJiv2kshFMonitoring> getMenuJiv2kshFMonitoringByYear(){
        return menuJivService.getMenuJiv2kshFMonitoringByYear();
    }

    @GetMapping(value = "/jivot/camelscolts")
    public List<MenuJiv2kshCSMonitoring> getMenuJiv2kshCSMonitoringByYear(){
        return menuJivService.getMenuJiv2kshCSMonitoringByYear();
    }

    @GetMapping(value = "/s/krs")
    public List<MenuJiv2sKRSMonitoring> getMenuJiv2sKRSMonitoringByYear(){
        return menuJivService.getMenuJiv2sKRSMonitoringByYear();
    }

    @GetMapping(value = "/s/cows")
    public List<MenuJiv2sCowsMonitoring> getMenuJiv2sCowsMonitoringByYear(){
        return menuJivService.getMenuJiv2sCowsMonitoringByYear();
    }

    @GetMapping(value = "/s/pigs")
    public List<MenuJiv2sPMonitoring> getMenuJiv2sPMonitoringByYear(){
        return menuJivService.getMenuJiv2sPMonitoringByYear();
    }

    @GetMapping(value = "/s/sheep")
    public List<MenuJiv2sSMonitoring> getMenuJiv2sSMonitoringByYear(){
        return menuJivService.getMenuJiv2sSMonitoringByYear();
    }

    @GetMapping(value = "/s/horses")
    public List<MenuJiv2sHMonitoring> getMenuJiv2sHMonitoringByYear(){
        return menuJivService.getMenuJiv2sHMonitoringByYear();
    }

    @GetMapping(value = "/s/camels")
    public List<MenuJiv2sCMonitoring> getMenuJiv2sCMonitoringByYear(){
        return menuJivService.getMenuJiv2sCMonitoringByYear();
    }

    @GetMapping(value = "/s/birds")
    public List<MenuJiv2sBMonitoring> getMenuJiv2sBMonitoringByYear(){
        return menuJivService.getMenuJiv2sBMonitoringByYear();
    }

    @GetMapping(value = "/s/yield")
    public List<MenuJiv2sYMonitoring> getMenuJiv2sYMonitoringByYear(){
        return menuJivService.getMenuJiv2sYMonitoringByYear();
    }

    @GetMapping(value = "/s/lambs")
    public List<MenuJiv2sLMonitoring> getMenuJiv2sLMonitoringByYear(){
        return menuJivService.getMenuJiv2sLMonitoringByYear();
    }

    @GetMapping(value = "/s/kids")
    public List<MenuJiv2sKMonitoring> getMenuJiv2sKMonitoringByYear(){
        return menuJivService.getMenuJiv2sKMonitoringByYear();
    }

    @GetMapping(value = "/s/foals")
    public List<MenuJiv2sFMonitoring> getMenuJiv2sFMonitoringByYear(){
        return menuJivService.getMenuJiv2sFMonitoringByYear();
    }

    @GetMapping(value = "/s/camelscolts")
    public List<MenuJiv2sCSMonitoring> getMenuJiv2sCSMonitoringByYear(){
        return menuJivService.getMenuJiv2sCSMonitoringByYear();
    }

    @GetMapping(value = "/hn/krs")
    public List<MenuJiv2hnKRSMonitoring> getMenuJiv2hnKRSMonitoringByYear(){
        return menuJivService.getMenuJiv2hnKRSMonitoringByYear();
    }

    @GetMapping(value = "/hn/cows")
    public List<MenuJiv2hnCowsMonitoring> getMenuJiv2hnCowsMonitoringByYear(){
        return menuJivService.getMenuJiv2hnCowsMonitoringByYear();
    }

    @GetMapping(value = "/hn/sheep")
    public List<MenuJiv2hnSMonitoring> getMenuJiv2hnSMonitoringByYear(){
        return menuJivService.getMenuJiv2hnSMonitoringByYear();
    }

    @GetMapping(value = "/hn/pigs")
    public List<MenuJiv2hnPMonitoring> getMenuJiv2hnPMonitoringByYear(){
        return menuJivService.getMenuJiv2hnPMonitoringByYear();
    }

    @GetMapping(value = "/hn/horses")
    public List<MenuJiv2hnHMonitoring> getMenuJiv2hnHMonitoringByYear(){
        return menuJivService.getMenuJiv2hnHMonitoringByYear();
    }

    @GetMapping(value = "/hn/camels")
    public List<MenuJiv2hnCAMonitoring> getMenuJiv2hnCAMonitoringByYear(){
        return menuJivService.getMenuJiv2hnCAMonitoringByYear();
    }

    @GetMapping(value = "/hn/birds")
    public List<MenuJiv2hnBMonitoring> getMenuJiv2hnBMonitoringByYear(){
        return menuJivService.getMenuJiv2hnBMonitoringByYear();
    }

    @GetMapping(value = "/hn/yield")
    public List<MenuJiv2hnYMonitoring> getMenuJiv2hnYMonitoringByYear(){
        return menuJivService.getMenuJiv2hnYMonitoringByYear();
    }

    @GetMapping(value = "/hn/lambs")
    public List<MenuJiv2hnLMonitoring> getMenuJiv2hnLMonitoringByYear(){
        return menuJivService.getMenuJiv2hnLMonitoringByYear();
    }

    @GetMapping(value = "/hn/kids")
    public List<MenuJiv2hnKMonitoring> getMenuJiv2hnKMonitoringByYear(){
        return menuJivService.getMenuJiv2hnKMonitoringByYear();
    }

    @GetMapping(value = "/hn/foals")
    public List<MenuJiv2hnFMonitoring> getMenuJiv2hnFMonitoringByYear(){
        return menuJivService.getMenuJiv2hnFMonitoringByYear();
    }

    @GetMapping(value = "/hn/camelscolts")
    public List<MenuJiv2hnCSMonitoring> getMenuJiv2hnCSMonitoringByYear(){
        return menuJivService.getMenuJiv2hnCSMonitoringByYear();
    }

    @GetMapping(value = "/v/vv")
    public List<ShValVypMonitoring> getShValVypMonitoring(){
        return menuJivService.getShValVypMonitoring();
    }

    @GetMapping(value = "/v/vs")
    public List<ShValSborMonitoring> getShValSborMonitoring(){
        return menuJivService.getShValSborMonitoring();
    }

    @GetMapping(value = "/v/vvp")
    public List<ShVvpMonitoring> getShVvpMonitoring(){
        return menuJivService.getShVvpMonitoring();
    }

    @GetMapping(value = "/v/jiv")
    public List<ShJivotMonitoring> getShJivotMonitoring(){
        return menuJivService.getShJivotMonitoring();
    }

    @GetMapping(value = "/v/or")
    public List<ShORMonitoring> getShORMonitoring(){
        return menuJivService.getShORMonitoring();
    }

    @GetMapping(value = "/v/ps")
    public List<ShPSMonitoring> getShPSMonitoring(){
        return menuJivService.getShPSMonitoring();
    }

    @GetMapping(value = "/v/p")
    public List<ShPMonitoring> getShPMonitoring(){
        return menuJivService.getShPMonitoring();
    }

    @GetMapping(value = "/v/pp")
    public List<ShPPMonitoring> getShPPMonitoring(){
        return menuJivService.getShPPMonitoring();
    }

    @GetMapping(value = "/v/ppop")
    public List<ShPpoPMonitoring> getShPpoPMonitoring(){
        return menuJivService.getShPpoPMonitoring();
    }

    @GetMapping(value = "/v/priplod")
    public List<ShPriplodMonitoring> getShPriplodMonitoring(){
        return menuJivService.getShPriplodMonitoring();
    }

    @GetMapping(value = "/v/pprod")
    public List<ShProizvProdMonitoring> getShProizvProdMonitoring(){
        return menuJivService.getShProizvProdMonitoring();
    }

    @GetMapping(value = "/v/rk")
    public List<ShRKMonitoring> getShRKMonitoring(){
        return menuJivService.getShRKMonitoring();
    }

    @GetMapping(value = "/v/rasten")
    public List<ShRastenMonitoring> getShRastenMonitoring(){
        return menuJivService.getShRastenMonitoring();
    }

    @GetMapping(value = "/v/ryba")
    public List<ShRybaMonitoring> getShRybaMonitoring(){
        return menuJivService.getShRybaMonitoring();
    }

    @GetMapping(value = "/v/rybolov")
    public List<ShRybolovMonitoring> getShRybolovMonitoring(){
        return menuJivService.getShRybolovMonitoring();
    }

    @GetMapping(value = "/v/up")
    public List<ShUPMonitoring> getShUPMonitoring(){
        return menuJivService.getShUPMonitoring();
    }

    @GetMapping(value = "/v/uroj")
    public List<ShUrojMonitoring> getShUrojMonitoring(){
        return menuJivService.getShUrojMonitoring();
    }



}
