package com.example.demo.controller;

import com.example.demo.domain.*;
import com.example.demo.service.PrognozVrpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/statistics/vrp-ekz-prog")
public class PrognozVrpController {
    @Autowired
    PrognozVrpService prognozVrpService;

    @GetMapping(value = "/dinamics", params = {"year"})
    public List<PrognozVrp> getDinamicsByYear(String year){
        return prognozVrpService.getPrognozVrpByYear(year);
    }

//PrognozVrpFact

    @GetMapping(value = "/vrpfact", params = {"year"})
    public List<PrognozVrpFact> getVrpfactByYear(String year){
        return prognozVrpService.getPrognozVrpFactByYear(year);
    }

//PrognozVrpEkzFact

    @GetMapping(value = "/vrpekzfact", params = {"year"})
    public List<PrognozVrpEkzFact> getVrpekzfactByYear(String year){
        return prognozVrpService.getPrognozVrpEkzFactByYear(year);
    }

    //PrognozVrpOblast

    @GetMapping(value = "/vrpoblast", params = {"scenario"})
    public List<PrognozVrpOblast> getVrpoblastByScenario(String scenario){
        return prognozVrpService.getPrognozVrpOblastByScenario(scenario);
    }

    //PrognozVrpOblastYear

    @GetMapping(value = "/vrpoblastyear", params = {"year"})
    public List<PrognozVrpOblastYear> getVrpoblastyearByYear(String year){
        return prognozVrpService.getPrognozVrpOblastYearByYear(year);
    }

    //PrognozVrpRaion
    @GetMapping(value = "/vrpraion", params = {"year"})
    public List<PrognozVrpRaion> getVrpraionByYear(String year){
        return prognozVrpService.getPrognozVrpRaionByYear(year);
    }

    //PrognozVrpRaionScenario

    @GetMapping(value = "/vrpraionscenario", params = {"scenario"})
    public List<PrognozVrpRaionScenario> getVrpraionscenarioByScenario(String scenario){
        return prognozVrpService.getPrognozVrpRaionScenarioByScenario(scenario);
    }

//PrognozVrpTable

        @GetMapping(value = "/vrptables", params = {"year", "scenario", "indicator"})
    public List<PrognozVrpTable> getPrognozVrpTableYear(
            @RequestParam("year") String year,
            @RequestParam("scenario") String scenario,
            @RequestParam("indicator")  String indicator
    ){
        return prognozVrpService.getPrognozVrpTableByYear(year, scenario, indicator);
    }

}
