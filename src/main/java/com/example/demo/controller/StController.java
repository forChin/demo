package com.example.demo.controller;

import com.example.demo.domain.St3Monitoring;
import com.example.demo.domain.St4Monitoring;
import com.example.demo.domain.StMonitoring;
import com.example.demo.service.StService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/statistics/st")



public class StController {

    @Autowired
    StService stService;

    @GetMapping(value = "/st")
    public List<StMonitoring> getStMonitoring(){
        return stService.getStMonitoring();
    }

    @GetMapping("/st/raions")
    public List<String>stmonitoringRaions(){
        return stService.getStMonitoringRaions();
    }

    @GetMapping("/st/years")
    public List<String>stmonitoringYears(){
        return stService.getStMonitoringYears();
    }

    @GetMapping("/st/periods")
    public List<String>stmonitoringPeriods(){
        return stService.getStMonitoringPeriods();
    }

    @GetMapping(value = "/stu", params = {"raion", "year", "period"})
    public List<StMonitoring> getStMonitoringByRaion(
            @RequestParam("raion") String raion,
            @RequestParam("year") String year,
            @RequestParam("period") String period
    ){
        return stService.getStMonitoringByRaion(raion, year, period);
    }

    @GetMapping(value = "/plan")
    public List<St3Monitoring> getSt3Monitoring(){
        return stService.getSt3Monitoring();
    }

    @GetMapping(value = "/coef")
    public List<St4Monitoring> getSt4Monitoring(){
        return stService.getSt4Monitoring();
    }




}
