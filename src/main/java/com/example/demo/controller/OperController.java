package com.example.demo.controller;


import com.example.demo.domain.*;
import com.example.demo.service.OperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/statistics/st")

public class OperController {

    @Autowired
    OperService operService;

    @GetMapping(value = "/o", params = {"region", "year", "month"})
    public List<OperMPMonitoring> getOperMPMonitoring(
            @RequestParam("region") String region,
            @RequestParam("year") String year,
            @RequestParam("month") String month
    ){
        return operService.getOperMPMonitoring(region, year, month);
    }

    @GetMapping(value = "/o/all")
    public List<OperMPMonitoring> getOperMPMonitoringByAll(){
        return operService.getOperMPMonitoringByAll();
    }

    @GetMapping("/o/regions")
    public List<String>opermpmonitoringRegions(){
        return operService.getOperMPMonitoringRegions();
    }

    @GetMapping("/o/years")
    public List<String>opermpmonitoringYears(){
        return operService.getOperMPMonitoringYears();
    }

    @GetMapping("/o/months")
    public List<String>opermpmonitoringMonths(){
        return operService.getOperMPMonitoringMonths();
    }

    @GetMapping(value = "/mop", params = {"year", "month"})
    public List<OperMOpMonitoring> getOperMOpMonitoring(
            @RequestParam("year") String year,
            @RequestParam("month") String month
    ){
        return operService.getOperMOpMonitoring(year, month);
    }

    @GetMapping(value = "/mop/all")
    public List<OperMOpMonitoring> getOperMOpMonitoringByAll(){
        return operService.getOperMOpMonitoringByAll();
    }


    @GetMapping("/mop/years")
    public List<String>opermopmonitoringYears(){
        return operService.getOperMOpMonitoringYears();
    }

    @GetMapping("/mop/months")
    public List<String>opermopmonitoringMonths(){
        return operService.getOperMOpMonitoringMonths();
    }

    @GetMapping(value = "/pr", params = {"region", "year", "month"})
    public List<OperMPrMonitoring> getOperMPrMonitoring(
            @RequestParam("region") String region,
            @RequestParam("year") String year,
            @RequestParam("month") String month
    ){
        return operService.getOperMPrMonitoring(region, year, month);
    }

    @GetMapping(value = "/pr/all")
    public List<OperMPrMonitoring> getOperMPrMonitoringByAll(){
        return operService.getOperMPrMonitoringByAll();
    }

    @GetMapping("/pr/regions")
    public List<String>opermprmonitoringRegions(){
        return operService.getOperMPrMonitoringRegions();
    }

    @GetMapping("/pr/years")
    public List<String>opermprmonitoringYears(){
        return operService.getOperMPrMonitoringYears();
    }

    @GetMapping("/pr/months")
    public List<String>opermprmonitoringMonths(){
        return operService.getOperMPrMonitoringMonths();
    }

    @GetMapping(value = "/pt", params = {"year", "month"})
    public List<OperMPtMonitoring> getOperMPtMonitoring(
            @RequestParam("year") String year,
            @RequestParam("month") String month
    ){
        return operService.getOperMPtMonitoring(year, month);
    }

    @GetMapping(value = "/pt/all")
    public List<OperMPtMonitoring> getOperMPtMonitoringByAll(){
        return operService.getOperMPtMonitoringByAll();
    }


    @GetMapping("/pt/years")
    public List<String>opermptmonitoringYears(){
        return operService.getOperMPtMonitoringYears();
    }

    @GetMapping("/pt/months")
    public List<String>opermptmonitoringMonths(){
        return operService.getOperMPtMonitoringMonths();
    }


    @GetMapping(value = "/msd", params = {"region", "ruralDistrict", "year", "month"})
    public List<OperMSDMonitoring> getOperMSDMonitoring(
            @RequestParam("region") String region,
            @RequestParam("ruralDistrict") String ruralDistrict,
            @RequestParam("year") String year,
            @RequestParam("month") String month
    ){
        return operService.getOperMSDMonitoring(region, ruralDistrict, year, month);
    }

    @GetMapping("/msd/regions")
    public List<String>opermsdmonitoringRegions(){
        return operService.getOperMSDMonitoringRegions();
    }

    @GetMapping("/msd/districts")
    public List<String>opermsdmonitoringDistricts(){
        return operService.getOperMSDMonitoringDistricts();
    }

    @GetMapping("/msd/years")
    public List<String>opermsdmonitoringYears(){
        return operService.getOperMSDMonitoringYears();
    }

    @GetMapping("/msd/months")
    public List<String>opermsdmonitoringMonths(){
        return operService.getOperMSDMonitoringMonths();
    }


    @GetMapping(value = "/msp", params = {"region", "year", "month"})
    public List<OperMSpMonitoring> getOperMSpMonitoring(
            @RequestParam("region") String region,
            @RequestParam("year") String year,
            @RequestParam("month") String month
    ){
        return operService.getOperMSpMonitoring(region, year, month);
    }

    @GetMapping("/msp/regions")
    public List<String>opermspmonitoringRegions(){
        return operService.getOperMSpMonitoringRegions();
    }

    @GetMapping("/msp/years")
    public List<String>opermspmonitoringYears(){
        return operService.getOperMSpMonitoringYears();
    }

    @GetMapping("/msp/months")
    public List<String>opermspmonitoringMonths(){
        return operService.getOperMSpMonitoringMonths();
    }

    @GetMapping(value = "/msp/all")
    public List<OperMSpMonitoring> getOperMSpMonitoringByAll(){
        return operService.getOperMSpMonitoringByAll();
    }

    @GetMapping(value = "/prois", params = {"region", "month"})
    public List<OperMProMonitoring> getOperMProMonitoringByRegion(
            @RequestParam("region") String region,
            @RequestParam("month") String month
    ){
        return operService.getOperMProMonitoringByRegion(region, month);
    }

    @GetMapping("/prois/regions")
    public List<String>opermpromonitoringRegions(){
        return operService.getOperMProMonitoringRegions();
    }

    @GetMapping("/prois/months")
    public List<String>opermpromonitoringMonths(){
        return operService.getOperMProMonitoringMonths();
    }

    @GetMapping("/prois/dates")
    public List<String>opermpromonitoringDates(){
        return operService.getOperMProMonitoringDates();
    }

    @GetMapping(value = "/pro/prois")
    public List<OperMProMonitoring> getOperMProMonitoring(){
        return operService.getOperMProMonitoring();
    }


}
