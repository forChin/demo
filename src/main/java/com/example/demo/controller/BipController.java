package com.example.demo.controller;

import com.example.demo.domain.*;
import com.example.demo.service.BipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/statistics/bip")

public class BipController {

    @Autowired
    BipService bipService;

    @GetMapping(value = "/bo", params = {"region"})
    public List<MenuBipObrazMonitoring> getMenuBipObrazMonitoringByRegion(String region){
        return bipService.getMenuBipObrazMonitoringByRegion(region);
    }

    @GetMapping("/bo/regions")
    public List<String> menubipobrazmonitoringRegions(){
        return bipService.getMenuBipObrazMonitoringRegions();
    }

    @GetMapping("/bo/rurals")
    public List<String> menubipobrazmonitoringRurals(){
        return bipService.getMenuBipObrazMonitoringRurals();
    }

    @GetMapping("/bo/rs")
    public List<String> menubipobrazmonitoringRSs(){
        return bipService.getMenuBipObrazMonitoringRSs();
    }

    @GetMapping(value = "/bipo", params = {"region", "ruralDistrict", "ruralSettlement"})
    public List<MenuBipObrazMonitoring> getMenuBipObrazMonitoringRegion2(
            @RequestParam("region") String region,
            @RequestParam("ruralDistrict") String ruralDistrict,
            @RequestParam("ruralSettlement") String ruralSettlement
    ){
        return bipService.getMenuBipObrazMonitoringByRegion2(region, ruralDistrict, ruralSettlement);
    }

    @GetMapping("/bz/regions")
    public List<String> menubipzdravmonitoringRegions(){
        return bipService.getMenuBipZdravMonitoringRegions();
    }

    @GetMapping("/bz/rurals")
    public List<String> menubipzdravmonitoringRurals(){
        return bipService.getMenuBipZdravMonitoringRurals();
    }

    @GetMapping("/bz/rs")
    public List<String> menubipzdravmonitoringRSs(){
        return bipService.getMenuBipZdravMonitoringRSs();
    }

    @GetMapping(value = "/zdravbip", params = {"region", "ruralDistrict", "ruralSettlement"})
    public List<MenuBipZdravMonitoring> getMenuBipZdravMonitoringRegion2(
            @RequestParam("region") String region,
            @RequestParam("ruralDistrict") String ruralDistrict,
            @RequestParam("ruralSettlement") String ruralSettlement
    )
    {
        return bipService.getMenuBipZdravMonitoringByRegion2(region, ruralDistrict, ruralSettlement);
    }

    @GetMapping(value = "/bz", params = {"region"})
    public List<MenuBipZdravMonitoring> getMenuBipZdravMonitoringByRegion(String region){
        return bipService.getMenuBipZdravMonitoringByRegion(region);
    }

    @GetMapping("/bd/regions")
    public List<String> menubipdorogimonitoringRegions(){
        return bipService.getMenuBipDorogiMonitoringRegions();
    }

    @GetMapping("/bd/roadSnp")
    public List<String> menubipdorogimonitoringRSnps(){
        return bipService.getMenuBipDorogiMonitoringRSnps();
    }

    @GetMapping("/bd/roadType")
    public List<String> menubipdorogimonitoringRTs(){
        return bipService.getMenuBipDorogiMonitoringRTs();
    }

    @GetMapping(value = "/bdorogi", params = {"region", "roadSnp", "roadType"})
    public List<MenuBipDorogiMonitoring> getMenuBipDorogiMonitoringRegion2(
            @RequestParam("region") String region,
            @RequestParam("roadSnp") String roadSnp,
            @RequestParam("roadType") String roadType
    ){
        return bipService.getMenuBipDorogiMonitoringByRegion2(region, roadSnp, roadType);
    }

    @GetMapping(value = "/bdor", params = {"region"})
    public List<MenuBipDorogiMonitoring> getMenuBipDorogiMonitoringByRegion(String region){
        return bipService.getMenuBipDorogiMonitoringByRegion(region);
    }

    @GetMapping("/bc/regions")
    public List<String> menubipculturamonitoringRegions(){
        return bipService.getMenuBipCulturaMonitoringRegions();
    }

    @GetMapping("/bc/rurals")
    public List<String> menubipculturamonitoringRDistricts(){
        return bipService.getMenuBipCulturaMonitoringRDistricts();
    }

    @GetMapping("/bc/rs")
    public List<String> menubipculturamonitoringRSettlements(){
        return bipService.getMenuBipCulturaMonitoringRSettlements();
    }

    @GetMapping(value = "/bcult", params = {"region"})
    public List<MenuBipCulturaMonitoring> getMenuBipCulturaMonitoringByRegion(String region){
        return bipService.getMenuBipCulturaMonitoringByRegion(region);
    }

    @GetMapping(value = "/bcultura", params = {"region", "ruralDistrict", "ruralSettlement"})
    public List<MenuBipCulturaMonitoring> getMenuBipCulturaMonitoringRegion2(
            @RequestParam("region") String region,
            @RequestParam("ruralDistrict") String ruralDistrict,
            @RequestParam("ruralSettlement") String ruralSettlement
    ){
        return bipService.getMenuBipCulturaMonitoringByRegion2(region, ruralDistrict, ruralSettlement);
    }

    @GetMapping(value = "/catalog-zdrav")
    public List<BCatalogZdravMonitoring> getBCatalogZdravMonitoring(){
        return bipService.getBCatalogZdravMonitoring();
    }

    @GetMapping(value = "/category-weight")
    public List<BCategoryWeightMonitoring> getBCategoryWeightMonitoring(){
        return bipService.getBCategoryWeightMonitoring();
    }

    @GetMapping(value = "/culture-points")
    public List<BCulturePointsMonitoring> getBCulturePointsMonitoring(){
        return bipService.getBCulturePointsMonitoring();
    }

    @GetMapping(value = "/dorogi-points")
    public List<BDorogiPointsMonitoring> getBDorogiPointsMonitoring(){
        return bipService.getBDorogiPointsMonitoring();
    }

    @GetMapping(value = "/edu-points")
    public List<BEduPointsMonitoring> getBEduPointsMonitoring(){
        return bipService.getBEduPointsMonitoring();
    }

    @GetMapping(value = "/inflation")
    public List<BInflationMonitoring> getBInflationMonitoring(){
        return bipService.getBInflationMonitoring();
    }

    @GetMapping(value = "/numbers")
    public List<BNumbersMonitoring> getBNumbersMonitoring(){
        return bipService.getBNumbersMonitoring();
    }

    @GetMapping(value = "/spisok-ddo")
    public List<BSpisokDdoMonitoring> getBSpisokDdoMonitoring(){
        return bipService.getBSpisokDdoMonitoring();
    }

    @GetMapping(value = "/spisok-dorogi")
    public List<BSpisokDorogiMonitoring> getBSpisokDorogiMonitoring(){
        return bipService.getBSpisokDorogiMonitoring();
    }

    @GetMapping(value = "/spisok-shkoly")
    public List<BSpisokShkolyMonitoring> getBSpisokShkolyMonitoring(){
        return bipService.getBSpisokShkolyMonitoring();
    }

    @GetMapping(value = "/spisok-zdrav")
    public List<BSpisokZdravMonitoring> getBSpisokZdravMonitoring(){
        return bipService.getBSpisokZdravMonitoring();
    }

    @GetMapping(value = "/zdrav-points")
    public List<BZdravPointsMonitoring> getBZdravPointsMonitoring(){
        return bipService.getBZdravPointsMonitoring();
    }

    @GetMapping(value = "/bobip")
    public List<MenuBipObrazMonitoring> getMenuBipObrazMonitoring(){
        return bipService.getMenuBipObrazMonitoring();
    }

    @GetMapping(value = "/bzbip")
    public List<MenuBipZdravMonitoring> getMenuBipZdravMonitoring(){
        return bipService.getMenuBipZdravMonitoring();
    }

    @GetMapping(value = "/bdbip")
    public List<MenuBipDorogiMonitoring> getMenuBipDorogiMonitoring(){
        return bipService.getMenuBipDorogiMonitoring();
    }

    @GetMapping(value = "/bcbip")
    public List<MenuBipCulturaMonitoring> getMenuBipCulturaMonitoring(){
        return bipService.getMenuBipCulturaMonitoring();
    }



}
