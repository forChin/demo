package com.example.demo.controller;

import java.util.List;
import java.util.Map;

import com.example.demo.domain.*;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.service.MonitoringService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/statistics/monitoring-sed")
public class MonitoringController {

	@Autowired
	MonitoringService monitoringService;
	
//	HEALTHCARE
	
	@GetMapping(value = "/healthcare", params = {"year", "byMonthes"})
	public HealthcareMonitoring getHealthcareMonitoring(
			@RequestParam("year") String year,
			@RequestParam("byMonthes") boolean byMonthes
	) {
		return monitoringService.getHealthcareMonitoringByYear(year);
	}
	
	@GetMapping("/healthcare/units")
	public HealthcareMonitoringUnits getHealthcareMonitoringUnits() {
		return monitoringService.getHealthcareMonitoringUnits();
	}
	
	@GetMapping(value = "/healthcare/years")
	public Map<String, String[]> getHealthcareYears() {
		return monitoringService.getHealthcareYears();
	}
	
	@GetMapping(value = "/healthcare", params = {"year", "byQuarters"})
	public HealthcareMonitoring2 getQuartersHealthcareMonitoring(
			@RequestParam("year") String year,
			@RequestParam("byQuarters") boolean byQuarters
	) {
		return monitoringService.getHealthcareMonitoringByQuartersOfYear(year);
	} 
	
	@GetMapping(value = "/healthcare", params = {"byYears"})
	public List<HealthcareMonitoring3> getHealthcareMonitoring3(
			@RequestParam("byYears") boolean byYears
	) {
		return monitoringService.getHealthcareMonitoring3();
	}
	
//	BUDGET
	
	@GetMapping("/budget")
	public List<BudgetMonitoring> getBudgetMonitoring() {
		return monitoringService.getBudgetMonitoring();
	}

//	Land Realtions

	@GetMapping(value = "/land-relations",  params = "year")
	public List<LandRelations> getLandRelations(@RequestParam("year") String year) {
		return monitoringService.getLandRelations(year);
	}

	@GetMapping("/land/years")
	public List<String> getLandYears() {
		return monitoringService.getLandYears();
	}

//	Architecture
	
	@GetMapping(value = "/architecture", params = "year")
	public List<ArchitectureMonitoring> getArchitecture(@RequestParam("year") String year) {
		return monitoringService.getArchitectureByYear(year);
	}
	

	@GetMapping("/architecture/years")
	public List<String> getArchitectureYears() {
		return monitoringService.getArchitectureYears();
	}
	
//	Goverment purchases

	@GetMapping(value = "/goverment-purchases", params = "year")
	public List<GovermentPurchases> getGovermentPurchasesByYear(@RequestParam("year") String year) {
		return monitoringService.getGovermentPurchasesByYear(year);
	}
	
	@GetMapping(value = "/goverment-purchases/years")
	public List<String> getGovermentPurchasesYears() {
		return monitoringService.getGovermentPurchasesYears();
	}
	
//	Construction
	
	@GetMapping(value = "/constructions", params = "year")
	public List<ConstructionMonitoring> getConstructionsByYear(@RequestParam("year") String year) {
		return monitoringService.getConstructionsByYear(year);
	}
	
	@GetMapping("/constructions/years")
	public List<String> getConstructionYears() {
		return monitoringService.getConstructionYears();
	}
	
//	State Labor Inspectorate aao
	
	@GetMapping(value = "/state-labor-inspectorate", params = "year")
	public List<StateLaborInspectorate> getStateLaborInspectorateByYear(@RequestParam("year") String year) {
		return monitoringService.getStateLaborInspectorateByYear(year);
	}
	

	@GetMapping("/state-labor-inspectorate/years")
	public List<String> getStateLaborInspectorateYears() {
		return monitoringService.getStateLaborInspectorateYears();
	}



//	State Labor Inspectorate okbot

	@GetMapping(value = "/stateokbot", params = "year")
	public List<StateLaborInspectorateOkbotMonitoring> getStateLaborInspectorateOkbotMonitoringByYear(@RequestParam("year") String year) {
		return monitoringService.getStateLaborInspectorateOkbotMonitoringByYear(year);
	}


	@GetMapping("/stateokbot/years")
	public List<String> getStateLaborInspectorateOkbotMonitoringYears() {
		return monitoringService.getStateLaborInspectorateOkbotMonitoringYears();
	}

//	State Labor Inspectorate okto

	@GetMapping(value = "/stateokto", params = "year")
	public List<StateLaborInspectorateOktoMonitoring> getStateLaborInspectorateOktoMonitoringByYear(@RequestParam("year") String year) {
		return monitoringService.getStateLaborInspectorateOktoMonitoringByYear(year);
	}


	@GetMapping("/stateokto/years")
	public List<String> getStateLaborInspectorateOktoMonitoringYears() {
		return monitoringService.getStateLaborInspectorateOktoMonitoringYears();
	}






//	Education
	
	@GetMapping("/education")
	public List<EducationMonitoring> getEducationStatistics() {
		return monitoringService.getEducationStatistics();
	}
	
//	Religion
	
	@GetMapping(value = "/religion", params = "year")
	public List<ReligionMonitoring> getReligionByYear(@RequestParam String year) {
		return monitoringService.getReligionByYear(year);
	}
	
	@GetMapping("religion/years")
	public List<String> getReligionYears() {
		return monitoringService.getReligionYears();
	}
	
//	Control Over Land Use Monitoring
	
	@GetMapping(value = "/control-over-land", params = "year")
	public List<ControlOverLandUseMonitoring> getControlOverLandUseMonitoringByYear(@RequestParam String year) {
		return monitoringService.getControlOverLandUseMonitoringByYear(year);
	}
	
	@GetMapping("/control-over-land/years")
	public List<String> getControlOverLandUseMonitoringYears() {
		return monitoringService.getControlOverLandUseMonitoringYears();
	}
	
//	Economic
	
	@GetMapping(value = "/economy", params = {"year", "byMonthes"})
	public List<EconomicMonitoring> getEconomicMonthesByYear(
			@RequestParam("year") String year,
			@RequestParam("byMonthes") boolean byMonthes
	) {
		return monitoringService.getEconomicMonthesByYear(year);
	}
	
	@GetMapping("/economy/years")
	public Map<String, List<String>> geteconomicYears() {
		return monitoringService.geteconomicYears();
	}
	
	@GetMapping(value = "/economy", params = {"year", "byQuarters"})
	public List<EconomicMonitoring2> getEconomicQuartersByYear(
			@RequestParam("year") String year,
			@RequestParam("byQuarters") boolean byQuarters
	) {
		return monitoringService.getEconomicQuartersByYear(year);
	}
	
//	Transport
	
	@GetMapping(value = "transport", params = {"year", "byMonthes"})
	public List<TransportMonitoring> getTransportMonthesByYear(
			@RequestParam("year") String year,
			@RequestParam("byMonthes") boolean byMonthes
	) {
		return monitoringService.getTransportMonthesByYear(year);
	}
	
	@GetMapping("transport/years")
	public List<String> getTransportYears() {
		return monitoringService.getTransportYears();
	}
	
	@GetMapping(value = "transport", params = "byYears")
	public List<TransportMonitoring2> getTransportsByYears( @RequestParam("byYears") boolean byYears) {
		return monitoringService.getTransportsByYears();
	}


	//	EnergetikaIJkx

	@GetMapping(value = "/energetika", params = {"year", "byMonthes"})
	public List<EnergetikaIJxk> getEnergetikaIJKXMonthesByYear(
			@RequestParam("year") String year,
			@RequestParam("byMonthes") boolean byMonthes
	) {
		return monitoringService.getEnergetikaIJkxMonthesByYears(year);
	}

	@GetMapping("/energetika/years")
	public List<String> getEnergetikaIJKXYears() {
		return monitoringService.getEnergetikaIJkxYears();
	}

	@GetMapping(value = "energetika", params = "byYears")
	public List<EnergetikaIJkx2> getEnergetikaIJkxsByYears (@RequestParam("byYears") boolean byYears) {
		return monitoringService.getEnergetikaIJkxsByYears();
	}



//	Agricultural Industry (Сель хоз)
	
	@GetMapping(value = "/agricultural-industry", params = "year")
	public List<AgriculturalIndustryMonitoring> getAgriculturalIndustryByYear(@RequestParam String year) {
		return monitoringService.getAgriculturalIndustryByYear(year);
	}
	
	@GetMapping("/agricultural-industry/years")
	public List<String> getAgriculturalIndustryYears() {
		return monitoringService.getAgriculturalIndustryYears();
	}
	
//	Veterenary
	
	@GetMapping(value = "/veterenary", params = "year")
	public List<VeterenaryMonitoring> getVeterenaryByYears(@RequestParam String year) {
		return monitoringService.getVeterenaryByYear(year);
	}
	
	@GetMapping("/veterenary/years")
	public List<String> getVeterenaryYears() {
		return monitoringService.getVeterenaryYears();
	}

	//Architectura2
	@GetMapping(value = "/architec", params = {"year"})
	public List<Architectura2> getArchitecByYear(String year){
		return monitoringService.getArchitectura2ByYear(year);
	}

	@GetMapping("/architec/years")
	public List<String> getgetArchitectura2Years() {
		return monitoringService.getArchitectura2Years();
	}


//Koordination

	@GetMapping(value = "/koord", params ={"byYears"})
		public List<KoordyearMonitoring>getKoordyearMonitoring(
				@RequestParam("byYears") boolean byYears
	){
		return monitoringService.getKoordyearMonitoring();
	}

//Koordquatr

	@GetMapping("/koord/units")
	public KoordquartMonitoringUnits getKoordquartMonitoringUnits(){
		return monitoringService.getKoordquartMonitoringUnits();
	}

	@GetMapping(value = "/koord/years")
	public Map<String, String[]>getKoordinYears(){
		return monitoringService.getKoordinYears();
	}

	@GetMapping(value = "/koord", params ={"year", "byQuarters"})
	public KoordquartMonitoring getQuatersKoordMonitoring(
			@RequestParam("year") String year,
			@RequestParam("byQuarters") boolean byQuarters
	) {
		return monitoringService.getKoordquartMonitoringByQuatersOfYear(year);
	}

//Koordmoth

	@GetMapping(value = "/koord", params = {"year", "byMonthes"})
	public KoordmonthMonitoring getKoordmonthMonitoring(
			@RequestParam("year") String year,
			@RequestParam("byMonthes") boolean byMonthes
	){
		return monitoringService.getKoordmonthMonitoringByYear(year);
	}

//Promyshlennost

	@GetMapping(value = "/prom", params = {"byYears"})
		public List<PromyshlennostYearMonitoring>getPromyshlennostYearMonitoring(
				@RequestParam("byYears") boolean byYears
	){
		return monitoringService.getPromByYears();
	}

//PromYear2

	@GetMapping(value = "/proms", params = {"byYears"})
		public List<PromyshlennostYear2Monitoring>getPromshlennostYear2Monitoring(
				@RequestParam("byYears") boolean byYears
	){
		return monitoringService.getProm2ByYears();
	}

//Promquatr

//	@GetMapping("/koord/units")
//	public KoordquartMonitoringUnits getKoordquartMonitoringUnits(){
//		return monitoringService.getKoordquartMonitoringUnits();
//	}
//
	@GetMapping(value = "/pr/years")
	public Map<String, String[]>getProminYears(){
		return monitoringService.getProminYears();
	}

	@GetMapping(value = "/prom", params ={"year", "byQuarters"})
	public PromyshlennostQuaterMonitoring getQuatersPromMonitoring(
			@RequestParam("year") String year,
			@RequestParam("byQuarters") boolean byQuarters
	) {
		return monitoringService.getPromyshlennostquartMonitoringByQuatersOfYear(year);
	}

//Promquatr_2

	@GetMapping(value = "/promy", params ={"year", "byQuarters"})
	public PromQuaterOropMonitoring getQuatersPromsMonitoring(
			@RequestParam("year") String year,
			@RequestParam("byQuarters") boolean byQuarters
	) {
		return monitoringService.getPromquartMonitoringByQuatersOfYear(year);
	}

//Promq_ort

	@GetMapping(value = "/promys", params ={"year", "byQuarters"})
	public PromQuaterOrtMonitoring getQuatersPromOrtMonitoring(
			@RequestParam("year") String year,
			@RequestParam("byQuarters") boolean byQuarters
	) {
		return monitoringService.getPromquartOrtMonitoringByQuatersOfYear(year);
	}

//Promq_ovs

	@GetMapping(value = "/promysh", params ={"year", "byQuarters"})
	public PromQuaterOvsMonitoring getQuatersPromOvsMonitoring(
			@RequestParam("year") String year,
			@RequestParam("byQuarters") boolean byQuarters
	) {
		return monitoringService.getPromquartOvsMonitoringByQuatersOfYear(year);
	}

//Prom_moth

	@GetMapping(value = "/prom", params = {"year", "byMonthes"})
	public PromMMonitoring getPrommonthMonitoring(
			@RequestParam("year") String year,
			@RequestParam("byMonthes") boolean byMonthes
	){
		return monitoringService.getPromSmonthMonitoringByYear(year);
	}

//Prom_m_oppr

    @GetMapping(value = "/promoppr", params = {"year", "byMonthes"})
    public PromMOpprMonitoring getPrommonthOpprMonitoring(
            @RequestParam("year") String year,
            @RequestParam("byMonthes") boolean byMonthes
    ){
        return monitoringService.getPromSmonthOpprMonitoringByYear(year);
    }




//Natural

	@GetMapping(value = "/natural", params = {"byYears"})
		public  List<NaturalResourcesYearMonitoring>getNaturalResourcesYearMonitoring(
				@RequestParam("byYears") boolean byYears
	){
		return monitoringService.getNaturalByYears();
	}

//NaturalYear2

	@GetMapping(value = "/naturals" , params = {"byYears"})
		public List<NaturalResourcesYear2Monitoring>getNaturalResourcesYear2Monitoring(
				@RequestParam("byYears") boolean byYears
	){
		return monitoringService.getNatural2ByYears();
	}

//Nat_q_opp

	@GetMapping(value = "/natopp", params ={"year", "byQuarters"})
	public NaturalResQuarterOppMonitoring getQuatersNaturalResQuarterOppMonitoring(
			@RequestParam("year") String year,
			@RequestParam("byQuarters") boolean byQuarters
	) {
		return monitoringService.getNaturalResQuarterOppMonitoringByQuatersOfYear(year);
	}

//Nat_q_oloh

	@GetMapping(value = "/natoloh", params ={"year", "byQuarters"})
	public NaturalResQuarterOlohMonitoring getQuatersNaturalResQuarterOlohMonitoring(
			@RequestParam("year") String year,
			@RequestParam("byQuarters") boolean byQuarters
	) {
		return monitoringService.getNaturalResQuarterOlohMonitoringByQuatersOfYear(year);
	}

//Nat_q_ogeevr

	@GetMapping(value = "/natogeevr", params ={"year", "byQuarters"})
	public NaturalResQuarterOgeevrMonitoring getQuatersNaturalResQuarterOgeevrMonitoring(
			@RequestParam("year") String year,
			@RequestParam("byQuarters") boolean byQuarters
	) {
		return monitoringService.getNaturalResQuarterOgeevrMonitoringByQuatersOfYear(year);
	}

//Nat_Half_oapkr

	@GetMapping(value = "/nathoapkr", params ={"year", "byHalfs"})
	public NatRhyOapkrMonitoring getHalfsNatRhyHalfOapkrMonitoring(
			@RequestParam("year") String year,
			@RequestParam("byHalfs") boolean byHalfs
	) {
		return monitoringService.getNatRhyOapkrMonitoringByHalfOfYear(year);
	}

//Nat_hy_ogeevr

	@GetMapping(value = "/nathogeevr", params ={"year", "byHalfs"})
	public NatRhyOgeevrMonitoring getHalfsNatRhyHalfOgeevrMonitoring(
			@RequestParam("year") String year,
			@RequestParam("byHalfs") boolean byHalfs
	) {
		return monitoringService.getNatRhyOgeevrMonitoringByHalfOfYear(year);
	}

//Nat_hy_oloh

	@GetMapping(value = "/natholoh", params ={"year", "byHalfs"})
	public NatRhyOlohMonitoring getHalfsNatRhyHalfOlohMonitoring(
			@RequestParam("year") String year,
			@RequestParam("byHalfs") boolean byHalfs
	) {
		return monitoringService.getNatRhyOlohMonitoringByHalfOfYear(year);
	}

//Nat_hy_onvh

	@GetMapping(value = "/nathonvh", params ={"year", "byHalfs"})
	public NatRhyOnvhMonitoring getHalfsNatRhyHalfOnvhMonitoring(
			@RequestParam("year") String year,
			@RequestParam("byHalfs") boolean byHalfs
	) {
		return monitoringService.getNatRhyOnvhMonitoringByHalfOfYear(year);
	}

//Nat_hy_opp

	@GetMapping(value = "/nathopp", params ={"year", "byHalfs"})
	public NatRhyOppMonitoring getHalfsNatRhyHalfOppMonitoring(
			@RequestParam("year") String year,
			@RequestParam("byHalfs") boolean byHalfs
	) {
		return monitoringService.getNatRhyOppMonitoringByHalfOfYear(year);
	}

	@GetMapping(value = "/nat/years")
	public Map<String, String[]>getNatinYears(){
		return monitoringService.getNatinYears();
	}





//Molodejnaya

	@GetMapping(value = "/molodejnaya", params = {"byYears"})
		public List<MolodejnayaPolitikaYearMonitoring>getMolodejnayaPolitikaYearMonitoring(
				@RequestParam("byYears") boolean byYears
	){
		return monitoringService.getMolodejnayaByYears();
	}

//MolodejP_q

	@GetMapping(value = "/molodejq", params ={"year", "byQuarters"})
	public MolodejPQMonitoring getQuatersMolodejPQMonitoring(
			@RequestParam("year") String year,
			@RequestParam("byQuarters") boolean byQuarters
	) {
		return monitoringService.getMolodejPQMonitoringByQuatersOfYear(year);
	}

//MolP_hy

	@GetMapping(value = "/molodejhy", params ={"year", "byHalfs"})
	public MolPhyMonitoring getHalfsMolPhyHalfMonitoring(
			@RequestParam("year") String year,
			@RequestParam("byHalfs") boolean byHalfs
	) {
		return monitoringService.getMolPhyMonitoringByHalfOfYear(year);
	}





//InternalP_q

	@GetMapping(value = "/internalq", params ={"year", "byQuarters"})
	public InternalPQMonitoring getQuatersInternalPQMonitoring(
			@RequestParam("year") String year,
			@RequestParam("byQuarters") boolean byQuarters
	) {
		return monitoringService.getInternalPQMonitoringByQuatersOfYear(year);
	}

//IntP_q_hy

	@GetMapping(value = "/intqhy", params ={"year", "byQuarters"})
	public InternalPhyMonitoring getQuatersInternalPhyMonitoring(
			@RequestParam("year") String year,
			@RequestParam("byQuarters") boolean byQuarters
	) {
		return monitoringService.getInternalPhyMonitoringByQuatersOfYear(year);
	}

	@GetMapping(value = "/inte/years")
	public Map<String, String[]>getInterinYears(){
		return monitoringService.getInterinYears();
	}

//LanguageDevelopmentYear

	@GetMapping(value = "/lang", params = {"byYears"})
		public List<LanguagesDevelopmentYearMonitoring>getLanguagesDevelopmentYearMonitoring(
				@RequestParam("byYears") boolean byYears
	){
		return monitoringService.getLangDevYearByYears();
	}

//LangDev_q

	@GetMapping(value = "/langq", params ={"year", "byQuarters"})
	public LanguagesDevQMonitoring getQuatersLanguagesDevQMonitoring(
			@RequestParam("year") String year,
			@RequestParam("byQuarters") boolean byQuarters
	) {
		return monitoringService.getLanguagesDevQMonitoringByQuatersOfYear(year);
	}

	@GetMapping(value = "/lang/years")
	public Map<String, String[]>getLanginYears(){
		return monitoringService.getLanginYears();
	}

//Archive_hy

	@GetMapping(value = "/archy", params ={"year", "byHalfs"})
	public ArchiveHyMonitoring getHalfsArchivehyHalfMonitoring(
			@RequestParam("year") String year,
			@RequestParam("byHalfs") boolean byHalfs
	) {
		return monitoringService.getArchivehyMonitoringByHalfOfYear(year);
	}

//Kult_hy

	@GetMapping(value = "/kulthy", params ={"year", "byHalfs"})
	public KulturahyMonitoring getHalfsKulturahyHalfMonitoring(
			@RequestParam("year") String year,
			@RequestParam("byHalfs") boolean byHalfs
	) {
		return monitoringService.getKulturahyMonitoringByHalfOfYear(year);
	}


//Fizkult

	@GetMapping("/fiz")
	public List<FizMonitoring> getFizMonitoring() {
		return monitoringService.getFizMonitoring();
	}

//	Nat_Month

	@GetMapping(value = "/natoap", params = {"year", "byMonthes"})
	public List<NatMonthOapikrMonitoring> getNatMonthOapikrMonthByYear(
			@RequestParam("year") String year,
			@RequestParam("byMonthes") boolean byMonthes
	) {
		return monitoringService.getNatMonthOapikrMonthByYear(year);
	}

	@GetMapping(value = "/natoge", params = {"year", "byMonthes"})
	public List<NatMonthOgeeivrMonitoring> getNatMonthOgeeivrMonthByYear(
			@RequestParam("year") String year,
			@RequestParam("byMonthes") boolean byMonthes
	) {
		return monitoringService.getNatMonthOgeeivrMonthByYear(year);
	}

	@GetMapping(value = "/natoni", params = {"year", "byMonthes"})
	public List<NatMonthOnivhMonitoring> getNatMonthOnivhMonthByYear(
			@RequestParam("year") String year,
			@RequestParam("byMonthes") boolean byMonthes
	) {
		return monitoringService.getNatMonthOnivhMonthByYear(year);
	}




}



