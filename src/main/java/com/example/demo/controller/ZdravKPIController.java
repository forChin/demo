package com.example.demo.controller;

import com.example.demo.domain.ZdravKCatalogMonitoring;
import com.example.demo.domain.ZdravKPIMonitoring;
import com.example.demo.domain.ZdravKPITableMonitoring;
import com.example.demo.domain.ZdravKSpisokMonitoring;
import com.example.demo.service.ZdravKPIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/statistics/zdravkpi")

public class ZdravKPIController {
    @Autowired
    ZdravKPIService zdravKPIService;

    @GetMapping(value = "/zdravkpitable", params = {"kindOfSport"})
    public List<ZdravKPITableMonitoring> getZdravKPITableMonitoringByKind(String kindOfSport){
        return zdravKPIService.getZdravKPITableMonitoringByKind(kindOfSport);
    }

    @GetMapping(value = "/ocenka", params = {"index"})
    public List<ZdravKCatalogMonitoring> getZdravKCatalogMonitoringByIndex(String index){
        return zdravKPIService.getZdravKCatalogMonitoringByIndex(index);
    }

    @GetMapping(value = "/catalog")
    public List<ZdravKCatalogMonitoring> getZdrav2KCatalogMonitoringByIndex(){
        return zdravKPIService.getZdrav2KCatalogMonitoringByIndex();
    }

    @GetMapping("/ocenka/index")
    public List<String> menukcatalogmonitoringIndex(){
        return zdravKPIService.getMenuKCatalogMonitoringIndex();
    }

    @GetMapping(value = "/spisok", params = {"raion"})
    public List<ZdravKSpisokMonitoring> getZdravKSpisokMonitoringByRaion(String raion){
        return zdravKPIService.getZdravKSpisokMonitoringByRaion(raion);
    }

    @GetMapping(value = "/spisok")
    public List<ZdravKSpisokMonitoring> getZdrav2KSpisokMonitoringByRaion(){
        return zdravKPIService.getZdrav2KSpisokMonitoringByRaion();
    }

    @GetMapping("/spisok/raion")
    public List<String> menukspisokmonitoringRaion(){
        return zdravKPIService.getMenuKSpisokMonitoringRaion();
    }

    @GetMapping("/spisok/rd")
    public List<String> menukspisokmonitoringRD(){
        return zdravKPIService.getMenuKSpisokMonitoringRD();
    }

    @GetMapping("/spisok/snp")
    public List<String> menukspisokmonitoringSnp(){
        return zdravKPIService.getMenuKSpisokMonitoringSnp();
    }

    @GetMapping(value = "/realkpi", params = {"region"})
    public List<ZdravKPIMonitoring> getZdravKPI2MonitoringByRegion(String region){
        return zdravKPIService.getZdravKPI2MonitoringByRegion(region);
    }

    @GetMapping("/realkpi/region")
    public List<String> menukpi2monitoringRegion(){
        return zdravKPIService.getMenuKPI2MonitoringRegions();
    }

    @GetMapping("/realkpi/years")
    public List<String> menukpi2monitoringYears(){
        return zdravKPIService.getMenuKPI2MonitoringYears();
    }

    @GetMapping(value = "/realkpi", params = {"year"})
    public List<ZdravKPIMonitoring> getZdravKPI3MonitoringByYear(String year){
        return zdravKPIService.getZdravKPI3MonitoringByYear(year);
    }

    @GetMapping(value = "/zdanie", params = {"raion", "ruralDistruct", "snp", "category"})
    public List<ZdravKSpisokMonitoring> getZdrav3KSpisokMonitoringByRaion(
            @RequestParam("raion") String raion,
            @RequestParam("ruralDistruct") String ruralDistruct,
            @RequestParam("snp") String snp,
            @RequestParam("category") String category
    ){
        return zdravKPIService.getZdrav3KSpisokMonitoringByRaion(raion, ruralDistruct, snp, category);
    }

}
