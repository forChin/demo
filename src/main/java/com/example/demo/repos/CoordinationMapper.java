package com.example.demo.repos;

import java.util.List;

import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.example.demo.domain.Coordination;

public interface CoordinationMapper {
	
	@Select("SELECT * FROM sed_y_koordin_raion WHERE o_year=#{year}")
	@Results(value={
		@Result(property = "year", column = "o_year"),
		@Result(property = "region", column = "o_region"),
		@Result(property = "area", column = "o_area"),
		@Result(property = "populationNumber", column = "o_number_population"),
		@Result(property = "countLabour", column="o_count_labour")
	})
	public List<Coordination> getLabourMarketByYear(String year);
	
}
