package com.example.demo.repos;

import com.example.demo.domain.St3Monitoring;
import com.example.demo.domain.St4Monitoring;
import com.example.demo.domain.StMonitoring;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface StMapper {

    @Select("SELECT * FROM strateg_upr_2")
    @Results({
            @Result(property = "raion", column = "o_raion"),
            @Result(property = "napravlenie", column = "o_napravlenie"),
            @Result(property = "razdel", column = "o_razdel"),
            @Result(property = "indikator", column = "o_indikator"),
            @Result(property = "edIzm", column = "o_ed_izm"),
            @Result(property = "number", column = "o_number"),
            @Result(property = "data", column = "o_data"),
            @Result(property = "fact", column = "o_fact"),
            @Result(property = "klPokazatel", column = "o_kl_pokazatel"),
            @Result(property = "tendenciya", column = "o_tendenciya"),
            @Result(property = "year", column = "o_year"),
            @Result(property = "nPeriod", column = "o_n_period"),
            @Result(property = "periodi4nost", column = "o_periodi4nost"),
            @Result(property = "period", column = "o_period"),
            @Result(property = "plan", column = "o_plan"),
            @Result(property = "raspred", column = "o_raspred")
    })
    public List<StMonitoring> getStMonitoring();


    @Select("SELECT DISTINCT o_raion FROM strateg_upr_2 ORDER BY o_raion")
    public List<String> getStMonitoringRaions();

    @Select("SELECT DISTINCT o_year FROM strateg_upr_2 ORDER BY o_year")
    public List<String> getStMonitoringYears();

    @Select("SELECT DISTINCT o_period FROM strateg_upr_2 ORDER BY o_period")
    public List<String> getStMonitoringPeriods();

    @Select("SELECT * FROM strateg_upr_2 where o_raion=#{raion} AND o_year=#{year} AND o_period=#{period}")
    @Results({
            @Result(property = "raion", column = "o_raion"),
            @Result(property = "napravlenie", column = "o_napravlenie"),
            @Result(property = "razdel", column = "o_razdel"),
            @Result(property = "indikator", column = "o_indikator"),
            @Result(property = "edIzm", column = "o_ed_izm"),
            @Result(property = "number", column = "o_number"),
            @Result(property = "data", column = "o_data"),
            @Result(property = "fact", column = "o_fact"),
            @Result(property = "klPokazatel", column = "o_kl_pokazatel"),
            @Result(property = "tendenciya", column = "o_tendenciya"),
            @Result(property = "year", column = "o_year"),
            @Result(property = "nPeriod", column = "o_n_period"),
            @Result(property = "periodi4nost", column = "o_periodi4nost"),
            @Result(property = "period", column = "o_period"),
            @Result(property = "plan", column = "o_plan"),
            @Result(property = "raspred", column = "o_raspred")
    })
    public List<StMonitoring> getStMonitoringByRaion(String raion, String year, String period);

    @Select("SELECT * FROM strateg_upr_3")
    @Results({
            @Result(property = "raion", column = "o_raion"),
            @Result(property = "napravlenie", column = "o_napravlenie"),
            @Result(property = "otrasl", column = "o_otrasl"),
            @Result(property = "pokazatel", column = "o_pokazatel"),
            @Result(property = "edIzm", column = "o_ed_izm"),
            @Result(property = "year", column = "o_year"),
            @Result(property = "zna4enie", column = "o_zna4enie")
    })
    public List<St3Monitoring> getSt3Monitoring();


    @Select("SELECT * FROM strateg_upr_4")
    @Results({
            @Result(property = "indikator1", column = "o_indikator_1"),
            @Result(property = "indikator2", column = "o_indikator_2"),
            @Result(property = "corr", column = "o_corr"),
            @Result(property = "stepenVl", column = "o_stepen_vl"),
            @Result(property = "napravVl", column = "o_naprav_vl")
    })
    public List<St4Monitoring> getSt4Monitoring();









}
