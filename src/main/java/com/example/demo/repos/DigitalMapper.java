package com.example.demo.repos;

import com.example.demo.domain.*;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface DigitalMapper {

    @Select("SELECT DISTINCT o_name AS name, o_industry AS industry, o_gategory AS gategory, " +
            "o_year_start AS yearStart, o_status_project AS statusProject, o_year_end AS yearEnd, " +
            "o_power AS power, o_unit AS unit, o_numer_objects AS numerObjects, o_budget_program AS budgetProgram, " +
            "o_customer AS customer, o_contractor AS contractor, o_region AS region, o_rural_district AS ruralDistrict, " +
            "o_rural_settlement AS ruralSettlement, o_latitude AS latitude, o_longitude AS longitude, o_number_population AS numberPopulation, " +
            "o_number_women AS numberWomen, o_number_man AS numberMan, o_numer_children AS numerChildren, o_total_project_cost AS totalProjectCost, o_unit_project AS unitProject, " +
            "o_year AS year, o_year_a AS yearA, o_local_budget AS localBudget, o_local_budget_a AS localBudgetA, o_republican_budget AS republicanBudget, o_republican_budget_a AS republicanBudgetA, o_other AS other, o_other_a AS otherA, o_source AS source, o_source_a AS sourceA, o_unit_a AS unitA, o_unit_a_a AS unitAA, " +
            "o_year_b AS yearB, o_year_c AS yearC, o_local_budget_b AS localBudgetB, o_local_budget_c AS localBudgetC, o_republican_budget_b AS republicanBudgetB, o_republican_budget_c AS republicanBudgetC, o_other_b AS otherB, o_other_c AS otherC, o_source_b AS sourceB, o_source_c AS sourceC, o_unit_a_b AS unitAB, o_unit_a_c AS unitAC, " +
            "o_year_d AS yearD, o_year_e AS yearE, o_local_budget_d AS localBudgetD, o_local_budget_e AS localBudgetE, o_republican_budget_d AS republicanBudgetD, o_republican_budget_e AS republicanBudgetE, o_other_d AS otherD, o_other_e AS otherE, o_source_d AS sourceD, o_source_e AS sourceE, o_unit_a_d AS unitAD, o_unit_a_e AS unitAE " +
            "FROM social_objects WHERE o_year_end=#{yearEnd} AND o_industry=#{industry} AND o_source=#{source}")
    public List<DigitalSocialObjectsMonitoring> getDigitalSocialObjectsMonitoringByYear(String yearEnd, String industry, String source);

    @Select("SELECT DISTINCT o_year_end FROM social_objects ORDER BY o_year_end")
    public List<String> getDigitalSocialObjectsMonitoringYearEnd();

    @Select("SELECT DISTINCT o_industry FROM social_objects ORDER BY o_industry")
    public List<String> getDigitalSocialObjectsMonitoringIndustry();

    @Select("SELECT DISTINCT o_source FROM social_objects ORDER BY o_source")
    public List<String> getDigitalSocialObjectsMonitoringSource();

    @Select("SELECT * FROM social_objects")
    @Results({
            @Result(property = "name", column = "o_name"),
            @Result(property = "industry", column = "o_industry"),
            @Result(property = "gategory", column = "o_gategory"),
            @Result(property = "yearStart", column = "o_year_start"),
            @Result(property = "statusProject", column = "o_status_project"),
            @Result(property = "yearEnd", column = "o_year_end"),
            @Result(property = "power", column = "o_power"),
            @Result(property = "unit", column = "o_unit"),
            @Result(property = "numerObjects", column = "o_numer_objects"),
            @Result(property = "budgetProgram", column = "o_budget_program"),
            @Result(property = "customer", column = "o_customer"),
            @Result(property = "contractor", column = "o_contractor"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "ruralDistrict", column = "o_rural_district"),
            @Result(property = "ruralSettlement", column = "o_rural_settlement"),
            @Result(property = "latitude", column = "o_latitude"),
            @Result(property = "longitude", column = "o_longitude"),
            @Result(property = "numberPopulation", column = "o_number_population"),
            @Result(property = "numberWomen", column = "o_number_women"),
            @Result(property = "numberMan", column = "o_number_man"),
            @Result(property = "numerChildren", column = "o_numer_children"),
            @Result(property = "totalProjectCost", column = "o_total_project_cost"),
            @Result(property = "unitProject", column = "o_unit_project"),
            @Result(property = "year", column = "o_year"),
            @Result(property = "localBudget", column = "o_local_budget"),
            @Result(property = "republicanBudget", column = "o_republican_budget"),
            @Result(property = "other", column = "o_other"),
            @Result(property = "source", column = "o_source"),
            @Result(property = "unitA", column = "o_unit_a")
    })
    public List<DigitalSocialObjectsMonitoring> getDigitalSocialObjectsMonitoring();

    @Select("SELECT * FROM diplom")
    @Results({
            @Result(property = "lastname", column = "o_lastname"),
            @Result(property = "firstname", column = "o_firstname"),
            @Result(property = "otchestvo", column = "o_otchestvo"),
            @Result(property = "idPerson", column = "o_id_person"),
            @Result(property = "pol", column = "o_pol"),
            @Result(property = "age", column = "o_age"),
            @Result(property = "areaFrom", column = "o_area_from"),
            @Result(property = "regionFrom", column = "o_region_from"),
            @Result(property = "ruralDistrictFrom", column = "o_rural_district_from"),
            @Result(property = "typeEducation", column = "o_type_education"),
            @Result(property = "profession", column = "o_profession"),
            @Result(property = "professionRus", column = "o_profession_rus"),
            @Result(property = "professionKz", column = "o_profession_kz"),
            @Result(property = "educationalInstitution", column = "o_educational_institution"),
            @Result(property = "areaIn", column = "o_area_in"),
            @Result(property = "regionIn", column = "o_region_in"),
            @Result(property = "ruralDistrictIn", column = "o_rural_district_in"),
            @Result(property = "ruralSettlementIn", column = "o_rural_settlement_in"),
            @Result(property = "workspace", column = "o_workspace"),
            @Result(property = "fieldActivity", column = "o_field_activity"),
            @Result(property = "activity", column = "o_activity"),
            @Result(property = "statusEmployee", column = "o_status_employee"),
            @Result(property = "commissionDecision", column = "o_commission_decision"),
            @Result(property = "contractNumber", column = "o_contract_number"),
            @Result(property = "sumGrant", column = "o_sum_grant"),
            @Result(property = "unit", column = "o_unit"),
            @Result(property = "needReturn", column = "o_need_return"),
            @Result(property = "refund", column = "o_refund"),
            @Result(property = "reasonReturn", column = "o_reason_return"),
            @Result(property = "contractSumm", column = "o_contract_summ"),
            @Result(property = "unitCredit", column = "o_unit_credit"),
            @Result(property = "balanceCredit", column = "o_balance_credit"),
            @Result(property = "reasonForRefund", column = "o_reason_for_refund"),
            @Result(property = "reasonProjectCompletion", column = "o_reason_project_completion"),
            @Result(property = "contractNumberA", column = "o_contract_number_a"),
            @Result(property = "dateBirthday", column = "o_date_birthday"),
            @Result(property = "dateGraduation", column = "o_date_graduation"),
            @Result(property = "arrivalDate", column = "o_arrival_date"),
            @Result(property = "dateDismissalTransfer", column = "o_date_dismissal_transfer"),
            @Result(property = "dataContract", column = "o_data_contract"),
            @Result(property = "dateReturn", column = "o_date_return"),
            @Result(property = "dateAgreementLoan", column = "o_date_agreement_loan"),
            @Result(property = "sbid", column = "sbid")
    })
    public List<DiplomMonitoring> getDiplomMonitoring();

    @Select("SELECT * FROM diplom_table")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "data", column = "o_data"),
            @Result(property = "summ", column = "o_summ"),
            @Result(property = "unit", column = "o_unit"),
            @Result(property = "sbid", column = "sbid")
    })
    public List<DiplomTableMonitoring> getDiplomTableMonitoring();

    @Select("SELECT * FROM snp_bd")
    @Results({
            @Result(property = "dname", column = "d_name"),
            @Result(property = "dregion", column = "d_region"),
            @Result(property = "draion", column = "d_raion"),
            @Result(property = "druralDistrict", column = "d_rural_district"),
            @Result(property = "dlatitude", column = "d_latitude"),
            @Result(property = "dlongitude", column = "d_longitude")
    })
    public List<DiplomSnpMonitoring> getDiplomSnpMonitoring();

    @Select("SELECT DISTINCT o_lastname AS lastname, o_firstname AS firstname, o_otchestvo AS otchestvo, " +
            "o_id_person AS idPerson, o_pol AS pol, o_age AS age, o_area_from AS areaFrom, o_region_from AS regionFrom, " +
            "o_rural_district_from AS ruralDistrictFrom, o_type_education AS typeEducation, o_profession AS profession, o_profession_rus AS professionRus, " +
            "o_profession_kz AS professionKz, o_educational_institution AS educationalInstitution, o_area_in AS areaIn, o_region_in AS regionIn, " +
            "o_rural_district_in AS ruralDistrictIn, o_rural_settlement_in AS ruralSettlementIn, o_workspace AS workspace, o_field_activity AS fieldActivity, " +
            "o_activity AS activity, o_status_employee AS statusEmployee, o_commission_decision AS commissionDecision, o_contract_number AS contractNumber, o_sum_grant AS sumGrant, " +
            "o_unit AS unit, o_need_return AS needReturn, o_refund AS refund, o_reason_return AS reasonReturn, o_contract_summ AS contractSumm, o_unit_credit AS unitCredit, " +
            "o_balance_credit AS balanceCredit, o_reason_for_refund AS reasonForRefund, o_reason_project_completion AS reasonProjectCompletion, o_contract_number_a AS contractNumberA, " +
            "o_date_birthday AS dateBirthday, o_date_graduation AS dateGraduation, o_arrival_date AS arrivalDate, o_date_dismissal_transfer AS dateDismissalTransfer, o_data_contract AS dataContract, " +
            "o_date_return AS dateReturn, o_date_agreement_loan AS dateAgreementLoan, sbid AS sbid " +
            "FROM diplom WHERE o_region_in=#{regionIn} AND o_field_activity=#{fieldActivity} AND o_type_education=#{typeEducation} AND o_status_employee=#{statusEmployee} AND o_pol=#{pol}")
    public List<DiplomMonitoring> getDiplomMonitoringByRegion(String regionIn, String fieldActivity, String typeEducation, String statusEmployee, String pol);

    @Select("SELECT DISTINCT o_region_in FROM diplom ORDER BY o_region_in")
    public List<String> getDiplomMonitoringRegionIn();

    @Select("SELECT DISTINCT o_field_activity FROM diplom ORDER BY o_field_activity")
    public List<String> getDiplomMonitoringFieldActivity();

    @Select("SELECT DISTINCT o_type_education FROM diplom ORDER BY o_type_education")
    public List<String> getDiplomMonitoringTypeEducation();

    @Select("SELECT DISTINCT o_status_employee FROM diplom ORDER BY o_status_employee")
    public List<String> getDiplomMonitoringStatusEmployee();

    @Select("SELECT DISTINCT o_pol FROM diplom ORDER BY o_pol")
    public List<String> getDiplomMonitoringPol();

    @Select("SELECT * FROM blok_ecological")
    @Results({
            @Result(property = "raion", column = "raion"),
            @Result(property = "ruralDistrict", column = "o_rural_district"),
            @Result(property = "ruralSettlement", column = "o_rural_settlement"),
            @Result(property = "report", column = "o_report"),
            @Result(property = "report2", column = "o_report2"),
            @Result(property = "itog", column = "o_itog"),
            @Result(property = "year", column = "o_year")
    })
    public List<BlokEcologicalMonitoring> getBlokEcologicalMonitoring();

    @Select("SELECT * FROM blok_economic")
    @Results({
            @Result(property = "raion", column = "raion"),
            @Result(property = "ruralDistrict", column = "o_rural_district"),
            @Result(property = "ruralSettlement", column = "o_rural_settlement"),
            @Result(property = "soilFertility", column = "o_soil_fertility"),
            @Result(property = "distanceFromMarkets", column = "o_distance_from_markets"),
            @Result(property = "presenceLargeEnterprise", column = "o_presence_large_enterprise"),
            @Result(property = "developmentAgriculturalProducts", column = "o_development_agricultural_products"),
            @Result(property = "countTractors", column = "o_count_tractors"),
            @Result(property = "countCombine", column = "o_count_combine"),
            @Result(property = "availabilityIrrigationsSystems", column = "o_availability_irrigations_systems"),
            @Result(property = "arable", column = "o_arable"),
            @Result(property = "pasturesNatural", column = "o_pastures_natural"),
            @Result(property = "pasturesForageCrops", column = "o_pastures_forage_crops"),
            @Result(property = "farmingA", column = "o_farming_a"),
            @Result(property = "farmingB", column = "o_farming_b"),
            @Result(property = "total", column = "o_total"),
            @Result(property = "year", column = "o_year")
    })
    public List<BlokEconomicMonitoring> getBlokEconomicMonitoring();


    @Select("SELECT * FROM blok_engineering")
    @Results({
            @Result(property = "raion", column = "raion"),
            @Result(property = "ruralDistrict", column = "o_rural_district"),
            @Result(property = "ruralSettlement", column = "o_rural_settlement"),
            @Result(property = "water2", column = "o_water2"),
            @Result(property = "roadDistrictCenter", column = "o_road_district_center"),
            @Result(property = "gas", column = "o_gas"),
            @Result(property = "electro", column = "o_electro"),
            @Result(property = "telephoneCommunication", column = "o_telephone_communication"),
            @Result(property = "itog", column = "o_itog"),
            @Result(property = "year", column = "o_year")
    })
    public List<BlokEngineeringMonitoring> getBlokEngineeringMonitoring();

    @Select("SELECT * FROM blok_social")
    @Results({
            @Result(property = "raion", column = "raion"),
            @Result(property = "ruralDistrict", column = "o_rural_district"),
            @Result(property = "ruralSettlement", column = "o_rural_settlement"),
            @Result(property = "populationConcentration", column = "o_population_concentration"),
            @Result(property = "educationSchools", column = "o_education_schools"),
            @Result(property = "preschoolInstitution", column = "o_preschool_institution"),
            @Result(property = "health", column = "o_health"),
            @Result(property = "employment", column = "o_employment"),
            @Result(property = "poverty", column = "o_poverty"),
            @Result(property = "itog", column = "o_itog"),
            @Result(property = "year", column = "o_year")
    })
    public List<BlokSocialMonitoring> getBlokSocialMonitoring();

    @Select("SELECT * FROM id_atyrau")
    @Results({
            @Result(property = "number", column = "number"),
            @Result(property = "snp", column = "snp")
    })
    public List<IdaMonitoring> getIdaMonitoring();

    @Select("SELECT * FROM id_inder")
    @Results({
            @Result(property = "number", column = "number"),
            @Result(property = "snp", column = "snp")
    })
    public List<IdinMonitoring> getIdinMonitoring();

    @Select("SELECT * FROM id_isatay")
    @Results({
            @Result(property = "number", column = "number"),
            @Result(property = "snp", column = "snp")
    })
    public List<IdisMonitoring> getIdisMonitoring();

    @Select("SELECT * FROM id_kurmangazy")
    @Results({
            @Result(property = "number", column = "number"),
            @Result(property = "snp", column = "snp")
    })
    public List<IdkuMonitoring> getIdkuMonitoring();

    @Select("SELECT * FROM id_kyzylkoga")
    @Results({
            @Result(property = "number", column = "number"),
            @Result(property = "snp", column = "snp")
    })
    public List<IdkyMonitoring> getIdkyMonitoring();

    @Select("SELECT * FROM id_mahambet")
    @Results({
            @Result(property = "number", column = "number"),
            @Result(property = "snp", column = "snp")
    })
    public List<IdmahMonitoring> getIdmahMonitoring();

    @Select("SELECT * FROM id_makat")
    @Results({
            @Result(property = "number", column = "number"),
            @Result(property = "snp", column = "snp")
    })
    public List<IdmakMonitoring> getIdmakMonitoring();

    @Select("SELECT * FROM id_zhylyoi")
    @Results({
            @Result(property = "number", column = "number"),
            @Result(property = "snp", column = "snp")
    })
    public List<IdzhMonitoring> getIdzhMonitoring();


    @Select("SELECT * FROM snp_bd")
    @Results({
            @Result(property = "name", column = "d_name"),
            @Result(property = "region", column = "d_region"),
            @Result(property = "raion", column = "d_raion"),
            @Result(property = "ruralDistrict", column = "d_rural_district"),
            @Result(property = "latitude", column = "d_latitude"),
            @Result(property = "longitude", column = "d_longitude")
    })
    public List<MenuSnpMonitoring> getMenuSnpMonitoring();


}
