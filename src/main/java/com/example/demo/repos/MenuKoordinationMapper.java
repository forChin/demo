package com.example.demo.repos;

import com.example.demo.domain.*;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface MenuKoordinationMapper {

    @Select("SELECT * FROM sed_y_koordin_region where o_year=#{year}")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "minimumWage", column = "o_minimum_wage"),
            @Result(property = "minimumPension", column = "o_minimum_pension"),
            @Result(property = "livingWage", column = "o_living_wage"),
            @Result(property = "sizeBasicPensionPayment", column = "o_size_basic_pension_payment"),
            @Result(property = "monthlyCalculationIndex", column = "o_monthly_calculation_index"),
            @Result(property = "numberOne", column = "o_number_one"),
            @Result(property = "amountPaymentsOne", column = "o_amount_payments_one"),
            @Result(property = "totalBenefitsOne", column = "o_total_benefits_one"),
            @Result(property = "numberTwo", column = "o_number_two"),
            @Result(property = "amountPaymentsTwo", column = "o_amount_payments_two"),
            @Result(property = "totalBenefitsTwo", column = "o_total_benefits_two"),
            @Result(property = "numberWithOneDisabled", column = "o_number_with_one_disabled"),
            @Result(property = "amountBenefitsWithOneDisabled", column = "o_amount_benefits_with_one_disabled"),
            @Result(property = "totalBenefitsWithOneDisabled", column = "o_total_benefits_with_one_disabled"),
            @Result(property = "numberWithSixDisabled", column = "o_number_with_six_disabled"),
            @Result(property = "amountBenefitsWithSixDisabled", column = "o_amount_benefits_with_six_disabled"),
            @Result(property = "totalBenefitsWithSixDisabled", column = "o_total_benefits_with_six_disabled"),
            @Result(property = "invalidsTheFirstAmount", column = "o_invalids_the_first_amount"),
            @Result(property = "invalidsFirstAmountTotal", column = "o_invalids_first_amount_total"),
            @Result(property = "invalidsSecond", column = "o_invalids_second"),
            @Result(property = "invalidsSecondAmount", column = "o_invalids_second_amount"),
            @Result(property = "invalidsSecondAmountTotal", column = "o_invalids_second_amount_total"),
            @Result(property = "invalidsThird", column = "o_invalids_third"),
            @Result(property = "invalidsThirdAmount", column = "o_invalids_third_amount"),
            @Result(property = "invalidsThirdAmountAmountTotal", column = "o_invalids_third_amount_amount_total"),
            @Result(property = "invalidsChildren", column = "o_invalids_children"),
            @Result(property = "amountInvalidsChildren", column = "o_amount_invalids_children"),
            @Result(property = "totalInvalidsChildren", column = "o_total_invalids_children"),
            @Result(property = "childbirthAllowance", column = "o_childbirth_allowance"),
            @Result(property = "amountMaternityBenefits", column = "o_amount_maternity_benefits"),
            @Result(property = "totalAmountMaternityBenefits", column = "o_totalamount_maternity_benefits"),
            @Result(property = "childbirthAllowanceA", column = "o_childbirth_allowance_a"),
            @Result(property = "amountMaternityBenefitsA", column = "o_amount_maternity_benefits_a"),
            @Result(property = "totalAmountMaternityBenefitsA", column = "o_totalamount_maternity_benefits_a"),
            @Result(property = "invalidsFirstGroup", column = "o_invalids_first_group"),
            @Result(property = "amountInvalidsFirstGroup", column = "o_amount_invalids_first_group"),
            @Result(property = "totalInvalidsFirstGroup", column = "o_total_invalids_first_group"),
            @Result(property = "invalidsSecondGroup", column = "o_invalids_second_group"),
            @Result(property = "amountInvalidsSecondGroup", column = "o_amount_invalids_second_group"),
            @Result(property = "totalInvalidsSecondGroup", column = "o_total_invalids_second_group"),
            @Result(property = "invalidsThirdGroup", column = "o_invalids_third_group"),
            @Result(property = "amountInvalidsThirdGroup", column = "o_amount_invalids_third_group"),
            @Result(property = "totalInvalidsThirdGroup", column = "o_total_invalids_third_group"),
            @Result(property = "largeMothersAllowance", column = "o_large_mothers_allowance"),
            @Result(property = "amountLargeMothersAllowance", column = "o_amount_large_mothers_allowance"),
            @Result(property = "totalLargeMothersAllowance", column = "o_total_large_mothers_allowance"),
            @Result(property = "allowanceRaisingDisabledChild", column = "o_allowance_raising_disabledchild"),
            @Result(property = "amountAllowanceRaiSingDisabledChild", column = "o_amount_allowance_raising_disabledchild"),
            @Result(property = "totalAllowanceRaisingDisabledChild", column = "o_total_allowance_raising_disabledchild"),
            @Result(property = "disabledCareAllowance", column = "o_disabled_care_allowance"),
            @Result(property = "amountDisabledCareAllowance", column = "o_amount_disabled_care_allowance"),
            @Result(property = "totalDisabledCareAllowance", column = "o_total_disabled_care_allowance"),
            @Result(property = "largeFamilyAllowance4Children", column = "o_large_family_allowance_4_children"),
            @Result(property = "amountLargeFamilyAllowance4Children", column = "o_amount_large_family_allowance_4_children"),
            @Result(property = "totalLargeFamilyAllowance4Children", column = "o_total_large_family_allowance_4_children"),
            @Result(property = "largeFamilyAllowance5Children", column = "o_large_family_allowance_5_children"),
            @Result(property = "amountLargeFamilyAllowance5Children", column = "o_amount_large_family_allowance_5_children"),
            @Result(property = "totalLargeFamilyAllowance5Children", column = "o_total_large_family_allowance_5_children"),
            @Result(property = "largeFamilyAllowance6Children", column = "o_large_family_allowance_6_children"),
            @Result(property = "amountLargeFamilyAllowance6Children", column = "o_amount_large_family_allowance_6_children"),
            @Result(property = "totalLargeFamilyAllowance6Children", column = "o_total_large_family_allowance_6_children"),
            @Result(property = "largeFamilyAllowance7Children", column = "o_large_family_allowance_7_children"),
            @Result(property = "amountLargeFamilyAllowance7Children", column = "o_amount_large_family_allowance_7_children"),
            @Result(property = "totalLargeFamilyAllowance7Children", column = "o_total_large_family_allowance_7_children"),
            @Result(property = "burialGrants", column = "o_burial_grants"),
            @Result(property = "amountBurialGrants", column = "o_amount_burial_grants"),
            @Result(property = "totalBurialGrants", column = "o_total_burial_grants"),
            @Result(property = "burialAllowancesParticipantsVov", column = "o_burial_allowances_participants_vov"),
            @Result(property = "amountBurialAllowancesParticipantsVov", column = "o_amount_burial_allowances_participants_vov"),
            @Result(property = "totalBurialAllowancesParticipantsVov", column = "o_total_burial_allowances_participants_vov"),
            @Result(property = "invalidsParticipantsVov", column = "o_invalids_participants_vov"),
            @Result(property = "amountInvalidsParticipantsVov", column = "o_amount_invalids_participants_vov"),
            @Result(property = "totalInvalidsParticipantsVov", column = "o_total_invalids_participants_vov"),
            @Result(property = "personsEquatedDisabledVov", column = "o_persons_equated_disabled_vov"),
            @Result(property = "amountPersonsEquatedDisabledVov", column = "o_amount_persons_equated_disabled_vov"),
            @Result(property = "totalPersonsEquatedDisabledVov", column = "o_total_persons_equated_disabled_vov"),
            @Result(property = "personsEquatedParticipants", column = "o_persons_equated_participants"),
            @Result(property = "amountPersonsEquatedParticipants", column = "o_amount_persons_equated_participants"),
            @Result(property = "totalPersonsEquatedParticipants", column = "o_total_persons_equated_participants"),
            @Result(property = "vdovamPogibshikh", column = "o_vdovam_pogibshikh"),
            @Result(property = "amountVdovamPogibshikh", column = "o_amount_vdovam_pogibshikh"),
            @Result(property = "totalVdovamPogibshikh", column = "o_total_vdovam_pogibshikh"),
            @Result(property = "abc", column = "o_abc"),
            @Result(property = "amountAbc", column = "o_amount_abc"),
            @Result(property = "totalAbc", column = "o_total_abc"),
            @Result(property = "benefitsFrontWorkers", column = "o_benefits_front_workers"),
            @Result(property = "amountBenefitsFrontWorkers", column = "o_amount_benefits_front_workers"),
            @Result(property = "totalBenefitsFrontWorkers", column = "o_total_benefits_front_workers"),
            @Result(property = "abcd", column = "o_abcd"),
            @Result(property = "amountAbcd", column = "o_amount_abcd"),
            @Result(property = "totalAbcd", column = "o_total_abcd"),
            @Result(property = "individualsAwardedTitles", column = "o_individuals_awarded_titles"),
            @Result(property = "amountIndividualsAwardedTitles", column = "o_amount_individuals_awarded_titles"),
            @Result(property = "totalIndividualsAwardedTitles", column = "o_total_individuals_awarded_titles"),
            @Result(property = "heroesSocialistLabor", column = "o_heroes_socialist_labor"),
            @Result(property = "amountHeroesSocialistLabor", column = "o_amount_heroes_socialist_labor"),
            @Result(property = "totalHeroesSocialistLabor", column = "o_total_heroes_socialist_labor"),
            @Result(property = "victimsPoliticalRepression", column = "o_victims_political_repression"),
            @Result(property = "amountVictimsPoliticalRepression", column = "o_amount_victims_political_repression"),
            @Result(property = "totalVictimsPoliticalRepression", column = "o_total_victims_political_repression"),
            @Result(property = "personalPensioners", column = "o_personal_pensioners"),
            @Result(property = "amountPersonalPensioners", column = "o_amount_personal_pensioners"),
            @Result(property = "totalPersonalPensioners", column = "o_total_personal_pensioners"),
            @Result(property = "workPermitIssued", column = "o_work_permit_issued"),
            @Result(property = "allocatedQuotas", column = "o_allocated_quotas"),
            @Result(property = "unemploymentRate", column = "o_unemployment_rate"),
            @Result(property = "youthUnemploymentRate", column = "o_youth_unemployment_rate"),
            @Result(property = "vacantNewJobs", column = "o_vacant_new_jobs"),
            @Result(property = "countSocialJobs", column = "o_count_social_jobs"),
            @Result(property = "countYouthPractice", column = "o_count_youth_practice"),
            @Result(property = "countPublicWorks", column = "o_count_public_works"),
            @Result(property = "countProgramParticipant", column = "o_count_program_participant"),
            @Result(property = "countVocationalTraining", column = "o_count_vocational_training")
    })
    List<MenuKoordinationMonitoring> getMenuKoordinationMonitoringByYear(String year);


//raion

    @Select("SELECT * FROM sed_y_koordin_raion where o_year=#{year}")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "numberPopulation", column = "o_number_population"),
            @Result(property = "countLabour", column = "o_count_labour"),
            @Result(property = "employedPopulation", column = "o_employed_population"),
            @Result(property = "hiredWorker", column = "o_hired_worker"),
            @Result(property = "selfEmployedPopulation", column = "o_self_employed_population"),
            @Result(property = "countDisabilitiers", column = "o_count_disabilities"),
            @Result(property = "productivelyEmployed", column = "o_productively_employed"),
            @Result(property = "unproductiveBusy", column = "o_unproductive_busy"),
            @Result(property = "unemployedPopulation", column = "o_unemployed_population"),
            @Result(property = "payUnemployedPopulation", column = "o_pay_unemployed_population"),
            @Result(property = "registeredUnemployedPopulation", column = "o_registered_unemployed_population"),
            @Result(property = "targetedSocialAssistance", column = "o_targeted_social_assistance"),
            @Result(property = "housingAssistance", column = "o_housing_assistance"),
            @Result(property = "guaranteedSocialPackage", column = "o_guaranteed_social_package"),
            @Result(property = "totalConditionalCashaid", column = "o_total_conditional_cashaid"),
            @Result(property = "totalUnconditionalCashaid", column = "o_total_unconditional_cashaid"),
            @Result(property = "housingAssistanceTotal", column = "o_housing_assistance_total"),
            @Result(property = "guaranteedSocialPackageTotal", column = "o_guaranteed_social_package_total"),
            @Result(property = "conditionalCashaid", column = "o_conditional_cashaid"),
            @Result(property = "unconditionalCashaid", column = "o_unconditional_cashaid"),
            @Result(property = "housingAssistanceAverageSize", column = "o_housing_assistance_average_size"),
            @Result(property = "guaranteedSocialPackageAverageSize", column = "o_guaranteed_social_package_average_size"),
            @Result(property = "employedPermanentJobs", column = "o_employed_permanent_jobs"),
            @Result(property = "aimedCommunityService", column = "o_aimed_community_service"),
            @Result(property = "aimedSocialJobs", column = "o_aimed_social_jobs"),
            @Result(property = "aimedYouthPractice", column = "o_aimed_youth_practice"),
            @Result(property = "altynAlka", column = "o_altyn_alka"),
            @Result(property = "kumisAlka", column = "o_kumis_alka"),
            @Result(property = "mothersManyChildren", column = "o_mothers_many_children"),
            @Result(property = "countPensioners", column = "o_count_pensioners"),
            @Result(property = "countPensionersAge", column = "o_count_pensioners_age"),
            @Result(property = "countPreferentialPension", column = "o_count_preferential_pension"),
            @Result(property = "countPensionRecipients", column = "o_count_pension_recipients"),
            @Result(property = "countVeteransVov", column = "o_count_veterans_vov"),
            @Result(property = "countInvalidsVov", column = "o_count_invalids_vov"),
            @Result(property = "countWidowsVov", column = "o_count_widows_vov"),
            @Result(property = "countVeteransAfghan", column = "o_count_veterans_afghan"),
            @Result(property = "countInvalidsAfghan", column = "o_count_invalids_afghan"),
            @Result(property = "countWidowsAfghans", column = "o_count_widows_afghans"),
            @Result(property = "countInvalids", column = "o_count_invalids"),
            @Result(property = "countInvalidsA", column = "o_count_invalids_a"),
            @Result(property = "countInvalidsB", column = "o_count_invalids_b"),
            @Result(property = "countInvalidsC", column = "o_count_invalids_c"),
            @Result(property = "countInvalidsChildren", column = "o_count_invalids_children"),
            @Result(property = "invalidsPermanentJobs", column = "o_invalids_permanent_jobs"),
            @Result(property = "countInvalidsPublicWorks", column = "o_count_invalids_public_works"),
            @Result(property = "countInvalidsSocialWork", column = "o_count_invalids_social_work"),
            @Result(property = "countInvalidsStudy", column = "o_count_invalids_study"),
            @Result(property = "completedIpr", column = "o_completed_ipr"),
            @Result(property = "fulfilledIpr", column = "o_fulfilled_ipr"),
            @Result(property = "developedIpr", column = "o_developed_ipr"),
            @Result(property = "totalAmount", column = "o_total_amount"),
            @Result(property = "totalEmployed", column = "o_total_employed"),
            @Result(property = "youthPractice", column = "o_youth_practice"),
            @Result(property = "publicWork", column = "o_public_work"),
            @Result(property = "professionalEducation", column = "o_professional_education"),
            @Result(property = "socialJobs", column = "o_social_jobs"),
            @Result(property = "permanentJobs", column = "o_permanent_jobss")
    })
    List<MenuKoordinationRaion> getMenuKoordinationRaionByYear(String year);

//regionT1

    @Select("SELECT * FROM sed_y_koordin_region_table_1 where o_year=#{year}")
    @Results({
            @Result(property = "name", column = "o_name"),
            @Result(property = "int1", column = "o_inte"),
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area")
    })
    List<MenuKoordinationMonitoringT1> getMenuKoordinationMonitoringT1ByYear(String year);

//regionT2

    @Select("SELECT * FROM sed_y_koordin_region_table_2 where o_year=#{year}")
    @Results({
            @Result(property = "name", column = "o_name"),
            @Result(property = "int1", column = "o_inte"),
            @Result(property = "int2", column = "o_int_2"),
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area")
    })
    List<MenuKoordinationMonitoringT2> getMenuKoordinationMonitoringT2ByYear(String year);

//raionT1

    @Select("SELECT * FROM sed_y_koordin_raion_table_1 where o_year=#{year}")
    @Results({
            @Result(property = "name", column = "o_name"),
            @Result(property = "int1", column = "o_inte"),
            @Result(property = "year", column = "o_year"),
            @Result(property = "region", column = "o_region")
    })
    List<MenuKoordinationRaionT1> getMenuKoordinationRaionT1ByYear(String year);

//raionT2

    @Select("SELECT * FROM sed_y_koordin_raion_table_2 where o_year=#{year}")
    @Results({
            @Result(property = "name", column = "o_name"),
            @Result(property = "int1", column = "o_inte"),
            @Result(property = "int2", column = "o_int_2"),
            @Result(property = "year", column = "o_year"),
            @Result(property = "region", column = "o_region")
    })
    List<MenuKoordinationRaionT2> getMenuKoordinationRaionT2ByYear(String year);

//sed_m_koordin_region

    @Select("SELECT *FROM sed_m_koordin_region where o_year=#{year}")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "jobFairsHeld", column = "o_job_fairs_held"),
            @Result(property = "employersTookPart", column = "o_employers_took_part"),
            @Result(property = "unemployed", column = "o_unemployed"),
            @Result(property = "vacanciesSubmitted", column = "o_vacancies_submitted"),
            @Result(property = "permanentJobs", column = "o_permanent_jobs"),
    })
    List<MenuKoordinationRegionM> getMenuKoordinationRegionMByYear(String year);

//year only

    @Select("SELECT DISTINCT o_year FROM sed_y_koordin_region ORDER BY o_year")
    public List<String> getMenuKoordinationMonitoringYears();

    @Select("SELECT DISTINCT o_year FROM sed_y_koordin_raion ORDER BY o_year")
    public List<String> getMenuKoordinationRaionYears();

    @Select("SELECT DISTINCT o_year FROM sed_y_koordin_region_table_1 ORDER BY o_year")
    public List<String> getMenuKoordinationMonitoringT1Years();

    @Select("SELECT DISTINCT o_year FROM sed_y_koordin_region_table_2 ORDER BY o_year")
    public List<String> getMenuKoordinationMonitoringT2Years();

    @Select("SELECT DISTINCT o_year FROM sed_y_koordin_raion_table_1 ORDER BY o_year")
    public List<String> getMenuKoordinationRaionT1Years();

    @Select("SELECT DISTINCT o_year FROM sed_y_koordin_raion_table_2 ORDER BY o_year")
    public List<String> getMenuKoordinationRaionT2Years();

    @Select("SELECT DISTINCT o_year FROM sed_m_koordin_region ORDER BY o_year")
    public List<String> getMenuKoordinationRegionMYears();

//Koordination Table

    @Select("SELECT DISTINCT o_year AS year, o_region AS region " +
            "FROM sed_y_koordin_raion_table_1 WHERE o_year=#{year} AND o_region=#{region}")
    public List<MenuTKRT1Monitoring> getMenuTKRT1MonitoringByYear(String year, String region);

    @Select("SELECT o_inte FROM sed_y_koordin_raion_table_1 WHERE o_year=#{year} " +
            "AND o_region=#{region} AND o_name=#{name}")
    public Double getMenuTKRT1MonitoringByYearAndRegionAndName(String year, String region, String name);

    @Select("SELECT DISTINCT o_year FROM sed_y_koordin_raion_table_1")

    public List<String> getMenuTKRT1MonitoringYears();

    @Select("SELECT DISTINCT o_region FROM sed_y_koordin_raion_table_1")

    public List<String> getMenuTKRT1MonitoringRegion();

    @Select("SELECT DISTINCT o_name FROM sed_y_koordin_raion_table_1")

    public List<String> getMenuTKRT1MonitoringNames();

// otdelno

    @Select("SELECT * FROM sed_y_koordin_region where o_year=#{year}")
    @Results({

            @Result(property = "numberOne", column = "o_number_one"),
            @Result(property = "numberTwo", column = "o_number_two"),
            @Result(property = "numberWithOneDisabled", column = "o_number_with_one_disabled"),
            @Result(property = "numberWithSixDisabled", column = "o_number_with_six_disabled"),
            @Result(property = "invalidsTheFirst", column = "o_invalids_the_first"),
            @Result(property = "invalidsSecond", column = "o_invalids_second"),
            @Result(property = "invalidsThird", column = "o_invalids_third"),
            @Result(property = "invalidsChildren", column = "o_invalids_children"),
            @Result(property = "childbirthAllowance", column = "o_childbirth_allowance"),
            @Result(property = "childbirthAllowanceA", column = "o_childbirth_allowance_a"),
            @Result(property = "invalidsFirstGroup", column = "o_invalids_first_group"),
            @Result(property = "invalidsSecondGroup", column = "o_invalids_second_group"),
            @Result(property = "invalidsThirdGroup", column = "o_invalids_third_group"),
            @Result(property = "largeMothersAllowance", column = "o_large_mothers_allowance"),
            @Result(property = "allowanceRaisingDisabledchild", column = "o_allowance_raising_disabledchild"),
            @Result(property = "disabledCareAllowance", column = "o_disabled_care_allowance"),
            @Result(property = "largeFamilyAllowance4Children", column = "o_large_family_allowance_4_children"),
            @Result(property = "largeFamilyAllowance5Children", column = "o_large_family_allowance_5_children"),
            @Result(property = "largeFamilyAllowance6Children", column = "o_large_family_allowance_6_children"),
            @Result(property = "largeFamilyAllowance7Children", column = "o_large_family_allowance_7_children"),
            @Result(property = "burialGrants", column = "o_burial_grants"),
            @Result(property = "burialAllowancesParticipantsVov", column = "o_burial_allowances_participants_vov"),
            @Result(property = "invalidsParticipantsVov", column = "o_invalids_participants_vov"),
            @Result(property = "personsEquatedDisabledVov", column = "o_persons_equated_disabled_vov"),
            @Result(property = "personsEquatedParticipants", column = "o_persons_equated_participants"),
            @Result(property = "vdovamPogibshikh", column = "o_vdovam_pogibshikh"),
            @Result(property = "abc", column = "o_abc"),
            @Result(property = "benefitsFrontWorkers", column = "o_benefits_front_workers"),
            @Result(property = "abcd", column = "o_abcd"),
            @Result(property = "individualsAwardedTitles", column = "o_individuals_awarded_titles"),
            @Result(property = "heroesSocialistLabor", column = "o_heroes_socialist_labor"),
            @Result(property = "victimsPoliticalRepression", column = "o_victims_political_repression"),
            @Result(property = "personalPensioners", column = "o_personal_pensioners"),
    })
    List<MenuKoord2Monitoring> getMenuKoord2MonitoringByYear(String year);


    @Select("SELECT * FROM sed_y_koordin_region where o_year=#{year}")
    @Results({

            @Result(property = "amountPaymentsOne", column = "o_amount_payments_one"),
            @Result(property = "amountPaymentsTwo", column = "o_amount_payments_two"),
            @Result(property = "amountBenefitsWithOneDisabled", column = "o_amount_benefits_with_one_disabled"),
            @Result(property = "amountBenefitsWithSixDisabled", column = "o_amount_benefits_with_six_disabled"),
            @Result(property = "invalidsTheFirstAmount", column = "o_invalids_the_first_amount"),
            @Result(property = "invalidsSecondAmount", column = "o_invalids_second_amount"),
            @Result(property = "invalidsThirdAmount", column = "o_invalids_third_amount"),
            @Result(property = "amountInvalidsChildren", column = "o_amount_invalids_children"),
            @Result(property = "amountMaternityBenefits", column = "o_amount_maternity_benefits"),
            @Result(property = "amountMaternityBenefitsA", column = "o_amount_maternity_benefits_a"),
            @Result(property = "amountInvalidsFirstGroup", column = "o_amount_invalids_first_group"),
            @Result(property = "amountInvalidsSecondGroup", column = "o_amount_invalids_second_group"),
            @Result(property = "amountInvalidsThirdGroup", column = "o_amount_invalids_third_group"),
            @Result(property = "amountLargeMothersAllowance", column = "o_amount_large_mothers_allowance"),
            @Result(property = "amountAllowanceRaisingDisabledchild", column = "o_amount_allowance_raising_disabledchild"),
            @Result(property = "amountDisabledCareAllowance", column = "o_amount_disabled_care_allowance"),
            @Result(property = "amountLargeFamilyAllowance4Children", column = "o_amount_large_family_allowance_4_children"),
            @Result(property = "amountLargeFamilyAllowance5Children", column = "o_amount_large_family_allowance_5_children"),
            @Result(property = "amountLargeFamilyAllowance6Children", column = "o_amount_large_family_allowance_6_children"),
            @Result(property = "amountLargeFamilyAllowance7Children", column = "o_amount_large_family_allowance_7_children"),
            @Result(property = "amountBurialGrants", column = "o_amount_burial_grants"),
            @Result(property = "amountBurialAllowancesParticipantsVov", column = "o_amount_burial_allowances_participants_vov"),
            @Result(property = "amountInvalidsParticipantsVov", column = "o_amount_invalids_participants_vov"),
            @Result(property = "amountPersonsEquatedDisabledVov", column = "o_amount_persons_equated_disabled_vov"),
            @Result(property = "amountPersonsEquatedParticipants", column = "o_amount_persons_equated_participants"),
            @Result(property = "amountVdovamPogibshikh", column = "o_amount_vdovam_pogibshikh"),
            @Result(property = "amountAbc", column = "o_amount_abc"),
            @Result(property = "amountBenefitsFrontWorkers", column = "o_amount_benefits_front_workers"),
            @Result(property = "amountAbcd", column = "o_amount_abcd"),
            @Result(property = "amountIndividualsAwardedTitles", column = "o_amount_individuals_awarded_titles"),
            @Result(property = "amountHeroesSocialistLabor", column = "o_amount_heroes_socialist_labor"),
            @Result(property = "amountVictimsPoliticalRepression", column = "o_amount_victims_political_repression"),
            @Result(property = "amountPersonalPensioners", column = "o_amount_personal_pensioners"),
    })
    List<MenuKoord3Monitoring> getMenuKoord3MonitoringByYear(String year);

    @Select("SELECT * FROM sed_y_koordin_region where o_year=#{year}")
    @Results({

            @Result(property = "totalBenefitsOne", column = "o_total_benefits_one"),
            @Result(property = "totalBenefitsTwo", column = "o_total_benefits_two"),
            @Result(property = "totalBenefitsWithOneDisabled", column = "o_total_benefits_with_one_disabled"),
            @Result(property = "totalBenefitsWithSixDisabled", column = "o_total_benefits_with_six_disabled"),
            @Result(property = "invalidsFirstAmountTotal", column = "o_invalids_first_amount_total"),
            @Result(property = "invalidsSecondAmountTotal", column = "o_invalids_second_amount_total"),
            @Result(property = "invalidsThirdAmountAmountTotal", column = "o_invalids_third_amount_amount_total"),
            @Result(property = "totalInvalidsChildren", column = "o_total_invalids_children"),
            @Result(property = "totalamountMaternityBenefits", column = "o_totalamount_maternity_benefits"),
            @Result(property = "totalamountMaternityBenefitsA", column = "o_totalamount_maternity_benefits_a"),
            @Result(property = "totalInvalidsFirstGroup", column = "o_total_invalids_first_group"),
            @Result(property = "totalInvalidsSecondGroup", column = "o_total_invalids_second_group"),
            @Result(property = "totalInvalidsThirdGroup", column = "o_total_invalids_third_group"),
            @Result(property = "totalLargeMothersAllowance", column = "o_total_large_mothers_allowance"),
            @Result(property = "totalAllowanceRaisingDisabledchild", column = "o_total_allowance_raising_disabledchild"),
            @Result(property = "totalDisabledCareAllowance", column = "o_total_disabled_care_allowance"),
            @Result(property = "totalLargeFamilyAllowance4Children", column = "o_total_large_family_allowance_4_children"),
            @Result(property = "totalLargeFamilyAllowance5Children", column = "o_total_large_family_allowance_5_children"),
            @Result(property = "totalLargeFamilyAllowance6Children", column = "o_total_large_family_allowance_6_children"),
            @Result(property = "totalLargeFamilyAllowance7Children", column = "o_total_large_family_allowance_7_children"),
            @Result(property = "totalBurialGrants", column = "o_total_burial_grants"),
            @Result(property = "totalBurialAllowancesParticipantsVov", column = "o_total_burial_allowances_participants_vov"),
            @Result(property = "totalInvalidsParticipantsVov", column = "o_total_invalids_participants_vov"),
            @Result(property = "totalPersonsEquatedDisabledVov", column = "o_total_persons_equated_disabled_vov"),
            @Result(property = "totalPersonsEquatedParticipants", column = "o_total_persons_equated_participants"),
            @Result(property = "totalVdovamPogibshikh", column = "o_total_vdovam_pogibshikh"),
            @Result(property = "totalAbc", column = "o_total_abc"),
            @Result(property = "totalBenefitsFrontWorkers", column = "o_total_benefits_front_workers"),
            @Result(property = "totalAbcd", column = "o_total_abcd"),
            @Result(property = "totalIndividualsAwardedTitles", column = "o_total_individuals_awarded_titles"),
            @Result(property = "totalHeroesSocialistLabor", column = "o_total_heroes_socialist_labor"),
            @Result(property = "totalVictimsPoliticalRepression", column = "o_total_victims_political_repression"),
            @Result(property = "totalPersonalPensioners", column = "o_total_personal_pensioners"),
    })
    List<MenuKoord4Monitoring> getMenuKoord4MonitoringByYear(String year);

    @Select("SELECT * FROM sed_y_koordin_raion_table_1")
    @Results({

            @Result(property = "inte", column = "o_inte"),
            @Result(property = "name", column = "o_name"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "year", column = "o_year")

    })
    List<MenuTKRT1allMonitoring> getMenuTKRT1allMonitoringByAll();

//
//    @Select("SELECT DISTINCT o_year AS year, o_region AS region " +
//            "FROM sed_y_koordin_raion_table_1 WHERE o_year=#{year} AND o_region=#{region}")
//    public List<MenuTKRT1Monitoring> getMenuTKRT1MonitoringByAll();

//sed_m_koordin_region

    @Select("SELECT *FROM sed_m_koordin_region")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "jobFairsHeld", column = "o_job_fairs_held"),
            @Result(property = "employersTookPart", column = "o_employers_took_part"),
            @Result(property = "unemployed", column = "o_unemployed"),
            @Result(property = "vacanciesSubmitted", column = "o_vacancies_submitted"),
            @Result(property = "permanentJobs", column = "o_permanent_jobs"),
            @Result(property = "month", column = "o_month")
    })
    List<MenuKoordinationRegionM2> getMenuKoordinationRegionMByAll();


}
