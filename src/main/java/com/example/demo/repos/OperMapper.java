package com.example.demo.repos;

import com.example.demo.domain.*;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface OperMapper {


    @Select("SELECT * FROM mon_prest where o_region=#{region} AND o_year=#{year} AND o_month=#{month}")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "period", column = "o_period"),
            @Result(property = "month", column = "o_month"),
            @Result(property = "numberErdr", column = "o_number_erdr"),
            @Result(property = "dateEvent", column = "o_date_event"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "registrationAuthority", column = "o_registration_authority"),
            @Result(property = "descriptionOffense", column = "o_description_offense"),
            @Result(property = "qualification", column = "o_qualification"),
            @Result(property = "dateStart", column = "o_date_start"),
            @Result(property = "time", column = "time")
    })
    public List<OperMPMonitoring> getOperMPMonitoring(String region, String year, String month);

    @Select("SELECT DISTINCT o_region FROM mon_prest ORDER BY o_region")
    public List<String> getOperMPMonitoringRegions();

    @Select("SELECT DISTINCT o_year FROM mon_prest ORDER BY o_year")
    public List<String> getOperMPMonitoringYears();

    @Select("SELECT DISTINCT o_month FROM mon_prest ORDER BY o_month")
    public List<String> getOperMPMonitoringMonths();

    @Select("SELECT * FROM mon_prest")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "period", column = "o_period"),
            @Result(property = "month", column = "o_month"),
            @Result(property = "numberErdr", column = "o_number_erdr"),
            @Result(property = "dateEvent", column = "o_date_event"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "registrationAuthority", column = "o_registration_authority"),
            @Result(property = "descriptionOffense", column = "o_description_offense"),
            @Result(property = "qualification", column = "o_qualification"),
            @Result(property = "dateStart", column = "o_date_start"),
            @Result(property = "time", column = "time")
    })
    public List<OperMPMonitoring> getOperMPMonitoringByAll();

    @Select("SELECT * FROM oper_mon_oil_price where o_year=#{year} AND o_month=#{month}")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "month", column = "o_month"),
            @Result(property = "dateEvent", column = "o_date_event"),
            @Result(property = "period", column = "o_period"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "unit", column = "o_unit"),
            @Result(property = "ai92", column = "o_ai_92"),
            @Result(property = "ai95", column = "o_ai_95"),
            @Result(property = "dieselWinter", column = "o_diesel_winter"),
            @Result(property = "dieselFuelOffseason", column = "o_diesel_fuel_offseason")
    })
    public List<OperMOpMonitoring> getOperMOpMonitoring(String year, String month);

    @Select("SELECT * FROM oper_mon_oil_price")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "month", column = "o_month"),
            @Result(property = "dateEvent", column = "o_date_event"),
            @Result(property = "period", column = "o_period"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "unit", column = "o_unit"),
            @Result(property = "ai92", column = "o_ai_92"),
            @Result(property = "ai95", column = "o_ai_95"),
            @Result(property = "dieselWinter", column = "o_diesel_winter"),
            @Result(property = "dieselFuelOffseason", column = "o_diesel_fuel_offseason")
    })
    public List<OperMOpMonitoring> getOperMOpMonitoringByAll();


    @Select("SELECT DISTINCT o_year FROM oper_mon_oil_price ORDER BY o_year")
    public List<String> getOperMOpMonitoringYears();

    @Select("SELECT DISTINCT o_month FROM oper_mon_oil_price ORDER BY o_month")
    public List<String> getOperMOpMonitoringMonths();

    @Select("SELECT * FROM oper_mon_pravonarusheniya where o_region=#{region} AND o_year=#{year} AND o_month=#{month}")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "month", column = "o_month"),
            @Result(property = "period", column = "o_period"),
            @Result(property = "dateEvent", column = "o_date_event"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "unitMisconducts", column = "o_unit_misconducts"),
            @Result(property = "totalMisdemeanors", column = "o_total_misdemeanors"),
            @Result(property = "administrativeLegalviolations", column = "o_administrative_legalviolations"),
            @Result(property = "civilLawviolations", column = "o_civil_lawviolations"),
            @Result(property = "disciplinaryOffences", column = "o_disciplinary_offences"),
            @Result(property = "unitCrime", column = "o_unit_crime"),
            @Result(property = "totalOffences", column = "o_total_offences"),
            @Result(property = "heavy", column = "o_heavy"),
            @Result(property = "especiallyGrave", column = "o_especially_grave"),
            @Result(property = "murder", column = "o_murder"),
            @Result(property = "heavyEspeciallyheavy", column = "o_heavy_especiallyheavy"),
            @Result(property = "dtp", column = "o_dtp"),
            @Result(property = "committedMinors", column = "o_committed_minors")
    })
    public List<OperMPrMonitoring> getOperMPrMonitoring(String region, String year, String month);

    @Select("SELECT * FROM oper_mon_pravonarusheniya")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "month", column = "o_month"),
            @Result(property = "period", column = "o_period"),
            @Result(property = "dateEvent", column = "o_date_event"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "unitMisconducts", column = "o_unit_misconducts"),
            @Result(property = "totalMisdemeanors", column = "o_total_misdemeanors"),
            @Result(property = "administrativeLegalviolations", column = "o_administrative_legalviolations"),
            @Result(property = "civilLawviolations", column = "o_civil_lawviolations"),
            @Result(property = "disciplinaryOffences", column = "o_disciplinary_offences"),
            @Result(property = "unitCrime", column = "o_unit_crime"),
            @Result(property = "totalOffences", column = "o_total_offences"),
            @Result(property = "heavy", column = "o_heavy"),
            @Result(property = "especiallyGrave", column = "o_especially_grave"),
            @Result(property = "murder", column = "o_murder"),
            @Result(property = "heavyEspeciallyheavy", column = "o_heavy_especiallyheavy"),
            @Result(property = "dtp", column = "o_dtp"),
            @Result(property = "committedMinors", column = "o_committed_minors")
    })
    public List<OperMPrMonitoring> getOperMPrMonitoringByAll();


    @Select("SELECT DISTINCT o_region FROM oper_mon_pravonarusheniya ORDER BY o_region")
    public List<String> getOperMPrMonitoringRegions();

    @Select("SELECT DISTINCT o_year FROM oper_mon_pravonarusheniya ORDER BY o_year")
    public List<String> getOperMPrMonitoringYears();

    @Select("SELECT DISTINCT o_month FROM oper_mon_pravonarusheniya ORDER BY o_month")
    public List<String> getOperMPrMonitoringMonths();


    @Select("SELECT * FROM oper_mon_prod_tov where o_year=#{year} AND o_month=#{month}")
    @Results({
            @Result(property = "area", column = "o_area"),
            @Result(property = "year", column = "o_year"),
            @Result(property = "month", column = "o_month"),
            @Result(property = "period", column = "o_period"),
            @Result(property = "dateEvent", column = "o_date_event"),
            @Result(property = "unit", column = "o_unit"),
            @Result(property = "wheatFlourFirstGrade", column = "o_wheat_flour_first_grade"),
            @Result(property = "wheatBread", column = "o_wheat_bread"),
            @Result(property = "horns", column = "o_horns"),
            @Result(property = "buckwheat", column = "o_buckwheat"),
            @Result(property = "polishedRice", column = "o_polished_rice"),
            @Result(property = "potato", column = "o_potato"),
            @Result(property = "carrots", column = "o_carrots"),
            @Result(property = "onion", column = "o_onion"),
            @Result(property = "cabbageWhite", column = "o_cabbage_white"),
            @Result(property = "sugar", column = "o_sugar"),
            @Result(property = "oil", column = "o_oil"),
            @Result(property = "beef", column = "o_beef"),
            @Result(property = "chickenMeat", column = "o_chicken_meat"),
            @Result(property = "milk", column = "o_milk"),
            @Result(property = "kefir", column = "o_kefir"),
            @Result(property = "butter", column = "o_butter"),
            @Result(property = "egg", column = "o_egg"),
            @Result(property = "salt", column = "o_salt"),
            @Result(property = "cottageCheese", column = "o_cottage_cheese")
    })
    public List<OperMPtMonitoring> getOperMPtMonitoring(String year, String month);

    @Select("SELECT * FROM oper_mon_prod_tov")
    @Results({
            @Result(property = "area", column = "o_area"),
            @Result(property = "year", column = "o_year"),
            @Result(property = "month", column = "o_month"),
            @Result(property = "period", column = "o_period"),
            @Result(property = "dateEvent", column = "o_date_event"),
            @Result(property = "unit", column = "o_unit"),
            @Result(property = "wheatFlourFirstGrade", column = "o_wheat_flour_first_grade"),
            @Result(property = "wheatBread", column = "o_wheat_bread"),
            @Result(property = "horns", column = "o_horns"),
            @Result(property = "buckwheat", column = "o_buckwheat"),
            @Result(property = "polishedRice", column = "o_polished_rice"),
            @Result(property = "potato", column = "o_potato"),
            @Result(property = "carrots", column = "o_carrots"),
            @Result(property = "onion", column = "o_onion"),
            @Result(property = "cabbageWhite", column = "o_cabbage_white"),
            @Result(property = "sugar", column = "o_sugar"),
            @Result(property = "oil", column = "o_oil"),
            @Result(property = "beef", column = "o_beef"),
            @Result(property = "chickenMeat", column = "o_chicken_meat"),
            @Result(property = "milk", column = "o_milk"),
            @Result(property = "kefir", column = "o_kefir"),
            @Result(property = "butter", column = "o_butter"),
            @Result(property = "egg", column = "o_egg"),
            @Result(property = "salt", column = "o_salt"),
            @Result(property = "cottageCheese", column = "o_cottage_cheese")
    })
    public List<OperMPtMonitoring> getOperMPtMonitoringByAll();



    @Select("SELECT DISTINCT o_year FROM oper_mon_prod_tov ORDER BY o_year")
    public List<String> getOperMPtMonitoringYears();

    @Select("SELECT DISTINCT o_month FROM oper_mon_prod_tov ORDER BY o_month")
    public List<String> getOperMPtMonitoringMonths();


    @Select("SELECT * FROM mon_sil_i_sred_dthcs where o_region=#{region} AND o_rural_district=#{ruralDistrict} AND o_year=#{year} AND o_month=#{month}")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "period", column = "o_period"),
            @Result(property = "month", column = "o_month"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "ruralDistrict", column = "o_rural_district"),
            @Result(property = "address", column = "o_address"),
            @Result(property = "dateEvent", column = "o_date_event"),
            @Result(property = "typeE", column = "o_type_e"),
            @Result(property = "descriptionSituation", column = "o_description_situation"),
            @Result(property = "numberLedgerInformation", column = "o_number_ledger_information"),
            @Result(property = "acceptedMeasure", column = "o_accepted_measure"),
            @Result(property = "time", column = "o_time")
    })
    public List<OperMSDMonitoring> getOperMSDMonitoring(String region, String ruralDistrict, String year, String month);

    @Select("SELECT DISTINCT o_region FROM mon_sil_i_sred_dthcs ORDER BY o_region")
    public List<String> getOperMSDMonitoringRegions();

    @Select("SELECT DISTINCT o_rural_district FROM mon_sil_i_sred_dthcs ORDER BY o_rural_district")
    public List<String> getOperMSDMonitoringDistricts();

    @Select("SELECT DISTINCT o_year FROM mon_sil_i_sred_dthcs ORDER BY o_year")
    public List<String> getOperMSDMonitoringYears();

    @Select("SELECT DISTINCT o_month FROM mon_sil_i_sred_dthcs ORDER BY o_month")
    public List<String> getOperMSDMonitoringMonths();


    @Select("SELECT * FROM oper_mon_skor_pom where o_region=#{region} AND o_year=#{year} AND o_month=#{month}")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "month", column = "o_month"),
            @Result(property = "dateEvent", column = "o_date_event"),
            @Result(property = "periodcity", column = "o_periodicity"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "numberCallsAmbulanceStation", column = "o_number_calls_ambulance_station"),
            @Result(property = "ofThemAdults", column = "o_of_them_adults"),
            @Result(property = "ofThemChildren", column = "o_of_them_children"),
            @Result(property = "ofthemTakenHospital", column = "o_ofthem_taken_hospital"),
            @Result(property = "ofthemPlacedHospital", column = "o_ofthem_placed_hospital"),
            @Result(property = "ofthemDtp", column = "o_ofthem_dtp"),
            @Result(property = "oftheseIndustrialInjuries", column = "o_ofthese_industrial_injuries"),
            @Result(property = "oftheseDomesticInjuries", column = "o_ofthese_domestic_injuries"),
            @Result(property = "ofthemBurns", column = "o_ofthem_burns"),
            @Result(property = "numberAmbulances", column = "o_number_ambulances"),
            @Result(property = "numberAmbulanceCrewsSsmp", column = "o_number_ambulance_crews_ssmp")
    })
    public List<OperMSpMonitoring> getOperMSpMonitoring(String region, String year, String month);

    @Select("SELECT * FROM oper_mon_skor_pom")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "month", column = "o_month"),
            @Result(property = "dateEvent", column = "o_date_event"),
            @Result(property = "periodcity", column = "o_periodicity"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "numberCallsAmbulanceStation", column = "o_number_calls_ambulance_station"),
            @Result(property = "ofThemAdults", column = "o_of_them_adults"),
            @Result(property = "ofThemChildren", column = "o_of_them_children"),
            @Result(property = "ofthemTakenHospital", column = "o_ofthem_taken_hospital"),
            @Result(property = "ofthemPlacedHospital", column = "o_ofthem_placed_hospital"),
            @Result(property = "ofthemDtp", column = "o_ofthem_dtp"),
            @Result(property = "oftheseIndustrialInjuries", column = "o_ofthese_industrial_injuries"),
            @Result(property = "oftheseDomesticInjuries", column = "o_ofthese_domestic_injuries"),
            @Result(property = "ofthemBurns", column = "o_ofthem_burns"),
            @Result(property = "numberAmbulances", column = "o_number_ambulances"),
            @Result(property = "numberAmbulanceCrewsSsmp", column = "o_number_ambulance_crews_ssmp")
    })
    public List<OperMSpMonitoring> getOperMSpMonitoringByAll();


    @Select("SELECT DISTINCT o_region FROM oper_mon_skor_pom ORDER BY o_region")
    public List<String> getOperMSpMonitoringRegions();

    @Select("SELECT DISTINCT o_year FROM oper_mon_skor_pom ORDER BY o_year")
    public List<String> getOperMSpMonitoringYears();

    @Select("SELECT DISTINCT o_month FROM oper_mon_skor_pom ORDER BY o_month")
    public List<String> getOperMSpMonitoringMonths();

    @Select("SELECT * FROM oper_mon_proishestvie where o_region=#{region} AND o_month=#{month}")
    @Results({
            @Result(property = "month", column = "o_month"),
            @Result(property = "dateEvent", column = "o_date_event"),
            @Result(property = "period", column = "o_period"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "totalEmergency", column = "o_total_emergency"),
            @Result(property = "unitA", column = "o_unit_a"),
            @Result(property = "lostA", column = "o_lost_a"),
            @Result(property = "unitB", column = "o_unit_b"),
            @Result(property = "hospitalizedA", column = "o_hospitalized_a"),
            @Result(property = "unitC", column = "o_unit_c"),
            @Result(property = "evacuatedA", column = "o_evacuated_a"),
            @Result(property = "unitD", column = "o_unit_d"),
            @Result(property = "rescuedA", column = "o_rescued_a"),
            @Result(property = "unitE", column = "o_unit_e"),
            @Result(property = "firesA", column = "o_fires_a"),
            @Result(property = "unitF", column = "o_unit_f"),
            @Result(property = "totalEmergencyB", column = "o_total_emergency_b"),
            @Result(property = "unitAA", column = "o_unit_aa"),
            @Result(property = "lostB", column = "o_lost_b"),
            @Result(property = "unitBB", column = "o_unit_bb"),
            @Result(property = "hospitalizedB", column = "o_hospitalized_b"),
            @Result(property = "unitCC", column = "o_unit_cc"),
            @Result(property = "evacuatedB", column = "o_evacuated_b"),
            @Result(property = "unitDD", column = "o_unit_dd"),
            @Result(property = "rescuedB", column = "o_rescued_b"),
            @Result(property = "unitEE", column = "o_unit_ee"),
            @Result(property = "firesB", column = "o_fires_b"),
            @Result(property = "unitFF", column = "o_unit_ff")
    })
    public List<OperMProMonitoring> getOperMProMonitoringByRegion(String region, String month);

    @Select("SELECT DISTINCT o_region FROM oper_mon_proishestvie ORDER BY o_region")
    public List<String> getOperMProMonitoringRegions();

    @Select("SELECT DISTINCT o_month FROM oper_mon_proishestvie ORDER BY o_month")
    public List<String> getOperMProMonitoringMonths();

    @Select("SELECT DISTINCT o_date_event FROM oper_mon_proishestvie ORDER BY o_date_event")
    public List<String> getOperMProMonitoringDates();

    @Select("SELECT * FROM oper_mon_proishestvie")
    @Results({
            @Result(property = "month", column = "o_month"),
            @Result(property = "dateEvent", column = "o_date_event"),
            @Result(property = "period", column = "o_period"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "totalEmergency", column = "o_total_emergency"),
            @Result(property = "unitA", column = "o_unit_a"),
            @Result(property = "lostA", column = "o_lost_a"),
            @Result(property = "unitB", column = "o_unit_b"),
            @Result(property = "hospitalizedA", column = "o_hospitalized_a"),
            @Result(property = "unitC", column = "o_unit_c"),
            @Result(property = "evacuatedA", column = "o_evacuated_a"),
            @Result(property = "unitD", column = "o_unit_d"),
            @Result(property = "rescuedA", column = "o_rescued_a"),
            @Result(property = "unitE", column = "o_unit_e"),
            @Result(property = "firesA", column = "o_fires_a"),
            @Result(property = "unitF", column = "o_unit_f"),
            @Result(property = "totalEmergencyB", column = "o_total_emergency_b"),
            @Result(property = "unitAA", column = "o_unit_aa"),
            @Result(property = "lostB", column = "o_lost_b"),
            @Result(property = "unitBB", column = "o_unit_bb"),
            @Result(property = "hospitalizedB", column = "o_hospitalized_b"),
            @Result(property = "unitCC", column = "o_unit_cc"),
            @Result(property = "evacuatedB", column = "o_evacuated_b"),
            @Result(property = "unitDD", column = "o_unit_dd"),
            @Result(property = "rescuedB", column = "o_rescued_b"),
            @Result(property = "unitEE", column = "o_unit_ee"),
            @Result(property = "firesB", column = "o_fires_b"),
            @Result(property = "unitFF", column = "o_unit_ff")
    })
    public List<OperMProMonitoring> getOperMProMonitoring();



}
