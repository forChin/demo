package com.example.demo.repos;

import com.example.demo.domain.*;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface MenuJivMapper {

//selskoe hoz - jivotnovodstvo

    @Select("SELECT * FROM ush_stat_jivotnovodstvo where o_region=#{region}")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "numberLivestockPoultry", column = "o_number_livestock_poultry"),
            @Result(property = "grossOutputProductsAff", column = "o_gross_output_products_aff"),
            @Result(property = "volumeGrossoutputAnimalhusbandry", column = "o_volume_grossoutput_animalhusbandry"),
            @Result(property = "ifoVolumeAnimalhusbandry", column = "o_ifo_volume_animalhusbandry"),
            @Result(property = "issuePeasantFamfarms", column = "o_issue_peasant_farmfarms"),
            @Result(property = "ifoIssuePeasantFamfarms", column = "o_ifo_issue_peasant_farmfarms"),
            @Result(property = "releaseAgriculturalEnterprises", column = "o_release_agricultural_enterprises"),
            @Result(property = "ifoReleaseAgriculturalEnterprises", column = "o_ifo_release_agricultural_enterprises"),
            @Result(property = "grossOutputprivateHouseholds", column = "o_gross_outputprivate_households"),
            @Result(property = "ifoGrossOutputprivateHouseholds", column = "o_ifo_gross_outputprivate_households"),
            @Result(property = "cattleFellDied", column = "o_cattle_fell_died"),
            @Result(property = "horsesFellDied", column = "o_horses_fell_died"),
            @Result(property = "sheepGoatsFellDied", column = "o_sheep_goats_fell_died"),
            @Result(property = "pigsFellDied", column = "o_pigs_fell_died"),
            @Result(property = "milkProduction", column = "o_milk_production"),
            @Result(property = "slaughterWeightB", column = "o_slaughter_weight_b"),
            @Result(property = "woolProduction", column = "o_wool_production"),
            @Result(property = "eggProduction", column = "o_egg_production"),
            @Result(property = "numberCattleFarms", column = "o_number_cattle_farms"),
            @Result(property = "numberCowsFarms", column = "o_number_cows_farms"),
            @Result(property = "numberSheepgoatsFarms", column = "o_number_sheepgoats_farms"),
            @Result(property = "numberPigsFarms", column = "o_number_pigs_farms"),
            @Result(property = "numberHorsesFarms", column = "o_number_horses_farms"),
            @Result(property = "numberCamelsFarms", column = "o_number_camels_farms"),
            @Result(property = "numberBirdsFarms", column = "o_number_birds_farms"),
            @Result(property = "calvesYieldFarms", column = "o_calves_yield_farms"),
            @Result(property = "outputLambsFarms", column = "o_output_lambs_farms"),
            @Result(property = "outletKidsFarms", column = "o_outlet_kids_farms"),
            @Result(property = "outputFoalsFarms", column = "o_output_foals_farms"),
            @Result(property = "outputCamelcoltsFarms", column = "o_output_camelscolts_farms"),
            @Result(property = "numberCattleAgricultural", column = "o_number_cattle_agricultural"),
            @Result(property = "numberCowsAgricultural", column = "o_number_cows_agricultural"),
            @Result(property = "numberSheepgoatsAgricultural", column = "o_number_sheepgoats_agricultural"),
            @Result(property = "numberPigsAgricultural", column = "o_number_pigs_agricultural"),
            @Result(property = "numberHorsesAgricultural", column = "o_number_horses_agricultural"),
            @Result(property = "numberCamelsAgricultural", column = "o_number_camels_agricultural"),
            @Result(property = "numberBirdsAgricultural", column = "o_number_birds_agricultural"),
            @Result(property = "calvesYieldAgricultural", column = "o_calves_yield_agricultural"),
            @Result(property = "outputLambsAgricultural", column = "o_output_lambs_agricultural"),
            @Result(property = "outletKidsAgricultural", column = "o_outlet_kids_agricultural"),
            @Result(property = "outputFoalsAgricultural", column = "o_output_foals_agricultural"),
            @Result(property = "outputCamelscoltsAgricultural", column = "o_output_camelscolts_agricultural"),
            @Result(property = "numberCattleHouseholds", column = "o_number_cattle_households"),
            @Result(property = "numberCowsHouseholds", column = "o_number_cows_households"),
            @Result(property = "numberSheepgoatsHouseholds", column = "o_number_sheepgoats_households"),
            @Result(property = "numberPigsHouseholds", column = "o_number_pigs_households"),
            @Result(property = "numberHorsesHouseholds", column = "o_number_horses_households"),
            @Result(property = "numberCamelsHouseholds", column = "o_number_camels_households"),
            @Result(property = "numberBirdHouseholdssB", column = "o_number_bird_householdss_b"),
            @Result(property = "calvesYieldHouseholds", column = "o_calves_yield_households"),
            @Result(property = "outputLambsHouseholds", column = "o_output_lambs_households"),
            @Result(property = "outletKidsHouseholds", column = "o_outlet_kids_households"),
            @Result(property = "outputFoalsHouseholds", column = "o_output_foals_households"),
            @Result(property = "outputCamelscoltsHouseholds", column = "o_output_camelscolts_households")
    })
    List<MenuJivMonitoring> getMenuJivMonitoringByYear(String region);

//year

    @Select("SELECT DISTINCT o_region FROM ush_stat_jivotnovodstvo ORDER BY o_region")
    public List<String> getMenuJivMonitoringYears();

//rastenevodstvo

        @Select("SELECT * FROM ush_stat_rastenievodstvo where o_year=#{year}")
        @Results({
                @Result(property = "year", column = "o_year"),
                @Result(property = "area", column = "o_area"),
                @Result(property = "region", column = "o_region"),
                @Result(property = "latitude", column = "o_latitude"),
                @Result(property = "longitude", column = "o_longitude"),
                @Result(property = "totalAcreage", column = "o_total_acreage"),
                @Result(property = "volumeProduction", column = "o_volume_production"),
                @Result(property = "ifoVolumeProduction", column = "o_ifo_volume_production"),
                @Result(property = "grossHarvestAll", column = "o_gross_harvest_all"),
                @Result(property = "watermelonGross", column = "o_watermelon_gross"),
                @Result(property = "potatoGross", column = "o_potato_gross"),
                @Result(property = "pepperGross", column = "o_pepper_gross"),
                @Result(property = "eggplantGross", column = "o_eggplant_gross"),
                @Result(property = "onionGross", column = "o_onion_gross"),
                @Result(property = "tomatoesGross", column = "o_tomatoes_gross"),
                @Result(property = "carrotGross", column = "o_carrot_gross"),
                @Result(property = "melonsGross", column = "o_melons_gross"),
                @Result(property = "beetGross", column = "o_beet_gross"),
                @Result(property = "cabbageGross", column = "o_cabbage_gross"),
                @Result(property = "cucumbersGross", column = "o_cucumbers_gross"),
                @Result(property = "sownAreaAll", column = "o_sown_area_all"),
                @Result(property = "watermelonSownArea", column = "o_watermelon_sown_area"),
                @Result(property = "potatoSownArea", column = "o_potato_sown_area"),
                @Result(property = "pepperSownArea", column = "o_pepper_sown_area"),
                @Result(property = "eggplantSownArea", column = "o_eggplant_sown_area"),
                @Result(property = "onionSownArea", column = "o_onion_sown_area"),
                @Result(property = "tomatoesSownArea", column = "o_tomatoes_sown_area"),
                @Result(property = "carrotSownArea", column = "o_carrot_sown_area"),
                @Result(property = "melonsSownArea", column = "o_melons_sown_area"),
                @Result(property = "beetSownArea", column = "o_beet_sown_area"),
                @Result(property = "cabbageSownArea", column = "o_cabbage_sown_area"),
                @Result(property = "cucumbersSownArea", column = "o_cucumbers_sown_area"),
                @Result(property = "yieldAll", column = "o_yield_all"),
                @Result(property = "watermelonYield", column = "o_watermelon_yield"),
                @Result(property = "potatoYield", column = "o_potato_yield"),
                @Result(property = "pepperYield", column = "o_pepper_yield"),
                @Result(property = "eggplantYield", column = "o_eggplant_yield"),
                @Result(property = "onionYield", column = "o_onion_yield"),
                @Result(property = "tomatoesYield", column = "o_tomatoes_yield"),
                @Result(property = "carrotYield", column = "o_carrot_yield"),
                @Result(property = "melonsYield", column = "o_melons_yield"),
                @Result(property = "beetYield", column = "o_beet_yield"),
                @Result(property = "cabbageYield", column = "o_cabbage_yield"),
                @Result(property = "cucumbersYield", column = "o_cucumbers_yield"),
                @Result(property = "cerealsLegumesPeasantFarmSownarea", column = "o_cereals_legumes_peasant_farm_sownarea"),
                @Result(property = "potatoPeasantFarmSownarea", column = "o_potato_peasant_farm_sownarea"),
                @Result(property = "melonCulturesPeasantFarmSownarea", column = "o_melon_cultures_peasant_farm_sownarea"),
                @Result(property = "forageCropsPeasantFarmSownarea", column = "o_forage_crops_peasant_farm_sownarea"),
                @Result(property = "cultureOlivesPeasantFarmSownarea", column = "o_culture_olives_peasant_farm_sownarea"),
                @Result(property = "vegetablesPeasantFarmSownarea", column = "o_vegetables_peasant_farm_sownarea"),
                @Result(property = "cerealsLegumesAgriculturalenterpriseSownarea", column = "o_cereals_legumes_agriculturalenterprise_sownarea"),
                @Result(property = "potatoAgriculturalenterpriseSownarea", column = "o_potato_agriculturalenterprise_sownarea"),
                @Result(property = "melonCulturesAgriculturalenterpriseSownarea", column = "o_melon_cultures_agriculturalenterprise_sownarea"),
                @Result(property = "forageCropsAgriculturalenterpriseSownarea", column = "o_forage_crops_agriculturalenterprise_sownarea"),
                @Result(property = "cultureOlivesAgriculturalenterpriseSownarea", column = "o_culture_olives_agriculturalenterprise_sownarea"),
                @Result(property = "vegetablesAgriculturalenterpriseSownarea", column = "o_vegetables_agriculturalenterprise_sownarea"),
                @Result(property = "cerealsLegumesHouseholdsSownarea", column = "o_cereals_legumes_households_sownarea"),
                @Result(property = "potatoHouseholdsSownarea", column = "o_potato_households_sownarea"),
                @Result(property = "melonCulturesHouseholdsSownarea", column = "o_melon_cultures_households_sownarea"),
                @Result(property = "forageCropsHouseholdsSownarea", column = "o_forage_crops_households_sownarea"),
                @Result(property = "cultureOlivesHouseholdsSownarea", column = "o_culture_olives_households_sownarea"),
                @Result(property = "vegetablesHouseholdsSownarea", column = "o_vegetables_households_sownarea"),
                @Result(property = "cerealsLegumesPeasantFarmCleanedarea", column = "o_cereals_legumes_peasant_farm_cleanedarea"),
                @Result(property = "potatoPeasantFarmCleanedarea", column = "o_potato_peasant_farm_cleanedarea"),
                @Result(property = "melonCulturesPeasantFarmCleanedarea", column = "o_melon_cultures_peasant_farm_cleanedarea"),
                @Result(property = "forageCropsPeasantFarmCleanedarea", column = "o_forage_crops_peasant_farm_cleanedarea"),
                @Result(property = "cultureOlivesPeasantFarmCleanedarea", column = "o_culture_olives_peasant_farm_cleanedarea"),
                @Result(property = "vegetablesPeasantFarmCleanedarea", column = "o_vegetables_peasant_farm_cleanedarea"),
                @Result(property = "cerealsLegumesAgriculturCleandarea", column = "o_cereals_legumes_agricultur_cleandarea"),
                @Result(property = "potatoAgriculturCleandarea", column = "o_potato_agricultur_cleandarea"),
                @Result(property = "melonCulturesAgriculturCleandarea", column = "o_melon_cultures_agricultur_cleandarea"),
                @Result(property = "forageCropsAgriculturCleandarea", column = "o_forage_crops_agricultur_cleandarea"),
                @Result(property = "cultureOlivesAgriculturCleandarea", column = "o_culture_olives_agricultur_cleandarea"),
                @Result(property = "vegetablesAgriculturCleandarea", column = "o_vegetables_agricultur_cleandarea"),
                @Result(property = "cerealsLegumesHouseholdsCleandarea", column = "o_cereals_legumes_households_cleandarea"),
                @Result(property = "potatoHouseholdsCleandarea", column = "o_potato_households_cleandarea"),
                @Result(property = "melonCulturesHouseholdsCleandarea", column = "o_melon_cultures_households_cleandarea"),
                @Result(property = "forageCropsHouseholdsCleandarea", column = "o_forage_crops_households_cleandarea"),
                @Result(property = "cultureOlivesHouseholdsCleandarea", column = "o_culture_olives_households_cleandarea"),
                @Result(property = "vegetablesHouseholdsCleandarea", column = "o_vegetables_households_cleandarea"),
                @Result(property = "cerealsLegumesPeasantFarmHarvest", column = "o_cereals_legumes_peasant_farm_harvest"),
                @Result(property = "potatoPeasantFarmHarvest", column = "o_potato_peasant_farm_harvest"),
                @Result(property = "melonCulturesPeasantFarmHarvest", column = "o_melon_cultures_peasant_farm_harvest"),
                @Result(property = "forageCropsPeasantFarmHarvest", column = "o_forage_crops_peasant_farm_harvest"),
                @Result(property = "cultureOlivesPeasantFarmHarvest", column = "o_culture_olives_peasant_farm_harvest"),
                @Result(property = "vegetablesPeasantFarmHarvest", column = "o_vegetables_peasant_farm_harvest"),
                @Result(property = "cerealsLegumesAgriculturHarvest", column = "o_cereals_legumes_agricultur_harvest"),
                @Result(property = "potatoAgriculturHarvest", column = "o_potato_agricultur_harvest"),
                @Result(property = "melonCulturesAgriculturHarvest", column = "o_melon_cultures_agricultur_harvest"),
                @Result(property = "forageCropsAgriculturHarvest", column = "o_forage_crops_agricultur_harvest"),
                @Result(property = "cultureOlivesAgriculturHarvest", column = "o_culture_olives_agricultur_harvest"),
                @Result(property = "vegetablesAgriculturHarvest", column = "o_vegetables_agricultur_harvest"),
                @Result(property = "cerealsLegumesHouseholdsHarvest", column = "o_cereals_legumes_households_harvest"),
                @Result(property = "potatoHouseholdsHarvest", column = "o_potato_households_harvest"),
                @Result(property = "melonCulturesHouseholdsHarvest", column = "o_melon_cultures_households_harvest"),
                @Result(property = "forageCropsHouseholdsHarvest", column = "o_forage_crops_households_harvest"),
                @Result(property = "cultureOlivesHouseholdsHarvest", column = "o_culture_olives_households_harvest"),
                @Result(property = "vegetablesHouseholdsHarvest", column = "o_vegetables_households_harvest"),
                @Result(property = "cerealsLegumesSquareStructure", column = "o_cereals_legumes_square_structure"),
                @Result(property = "potatoSquareStructure", column = "o_potato_square_structure"),
                @Result(property = "melonCulturesSquareStructure", column = "o_melon_cultures_square_structure"),
                @Result(property = "forageCropsSquareStructure", column = "o_forage_crops_square_structure"),
                @Result(property = "cultureOlivesSquareStructure", column = "o_culture_olives_square_structure"),
                @Result(property = "vegetablesSquareStructure", column = "o_vegetables_square_structure")
        })
    List<MenuRasMonitoring> getMenuRasMonitoringByYear(String year);

    @Select("SELECT * FROM ush_stat_rastenievodstvo where o_region=#{region}")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "latitude", column = "o_latitude"),
            @Result(property = "longitude", column = "o_longitude"),
            @Result(property = "totalAcreage", column = "o_total_acreage"),
            @Result(property = "volumeProduction", column = "o_volume_production"),
            @Result(property = "ifoVolumeProduction", column = "o_ifo_volume_production"),
            @Result(property = "grossHarvestAll", column = "o_gross_harvest_all"),
            @Result(property = "watermelonGross", column = "o_watermelon_gross"),
            @Result(property = "potatoGross", column = "o_potato_gross"),
            @Result(property = "pepperGross", column = "o_pepper_gross"),
            @Result(property = "eggplantGross", column = "o_eggplant_gross"),
            @Result(property = "onionGross", column = "o_onion_gross"),
            @Result(property = "tomatoesGross", column = "o_tomatoes_gross"),
            @Result(property = "carrotGross", column = "o_carrot_gross"),
            @Result(property = "melonsGross", column = "o_melons_gross"),
            @Result(property = "beetGross", column = "o_beet_gross"),
            @Result(property = "cabbageGross", column = "o_cabbage_gross"),
            @Result(property = "cucumbersGross", column = "o_cucumbers_gross"),
            @Result(property = "sownAreaAll", column = "o_sown_area_all"),
            @Result(property = "watermelonSownArea", column = "o_watermelon_sown_area"),
            @Result(property = "potatoSownArea", column = "o_potato_sown_area"),
            @Result(property = "pepperSownArea", column = "o_pepper_sown_area"),
            @Result(property = "eggplantSownArea", column = "o_eggplant_sown_area"),
            @Result(property = "onionSownArea", column = "o_onion_sown_area"),
            @Result(property = "tomatoesSownArea", column = "o_tomatoes_sown_area"),
            @Result(property = "carrotSownArea", column = "o_carrot_sown_area"),
            @Result(property = "melonsSownArea", column = "o_melons_sown_area"),
            @Result(property = "beetSownArea", column = "o_beet_sown_area"),
            @Result(property = "cabbageSownArea", column = "o_cabbage_sown_area"),
            @Result(property = "cucumbersSownArea", column = "o_cucumbers_sown_area"),
            @Result(property = "yieldAll", column = "o_yield_all"),
            @Result(property = "watermelonYield", column = "o_watermelon_yield"),
            @Result(property = "potatoYield", column = "o_potato_yield"),
            @Result(property = "pepperYield", column = "o_pepper_yield"),
            @Result(property = "eggplantYield", column = "o_eggplant_yield"),
            @Result(property = "onionYield", column = "o_onion_yield"),
            @Result(property = "tomatoesYield", column = "o_tomatoes_yield"),
            @Result(property = "carrotYield", column = "o_carrot_yield"),
            @Result(property = "melonsYield", column = "o_melons_yield"),
            @Result(property = "beetYield", column = "o_beet_yield"),
            @Result(property = "cabbageYield", column = "o_cabbage_yield"),
            @Result(property = "cucumbersYield", column = "o_cucumbers_yield"),
            @Result(property = "cerealsLegumesPeasantFarmSownarea", column = "o_cereals_legumes_peasant_farm_sownarea"),
            @Result(property = "potatoPeasantFarmSownarea", column = "o_potato_peasant_farm_sownarea"),
            @Result(property = "melonCulturesPeasantFarmSownarea", column = "o_melon_cultures_peasant_farm_sownarea"),
            @Result(property = "forageCropsPeasantFarmSownarea", column = "o_forage_crops_peasant_farm_sownarea"),
            @Result(property = "cultureOlivesPeasantFarmSownarea", column = "o_culture_olives_peasant_farm_sownarea"),
            @Result(property = "vegetablesPeasantFarmSownarea", column = "o_vegetables_peasant_farm_sownarea"),
            @Result(property = "cerealsLegumesAgriculturalenterpriseSownarea", column = "o_cereals_legumes_agriculturalenterprise_sownarea"),
            @Result(property = "potatoAgriculturalenterpriseSownarea", column = "o_potato_agriculturalenterprise_sownarea"),
            @Result(property = "melonCulturesAgriculturalenterpriseSownarea", column = "o_melon_cultures_agriculturalenterprise_sownarea"),
            @Result(property = "forageCropsAgriculturalenterpriseSownarea", column = "o_forage_crops_agriculturalenterprise_sownarea"),
            @Result(property = "cultureOlivesAgriculturalenterpriseSownarea", column = "o_culture_olives_agriculturalenterprise_sownarea"),
            @Result(property = "vegetablesAgriculturalenterpriseSownarea", column = "o_vegetables_agriculturalenterprise_sownarea"),
            @Result(property = "cerealsLegumesHouseholdsSownarea", column = "o_cereals_legumes_households_sownarea"),
            @Result(property = "potatoHouseholdsSownarea", column = "o_potato_households_sownarea"),
            @Result(property = "melonCulturesHouseholdsSownarea", column = "o_melon_cultures_households_sownarea"),
            @Result(property = "forageCropsHouseholdsSownarea", column = "o_forage_crops_households_sownarea"),
            @Result(property = "cultureOlivesHouseholdsSownarea", column = "o_culture_olives_households_sownarea"),
            @Result(property = "vegetablesHouseholdsSownarea", column = "o_vegetables_households_sownarea"),
            @Result(property = "cerealsLegumesPeasantFarmCleanedarea", column = "o_cereals_legumes_peasant_farm_cleanedarea"),
            @Result(property = "potatoPeasantFarmCleanedarea", column = "o_potato_peasant_farm_cleanedarea"),
            @Result(property = "melonCulturesPeasantFarmCleanedarea", column = "o_melon_cultures_peasant_farm_cleanedarea"),
            @Result(property = "forageCropsPeasantFarmCleanedarea", column = "o_forage_crops_peasant_farm_cleanedarea"),
            @Result(property = "cultureOlivesPeasantFarmCleanedarea", column = "o_culture_olives_peasant_farm_cleanedarea"),
            @Result(property = "vegetablesPeasantFarmCleanedarea", column = "o_vegetables_peasant_farm_cleanedarea"),
            @Result(property = "cerealsLegumesAgriculturCleandarea", column = "o_cereals_legumes_agricultur_cleandarea"),
            @Result(property = "potatoAgriculturCleandarea", column = "o_potato_agricultur_cleandarea"),
            @Result(property = "melonCulturesAgriculturCleandarea", column = "o_melon_cultures_agricultur_cleandarea"),
            @Result(property = "forageCropsAgriculturCleandarea", column = "o_forage_crops_agricultur_cleandarea"),
            @Result(property = "cultureOlivesAgriculturCleandarea", column = "o_culture_olives_agricultur_cleandarea"),
            @Result(property = "vegetablesAgriculturCleandarea", column = "o_vegetables_agricultur_cleandarea"),
            @Result(property = "cerealsLegumesHouseholdsCleandarea", column = "o_cereals_legumes_households_cleandarea"),
            @Result(property = "potatoHouseholdsCleandarea", column = "o_potato_households_cleandarea"),
            @Result(property = "melonCulturesHouseholdsCleandarea", column = "o_melon_cultures_households_cleandarea"),
            @Result(property = "forageCropsHouseholdsCleandarea", column = "o_forage_crops_households_cleandarea"),
            @Result(property = "cultureOlivesHouseholdsCleandarea", column = "o_culture_olives_households_cleandarea"),
            @Result(property = "vegetablesHouseholdsCleandarea", column = "o_vegetables_households_cleandarea"),
            @Result(property = "cerealsLegumesPeasantFarmHarvest", column = "o_cereals_legumes_peasant_farm_harvest"),
            @Result(property = "potatoPeasantFarmHarvest", column = "o_potato_peasant_farm_harvest"),
            @Result(property = "melonCulturesPeasantFarmHarvest", column = "o_melon_cultures_peasant_farm_harvest"),
            @Result(property = "forageCropsPeasantFarmHarvest", column = "o_forage_crops_peasant_farm_harvest"),
            @Result(property = "cultureOlivesPeasantFarmHarvest", column = "o_culture_olives_peasant_farm_harvest"),
            @Result(property = "vegetablesPeasantFarmHarvest", column = "o_vegetables_peasant_farm_harvest"),
            @Result(property = "cerealsLegumesAgriculturHarvest", column = "o_cereals_legumes_agricultur_harvest"),
            @Result(property = "potatoAgriculturHarvest", column = "o_potato_agricultur_harvest"),
            @Result(property = "melonCulturesAgriculturHarvest", column = "o_melon_cultures_agricultur_harvest"),
            @Result(property = "forageCropsAgriculturHarvest", column = "o_forage_crops_agricultur_harvest"),
            @Result(property = "cultureOlivesAgriculturHarvest", column = "o_culture_olives_agricultur_harvest"),
            @Result(property = "vegetablesAgriculturHarvest", column = "o_vegetables_agricultur_harvest"),
            @Result(property = "cerealsLegumesHouseholdsHarvest", column = "o_cereals_legumes_households_harvest"),
            @Result(property = "potatoHouseholdsHarvest", column = "o_potato_households_harvest"),
            @Result(property = "melonCulturesHouseholdsHarvest", column = "o_melon_cultures_households_harvest"),
            @Result(property = "forageCropsHouseholdsHarvest", column = "o_forage_crops_households_harvest"),
            @Result(property = "cultureOlivesHouseholdsHarvest", column = "o_culture_olives_households_harvest"),
            @Result(property = "vegetablesHouseholdsHarvest", column = "o_vegetables_households_harvest"),
            @Result(property = "cerealsLegumesSquareStructure", column = "o_cereals_legumes_square_structure"),
            @Result(property = "potatoSquareStructure", column = "o_potato_square_structure"),
            @Result(property = "melonCulturesSquareStructure", column = "o_melon_cultures_square_structure"),
            @Result(property = "forageCropsSquareStructure", column = "o_forage_crops_square_structure"),
            @Result(property = "cultureOlivesSquareStructure", column = "o_culture_olives_square_structure"),
            @Result(property = "vegetablesSquareStructure", column = "o_vegetables_square_structure")
    })
    List<MenuRasMonitoring> getMenuRasMonitoringByRegion(String region);


//year

    @Select("SELECT DISTINCT o_year FROM ush_stat_rastenievodstvo ORDER BY o_year")
    public List<String> getMenuRasMonitoringYears();

    @Select("SELECT DISTINCT o_region FROM ush_stat_rastenievodstvo ORDER BY o_region")
    public List<String> getMenuRasMonitoringRegions();

//rybolovstvo

    @Select("SELECT * FROM ush_stat_ryb_i_aqva_2 where o_year=#{year}")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "month", column = "o_month"),
            @Result(property = "allocatedQuota", column = "o_allocated_quota"),
            @Result(property = "developmentQuota", column = "o_development_quota"),
            @Result(property = "riverKigach", column = "o_river_kigach"),
            @Result(property = "riverUral", column = "o_river_ural"),
            @Result(property = "caspianSea", column = "o_caspian_sea"),
            @Result(property = "total", column = "o_total"),
            @Result(property = "uralAtyrauHatchery", column = "o_ural_atyrau_hatchery"),
            @Result(property = "uralAtyrauUterine", column = "o_ural_atyrau_uterine"),
            @Result(property = "atyrauHatchery", column = "o_atyrau_hatchery"),
            @Result(property = "atyrauUterine", column = "o_atyrau_uterine"),
            @Result(property = "note", column = "o_note")
    })
    List<MenuRybAqva2Monitoring> getMenuRybAqva2MonitoringByYear(String year);

//year

    @Select("SELECT DISTINCT o_year FROM ush_stat_ryb_i_aqva_2 ORDER BY o_year")
    public List<String> getMenuRybAqva2MonitoringYears();


    @Select("SELECT * FROM ush_stat_ryb_i_aqva_table where view_fish=#{viewFish}")
    @Results({
            @Result(property = "viewFish", column = "view_fish"),
            @Result(property = "fishCatch", column = "fish_catch")
    })
    List<MenuRybAqvaTable> getMenuRybAqvaTableByView(String view);

    @Select("SELECT * FROM ush_stat_ryb_i_aqva_table")
    @Results({
            @Result(property = "viewFish", column = "view_fish"),
            @Result(property = "fishCatch", column = "fish_catch")
    })
    List<MenuRybAqvaTable> getMenu2RybAqvaTableByView();

    @Select("SELECT DISTINCT view_fish FROM ush_stat_ryb_i_aqva_table ORDER BY view_fish")
    public List<String> getMenuRybAqvaTableViews();

    @Select("SELECT * FROM ush_stat_ryb_i_aqva_2_table where o_year=#{year}")
    @Results({
            @Result(property = "country", column = "country"),
            @Result(property = "exportTonna", column = "export_tonna"),
            @Result(property = "year", column = "o_year")
    })
    List<MenuRybAqva2TableMonitoring> getMenuRybAqva2TableMonitoringByYear(String year);

    @Select("SELECT DISTINCT o_year FROM ush_stat_ryb_i_aqva_2_table ORDER BY o_year")
    public List<String> getMenuRybAqva2TableYears();

//veterinar

    @Select("SELECT * FROM ush_stat_veterinariya_table where o_year=#{year}")
    @Results({
            @Result(property = "vac", column = "o_vac"),
            @Result(property = "type", column = "o_type"),
            @Result(property = "year", column = "o_year"),
            @Result(property = "region", column = "o_region")
    })
    List<MenuVetTableMonitoring> getMenuVetTableMonitoringByYear(String year);

    @Select("SELECT * FROM ush_stat_veterinariya_table where o_region=#{region}")
    @Results({
            @Result(property = "vac", column = "o_vac"),
            @Result(property = "type", column = "o_type"),
            @Result(property = "year", column = "o_year"),
            @Result(property = "region", column = "o_region")
    })
    List<MenuVetTableMonitoring> getMenuVetTableMonitoringByRegion(String region);

    @Select("SELECT * FROM ush_stat_veterinariya_table where o_type=#{type}")
    @Results({
            @Result(property = "vac", column = "o_vac"),
            @Result(property = "type", column = "o_type"),
            @Result(property = "year", column = "o_year"),
            @Result(property = "region", column = "o_region")
    })
    List<MenuVetTableMonitoring> getMenuVetTableMonitoringByType(String type);

    @Select("SELECT DISTINCT o_year FROM ush_stat_veterinariya_table ORDER BY o_year")
    public List<String> getMenuVetTableMonitoringYears();

    @Select("SELECT DISTINCT o_type FROM ush_stat_veterinariya_table ORDER BY o_type")
    public List<String> getMenuVetTableMonitoringTypes();


// jiv

    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "numberLivestockPoultry", column = "o_number_livestock_poultry"),

            @Result(property = "numberCattleFarms", column = "o_number_cattle_farms"),
            @Result(property = "numberCowsFarms", column = "o_number_cows_farms"),
            @Result(property = "numberSheepgoatsFarms", column = "o_number_sheepgoats_farms"),
            @Result(property = "numberPigsFarms", column = "o_number_pigs_farms"),
            @Result(property = "numberHorsesFarms", column = "o_number_horses_farms"),
            @Result(property = "numberCamelsFarms", column = "o_number_camels_farms"),
            @Result(property = "numberBirdsFarms", column = "o_number_birds_farms"),
            @Result(property = "calvesYieldFarms", column = "o_calves_yield_farms"),
            @Result(property = "outputLambsFarms", column = "o_output_lambs_farms"),
            @Result(property = "outletKidsFarms", column = "o_outlet_kids_farms"),
            @Result(property = "outputFoalsFarms", column = "o_output_foals_farms"),
            @Result(property = "outputCamelscoltsFarms", column = "o_output_camelscolts_farms")
    })
    List<MenuJiv2Monitoring> getMenuJiv2MonitoringByYear();

    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "numberLivestockPoultry", column = "o_number_livestock_poultry"),

            @Result(property = "numberCattleFarms", column = "o_number_cattle_farms")
    })
    List<MenuJiv2kshMonitoring> getMenuJiv2kshMonitoringByYear();

    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "numberLivestockPoultry", column = "o_number_livestock_poultry"),

            @Result(property = "numberCowsFarms", column = "o_number_cows_farms")
    })
    List<MenuJiv2kshCowsMonitoring> getMenuJiv2kshCowsMonitoringByYear();

    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "numberSheepgoatsFarms", column = "o_number_sheepgoats_farms")
    })
    List<MenuJiv2kshSGMonitoring> getMenuJiv2kshSGMonitoringByYear();

    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "numberPigsFarms", column = "o_number_pigs_farms")
            })
    List<MenuJiv2kshPMonitoring> getMenuJiv2kshPMonitoringByYear();

    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "numberHorsesFarms", column = "o_number_horses_farms")
    })
    List<MenuJiv2kshHMonitoring> getMenuJiv2kshHMonitoringByYear();


    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "numberCamelsFarms", column = "o_number_camels_farms")
    })
    List<MenuJiv2kshCMonitoring> getMenuJiv2kshCMonitoringByYear();


    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "numberBirdsFarms", column = "o_number_birds_farms")
    })
    List<MenuJiv2kshBMonitoring> getMenuJiv2kshBMonitoringByYear();


    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "calvesYieldFarms", column = "o_calves_yield_farms")
    })
    List<MenuJiv2kshYMonitoring> getMenuJiv2kshYMonitoringByYear();


    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "outputLambsFarms", column = "o_output_lambs_farms")
    })
    List<MenuJiv2kshLMonitoring> getMenuJiv2kshLMonitoringByYear();


    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "outletKidsFarms", column = "o_outlet_kids_farms")
    })
    List<MenuJiv2kshKMonitoring> getMenuJiv2kshKMonitoringByYear();


    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "outputFoalsFarms", column = "o_output_foals_farms")
    })
    List<MenuJiv2kshFMonitoring> getMenuJiv2kshFMonitoringByYear();


    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "outputCamelscoltsFarms", column = "o_output_camelscolts_farms")
    })
    List<MenuJiv2kshCSMonitoring> getMenuJiv2kshCSMonitoringByYear();

    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "numberCattleAgricultural", column = "o_number_cattle_agricultural")
    })
    List<MenuJiv2sKRSMonitoring> getMenuJiv2sKRSMonitoringByYear();

    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "numberCowsAgricultural", column = "o_number_cows_agricultural")
    })
    List<MenuJiv2sCowsMonitoring> getMenuJiv2sCowsMonitoringByYear();

    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "numberSheepgoatsAgricultural", column = "o_number_sheepgoats_agricultural")
    })
    List<MenuJiv2sSMonitoring> getMenuJiv2sSMonitoringByYear();

    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "numberPigsAgricultural", column = "o_number_pigs_agricultural")
    })
    List<MenuJiv2sPMonitoring> getMenuJiv2sPMonitoringByYear();

    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "numberHorsesAgricultural", column = "o_number_horses_agricultural")
    })
    List<MenuJiv2sHMonitoring> getMenuJiv2sHMonitoringByYear();

    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "numberCamelsAgricultural", column = "o_number_camels_agricultural")
    })
    List<MenuJiv2sCMonitoring> getMenuJiv2sCMonitoringByYear();

    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "numberBirdsAgricultural", column = "o_number_birds_agricultural")
    })
    List<MenuJiv2sBMonitoring> getMenuJiv2sBMonitoringByYear();

    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "calvesYieldAgricultural", column = "o_calves_yield_agricultural")
    })
    List<MenuJiv2sYMonitoring> getMenuJiv2sYMonitoringByYear();

    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "outputLambsAgricultural", column = "o_output_lambs_agricultural")
    })
    List<MenuJiv2sLMonitoring> getMenuJiv2sLMonitoringByYear();

    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "outletKidsAgricultural", column = "o_outlet_kids_agricultural")
    })
    List<MenuJiv2sKMonitoring> getMenuJiv2sKMonitoringByYear();

    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "outputFoalsAgricultural", column = "o_output_foals_agricultural")
    })
    List<MenuJiv2sFMonitoring> getMenuJiv2sFMonitoringByYear();

    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "outputCamelscoltsAgricultural", column = "o_output_camelscolts_agricultural")
    })
    List<MenuJiv2sCSMonitoring> getMenuJiv2sCSMonitoringByYear();

    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "numberCattleHouseholds", column = "o_number_cattle_households")
    })
    List<MenuJiv2hnKRSMonitoring> getMenuJiv2hnKRSMonitoringByYear();

    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "numberCowsHouseholds", column = "o_number_cows_households")
    })
    List<MenuJiv2hnCowsMonitoring> getMenuJiv2hnCowsMonitoringByYear();

    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "numberSheepgoatsHouseholds", column = "o_number_sheepgoats_households")
    })
    List<MenuJiv2hnSMonitoring> getMenuJiv2hnSMonitoringByYear();

    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "numberPigsHouseholds", column = "o_number_pigs_households")
    })
    List<MenuJiv2hnPMonitoring> getMenuJiv2hnPMonitoringByYear();

    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "numberHorsesHouseholds", column = "o_number_horses_households")
    })
    List<MenuJiv2hnHMonitoring> getMenuJiv2hnHMonitoringByYear();

    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "numberCamelsHouseholds", column = "o_number_camels_households"),
    })
    List<MenuJiv2hnCAMonitoring> getMenuJiv2hnCAMonitoringByYear();

    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "numberBirdsHouseholds", column = "o_number_bird_householdss_b"),
    })
    List<MenuJiv2hnBMonitoring> getMenuJiv2hnBMonitoringByYear();

    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "calvesYieldHouseholds", column = "o_calves_yield_households"),
    })
    List<MenuJiv2hnYMonitoring> getMenuJiv2hnYMonitoringByYear();

    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "outputLambsHouseholds", column = "o_output_lambs_households"),
    })
    List<MenuJiv2hnLMonitoring> getMenuJiv2hnLMonitoringByYear();

    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "outletKidsHouseholds", column = "o_outlet_kids_households"),
    })
    List<MenuJiv2hnKMonitoring> getMenuJiv2hnKMonitoringByYear();

    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "outputFoalsHouseholds", column = "o_output_foals_households"),
    })
    List<MenuJiv2hnFMonitoring> getMenuJiv2hnFMonitoringByYear();

    @Select("SELECT * FROM ush_stat_jivotnovodstvo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "outputCamelscoltsHouseholds", column = "o_output_camelscolts_households")
    })
    List<MenuJiv2hnCSMonitoring> getMenuJiv2hnCSMonitoringByYear();

    @Select("SELECT * FROM sh_valovyi_vypusk")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "raion", column = "o_raion"),
            @Result(property = "name", column = "o_name"),
            @Result(property = "pokazatel", column = "o_pokazatel"),
            @Result(property = "index", column = "o_index")
    })
    List<ShValVypMonitoring> getShValVypMonitoring();

    @Select("SELECT * FROM sh_valovyi_sbor")
    @Results({
            @Result(property = "kultura", column = "o_kultura"),
            @Result(property = "year", column = "o_year"),
            @Result(property = "raion", column = "o_raion"),
            @Result(property = "kategory", column = "o_kategory"),
            @Result(property = "pokazatel", column = "o_pokazatel")
    })
    List<ShValSborMonitoring> getShValSborMonitoring();

    @Select("SELECT * FROM sh_vvp_po_kategoriyam")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "kategory", column = "o_kategory"),
            @Result(property = "pokazatel", column = "o_pokazatel"),
            @Result(property = "index", column = "o_index")

    })
    List<ShVvpMonitoring> getShVvpMonitoring();

    @Select("SELECT * FROM sh_jivotnovod")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "raion", column = "o_raion"),
            @Result(property = "name", column = "o_name"),
            @Result(property = "kategory", column = "o_kategory"),
            @Result(property = "pokazatel", column = "o_pokazatel")

    })
    List<ShJivotMonitoring> getShJivotMonitoring();

    @Select("SELECT * FROM sh_obyem_ryby")
    @Results({
            @Result(property = "obyem", column = "o_obyem"),
            @Result(property = "index", column = "o_index"),
            @Result(property = "year", column = "o_year")
    })
    List<ShORMonitoring> getShORMonitoring();

    @Select("SELECT * FROM sh_padej_skota")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "name", column = "o_name"),
            @Result(property = "pokazatel", column = "o_pokazatel")
    })
    List<ShPSMonitoring> getShPSMonitoring();

    @Select("SELECT * FROM sh_pogolovya")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "name", column = "o_name"),
            @Result(property = "pokazatel", column = "o_pokazatel")
    })
    List<ShPMonitoring> getShPMonitoring();

    @Select("SELECT * FROM sh_posevnaya_pl")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "raion", column = "o_raion"),
            @Result(property = "name", column = "o_name"),
            @Result(property = "kategory", column = "o_kategory"),
            @Result(property = "pokazatel", column = "o_pokazatel")
    })
    List<ShPPMonitoring> getShPPMonitoring();

    @Select("SELECT * FROM sh_posevnaya_po_produkt")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "name", column = "o_name"),
            @Result(property = "raion", column = "o_raion"),
            @Result(property = "produkt", column = "o_produkt"),
            @Result(property = "pokazatel", column = "o_pokazatel"),
            @Result(property = "shirota", column = "o_shirota"),
            @Result(property = "dolgota", column = "o_dolgota")
    })
    List<ShPpoPMonitoring> getShPpoPMonitoring();

    @Select("SELECT * FROM sh_priplod")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "name", column = "o_name"),
            @Result(property = "pokazatel", column = "o_pokazatel")
    })
    List<ShPriplodMonitoring> getShPriplodMonitoring();

    @Select("SELECT * FROM sh_proizv_prod")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "raion", column = "o_raion"),
            @Result(property = "name", column = "o_name"),
            @Result(property = "pokazatel", column = "o_pokazatel")
    })
    List<ShProizvProdMonitoring> getShProizvProdMonitoring();

    @Select("SELECT * FROM sh_raion_kat")
    @Results({
            @Result(property = "raion", column = "o_raion"),
            @Result(property = "kategory", column = "o_kategory")
    })
    List<ShRKMonitoring> getShRKMonitoring();

    @Select("SELECT * FROM sh_rasten")
    @Results({
            @Result(property = "name", column = "o_name"),
            @Result(property = "kultura", column = "o_kultura"),
            @Result(property = "year", column = "o_year"),
            @Result(property = "raion", column = "o_raion"),
            @Result(property = "kategory", column = "o_kategory"),
            @Result(property = "pokazatel", column = "o_pokazatel")

    })
    List<ShRastenMonitoring> getShRastenMonitoring();

    @Select("SELECT * FROM sh_ryba")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "name", column = "o_name"),
            @Result(property = "pokazatel", column = "o_pokazatel")

    })
    List<ShRybaMonitoring> getShRybaMonitoring();

    @Select("SELECT * FROM sh_rybolovstvo")
    @Results({
            @Result(property = "raion", column = "o_raion"),
            @Result(property = "ulov", column = "o_ulov"),
            @Result(property = "view", column = "o_view"),
            @Result(property = "year", column = "o_year")

    })
    List<ShRybolovMonitoring> getShRybolovMonitoring();

    @Select("SELECT * FROM sh_ubrannaya_pl")
    @Results({
            @Result(property = "kultura", column = "o_kultura"),
            @Result(property = "year", column = "o_year"),
            @Result(property = "raion", column = "o_raion"),
            @Result(property = "kategory", column = "o_kategory"),
            @Result(property = "pokazatel", column = "o_pokazatel"),
    })
    List<ShUPMonitoring> getShUPMonitoring();

    @Select("SELECT * FROM sh_urojainost")
    @Results({
            @Result(property = "kultura", column = "o_kultura"),
            @Result(property = "year", column = "o_year"),
            @Result(property = "raion", column = "o_raion"),
            @Result(property = "kategory", column = "o_kategory"),
            @Result(property = "pokazatel", column = "o_pokazatel"),
    })
    List<ShUrojMonitoring> getShUrojMonitoring();








}
