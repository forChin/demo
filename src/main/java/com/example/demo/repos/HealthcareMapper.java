package com.example.demo.repos;

import java.util.List;

import com.example.demo.domain.*;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

public interface HealthcareMapper {

	String selectDemographicsRows = "select o_area AS area, o_region AS region, o_year AS year, o_fertility AS fertility, "
			+ "o_overall_mortality_rate AS overallMortalityRate, o_natural_population_growth AS naturalPopulationGrowth, "
			+ "o_life_expectancy AS lifeExpectancy "
			+ "FROM zdrav_kpi";

	String selectMortalitiesRows = "select o_area AS area, o_region AS region, "
			+ "o_year AS year, o_maternal_mortality AS maternalMortality, "
			+ "o_infant_mortality AS infantMortality, o_mortality_from_tuberculosis AS tuberculosisMortality, "
			+ "o_death_from_cancer AS cancerMortality, o_mortality_from_injuries AS injuriesMortality,"
			+ "o_mortality_from_of_the AS circulatoryDiseaseMortality, "
			+ "o_mortality_heart_disease AS heartDiseaseMortality FROM zdrav_kpi";
	
	String selectMorbidityRows = "SELECT o_area AS area, o_region AS region, o_year AS year, o_total_morbidity_population AS total, "
			+ "o_lncidence_of_tuberculosis AS tuberculosis, "
			+ "o_incidence_of_cancer AS cancer, o_morbidity_mbd AS mbd, o_morbidity_mbdsu AS mbdSu, "
			+ "o_morbidity_from_injuries AS injuries, o_morbidity_blood_circulation AS bloodCirculation, "
			+ "o_morbidity_arterial_hypertension AS arterialhypertension, o_morbidity_coronary_heart_disease AS "
			+ "coronaryHeartDisease, o_morbidity_acute_myocardial_infarction AS acuteMyocardialInfarction, "
			+ "o_prevalence_of_hiv As hiv FROM zdrav_kpi";
	
	String selectOrganizationsRows = "SELECT o_area AS area, o_region AS region , o_year AS year, o_number_providing_inpatient_care AS inpatientCare, "
			+ "o_number_organizations_patient_polyclinic_help AS patientPolyclinicHelp, "
			+ "o_number_of_hospital_beds AS totalNumberOfHospitalBeds, "
			+ "o_number_dhb_mon_system AS hospitalOrganizationsWithBeds, o_number_oo_hbdh AS patientPolyclinicsWithBeds, "
			+ "o_provision_hospital_beds AS provisionHospitalBeds FROM zdrav_kpi";

	String selectPersonnelRows = "SELECT o_area AS area, o_region AS region, o_year AS year, "
			+ "o_number_doctors AS numberOfDoctors, o_number_nurses AS numberOfAverageMedicalStaff, "
			+ "o_staffing_medical_personnel AS StaffingOfDoctors, o_provision_medical_personnel AS provisionOfAverageMedicalStaff, "
			+ "o_provision_population_doctors AS provisionOfDoctors, o_number_medical_staff_positions AS numberOfMedicalStaffPositions, "
			+ "o_number_medical_positions_held AS numberOfHeldMedicalPositions FROM zdrav_kpi";
	
//	Methods for demography statistics
	
	@Select(selectDemographicsRows + " WHERE o_year = #{year} ORDER BY o_region")
	public List<Demography> getDemographicsByYear(String year);
	
	@Select(selectDemographicsRows + " WHERE o_region = #{region} ORDER BY o_year")
	public List<Demography> getDemographicsByRegion(String region);
	
//	Methods for mortality statistics
	
	@Select(selectMortalitiesRows + " WHERE o_year = #{year} ORDER BY o_region")
	public List<Mortality> getMortalitiesByYear(String year);

	@Select(selectMortalitiesRows + " WHERE o_region = #{region} ORDER BY o_year")
	public List<Mortality> getMortalitiesByRegion(String region);
	
//	Methods for morbidity statistics
	
	@Select(selectMorbidityRows + " WHERE o_year = #{year} ORDER BY o_region")
	public List<Morbidity> getMorbiditiesByYear(String year);
	
	@Select(selectMorbidityRows + " WHERE o_region = #{region} ORDER BY o_year")
	public List<Morbidity> getMorbiditiesByRegion(String region);
	
//	Methods for organization statistics
	
	@Select(selectOrganizationsRows + " WHERE o_year = #{year} ORDER BY o_region")
	public List<OrganizationsInfo> getOrganizationsInfoByYear(String year);
	
//	Methods for personnel statistics
	
	@Select(selectPersonnelRows + " WHERE o_region = #{region} ORDER BY o_year")
	public List<Personnel> getPersonnelByRegion(String region);
	
	@Select(selectPersonnelRows + " WHERE o_year = #{year} ORDER BY o_region")
	public List<Personnel> getPersonnelByYear(String year);


	@Select("SELECT * FROM zdrav_kpi where o_year=#{year} AND o_region=#{region}")
	@Results({
			@Result(property = "year", column = "o_year"),
			@Result(property = "area", column = "o_area"),
			@Result(property = "region", column = "o_region"),
			@Result(property = "fertility", column = "o_fertility"),
			@Result(property = "overallMortalityRate", column = "o_overall_mortality_rate"),
			@Result(property = "naturalPopulationGrowth", column = "o_natural_population_growth"),
			@Result(property = "lifeExpectancy", column = "o_life_expectancy"),
			@Result(property = "maternalMortality", column = "o_maternal_mortality"),
			@Result(property = "infantMortality", column = "o_infant_mortality"),
			@Result(property = "mortalityFromTuberculosis", column = "o_mortality_from_tuberculosis"),
			@Result(property = "mortalityFromOfThe", column = "o_mortality_from_of_the"),
			@Result(property = "deathFromCancer", column = "o_death_from_cancer"),
			@Result(property = "mortalityFromInjuries", column = "o_mortality_from_injuries"),
			@Result(property = "mortalityHeartDisease", column = "o_mortality_heart_disease"),
			@Result(property = "totalMorbidityPopulation", column = "o_total_morbidity_population"),
			@Result(property = "lncidenceOfTuberculosis", column = "o_lncidence_of_tuberculosis"),
			@Result(property = "incidenceOfCancer", column = "o_incidence_of_cancer"),
			@Result(property = "morbidityMbd", column = "o_morbidity_mbd"),
			@Result(property = "morbidityMbdsu", column = "o_morbidity_mbdsu"),
			@Result(property = "morbidityFromInjuries", column = "o_morbidity_from_injuries"),
			@Result(property = "morbidityBloodCirculation", column = "o_morbidity_blood_circulation"),
			@Result(property = "morbidityArterialHypertension", column = "o_morbidity_arterial_hypertension"),
			@Result(property = "morbidityCoronaryHeartDisease", column = "o_morbidity_coronary_heart_disease"),
			@Result(property = "morbidityAcuteMyocardialInfarction", column = "o_morbidity_acute_myocardial_infarction"),
			@Result(property = "prevalenceOfHiv", column = "o_prevalence_of_hiv"),
			@Result(property = "numberProvidingInpatientCare", column = "o_number_providing_inpatient_care"),
			@Result(property = "numberOfHospitalBeds", column = "o_number_of_hospital_beds"),
			@Result(property = "provisionHospitalBeds", column = "o_provision_hospital_beds"),
			@Result(property = "numberResidentsAho", column = "o_number_residents_aho"),
			@Result(property = "numberDhbMonSystem", column = "o_number_dhb_mon_system"),
			@Result(property = "totalNumberBdsTna", column = "o_total_number_bds_tna"),
			@Result(property = "numberOrganizationsPatientPolyclinicHelp", column = "o_number_organizations_patient_polyclinic_help"),
			@Result(property = "numberVisitDoctors", column = "o_number_visit_doctors"),
			@Result(property = "numberOoHbdh", column = "o_number_oo_hbdh"),
			@Result(property = "numberDoctors", column = "o_number_doctors"),
			@Result(property = "provisionPopulationDoctors", column = "o_provision_population_doctors"),
			@Result(property = "numberMedicalStaffPositions", column = "o_number_medical_staff_positions"),
			@Result(property = "numberMedicalPositionsHeld", column = "o_number_medical_positions_held"),
			@Result(property = "numberNurses", column = "o_number_nurses"),
			@Result(property = "provisionMedicalPersonnel", column = "o_provision_medical_personnel"),
			@Result(property = "staffingMedicalPersonnel", column = "o_staffing_medical_personnel")
	})
	List<ZkpiMonitoring> getZkpiMonitoringByYearAndRegion(String year, String region);

	@Select("SELECT * FROM zdrav_kpi")
	@Results({
			@Result(property = "year", column = "o_year"),
			@Result(property = "area", column = "o_area"),
			@Result(property = "region", column = "o_region"),
			@Result(property = "fertility", column = "o_fertility"),
			@Result(property = "overallMortalityRate", column = "o_overall_mortality_rate"),
			@Result(property = "naturalPopulationGrowth", column = "o_natural_population_growth"),
			@Result(property = "lifeExpectancy", column = "o_life_expectancy")
	})
	List<Demography> getDemographyByYearAndRegion();


	@Select("SELECT * FROM zdrav_kpi where o_year=#{year} AND o_region=#{region}")
	@Results({
			@Result(property = "year", column = "o_year"),
			@Result(property = "area", column = "o_area"),
			@Result(property = "region", column = "o_region"),
			@Result(property = "maternalMortality", column = "o_maternal_mortality"),
			@Result(property = "infantMortality", column = "o_infant_mortality"),
			@Result(property = "TuberculosisMortality", column = "o_mortality_from_tuberculosis"),
			@Result(property = "injuriesMortality", column = "o_mortality_from_injuries"),
			@Result(property = "heartDiseaseMortality", column = "o_mortality_heart_disease"),
			@Result(property = "circulatoryDiseaseMortality", column = "o_mortality_from_of_the"),
			@Result(property = "cancerMortality", column = "o_death_from_cancer")
	})
	List<Mortality> getMortalityByYearAndRegion(String year, String region);


	@Select("SELECT * FROM zdrav_kpi where o_year=#{year} AND o_region=#{region}")
	@Results({
			@Result(property = "year", column = "o_year"),
			@Result(property = "area", column = "o_area"),
			@Result(property = "region", column = "o_region"),
			@Result(property = "total", column = "o_total_morbidity_population"),
			@Result(property = "injuries", column = "o_morbidity_from_injuries"),
			@Result(property = "bloodCirculation", column = "o_morbidity_blood_circulation"),
			@Result(property = "arterialhypertension", column = "o_morbidity_arterial_hypertension"),
			@Result(property = "coronaryHeartDisease", column = "o_morbidity_coronary_heart_disease"),
			@Result(property = "acuteMyocardialInfarction", column = "o_morbidity_acute_myocardial_infarction"),
			@Result(property = "mbd", column = "o_morbidity_mbd"),
			@Result(property = "mbdSu", column = "o_morbidity_mbdsu"),
			@Result(property = "tuberculosis", column = "o_lncidence_of_tuberculosis"),
			@Result(property = "cancer", column = "o_incidence_of_cancer"),
			@Result(property = "hiv", column = "o_prevalence_of_hiv")
	})
	List<Morbidity> getMorbidityByYearAndRegion(String year, String region);


	@Select("SELECT * FROM zdrav_kpi where o_year=#{year} AND o_region=#{region}")
	@Results({
			@Result(property = "year", column = "o_year"),
			@Result(property = "area", column = "o_area"),
			@Result(property = "region", column = "o_region"),
			@Result(property = "inpatientCare", column = "o_number_providing_inpatient_care"),
			@Result(property = "patientPolyclinicHelp", column = "o_number_organizations_patient_polyclinic_help"),
			@Result(property = "totalNumberOfHospitalBeds", column = "o_number_of_hospital_beds"),
			@Result(property = "hospitalOrganizationsWithBeds", column = "o_number_dhb_mon_system"),
			@Result(property = "patientPolyclinicsWithBeds", column = "o_number_oo_hbdh"),
			@Result(property = "provisionHospitalBeds", column = "o_provision_hospital_beds")
	})
	List<OrganizationsInfo> getOrganizationsInfoByYearAndRegion(String year, String region);


	@Select("SELECT * FROM zdrav_kpi where o_year=#{year} AND o_region=#{region}")
	@Results({
			@Result(property = "year", column = "o_year"),
			@Result(property = "area", column = "o_area"),
			@Result(property = "region", column = "o_region"),
			@Result(property = "numberOfDoctors", column = "o_number_doctors"),
			@Result(property = "numberOfAverageMedicalStaff", column = "o_number_nurses"),
			@Result(property = "StaffingOfDoctors", column = "o_staffing_medical_personnel"),
			@Result(property = "provisionOfAverageMedicalStaff", column = "o_provision_medical_personnel"),
			@Result(property = "provisionOfDoctors", column = "o_provision_population_doctors"),
			@Result(property = "numberOfMedicalStaffPositions", column = "o_number_medical_staff_positions"),
			@Result(property = "numberOfHeldMedicalPositions", column = "o_number_medical_positions_held")
	})
	List<Personnel> getPersonnelByYearAndRegion(String year, String region);



}
