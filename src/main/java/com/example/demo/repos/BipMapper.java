package com.example.demo.repos;

import com.example.demo.domain.*;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface BipMapper {

//bip_obraz

    @Select("SELECT * FROM bip_obraz where o_region=#{region}")
    @Results({
            @Result(property = "allocatedAmountCurrentMoment", column = "o_allocated_amount_current_moment"),
            @Result(property = "capacity", column = "o_capacity"),
            @Result(property = "category", column = "o_category"),
            @Result(property = "industry", column = "o_industry"),
            @Result(property = "instruction", column = "o_instruction"),
            @Result(property = "imageProject", column = "o_image_project"),
            @Result(property = "nameProject", column = "o_name_project"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "ruralDistrict", column = "o_rural_district"),
            @Result(property = "ruralSettlement", column = "o_rural_settlement"),
            @Result(property = "statusProject", column = "o_status_project"),
            @Result(property = "totalProjectCost", column = "o_total_project_cost"),
            @Result(property = "typeObject", column = "o_type_object"),
            @Result(property = "unit", column = "o_unit")
    })
    public List<MenuBipObrazMonitoring> getMenuBipObrazMonitoringByRegion(String region);

//all region

    @Select("SELECT DISTINCT o_rural_district FROM bip_obraz ORDER BY o_rural_district")
    public List<String> getMenuBipObrazMonitoringRurals();

    @Select("SELECT DISTINCT o_region FROM bip_obraz ORDER BY o_region")
    public List<String> getMenuBipObrazMonitoringRegions();

    @Select("SELECT DISTINCT o_rural_settlement FROM bip_obraz ORDER BY o_rural_settlement")
    public List<String> getMenuBipObrazMonitoringRSs();

//r rd rs

    @Select("SELECT DISTINCT o_region AS region, o_rural_district AS ruralDistrict, o_rural_settlement AS ruralSettlement, " +
            "o_allocated_amount_current_moment AS allocatedAmountCurrentMoment, o_capacity AS capacity, o_category AS category, " +
            "o_industry AS industry, o_instruction AS instruction, o_image_project AS imageProject, o_name_project AS nameProject, " +
            "o_status_project AS statusProject, o_total_project_cost AS totalProjectCost, o_type_object AS typeObject, o_unit AS unit " +
            "FROM bip_obraz WHERE o_region=#{region} AND o_rural_district=#{ruralDistrict} AND o_rural_settlement=#{ruralSettlement}")
    public List<MenuBipObrazMonitoring> getMenuBipObrazMonitoringByRegion2(String region, String ruralDistrict, String ruralSettlement);

//bip_zdrav  all_region

    @Select("SELECT DISTINCT o_rural_district FROM bip_zdrav ORDER BY o_rural_district")
    public List<String> getMenuBipZdravMonitoringRurals();

    @Select("SELECT DISTINCT o_region FROM bip_zdrav ORDER BY o_region")
    public List<String> getMenuBipZdravMonitoringRegions();

    @Select("SELECT DISTINCT o_rural_settlement FROM bip_zdrav ORDER BY o_rural_settlement")
    public List<String> getMenuBipZdravMonitoringRSs();

//r rd rs

    @Select("SELECT DISTINCT o_region AS region, o_rural_district AS ruralDistrict, o_rural_settlement AS ruralSettlement, " +
            "o_allocated_amount_current_moment AS allocatedAmountCurrentMoment, o_capacity AS capacity, o_category AS category, " +
            "o_industry AS industry, o_instruction AS instruction, o_image_project AS imageProject, o_name_project AS nameProject, " +
            "o_status_project AS statusProject, o_total_project_cost AS totalProjectCost, o_unit AS unit " +
            "FROM bip_zdrav WHERE o_region=#{region} AND o_rural_district=#{ruralDistrict} AND o_rural_settlement=#{ruralSettlement}")
    public List<MenuBipZdravMonitoring> getMenuBipZdravMonitoringByRegion2(String region, String ruralDistrict, String ruralSettlement);

    @Select("SELECT * FROM bip_zdrav where o_region=#{region}")
    @Results({
            @Result(property = "allocatedAmountCurrentMoment", column = "o_allocated_amount_current_moment"),
            @Result(property = "capacity", column = "o_capacity"),
            @Result(property = "category", column = "o_category"),
            @Result(property = "industry", column = "o_industry"),
            @Result(property = "instruction", column = "o_instruction"),
            @Result(property = "imageProject", column = "o_image_project"),
            @Result(property = "nameProject", column = "o_name_project"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "ruralDistrict", column = "o_rural_district"),
            @Result(property = "ruralSettlement", column = "o_rural_settlement"),
            @Result(property = "statusProject", column = "o_status_project"),
            @Result(property = "totalProjectCost", column = "o_total_project_cost"),
            @Result(property = "unit", column = "o_unit")
    })
    public List<MenuBipZdravMonitoring> getMenuBipZdravMonitoringByRegion(String region);

//bip_dorogi  all_region

    @Select("SELECT DISTINCT o_region FROM bip_dorogi ORDER BY o_region")
    public List<String> getMenuBipDorogiMonitoringRegions();

    @Select("SELECT DISTINCT o_road_snp FROM bip_dorogi ORDER BY o_road_snp")
    public List<String> getMenuBipDorogiMonitoringRSnps();

    @Select("SELECT DISTINCT o_road_type FROM bip_dorogi ORDER BY o_road_type")
    public List<String> getMenuBipDorogiMonitoringRTs();

//r rd rs

    @Select("SELECT DISTINCT o_region AS region, o_road_snp AS roadSnp, o_road_type AS roadType, " +
            "o_allocated_amount_current_moment AS allocatedAmountCurrentMoment, " +
            "o_industry AS industry, o_instruction AS instruction, o_image_project AS imageProject, o_length AS length, o_name_project AS nameProject, " +
            "o_status_project AS statusProject, o_total_project_cost AS totalProjectCoast, o_unit AS unit " +
            "FROM bip_dorogi WHERE o_region=#{region} AND o_road_snp=#{roadSnp} AND o_road_type=#{roadType}")
    public List<MenuBipDorogiMonitoring> getMenuBipDorogiMonitoringByRegion2(String region, String roadSnp, String roadType);

    @Select("SELECT * FROM bip_dorogi where o_region=#{region}")
    @Results({
            @Result(property = "allocatedAmountCurrentMoment", column = "o_allocated_amount_current_moment"),
            @Result(property = "industry", column = "o_industry"),
            @Result(property = "instruction", column = "o_instruction"),
            @Result(property = "imageProject", column = "o_image_project"),
            @Result(property = "length", column = "o_length"),
            @Result(property = "nameProject", column = "o_name_project"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "roadSnp", column = "o_road_snp"),
            @Result(property = "roadType", column = "o_road_type"),
            @Result(property = "statusProject", column = "o_status_project"),
            @Result(property = "totalProjectCoast", column = "o_total_project_cost"),
            @Result(property = "unit", column = "o_unit")
    })
    public List<MenuBipDorogiMonitoring> getMenuBipDorogiMonitoringByRegion(String region);

//bip_dorogi  all_region

    @Select("SELECT DISTINCT o_region FROM bip_cultura ORDER BY o_region")
    public List<String> getMenuBipCulturaMonitoringRegions();

    @Select("SELECT DISTINCT o_rural_district FROM bip_cultura ORDER BY o_rural_district")
    public List<String> getMenuBipCulturaMonitoringRDistricts();

    @Select("SELECT DISTINCT o_rural_settlement FROM bip_cultura ORDER BY o_rural_settlement")
    public List<String> getMenuBipCulturaMonitoringRSettlements();

    @Select("SELECT * FROM bip_cultura where o_region=#{region}")
    @Results({
            @Result(property = "allocatedAmountCurrentMoment", column = "o_allocated_amount_current_moment"),
            @Result(property = "capacity", column = "o_capacity"),
            @Result(property = "category", column = "o_category"),
            @Result(property = "industry", column = "o_industry"),
            @Result(property = "instruction", column = "o_instruction"),
            @Result(property = "imageProject", column = "o_image_project"),
            @Result(property = "nameProject", column = "o_name_project"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "ruralDistrict", column = "o_rural_district"),
            @Result(property = "ruralSettlement", column = "o_rural_settlement"),
            @Result(property = "statusCulture", column = "o_status_culture"),
            @Result(property = "totalProjectCoast", column = "o_total_project_cost"),
            @Result(property = "unit", column = "o_unit")
    })
    List<MenuBipCulturaMonitoring> getMenuBipCulturaMonitoringByRegion(String region);

    @Select("SELECT DISTINCT o_region AS region, o_rural_district AS ruralDistrict, o_rural_settlement AS ruralSettlement, " +
            "o_allocated_amount_current_moment AS allocatedAmountCurrentMoment, o_capacity AS capacity, o_category AS category, " +
            "o_industry AS industry, o_instruction AS instruction, o_image_project AS imageProject, o_name_project AS nameProject, " +
            "o_status_culture AS statusCulture, o_total_project_cost AS totalProjectCoast, o_unit AS unit " +
            "FROM bip_cultura WHERE o_region=#{region} AND o_rural_district=#{ruralDistrict} AND o_rural_settlement=#{ruralSettlement}")
    public List<MenuBipCulturaMonitoring> getMenuBipCulturaMonitoringByRegion2(String region, String ruralDistrict, String ruralSettlement);

    @Select("SELECT * FROM new_catalog_zdrav")
    @Results({
            @Result(property = "ot", column = "o_ot"),
            @Result(property = "doo", column = "o_do"),
            @Result(property = "number", column = "o_number"),
            @Result(property = "object", column = "o_object")
    })
    public List<BCatalogZdravMonitoring> getBCatalogZdravMonitoring();

    @Select("SELECT * FROM new_category_weight")
    @Results({
            @Result(property = "type", column = "o_type"),
            @Result(property = "weight", column = "o_weight")
    })
    public List<BCategoryWeightMonitoring> getBCategoryWeightMonitoring();

    @Select("SELECT * FROM new_culture_points")
    @Results({
            @Result(property = "index", column = "o_index"),
            @Result(property = "maxPoint", column = "o_max_point")
    })
    public List<BCulturePointsMonitoring> getBCulturePointsMonitoring();

    @Select("SELECT * FROM new_dorogi_points")
    @Results({
            @Result(property = "index", column = "o_index"),
            @Result(property = "maxPoint", column = "o_max_point")
    })
    public List<BDorogiPointsMonitoring> getBDorogiPointsMonitoring();

    @Select("SELECT * FROM new_edu_points")
    @Results({
            @Result(property = "index", column = "o_index"),
            @Result(property = "maxPoint", column = "o_max_point")
    })
    public List<BEduPointsMonitoring> getBEduPointsMonitoring();

    @Select("SELECT * FROM new_inflation")
    @Results({
            @Result(property = "column", column = "o_column"),
            @Result(property = "o2011", column = "o_2011"),
            @Result(property = "o2012", column = "o_2012"),
            @Result(property = "o2013", column = "o_2013"),
            @Result(property = "o2014", column = "o_2014"),
            @Result(property = "o2015", column = "o_2015"),
            @Result(property = "o2016", column = "o_2016"),
            @Result(property = "o2017", column = "o_2017"),
            @Result(property = "o2018", column = "o_2018"),
            @Result(property = "o2019", column = "o_2019"),
            @Result(property = "o2020", column = "o_2020")
    })
    public List<BInflationMonitoring> getBInflationMonitoring();

    @Select("SELECT * FROM new_numbers")
    @Results({
            @Result(property = "area", column = "o_area"),
            @Result(property = "ruralDistrict", column = "o_rural_district"),
            @Result(property = "snp", column = "o_snp"),
            @Result(property = "totalPopulation", column = "o_total_population"),
            @Result(property = "totalMans", column = "o_total_mans"),
            @Result(property = "mans0w16", column = "o_mans_0_16"),
            @Result(property = "mans16w30", column = "o_mans_16_30"),
            @Result(property = "mans30w63", column = "o_mans_30_63"),
            @Result(property = "w63plus", column = "o_63_plus"),
            @Result(property = "totalWomans", column = "o_total_womans"),
            @Result(property = "womans0w16", column = "o_womans_0_16"),
            @Result(property = "womans16w30", column = "o_womans_16_30"),
            @Result(property = "womans30w57", column = "o_womans_30_57"),
            @Result(property = "womans57plus", column = "o_womans_57_plus"),
            @Result(property = "totalKids", column = "o_total_kids"),
            @Result(property = "kids0w6", column = "o_kids_0_6"),
            @Result(property = "kids6w11", column = "o_kids_6_11"),
            @Result(property = "kids11w16", column = "o_kids_11_16"),
            @Result(property = "kids16plus", column = "o_kids_16_plus"),
            @Result(property = "borns", column = "o_borns"),
            @Result(property = "deads", column = "o_deads"),
            @Result(property = "arrived", column = "o_arrived"),
            @Result(property = "retired", column = "o_retired")
    })
    public List<BNumbersMonitoring> getBNumbersMonitoring();

    @Select("SELECT * FROM new_spisok_ddo")
    @Results({
            @Result(property = "nameDdo", column = "o_name_ddo"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "raion", column = "o_raion"),
            @Result(property = "ruralDistrict", column = "o_rural_district"),
            @Result(property = "snp", column = "o_snp"),
            @Result(property = "adres", column = "o_adres"),
            @Result(property = "yearConstruction", column = "o_year_construction"),
            @Result(property = "accidentRate", column = "o_accident_rate")
    })
    public List<BSpisokDdoMonitoring> getBSpisokDdoMonitoring();

    @Select("SELECT * FROM new_spisok_dorogi")
    @Results({
            @Result(property = "roadType", column = "o_road_type"),
            @Result(property = "raion", column = "o_raion"),
            @Result(property = "roadName", column = "o_road_name"),
            @Result(property = "raiRoad", column = "o_rai_road"),
            @Result(property = "oblRoad", column = "o_obl_road"),
            @Result(property = "extent", column = "o_extent"),
            @Result(property = "extentHor", column = "o_extent_hor"),
            @Result(property = "extentUd", column = "o_extent_ud"),
            @Result(property = "extentNeud", column = "o_extent_neud"),
            @Result(property = "state", column = "o_state"),
            @Result(property = "stateHor", column = "o_state_hor"),
            @Result(property = "stateUd", column = "o_state_ud"),
            @Result(property = "stateNeud", column = "o_state_neud")
    })
    public List<BSpisokDorogiMonitoring> getBSpisokDorogiMonitoring();

    @Select("SELECT * FROM new_spisok_shkoly")
    @Results({
            @Result(property = "schoolName", column = "o_school_name"),
            @Result(property = "area", column = "o_area"),
            @Result(property = "raion", column = "o_raion"),
            @Result(property = "ruralDistruct", column = "o_rural_distruct"),
            @Result(property = "snp", column = "o_snp"),
            @Result(property = "adress", column = "o_adress"),
            @Result(property = "yearConstruction", column = "o_year_construction"),
            @Result(property = "yearCaprenovation", column = "o_year_caprenovation"),
            @Result(property = "accidentRate", column = "o_accident_rate")
    })
    public List<BSpisokShkolyMonitoring> getBSpisokShkolyMonitoring();

    @Select("SELECT * FROM new_spisok_zdrav")
    @Results({
            @Result(property = "raion", column = "o_raion"),
            @Result(property = "ruralDistruct", column = "o_rural_distruct"),
            @Result(property = "snp", column = "o_snp"),
            @Result(property = "remoteness", column = "o_remoteness"),
            @Result(property = "category", column = "o_category"),
            @Result(property = "yearConstruction", column = "o_year_construction"),
            @Result(property = "yearCapRenovation", column = "o_year_cap_renovation"),
            @Result(property = "type", column = "o_type"),
            @Result(property = "accidentRate", column = "o_accident_rate")
    })
    public List<BSpisokZdravMonitoring> getBSpisokZdravMonitoring();

    @Select("SELECT * FROM new_zdrav_points")
    @Results({
            @Result(property = "index", column = "o_index"),
            @Result(property = "maxPoint", column = "o_max_point")
    })
    public List<BZdravPointsMonitoring> getBZdravPointsMonitoring();

//vse

    //bip_obraz

    @Select("SELECT * FROM bip_obraz")
    @Results({
            @Result(property = "allocatedAmountCurrentMoment", column = "o_allocated_amount_current_moment"),
            @Result(property = "capacity", column = "o_capacity"),
            @Result(property = "category", column = "o_category"),
            @Result(property = "industry", column = "o_industry"),
            @Result(property = "instruction", column = "o_instruction"),
            @Result(property = "imageProject", column = "o_image_project"),
            @Result(property = "nameProject", column = "o_name_project"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "ruralDistrict", column = "o_rural_district"),
            @Result(property = "ruralSettlement", column = "o_rural_settlement"),
            @Result(property = "statusProject", column = "o_status_project"),
            @Result(property = "totalProjectCost", column = "o_total_project_cost"),
            @Result(property = "typeObject", column = "o_type_object"),
            @Result(property = "unit", column = "o_unit")
    })
    public List<MenuBipObrazMonitoring> getMenuBipObrazMonitoring();

    @Select("SELECT * FROM bip_zdrav")
    @Results({
            @Result(property = "allocatedAmountCurrentMoment", column = "o_allocated_amount_current_moment"),
            @Result(property = "capacity", column = "o_capacity"),
            @Result(property = "category", column = "o_category"),
            @Result(property = "industry", column = "o_industry"),
            @Result(property = "instruction", column = "o_instruction"),
            @Result(property = "imageProject", column = "o_image_project"),
            @Result(property = "nameProject", column = "o_name_project"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "ruralDistrict", column = "o_rural_district"),
            @Result(property = "ruralSettlement", column = "o_rural_settlement"),
            @Result(property = "statusProject", column = "o_status_project"),
            @Result(property = "totalProjectCost", column = "o_total_project_cost"),
            @Result(property = "unit", column = "o_unit")
    })
    public List<MenuBipZdravMonitoring> getMenuBipZdravMonitoring();

    @Select("SELECT * FROM bip_dorogi")
    @Results({
            @Result(property = "allocatedAmountCurrentMoment", column = "o_allocated_amount_current_moment"),
            @Result(property = "industry", column = "o_industry"),
            @Result(property = "instruction", column = "o_instruction"),
            @Result(property = "imageProject", column = "o_image_project"),
            @Result(property = "length", column = "o_length"),
            @Result(property = "nameProject", column = "o_name_project"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "roadSnp", column = "o_road_snp"),
            @Result(property = "roadType", column = "o_road_type"),
            @Result(property = "statusProject", column = "o_status_project"),
            @Result(property = "totalProjectCoast", column = "o_total_project_cost"),
            @Result(property = "unit", column = "o_unit")
    })
    public List<MenuBipDorogiMonitoring> getMenuBipDorogiMonitoring();

    @Select("SELECT * FROM bip_cultura")
    @Results({
            @Result(property = "allocatedAmountCurrentMoment", column = "o_allocated_amount_current_moment"),
            @Result(property = "capacity", column = "o_capacity"),
            @Result(property = "category", column = "o_category"),
            @Result(property = "industry", column = "o_industry"),
            @Result(property = "instruction", column = "o_instruction"),
            @Result(property = "imageProject", column = "o_image_project"),
            @Result(property = "nameProject", column = "o_name_project"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "ruralDistrict", column = "o_rural_district"),
            @Result(property = "ruralSettlement", column = "o_rural_settlement"),
            @Result(property = "statusCulture", column = "o_status_culture"),
            @Result(property = "totalProjectCoast", column = "o_total_project_cost"),
            @Result(property = "unit", column = "o_unit")
    })
    List<MenuBipCulturaMonitoring> getMenuBipCulturaMonitoring();



}
