package com.example.demo.repos;

import com.example.demo.domain.ZdravKCatalogMonitoring;
import com.example.demo.domain.ZdravKPIMonitoring;
import com.example.demo.domain.ZdravKPITableMonitoring;
import com.example.demo.domain.ZdravKSpisokMonitoring;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ZdravKPIMapper {

    @Select("SELECT * FROM zdrav_kpi_table where kind_of_sport=#{kindOfSport}")
    @Results({
            @Result(property = "kindOfSport", column = "kind_of_sport"),
            @Result(property = "amount", column = "amount"),
            @Result(property = "contingent", column = "contingent")
    })
    List<ZdravKPITableMonitoring> getZdravKPITableMonitoringByKind(String kindOfSport);


    @Select("SELECT * FROM new_zdravooh_catalog_zdrav where o_index=#{index}")
    @Results({
            @Result(property = "index", column = "o_index"),
            @Result(property = "share", column = "o_share"),
            @Result(property = "point", column = "o_point"),
            @Result(property = "source", column = "o_source"),
            @Result(property = "soursPoints", column = "o_soursPoints")
    })
    List<ZdravKCatalogMonitoring> getZdravKCatalogMonitoringByIndex(String index);

    @Select("SELECT * FROM new_zdravooh_catalog_zdrav")
    @Results({
            @Result(property = "index", column = "o_index"),
            @Result(property = "share", column = "o_share"),
            @Result(property = "point", column = "o_point"),
            @Result(property = "source", column = "o_source"),
            @Result(property = "soursPoints", column = "o_soursPoints")
    })
    List<ZdravKCatalogMonitoring> getZdrav2KCatalogMonitoringByIndex();

//year

    @Select("SELECT DISTINCT o_index FROM new_zdravooh_catalog_zdrav ORDER BY o_index")
    public List<String> getMenuKCatalogMonitoringIndex();

    @Select("SELECT * FROM new_zdravooh_spisok_zdrav where o_raion=#{raion}")
    @Results({
            @Result(property = "raion", column = "o_raion"),
            @Result(property = "ruralDistruct", column = "o_rural_distruct"),
            @Result(property = "snp", column = "o_snp"),
            @Result(property = "remoteness", column = "o_remoteness"),
            @Result(property = "category", column = "o_category"),
            @Result(property = "yearConstruction", column = "o_year_construction"),
            @Result(property = "yearCapRenovation", column = "o_year_cap_renovation"),
            @Result(property = "type", column = "o_type"),
            @Result(property = "accidentRate", column = "o_accident_rate")
    })
    List<ZdravKSpisokMonitoring> getZdravKSpisokMonitoringByRaion(String raion);

    @Select("SELECT * FROM new_zdravooh_spisok_zdrav")
    @Results({
            @Result(property = "raion", column = "o_raion"),
            @Result(property = "ruralDistruct", column = "o_rural_distruct"),
            @Result(property = "snp", column = "o_snp"),
            @Result(property = "remoteness", column = "o_remoteness"),
            @Result(property = "category", column = "o_category"),
            @Result(property = "yearConstruction", column = "o_year_construction"),
            @Result(property = "yearCapRenovation", column = "o_year_cap_renovation"),
            @Result(property = "type", column = "o_type"),
            @Result(property = "accidentRate", column = "o_accident_rate")
    })
    List<ZdravKSpisokMonitoring> getZdrav2KSpisokMonitoringByRaion();

    @Select("SELECT DISTINCT o_raion AS raion, o_rural_distruct AS ruralDistruct, o_snp AS snp, o_remoteness AS remoteness, " +
            "o_category AS category, o_year_construction AS yearConstruction, o_year_cap_renovation AS yearCapRenovation, " +
            "o_type AS type, o_accident_rate AS accidentRate " +
            "FROM new_zdravooh_spisok_zdrav WHERE o_raion=#{raion} AND o_rural_distruct=#{ruralDistruct} AND o_snp=#{snp} AND o_category=#{category}")
    List<ZdravKSpisokMonitoring> getZdrav3KSpisokMonitoringByRaion(String raion, String ruralDistruct, String snp, String category);

    @Select("SELECT DISTINCT o_raion FROM new_zdravooh_spisok_zdrav ORDER BY o_raion")
    public List<String> getMenuKSpisokMonitoringRaion();

    @Select("SELECT DISTINCT o_rural_distruct FROM new_zdravooh_spisok_zdrav ORDER BY o_rural_distruct")
    public List<String> getMenuKSpisokMonitoringRD();

    @Select("SELECT DISTINCT o_snp FROM new_zdravooh_spisok_zdrav ORDER BY o_snp")
    public List<String> getMenuKSpisokMonitoringSnp();

    @Select("SELECT * FROM zdrav_kpi where o_region=#{region}")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "fertility", column = "o_fertility"),
            @Result(property = "overallMortalityRate", column = "o_overall_mortality_rate"),
            @Result(property = "naturalPopulationGrowth", column = "o_natural_population_growth"),
            @Result(property = "lifeExpectancy", column = "o_life_expectancy"),
            @Result(property = "maternalMortality", column = "o_maternal_mortality"),
            @Result(property = "infantMortality", column = "o_infant_mortality"),
            @Result(property = "mortalityFromTuberculosis", column = "o_mortality_from_tuberculosis"),
            @Result(property = "mortalityFromOfThe", column = "o_mortality_from_of_the"),
            @Result(property = "deathFromCancer", column = "o_death_from_cancer"),
            @Result(property = "mortalityFromInjuries", column = "o_mortality_from_injuries"),
            @Result(property = "mortalityHeartDisease", column = "o_mortality_heart_disease"),
            @Result(property = "totalMorbidityPopulation", column = "o_total_morbidity_population"),
            @Result(property = "lncidenceOfTuberculosis", column = "o_lncidence_of_tuberculosis"),
            @Result(property = "incidenceOfCancer", column = "o_incidence_of_cancer"),
            @Result(property = "morbidityMbd", column = "o_morbidity_mbd"),
            @Result(property = "morbidityMbdsu", column = "o_morbidity_mbdsu"),
            @Result(property = "morbidityFromInjuries", column = "o_morbidity_from_injuries"),
            @Result(property = "morbidityBloodCirculation", column = "o_morbidity_blood_circulation"),
            @Result(property = "morbidityArterialHypertension", column = "o_morbidity_arterial_hypertension"),
            @Result(property = "morbidityCoronaryHeartDisease", column = "o_morbidity_coronary_heart_disease"),
            @Result(property = "morbidityAcuteMyocardialInfarction", column = "o_morbidity_acute_myocardial_infarction"),
            @Result(property = "prevalenceOfHiv", column = "o_prevalence_of_hiv"),
            @Result(property = "numberProvidingInpatientCare", column = "o_number_providing_inpatient_care"),
            @Result(property = "numberOfHospitalBeds", column = "o_number_of_hospital_beds"),
            @Result(property = "provisionHospitalBeds", column = "o_provision_hospital_beds"),
            @Result(property = "numberResidentsAho", column = "o_number_residents_aho"),
            @Result(property = "numberDhbMonSystem", column = "o_number_dhb_mon_system"),
            @Result(property = "totalNumberBdsTna", column = "o_total_number_bds_tna"),
            @Result(property = "numberOrganizationsPatientPolyclinicHelp", column = "o_number_organizations_patient_polyclinic_help"),
            @Result(property = "numberVisitDoctors", column = "o_number_visit_doctors"),
            @Result(property = "numberOoHbdh", column = "o_number_oo_hbdh"),
            @Result(property = "numberDoctors", column = "o_number_doctors"),
            @Result(property = "provisionPopulationDoctors", column = "o_provision_population_doctors"),
            @Result(property = "numberMedicalStaffPositions", column = "o_number_medical_staff_positions"),
            @Result(property = "numberMedicalPositionsHeld", column = "o_number_medical_positions_held"),
            @Result(property = "numberNurses", column = "o_number_nurses"),
            @Result(property = "provisionMedicalPersonnel", column = "o_provision_medical_personnel"),
            @Result(property = "staffingMedicalPersonnel", column = "o_staffing_medical_personnel")
    })
    List<ZdravKPIMonitoring> getZdravKPI2MonitoringByRegion(String region);

    @Select("SELECT DISTINCT o_region FROM zdrav_kpi ORDER BY o_region")
    public List<String> getMenuKPI2MonitoringRegions();

    @Select("SELECT DISTINCT o_year FROM zdrav_kpi ORDER BY o_year")
    public List<String> getMenuKPI2MonitoringYears();

    @Select("SELECT * FROM zdrav_kpi where o_year=#{year}")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "fertility", column = "o_fertility"),
            @Result(property = "overallMortalityRate", column = "o_overall_mortality_rate"),
            @Result(property = "naturalPopulationGrowth", column = "o_natural_population_growth"),
            @Result(property = "lifeExpectancy", column = "o_life_expectancy"),
            @Result(property = "maternalMortality", column = "o_maternal_mortality"),
            @Result(property = "infantMortality", column = "o_infant_mortality"),
            @Result(property = "mortalityFromTuberculosis", column = "o_mortality_from_tuberculosis"),
            @Result(property = "mortalityFromOfThe", column = "o_mortality_from_of_the"),
            @Result(property = "deathFromCancer", column = "o_death_from_cancer"),
            @Result(property = "mortalityFromInjuries", column = "o_mortality_from_injuries"),
            @Result(property = "mortalityHeartDisease", column = "o_mortality_heart_disease"),
            @Result(property = "totalMorbidityPopulation", column = "o_total_morbidity_population"),
            @Result(property = "lncidenceOfTuberculosis", column = "o_lncidence_of_tuberculosis"),
            @Result(property = "incidenceOfCancer", column = "o_incidence_of_cancer"),
            @Result(property = "morbidityMbd", column = "o_morbidity_mbd"),
            @Result(property = "morbidityMbdsu", column = "o_morbidity_mbdsu"),
            @Result(property = "morbidityFromInjuries", column = "o_morbidity_from_injuries"),
            @Result(property = "morbidityBloodCirculation", column = "o_morbidity_blood_circulation"),
            @Result(property = "morbidityArterialHypertension", column = "o_morbidity_arterial_hypertension"),
            @Result(property = "morbidityCoronaryHeartDisease", column = "o_morbidity_coronary_heart_disease"),
            @Result(property = "morbidityAcuteMyocardialInfarction", column = "o_morbidity_acute_myocardial_infarction"),
            @Result(property = "prevalenceOfHiv", column = "o_prevalence_of_hiv"),
            @Result(property = "numberProvidingInpatientCare", column = "o_number_providing_inpatient_care"),
            @Result(property = "numberOfHospitalBeds", column = "o_number_of_hospital_beds"),
            @Result(property = "provisionHospitalBeds", column = "o_provision_hospital_beds"),
            @Result(property = "numberResidentsAho", column = "o_number_residents_aho"),
            @Result(property = "numberDhbMonSystem", column = "o_number_dhb_mon_system"),
            @Result(property = "totalNumberBdsTna", column = "o_total_number_bds_tna"),
            @Result(property = "numberOrganizationsPatientPolyclinicHelp", column = "o_number_organizations_patient_polyclinic_help"),
            @Result(property = "numberVisitDoctors", column = "o_number_visit_doctors"),
            @Result(property = "numberOoHbdh", column = "o_number_oo_hbdh"),
            @Result(property = "numberDoctors", column = "o_number_doctors"),
            @Result(property = "provisionPopulationDoctors", column = "o_provision_population_doctors"),
            @Result(property = "numberMedicalStaffPositions", column = "o_number_medical_staff_positions"),
            @Result(property = "numberMedicalPositionsHeld", column = "o_number_medical_positions_held"),
            @Result(property = "numberNurses", column = "o_number_nurses"),
            @Result(property = "provisionMedicalPersonnel", column = "o_provision_medical_personnel"),
            @Result(property = "staffingMedicalPersonnel", column = "o_staffing_medical_personnel")
    })
    List<ZdravKPIMonitoring> getZdravKPI3MonitoringByYear(String year);

}
