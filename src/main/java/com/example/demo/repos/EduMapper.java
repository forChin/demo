package com.example.demo.repos;

import com.example.demo.domain.*;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface EduMapper {
    @Select("SELECT * FROM new_edu_catalog_college_points")
    @Results({
            @Result(property = "index", column = "o_index"),
            @Result(property = "shareInfluence", column = "o_share_influence"),
            @Result(property = "point", column = "o_point")
    })
    List<EduddoMonitoring> getEduddoMonitoring();

    @Select("SELECT * FROM new_edu_catalog_ddo_points")
    @Results({
            @Result(property = "index", column = "o_index"),
            @Result(property = "shareInfluence", column = "o_share_influence"),
            @Result(property = "point", column = "o_point")
    })
    List<EduCatalogDdoMonitoring> getEduCatalogDdoMonitoring();

    @Select("SELECT * FROM new_edu_catalog_school_points")
    @Results({
            @Result(property = "index", column = "o_index"),
            @Result(property = "shareInfluence", column = "o_share_influence"),
            @Result(property = "point", column = "o_point"),
            @Result(property = "column4", column = "column4")
    })
    List<EduCatalogSchoolMonitoring> getEduCatalogSchoolMonitoring();

    @Select("SELECT * FROM new_edu_college")
    @Results({
            @Result(property = "raion", column = "o_raion"),
            @Result(property = "year", column = "o_year"),
            @Result(property = "nameTipo", column = "o_name_tipo"),
            @Result(property = "countTeachers", column = "o_count_teachers"),
            @Result(property = "countAcceptedStudents", column = "o_count_accepted_students"),
            @Result(property = "countGraduates", column = "o_count_graduates"),
            @Result(property = "countReleasedStudents", column = "o_count_released_students"),
            @Result(property = "countDesignCapacity", column = "o_count_design_capacity"),
            @Result(property = "countCategoryC", column = "o_count_category_c"),
            @Result(property = "countCategoryA", column = "o_count_category_a"),
            @Result(property = "countCategoryB", column = "o_count_category_b"),
            @Result(property = "countMagistrate", column = "o_count_magistrate"),
            @Result(property = "countDoctorEngineering", column = "o_count_doctor_engineering"),
            @Result(property = "countCategoryD", column = "o_count_category_d"),
            @Result(property = "countEducationHigher", column = "o_count_education_higher"),
            @Result(property = "countEducationColleges", column = "o_count_education_colleges"),
            @Result(property = "countGeneralSecondaryEducation", column = "o_count_general_secondary_education")
    })
    List<EduCollegeMonitoring> getEduCollegeMonitoring();

    @Select("SELECT * FROM new_edu_ddo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "region", column = "o_region"),
            @Result(property = "ruralDistrict", column = "o_rural_district"),
            @Result(property = "snp", column = "o_snp"),
            @Result(property = "ddoName", column = "o_ddo_name"),
            @Result(property = "amountKids", column = "o_amount_kids"),
            @Result(property = "amountFrames", column = "o_amount_frames"),
            @Result(property = "amountPlace", column = "o_amount_place"),
            @Result(property = "numberA", column = "o_number_a"),
            @Result(property = "numberB", column = "o_number_b"),
            @Result(property = "numberC", column = "o_number_c"),
            @Result(property = "numberD", column = "o_number_d"),
            @Result(property = "numberE", column = "o_number_e"),
            @Result(property = "numberF", column = "o_number_f"),
            @Result(property = "numberG", column = "o_number_g"),
            @Result(property = "numberH", column = "o_number_h"),
            @Result(property = "numberI", column = "o_number_i"),
            @Result(property = "vuz", column = "o_vuz"),
            @Result(property = "ssuz", column = "o_ssuz"),
            @Result(property = "adres", column = "o_adres"),
            @Result(property = "yearContruction", column = "o_year_contruction"),
            @Result(property = "contractNumber", column = "o_contact_number"),
            @Result(property = "workTimeS", column = "o_work_time_s"),
            @Result(property = "workTimeDo", column = "o_work_time_do"),
            @Result(property = "surname", column = "o_surname"),
            @Result(property = "name", column = "o_name"),
            @Result(property = "middlName", column = "o_middl_name"),
            @Result(property = "typeDdo", column = "o_type_ddo"),
            @Result(property = "yearKapRenovation", column = "o_year_kap_renovation")
    })
    List<Edu2DdoMonitoring> getEdu2DdoMonitoring();

    @Select("SELECT * FROM new_edu_ddo_need")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "raion", column = "o_raion"),
            @Result(property = "ruralDitrict", column = "o_rural_ditrict"),
            @Result(property = "snp", column = "o_snp"),
            @Result(property = "ddoName", column = "o_ddo_name"),
            @Result(property = "candidate", column = "o_candidate"),
            @Result(property = "count", column = "o_count"),
            @Result(property = "languageInstruction", column = "o_language_instruction")
    })
    List<EduDdoNeedMonitoring> getEduDdoNeedMonitoring();

    @Select("SELECT * FROM new_edu_numbers")
    @Results({
            @Result(property = "raion", column = "o_raion"),
            @Result(property = "ruralDistruct", column = "o_rural_distruct"),
            @Result(property = "snp", column = "o_snp"),
            @Result(property = "totalPopulation", column = "o_total_population"),
            @Result(property = "totalMans", column = "o_total_mans"),
            @Result(property = "mans0w16", column = "o_mans_0_16"),
            @Result(property = "mans16w30", column = "o_mans_16_30"),
            @Result(property = "mans30w63", column = "o_mans_30_63"),
            @Result(property = "w63plus", column = "o_63_plus"),
            @Result(property = "totalWomans", column = "o_total_womans"),
            @Result(property = "womans0w16", column = "o_womans_0_16"),
            @Result(property = "womans16w30", column = "o_womans_16_30"),
            @Result(property = "womans30w57", column = "o_womans_30_57"),
            @Result(property = "womans57plus", column = "o_womans_57_plus"),
            @Result(property = "totalKids", column = "o_total_kids"),
            @Result(property = "kids0w6", column = "o_kids_0_6"),
            @Result(property = "kids6w11", column = "o_kids_6_11"),
            @Result(property = "kids11w16", column = "o_kids_11_16"),
            @Result(property = "kids16plus", column = "o_kids_16_plus"),
            @Result(property = "borns", column = "o_borns"),
            @Result(property = "deads", column = "o_deads"),
            @Result(property = "arrived", column = "o_arrived"),
            @Result(property = "retired", column = "o_retired")
    })
    List<EduNumbersMonitoring> getEduNumbersMonitoring();

    @Select("SELECT * FROM new_edu_queue_ddo")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "raion", column = "o_raion"),
            @Result(property = "queue", column = "o_queue")
    })
    List<EduQueueDdoMonitoring> getEduQueueDdoMonitoring();


    @Select("SELECT * FROM new_edu_school")
    @Results({
            @Result(property = "raion", column = "o_raion"),
            @Result(property = "ruralDistrict", column = "o_rural_district"),
            @Result(property = "snp", column = "o_snp"),
            @Result(property = "year", column = "o_year"),
            @Result(property = "nameSchool", column = "o_name_school"),
            @Result(property = "countPlace", column = "o_count_place"),
            @Result(property = "countStudents", column = "o_count_students"),
            @Result(property = "countGraduates", column = "o_count_graduates"),
            @Result(property = "countParticipantsUnt", column = "o_count_participants_unt"),
            @Result(property = "countTeachers", column = "o_count_teachers"),
            @Result(property = "categoryHigh", column = "o_category_high"),
            @Result(property = "category1", column = "o_category_1"),
            @Result(property = "category2", column = "o_category_2"),
            @Result(property = "categoryNot", column = "o_category_not"),
            @Result(property = "w025", column = "o_0_25"),
            @Result(property = "w2534", column = "o_25_34"),
            @Result(property = "w3544", column = "o_35_44"),
            @Result(property = "w4554", column = "o_45_54"),
            @Result(property = "w5564", column = "o_55_64"),
            @Result(property = "w64up", column = "o_64_up"),
            @Result(property = "countEducationHigher", column = "o_count_education_higher"),
            @Result(property = "countEducationColleges", column = "o_count_education_colleges"),
            @Result(property = "generalSecondaryEducation", column = "o_general_secondary_education"),
            @Result(property = "countGoldMedal", column = "o_count_gold_medal"),
            @Result(property = "countCertificateDistinction", column = "o_count_certificate_distinction"),
            @Result(property = "countUnsatisfactoryEvaluation", column = "o_count_unsatisfactory_evaluation"),
            @Result(property = "countTheLowestRating", column = "o_count_the_lowest_rating"),
            @Result(property = "schoolAddress", column = "o_school_address"),
            @Result(property = "typeInstitution", column = "o_type_institution"),
            @Result(property = "yearConstruction", column = "o_year_construction"),
            @Result(property = "yearKapRenovation", column = "o_year_kap_renovation"),
            @Result(property = "contactNumber", column = "o_contact_number"),
            @Result(property = "workingHours", column = "o_working_hours"),
            @Result(property = "workingHoursA", column = "o_working_hours_a"),
            @Result(property = "lastname", column = "o_lastname"),
            @Result(property = "firstname", column = "o_firstname"),
            @Result(property = "otchestvo", column = "o_otchestvo")

    })
    List<EduSchoolMonitoring> getEduSchoolMonitoring();

    @Select("SELECT * FROM new_edu_school_need")
    @Results({
            @Result(property = "year", column = "o_year"),
            @Result(property = "raion", column = "o_raion"),
            @Result(property = "ruralDitrict", column = "o_rural_ditrict"),
            @Result(property = "snp", column = "o_snp"),
            @Result(property = "schoolName", column = "o_school_name"),
            @Result(property = "need", column = "o_need"),
            @Result(property = "count", column = "o_count"),
            @Result(property = "languageInstruction", column = "o_language_instruction")
    })
    List<EduSchoolNeedMonitoring> getEduSchoolNeedMonitoring();


}
